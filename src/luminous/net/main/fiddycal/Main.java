package luminous.net.main.fiddycal;

import luminous.net.main.fiddycal.Commands.Staff.Broadcast;
import luminous.net.main.fiddycal.Commands.Staff.StaffChat;
import luminous.net.main.fiddycal.Commands.Staff.TeleportHere;
import luminous.net.main.fiddycal.Commands.Staff.Website;
import luminous.net.main.fiddycal.Managers.WorldManager;
import luminous.net.main.fiddycal.Managers.Connections.SQL;
import luminous.net.main.fiddycal.Managers.Connections.Servers;
import luminous.net.main.fiddycal.Managers.Convictions.Report;
import luminous.net.main.fiddycal.Managers.DisguiseManager.DisguiseManager;
import luminous.net.main.fiddycal.Managers.DisguiseManager.reflection.PlayerModifier;
import luminous.net.main.fiddycal.Managers.DisguiseManager.skins.SkinCreator;
import luminous.net.main.fiddycal.Managers.DisguiseManager.skins.USkinCreator;
import luminous.net.minecraftsg.net.Commands.*;
import luminous.net.minecraftsg.net.Gamemodes.BG.BGGame;
import luminous.net.minecraftsg.net.Gamemodes.BG.BGGameManager;
import luminous.net.minecraftsg.net.Gamemodes.BG.BGListeners;
import luminous.net.minecraftsg.net.Gamemodes.BG.BGStats;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGame;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGameManager;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGListeners;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGStats;
import luminous.net.minecraftsg.net.Gamemodes.a_GUI.ClickItems;
import luminous.net.minecraftsg.net.Gamemodes.a_GUI.GUI;
import luminous.net.minecraftsg.net.Gamemodes.SGMaker.SGMakerGame;
import luminous.net.minecraftsg.net.Gamemodes.SGMaker.SGMakerGameManager;
import luminous.net.minecraftsg.net.Managers.HidePlayers;
import luminous.net.minecraftsg.net.Managers.PointsManager;
import luminous.net.minecraftsg.net.Managers.SignsManager;
import luminous.net.minecraftsg.net.Managers.ZimeGames;
import luminous.net.minecraftsg.net.Staff.ModModeCore;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import luminous.net.main.fiddycal.Commands.BuildMode;
import luminous.net.main.fiddycal.Commands.Fix;
import luminous.net.main.fiddycal.Commands.Fly;
import luminous.net.main.fiddycal.Commands.Help;
import luminous.net.main.fiddycal.Commands.Hub;
import luminous.net.main.fiddycal.Commands.Ping;
import luminous.net.main.fiddycal.Commands.TeamSpeak;
import luminous.net.main.fiddycal.Commands.Teleport;
import luminous.net.main.fiddycal.Commands.Weather;
import luminous.net.main.fiddycal.Commands.Disguise.Disguise;
import luminous.net.main.fiddycal.Commands.Disguise.Undisguise;
import luminous.net.main.fiddycal.Events.Connect;
import luminous.net.main.fiddycal.Events.Disconnect;
import luminous.net.main.fiddycal.Events.HubEvents;
import luminous.net.main.fiddycal.Managers.ChatManager;
import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.main.fiddycal.Managers.RankManager;

import org.bukkit.scheduler.BukkitScheduler;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main extends JavaPlugin implements Listener {
	
	public static Main main;
	public static boolean changingWeather;
	public static boolean maintenance;
	
	private SGGameManager sgGameManager;
	private SGMakerGameManager sgMakerGameManager;
	private BGGameManager bgGameManager;
	private ZimeGames zimeGames;
	
	private ModModeCore mmc;
    private DisguiseManager disguiseManager;
    private PlayerModifier modifier;
    private USkinCreator reciever;

	public void onEnable() {

		// Load default config content
		getConfig().options().copyDefaults(getConfig().contains("Spawn"));
		saveConfig();

		getConfig().set("Unicode.arrow", "»");
		getConfig().set("Unicode.square", "█");
		saveConfig();
		
		// Create files if they are not found
		//Creates Core Files
		ConfigManager.createUUIDs();
		ConfigManager.createPlayers();
		ConfigManager.createReports();
		ConfigManager.createChatLogFolder();
		//Creates SG Files
		ConfigManager.createSGDataFolder();
		ConfigManager.createSGData();
		ConfigManager.createSGStats();
		//Creates BG Files
		ConfigManager.createBGDataFolder();
		ConfigManager.createBGData();
		ConfigManager.createBGStats();
		//Creates BG Kits File
		ConfigManager.createBGKits();
		// Loading all worlds
		if (getConfig().getStringList("Worlds") != null) {
			for (String world : getConfig().getStringList("Worlds")) {
				if (Bukkit.getWorld(world) == null) {
					Bukkit.getServer().createWorld(new WorldCreator(world));
				}
			}
		}
		
		PluginCommand plgCmd = this.getServer().getPluginCommand("disguise");
		if (plgCmd.getPlugin().getDescription().getName().equals("LibsDisguises"))
		    plgCmd.setExecutor(this);
		
		PluginCommand plgCmd1 = this.getServer().getPluginCommand("undisguise");
		if (plgCmd1.getPlugin().getDescription().getName().equals("LibsDisguises"))
		    plgCmd1.setExecutor(this);
		
		PluginCommand plgCmd2 = this.getServer().getPluginCommand("dis");
		if (plgCmd2.getPlugin().getDescription().getName().equals("LibsDisguises"))
		    plgCmd2.setExecutor(this);
		
		PluginCommand plgCmd3 = this.getServer().getPluginCommand("d");
		if (plgCmd3.getPlugin().getDescription().getName().equals("VoxelSniper"))
		    plgCmd3.setExecutor(this);
		
	    // Registered core commands
		getCommand("broadcast").setExecutor(new Broadcast());
		getCommand("build").setExecutor(new BuildMode());
		getCommand("ghostfix").setExecutor(new Fix());
		getCommand("fly").setExecutor(new Fly());
		getCommand("help").setExecutor(new Help());
		getCommand("hub").setExecutor(new Hub());
		getCommand("list").setExecutor(new List());
		getCommand("report").setExecutor(new Report());
		getCommand("teamspeak").setExecutor(new TeamSpeak());
		getCommand("website").setExecutor(new Website());
		getCommand("weather").setExecutor(new Weather());
		getCommand("teleport").setExecutor(new Teleport());
		getCommand("teleporthere").setExecutor(new TeleportHere());
		getCommand("spectate").setExecutor(new Spectate());
		getCommand("ping").setExecutor(new Ping());
		getCommand("disguise").setExecutor(new Disguise());
		getCommand("undisguise").setExecutor(new Undisguise());

		// Registered non-core (besides rank system) commands
		getCommand("silentjoin").setExecutor(new SilentJoin());
		getCommand("administration").setExecutor(new Admin());
		getCommand("statistics").setExecutor(new Stats());
		getCommand("sponsor").setExecutor(new Sponsor());
		getCommand("staff").setExecutor(new StaffChat());
		getCommand("message").setExecutor(new Message());
		getCommand("bounty").setExecutor(new Bounty());
		getCommand("reply").setExecutor(new Reply());
		getCommand("info").setExecutor(new Info());
		getCommand("vote").setExecutor(new Vote());

	    // Registered core events
		getServer().getPluginManager().registerEvents(this, this);
		getServer().getPluginManager().registerEvents(new Connect(), this);
		getServer().getPluginManager().registerEvents(new Disconnect(), this);
		getServer().getPluginManager().registerEvents(new Servers(), this);
		getServer().getPluginManager().registerEvents(new ChatManager(), this);
		getServer().getPluginManager().registerEvents(new HubEvents(), this);
		getServer().getPluginManager().registerEvents(new Weather(), this);
		getServer().getPluginManager().registerEvents(new SQL(), this);
		
		// Registered non-core events
		getServer().getPluginManager().registerEvents(new Sponsor(), this);
		getServer().getPluginManager().registerEvents(new PointsManager(), this);
		getServer().getPluginManager().registerEvents(new HidePlayers(), this);
		getServer().getPluginManager().registerEvents(new ClickItems(), this);
		getServer().getPluginManager().registerEvents(new SGListeners(), this);
		getServer().getPluginManager().registerEvents(new BGListeners(), this);
		getServer().getPluginManager().registerEvents(new SignsManager(), this);
		getServer().getPluginManager().registerEvents(new SGStats(), this);
		getServer().getPluginManager().registerEvents(new BGStats(), this);
		getServer().getPluginManager().registerEvents(new GUI(), this);

		// DO NOT ADD ANYTHING BELOW THIS. Exceptions due to runnable:
		main = this;
		zimeGames = new ZimeGames();
		sgGameManager = new SGGameManager();
		sgMakerGameManager = new SGMakerGameManager();
		bgGameManager = new BGGameManager();
	    mmc = new ModModeCore();
	    
	    modifier = new PlayerModifier();
		disguiseManager = new DisguiseManager();
		reciever = new USkinCreator();

		this.getZimeGames().newGame("Hub-1");

		//Create SG Games
		if (ConfigManager.sgDataCFG != null) {
			for (String id : ConfigManager.sgDataCFG.getConfigurationSection("Lobbies").getKeys(false)) {
				new SGGame(Integer.parseInt(id));
			}
		}
		
		if (ConfigManager.bgDataCFG != null) {
			if (ConfigManager.bgDataCFG.getConfigurationSection("Maps") != null) {
				new BGGame(1);
			}
		}

		BukkitScheduler scheduler = getServer().getScheduler();
		scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
			@Override
			public void run() {
				for (Player pl: main.getServer().getOnlinePlayers()){
					if (pl.getScoreboard().getTeam("time") != null){
						Date now = new Date();
						SimpleDateFormat theDate = new SimpleDateFormat("dd MMM yyyy");
						SimpleDateFormat theTime = new SimpleDateFormat("hh:mm a z");
						SimpleDateFormat theDateAndTime = new SimpleDateFormat("dd.MM HH:mm");

						pl.getScoreboard().getTeam("time").setPrefix(MessageManager.c("&e" + theTime.format(now)));
						pl.getScoreboard().getTeam("date").setPrefix(MessageManager.c("&e" + theDate.format(now)));
						pl.getScoreboard().getTeam("datetime").setPrefix(MessageManager.c("&7" + theDateAndTime.format(now) + "Z"));
					}
				}
			}
		}, 0L, 60L);
		
		changingWeather = false;
		for (World world : Bukkit.getWorlds()) {
    		world.setStorm(false);
    		world.setThundering(false);
			for (Entity e : world.getEntities()){
                if (e.getType() == EntityType.COW
                	|| e.getType() == EntityType.SLIME
                	|| e.getType() == EntityType.SKELETON
                	|| e.getType() == EntityType.ZOMBIE
                	|| e.getType() == EntityType.OCELOT
                	|| e.getType() == EntityType.CREEPER
                	|| e.getType() == EntityType.ENDERMAN
                	|| e.getType() == EntityType.SPIDER
                	|| e.getType() == EntityType.PIG_ZOMBIE
                	|| e.getType() == EntityType.CAVE_SPIDER
                	|| e.getType() == EntityType.SILVERFISH
                	|| e.getType() == EntityType.BLAZE
                	|| e.getType() == EntityType.BAT
                	|| e.getType() == EntityType.MAGMA_CUBE
                	|| e.getType() == EntityType.WITCH
                	|| e.getType() == EntityType.PIG
                	|| e.getType() == EntityType.SHEEP){
                    e.remove();
                }
            }
		}
		changingWeather = true;
		maintenance = false;
		
	}
	
	public void onDisable() {
		try {
			if (SQL.getSQLConnection() != null || !SQL.getSQLConnection().isClosed()) {
				SQL.getSQLConnection().close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		for (SGGame game : this.getSGGameManager().getGames()){
			WorldManager.deleteWorld("", "SGGame-" + game.getID());
		}
		for (SGMakerGame game : this.getSGMakerGameManager().getGames()){
			WorldManager.deleteWorld("", "SGMakerGame-" + game.getID());
		}
		for (BGGame game : this.getBGGameManager().getGames()){
			WorldManager.deleteWorld("", "BGGame-" + game.getID());
		}
		main = null;
		// DO NOT ADD ANYTHING BELOW THIS.
	}
	
	public PlayerModifier getPlayerModifier() {
		return modifier;
	}
	
	public SkinCreator getSkinCreator() {
		return reciever;
	}

	public SGGameManager getSGGameManager() {
		return sgGameManager;
	}
	
	public SGMakerGameManager getSGMakerGameManager() {
		return sgMakerGameManager;
	}

	public BGGameManager getBGGameManager(){
		return bgGameManager;
	}
	
	public DisguiseManager getDisguiseManager() {
        return disguiseManager;
    }

	public ModModeCore getModMode(){
		return mmc;
	}

	public ZimeGames getZimeGames(){
		return zimeGames;
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onCommand(PlayerCommandPreprocessEvent event) {
		
		// Minecraft Commands.
	    boolean plugins = event.getMessage().toLowerCase().startsWith("/plugins");
	    boolean pl = event.getMessage().equalsIgnoreCase("/pl");
	    boolean pl2 = event.getMessage().toLowerCase().startsWith("/pl");
	    boolean gc = event.getMessage().equalsIgnoreCase("/gc");
	    boolean icanhasbukkit = event.getMessage().toLowerCase().startsWith("/icanhasbukkit");
	    boolean unknown = event.getMessage().toLowerCase().startsWith("/?");
	    boolean version = event.getMessage().toLowerCase().startsWith("/version");
	    boolean ver = event.getMessage().toLowerCase().startsWith("/ver");
	    boolean bukkitplugin = event.getMessage().toLowerCase().startsWith("/bukkit:plugins");
	    boolean bukkitpl = event.getMessage().toLowerCase().startsWith("/bukkit:pl");
	    boolean bukkitunknown = event.getMessage().toLowerCase().startsWith("/bukkit:?");
	    boolean about = event.getMessage().toLowerCase().startsWith("/about");
	    boolean a = event.getMessage().toLowerCase().equalsIgnoreCase("/a");
	    boolean bukkitabout = event.getMessage().toLowerCase().startsWith("/bukkit:about");
	    boolean bukkita = event.getMessage().toLowerCase().startsWith("/bukkit:a");
	    boolean bukkitversion = event.getMessage().toLowerCase().startsWith("/bukkit:version");
	    boolean bukkitver = event.getMessage().toLowerCase().startsWith("/bukkit:ver");
	    boolean bukkithelp = event.getMessage().toLowerCase().startsWith("/bukkit:help");
	    boolean whisper = event.getMessage().toLowerCase().startsWith("/whisper");
	    boolean gamemode = event.getMessage().toLowerCase().startsWith("/gamemode");
	    boolean restart = event.getMessage().toLowerCase().startsWith("/restart");
	    boolean me = event.getMessage().toLowerCase().startsWith("/me");
	    boolean server = event.getMessage().toLowerCase().startsWith("/server");
	    boolean gamerule = event.getMessage().toLowerCase().startsWith("/gamerule");
	    
	    // Plugin Commands.
	    boolean mute = event.getMessage().toLowerCase().startsWith("/mute");
	    boolean ban = event.getMessage().toLowerCase().startsWith("/ban");
	    boolean pardonb = event.getMessage().toLowerCase().startsWith("/pardonb");
	    boolean pardonm = event.getMessage().toLowerCase().startsWith("/pardonm");
	    boolean br = event.getMessage().toLowerCase().startsWith("/br");
	    boolean up = event.getMessage().toLowerCase().startsWith("/up");
	    boolean thru = event.getMessage().toLowerCase().startsWith("/thru");
	    boolean slash = event.getMessage().toLowerCase().startsWith("//");
	    boolean sr = event.getMessage().toLowerCase().startsWith("/sr");
	    boolean skin = event.getMessage().toLowerCase().startsWith("/skin");
	    boolean debug = event.getMessage().toLowerCase().startsWith("/rainbow");
	    
	    // Allowed Commands.
	    boolean weather = event.getMessage().toLowerCase().startsWith("/weather");
	    boolean tban = event.getMessage().toLowerCase().startsWith("/tban");
	    boolean giverank = event.getMessage().toLowerCase().startsWith("/giverank");
	    boolean join = event.getMessage().toLowerCase().startsWith("/join");
	    boolean hub = event.getMessage().toLowerCase().startsWith("/hub");
	    boolean hi = event.getMessage().toLowerCase().startsWith("/hi");
	    boolean hackeridentification = event.getMessage().toLowerCase().startsWith("/hackeridentification");
	    boolean ws = event.getMessage().toLowerCase().startsWith("/ws");
	    boolean website = event.getMessage().toLowerCase().startsWith("/website");
	    boolean ts = event.getMessage().toLowerCase().startsWith("/ts");
	    boolean teamspeak = event.getMessage().toLowerCase().startsWith("/teamspeak");
	    boolean fly = event.getMessage().toLowerCase().startsWith("/fly");
	    boolean gotocommand = event.getMessage().toLowerCase().startsWith("/goto");
	    boolean tp = event.getMessage().toLowerCase().startsWith("/tp");
	    boolean teleport = event.getMessage().toLowerCase().startsWith("/teleport");
	    boolean staff = event.getMessage().toLowerCase().startsWith("/staff");
	    boolean staffchat = event.getMessage().toLowerCase().startsWith("/staffchat");
	    boolean inv = event.getMessage().toLowerCase().startsWith("/inv");
	    boolean inventory = event.getMessage().toLowerCase().startsWith("/inventory");
	    boolean build = event.getMessage().toLowerCase().startsWith("/build");
	    boolean broadcast = event.getMessage().toLowerCase().startsWith("/broadcast");
	    boolean bc = event.getMessage().toLowerCase().startsWith("/bc");
	    boolean help = event.getMessage().toLowerCase().startsWith("/help");
	    boolean report = event.getMessage().toLowerCase().startsWith("/report");
	    boolean disguise = event.getMessage().toLowerCase().equals("/disguise");
	    boolean dis = event.getMessage().toLowerCase().startsWith("/dis");
	    boolean undisguise = event.getMessage().toLowerCase().equals("/undisguise");
	    boolean ud = event.getMessage().toLowerCase().equals("/ud");
	    boolean list = event.getMessage().toLowerCase().startsWith("/list");
	    boolean fix = event.getMessage().toLowerCase().startsWith("/fix");
	    boolean gf = event.getMessage().toLowerCase().startsWith("/gf");
	    boolean ghostfix = event.getMessage().toLowerCase().startsWith("/ghostfix");
	    boolean deghost = event.getMessage().toLowerCase().startsWith("/deghost");
	    boolean ghost = event.getMessage().toLowerCase().startsWith("/ghost");
	    boolean admin = event.getMessage().toLowerCase().startsWith("/admin");
	    boolean won = event.getMessage().toLowerCase().startsWith("/won");
	    boolean points = event.getMessage().toLowerCase().startsWith("/points");
	    boolean notify = event.getMessage().toLowerCase().startsWith("/notify");
	    boolean apply = event.getMessage().toLowerCase().startsWith("/apply");
	    boolean buildserver = event.getMessage().toLowerCase().startsWith("/buildserver");
	    boolean ipcheckk = event.getMessage().toLowerCase().startsWith("/ipcheck");
	    boolean alts = event.getMessage().toLowerCase().startsWith("/alts");
	    boolean ip = event.getMessage().toLowerCase().startsWith("/ip");
	    boolean lmcsg = event.getMessage().toLowerCase().startsWith("/lmcsg");
	    boolean kit = event.getMessage().toLowerCase().startsWith("/kit");
	    boolean screenshare = event.getMessage().toLowerCase().startsWith("/screenshare");
	    boolean ss = event.getMessage().toLowerCase().startsWith("/ss");
	    boolean sharescreen = event.getMessage().toLowerCase().startsWith("/sharescreen");
	    boolean helpop = event.getMessage().toLowerCase().startsWith("/helpop");
	    boolean announcements = event.getMessage().toLowerCase().startsWith("/announcements");
	    boolean ann = event.getMessage().toLowerCase().startsWith("/ann");
	    boolean announcement = event.getMessage().toLowerCase().startsWith("/announcement");
	    boolean annrepeat = event.getMessage().toLowerCase().startsWith("/annrepeat");
	    boolean dm = event.getMessage().toLowerCase().startsWith("/dm");
	    boolean referral = event.getMessage().toLowerCase().startsWith("/referral");
	    boolean sidebar = event.getMessage().toLowerCase().startsWith("/sidebar");
	    boolean sbar = event.getMessage().toLowerCase().startsWith("/sbar");
	    boolean sb = event.getMessage().toLowerCase().startsWith("/sb");
	    boolean stats = event.getMessage().toLowerCase().startsWith("/stats");
	    boolean message = event.getMessage().toLowerCase().startsWith("/message");
	    boolean msg = event.getMessage().toLowerCase().startsWith("/msg");
	    boolean tell = event.getMessage().toLowerCase().startsWith("/tell");
	    boolean top = event.getMessage().toLowerCase().startsWith("/top");
	    boolean dupeip = event.getMessage().toLowerCase().startsWith("/dupeip");
	    boolean tphere = event.getMessage().toLowerCase().startsWith("/tphere");
	    boolean kick = event.getMessage().toLowerCase().startsWith("/kick");
	    boolean speed = event.getMessage().toLowerCase().startsWith("/speed");
	    boolean spawn = event.getMessage().toLowerCase().startsWith("/spawn");
	    boolean bounty = event.getMessage().toLowerCase().startsWith("/bounty");
	    boolean ping = event.getMessage().toLowerCase().startsWith("/ping");
	    boolean ms = event.getMessage().toLowerCase().startsWith("/ms");
	    boolean i = event.getMessage().toLowerCase().startsWith("/i");
	    boolean r = event.getMessage().toLowerCase().startsWith("/r");
	    boolean l = event.getMessage().toLowerCase().startsWith("/l");
	    boolean s = event.getMessage().toLowerCase().startsWith("/s");
	    boolean d = event.getMessage().toLowerCase().startsWith("/d");
	    boolean v = event.getMessage().toLowerCase().startsWith("/v");
	    boolean o = event.getMessage().toLowerCase().startsWith("/o");
	    boolean a1 = event.getMessage().toLowerCase().startsWith("/atps");
	    boolean a2 = event.getMessage().toLowerCase().startsWith("/anotify");
	    boolean a3 = event.getMessage().toLowerCase().startsWith("/areload");

	    Player player = event.getPlayer();
	    
	    if (event.getMessage().startsWith("/")) {
	    	
	    	if ((debug)) {
	    		if (player.getName().equals("5Ocal")
	    			|| player.getName().equals("treble_maker07")) {
	    			player.sendMessage("player.getName(): " + player.getName());
	    			player.sendMessage("player.getDisplayName(): " + player.getDisplayName());
	    			player.sendMessage("ChatColor.stripColor(player.getName()): " + ChatColor.stripColor(player.getDisplayName()));
	    			
	    			if (event.getMessage().contains("/rainbow: ")) {
	    				String name = event.getMessage().replace("/rainbow: ", "");
	    				if (getServer().getPlayer(name) != null) {
	    					
	    					Player p = getServer().getPlayer(name);
	    					String devName = RankManager.getDeveloperName(p.getName());
	    				    p.setDisplayName(devName);
	    				    
	    				    ConfigManager.playersCFG.set(player.getUniqueId().toString() + ".Displayname", devName);
	    				    ConfigManager.savePlayers();
	    				    
	    				    player.sendMessage(MessageManager.c("&aYou set " + p.getName() + "'s display name to " + devName + "."));
	    				    
	    				} else player.sendMessage(MessageManager.c("&cThe player " + name + " is not online."));
	    			}
	    			
			    	event.setCancelled(true);
	    			return;
	    		}
	    		player.sendMessage(MessageManager.unknownCommand);
		    	event.setCancelled(true);
		    	return;
	    	}
	    	
	    	if ((a1) || (a2) || (a3) || (skin) || (sr)) {
				player.sendMessage(MessageManager.unknownCommand);
		    	event.setCancelled(true);
		    	return;
		    }
		    
		    if ((o) && player.getName().equals("5Ocal")) {
		    	
		    	/* UUID id = UUID.randomUUID();
		    	String fileName = id.toString() + "#" + player.getUniqueId().toString();
		    	
	            Recorder recorder = Overwatch.getInstance().getRecorder();
	            Key key = Overwatch.getInstance().getKeyGenerator().generate(7);
		    	CaptureRecord cr = new CaptureRecord(fileName);
	            cr.setRecorder(player.getName());
	            recorder.registerRecording(player, cr);
	            Bukkit.getPluginManager().callEvent((Event)new RecordStartEvent(cr, player, player));
	            
				player.sendMessage("You are now recording yourself....");
				new BukkitRunnable() {
		            @Override
		            public void run() {
		            	recorder.saveCap(cr, Overwatch.getInstance().getRecordPath() + "/" + fileName + "/");
		            	try {
		    	            File dest = new File("./plugins/Overwatch/caps/" + fileName);
		    	            File source = new File("./plugins/SGIN-Core/Overwatch/" + fileName);
		    	            FileUtils.copyDirectory(source, dest);
		    	        } catch (IOException e) {
		    	            e.printStackTrace();
		    	        }
		    			player.sendMessage("You stopped recording: " + key.getKey());
		            	if (recorder.getCap(fileName) != null) {
			            	recorder.playCap(cr, player, player, "The Suspect");
		            	} else {
		            		player.sendMessage("Cap doesn't exist... :(");
		            	}
		            }
		        }.runTaskLater(Main.main, 120L); */
		    	player.sendMessage(MessageManager.unknownCommand);
	  	    	event.setCancelled(true);
		    	return;
		    }
		    
		    if ((plugins) || (whisper) || (pl) || (bukkitunknown) || (unknown)
			  	    || (bukkitplugin) || (pl2) || (bukkitpl) || (version) || (ver) ||  (gc) 
			  	    || (icanhasbukkit) ||  (a) || (about) ||  (bukkitversion) || (me)
			  	    ||  (bukkitver) ||  (bukkitabout)  ||  (bukkita) ||  (bukkithelp)) {
		    		
		    		player.sendMessage(ChatColor.RED + "I'm sorry, but you do not have permission"
		    			+ " to perform this command. Please contact the server administrators"
			  	    	+ " if you believe that this is in error.");
			  	    	    		
			  	 	    event.setCancelled(true);
			  	 	    return;
			  	 	    	
			}
		    
		    if ((dupeip)) {
				if (player.isOp()) {
					return;
				} else {
					player.sendMessage(MessageManager.unknownCommand);
		    		event.setCancelled(true);
		    		return;
				}
		    }
		    
		    if (event.getMessage().startsWith("/litebans")) {
		    	player.sendMessage(MessageManager.unknownCommand);
	  	    	event.setCancelled(true);
		 	    return;
		    }
	    	
	    	if ((plugins) || (whisper) || (pl) || (pl2) || (bukkitunknown) || (unknown)
		  	    	  || (bukkitplugin) ||  (bukkitpl) || (version) || (ver) ||  (gc) 
		  	    	  || (icanhasbukkit) ||  (a) || (about) ||  (bukkitversion) || (me)
		  	    	  ||  (bukkitver)||  (bukkitabout)  ||  (bukkita) ||  (bukkithelp)) {
	    		
	    		player.sendMessage(ChatColor.RED + "I'm sorry, but you do not have permission"
	    			+ " to perform this command. Please contact the server administrators"
		  	    	+ " if you believe that this is in error.");
		  	    	    		
		  	 	    event.setCancelled(true);
		  	 	    return;
		  	 	    	
		  	}
	    	
	    	if ((tban)) {
		  	
	    		if (!player.isOp()) {
	    			player.sendMessage(MessageManager.unknownCommand);
		  	    	event.setCancelled(true);
	  	 	    	return;
		  	    }
	    	
		  	}
	    	
	    	if (!((tban) || (server) || (speed) || (thru) || (up) || (br) || (slash) || (restart) || (gamemode) 
	    			|| (tphere) || (giverank) || (join) || (hub) || (hi) || (hackeridentification) || (ws) 
		    	    || (website) || (ts) || (teamspeak) || (fly) || (gotocommand) || (tp) || (teleport) || (s) 
		    	    || (staff) || (staffchat) || (inv) || (inventory) || (build) || (broadcast) || (bc) || (help)
		    	    || (report) || (disguise) || (d) || (dis) || (undisguise) || (ud) || (list) 
		    	    || (l) || (fix) || (gf) || (ghostfix) || (deghost) || (ghost) || (admin) || (won) || (points) 
		    	    || (notify) || (ban) || (apply) || (buildserver) || (ipcheckk) || (alts) || (ip) || (lmcsg) 
		    	    || (kit) || (screenshare) || (ss) || (sharescreen) || (helpop) || (announcements) || (ann) 
		    	    || (announcement) || (kick) || (annrepeat) || (dm) || (referral) || (sidebar) || (sbar) || (sb)
		    	    || (stats) || (r) || (spawn) ||  (bounty) || (i) || (weather) || (message) || (gamerule) 
		    	    || (top) || (msg) || (ms) || (ping) || (tell) || (v) || (mute) || (pardonm) || (pardonb))) {
	    		
	    		player.sendMessage(MessageManager.unknownCommand);
		    	event.setCancelled(true);
		    	return;
		    	
		    }
	    	
		  	return;
		  	
		}
			
	    Date now = new Date();
		SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat time = new SimpleDateFormat("hh:mm:ss a z");
		
		File chatlogs = new File("plugins/SGIN-Core/Logs", 
				event.getPlayer().getName() + ".yml");
		
		YamlConfiguration playerChat = YamlConfiguration.loadConfiguration(chatlogs);
		if (chatlogs.exists()) {
			if (disguiseManager.isDisguised(player)) {
				playerChat.set("(" + date.format(now) + ") [" + time.format(now) + "] " 
				+ player.getName() + " (" + ChatColor.stripColor(player.getDisplayName()) + "): ", event.getMessage());
			} else {
				playerChat.set("(" + date.format(now) + ") [" + time.format(now) + "] " 
				+ player.getName() + ": ", event.getMessage());
			}
			try {
				playerChat.save(chatlogs);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
    
    }
	
	@EventHandler
	public void preLogin(AsyncPlayerPreLoginEvent event) {
		FileConfiguration config = ConfigManager.playersCFG;
		if (maintenance == false) { return; }
		String kickmsg = MessageManager.c("\n\n"
		+ "&4You have been temporarily denied access to &bMinecraftSG.net&8."
		+ "\n&cUnfortunately the server is in heavy development&8."
		+ "\n\n&7For more information check out our forums&8: &ehttp://minecraftsg.net/forums"
		+ "\n&3Thread&8: &8[&eSquashing bugs&8, &eCreating location nodes&8]");
		if (config.contains(event.getUniqueId().toString() + ".Group")) {
			if (config.getString(event.getUniqueId().toString() + ".Group").equalsIgnoreCase("Member")
				|| config.getString(event.getUniqueId().toString() + ".Group").equalsIgnoreCase("Champion")
				|| config.getString(event.getUniqueId().toString() + ".Group").equalsIgnoreCase("Legend")
				|| config.getString(event.getUniqueId().toString() + ".Group").equalsIgnoreCase("Guardian")
				|| config.getString(event.getUniqueId().toString() + ".Group").equalsIgnoreCase("Elite")
				|| config.getString(event.getUniqueId().toString() + ".Group").equalsIgnoreCase("Royal")
				|| config.getString(event.getUniqueId().toString() + ".Group").equalsIgnoreCase("MapMaker")
				|| config.getString(event.getUniqueId().toString() + ".Group").equalsIgnoreCase("VIP")) {
				event.setLoginResult(Result.KICK_OTHER);
				event.setKickMessage(kickmsg);
			}
		} else {
			event.setLoginResult(Result.KICK_OTHER);
			event.setKickMessage(kickmsg);
		}
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		if (maintenance == false) { return; }
		event.getPlayer().sendMessage(MessageManager.c(MessageManager.serverPrefix + " &4The server is currently"
		+ " in development, normal players no longer have access. Staff can join for Beta purposes only."));
	}
	
	@EventHandler
	public void blockphysics(BlockPhysicsEvent event) {
		if (event.getBlock().getType() == Material.GRAVEL) {
			event.setCancelled(true);
		}
	}

}
