package luminous.net.main.fiddycal.Events;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.WeatherType;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.block.BlockIgniteEvent.IgniteCause;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import luminous.net.main.fiddycal.Main;

public class HubEvents implements Listener {

	@EventHandler
	public void damage(EntityDamageEvent event) {
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
		    World hub = Bukkit.getWorld(Main.main.getConfig().getString("Spawn.world"));
		    if (player.getWorld().equals(hub)) {
			    event.setCancelled(true);
		    }
		}
	}


	@EventHandler
	public void blockBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
	    World hub = Bukkit.getWorld(Main.main.getConfig().getString("Spawn.world"));
	    if (player.getWorld().equals(hub)) {
		    event.setCancelled(true);
	    }
	}
	
	@EventHandler
	public void blockPlace(BlockPlaceEvent event) {
		Player player = event.getPlayer();
	    World hub = Bukkit.getWorld(Main.main.getConfig().getString("Spawn.world"));
	    if (player.getWorld().equals(hub)) {
		    event.setCancelled(true);
	    }
	}

	@EventHandler
	public void itemDrop(PlayerDropItemEvent event) {
		Player player = event.getPlayer();
	    World hub = Bukkit.getWorld(Main.main.getConfig().getString("Spawn.world"));
	    if (player.getWorld().equals(hub)) {
		    event.setCancelled(true);
	    }
	}
	
	@EventHandler
	public void itemPickup(PlayerPickupItemEvent event) {
		Player player = event.getPlayer();
	    World hub = Bukkit.getWorld(Main.main.getConfig().getString("Spawn.world"));
	    if (player.getWorld().equals(hub)) {
		    event.setCancelled(true);
	    }
	}
	
	@EventHandler
	public void food(FoodLevelChangeEvent event) {
		Player player = (Player) event.getEntity();
	    World hub = Bukkit.getWorld(Main.main.getConfig().getString("Spawn.world"));
	    if (player.getWorld().equals(hub)) {
		    event.setCancelled(true);
	    }
	}
	
	@EventHandler
	public void vineGrowth(BlockSpreadEvent event) {
		if (event.getSource().getType() == Material.VINE) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void fireSpread(BlockIgniteEvent event) {
	    if (event.getCause() == IgniteCause.SPREAD) {
	        event.setCancelled(true);
	    }
	}
	
	@EventHandler
	public void trampleCrops(PlayerInteractEvent event) {
		if(event.getAction() == Action.PHYSICAL && 
			event.getClickedBlock().getType() == Material.SOIL)
	        event.setCancelled(true);
    }

	@EventHandler
	public void playerMove(PlayerMoveEvent event){
		World hub = Bukkit.getWorld(Main.main.getConfig().getString("Spawn.world"));
		if (event.getPlayer().getWorld().equals(hub)){
			if (event.getPlayer().getLocation().getY() < 20){
				Connect.teleportToSpawn(event.getPlayer());
			}
		}
	}
	
	@EventHandler
	public void thunder(PlayerChangedWorldEvent event) {
		Player player = event.getPlayer();
		player.getWorld().setThunderDuration(0);
		player.getWorld().setThundering(false);
		player.setPlayerWeather(WeatherType.CLEAR);
	}
}
