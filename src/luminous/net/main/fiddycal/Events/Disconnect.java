package luminous.net.main.fiddycal.Events;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.MessageManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class Disconnect implements Listener {

	@EventHandler
	public void serverQuit(PlayerQuitEvent event) {
		event.setQuitMessage(null);
		if (Main.main.getSGGameManager().getGameByPlayer(event.getPlayer().getName()) != null){
			Main.main.getSGGameManager().getGameByPlayer(event.getPlayer().getName()).leaveGame(event.getPlayer());
		}
		if (Main.main.getBGGameManager().getGameByPlayer(event.getPlayer().getName()) != null){
			Main.main.getBGGameManager().getGameByPlayer(event.getPlayer().getName()).leaveGame(event.getPlayer());
		}
		for (Player pl :Main.main.getServer().getOnlinePlayers()){
			if (pl.getScoreboard().getTeam("hubplayers") != null){
				Integer newInt =  Main.main.getServer().getOnlinePlayers().size() - 1;
				pl.getScoreboard().getTeam("hubplayers").setSuffix(MessageManager.c(" &e") + newInt);
			}
		}
	}
	
}
