package luminous.net.main.fiddycal.Events;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.WeatherType;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Commands.Fly;
import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.main.fiddycal.Managers.RankManager;
import luminous.net.main.fiddycal.Managers.StaffUtil;
import luminous.net.minecraftsg.net.Sidebar.Hub;

public class Connect implements Listener {

	
	public static void sendToLobby(Player player) {
		player.getInventory().clear();
		player.getInventory().setHelmet(null);
		player.getInventory().setChestplate(null);
		player.getInventory().setLeggings(null);
		player.getInventory().setBoots(null);
		for (PotionEffect effect : player.getActivePotionEffects()) {
			player.removePotionEffect(effect.getType());
		}
		teleportToSpawn(player);
		player.setHealth(20.0);
		player.setFoodLevel(20);
		player.setExp(0);
		Fly.startFlight(player);
		Float f = (float) 0.3;
		player.setWalkSpeed(f);
		Hub.setHubScoreboard(player);
		player.setPlayerWeather(WeatherType.CLEAR);

		player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bYou have &6" + 0 + " &bMinecraftSG Hub Credits&8!"));
		player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bAccess the shop with the &6Gold Bar&8!"));
		player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &7Hub chat is currently in &8global &7mode."));
		
		// player.sendMessage(MessageManager.c("&a&lYou have upgrades waiting&8!"));
		// player.sendMessage(MessageManager.c("&aType /upgrades to view all unredeemed upgrades&8!"));
		
		if (ConfigManager.playersCFG.getString(player.getUniqueId().toString() + ".Hub.Player-Visibility") == null) {
			ConfigManager.playersCFG.set(player.getUniqueId().toString() + ".Hub.Player-Visibility", true);
			ConfigManager.savePlayers(); for (Player p : Main.main.getServer().getOnlinePlayers()) {
				if (p.getWorld() == Main.main.getServer().getWorld(Main.main.getConfig().getString("Spawn.world"))) {
					player.showPlayer(p);
				}
			}
		} else {
			if (ConfigManager.playersCFG.getBoolean(player.getUniqueId().toString() + ".Hub.Player-Visibility") == false) {
				for (Player p : Main.main.getServer().getOnlinePlayers()) {
					if (p.getWorld() == Main.main.getServer().getWorld(Main.main.getConfig().getString("Spawn.world"))) {
						player.hidePlayer(p);
					}
				}
				player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &aAll players are now hidden!"));
			} else {
				for (Player p : Main.main.getServer().getOnlinePlayers()) {
					if (p.getWorld() == Main.main.getServer().getWorld(Main.main.getConfig().getString("Spawn.world"))) {
						player.showPlayer(p);
					}
				}
			}
		}
		
		player.sendMessage(MessageManager.c(" "));
		player.sendMessage(MessageManager.c("&a&lThis is an old core rewritten (/i ingame) for the MCGamer reunion held on &6&l11/19/2017 AEDT (mm/dd/yyy) &a&lhosted by our fabulous &5treble_maker07&a&l."));
		player.sendMessage(MessageManager.c("&4&lEXPECT bugs. Unless urgent, further patches will not be made for this core."));
		
		if (ConfigManager.playersCFG.getString(player.getUniqueId().toString() + ".SGMaker.Lobbies-left") == null) {
        	if (StaffUtil.isPremium(player)) {
        		FileConfiguration config = ConfigManager.playersCFG; int lob = 0;
                if (config.getString(player.getUniqueId().toString() + ".Group").equals("champion")) {
            		lob = 5;
                }
                if (config.getString(player.getUniqueId().toString() + ".Group").equals("legend")) {
                	lob = 10;
                }
                if (config.getString(player.getUniqueId().toString() + ".Group").equals("guardian")) {
                	lob = 15;
                }
                if (config.getString(player.getUniqueId().toString() + ".Group").equals("elite")) {
                	lob = 25;
                }
                ConfigManager.playersCFG.set(player.getUniqueId().toString() + ".SGMaker.Lobbies-left", lob);
        		ConfigManager.savePlayers();
                if (config.getString(player.getUniqueId().toString() + ".Group").equals("royal")
        	        || StaffUtil.isStaff(player)) {
                	ConfigManager.playersCFG.set(player.getUniqueId().toString() + ".SGMaker.Lobbies-left", "*");
                	ConfigManager.savePlayers();
                }
        	} else {
        		ConfigManager.playersCFG.set(player.getUniqueId().toString() + ".SGMaker.Lobbies-left", 0);
            	ConfigManager.savePlayers();
        	}
        }
	}

	public static void teleportToSpawn(Player player){
		FileConfiguration config = Main.main.getConfig();
		String world = config.getString("Spawn.world");
		int x = Integer.parseInt(config.getString("Spawn.x"));
		int y = Integer.parseInt(config.getString("Spawn.y"));
		int z = Integer.parseInt(config.getString("Spawn.z"));
		Location spawn = new Location(Bukkit.getWorld(world), x + .5, y, z + .5, 
		Float.parseFloat("-90.0"), Float.parseFloat("0.0"));
		player.teleport(spawn);
	}
	
	public static void giveLobbyItems(Player player) {
		
		ItemStack navigation = new ItemStack(Material.COMPASS, 1);
		ItemMeta navigationMeta = navigation.getItemMeta();
		List<String> navigationLore = new ArrayList<String>();
		navigationMeta.setDisplayName(MessageManager.c("&b&lQuick Teleport &7- Right click to teleport!"));
		navigationMeta.setLore(navigationLore);
		navigation.setItemMeta(navigationMeta);

		ItemStack showhideplayers = new ItemStack(Material.WATCH, 1);
		ItemMeta showhideplayersMeta = navigation.getItemMeta();
		List<String> showhideplayersLore = new ArrayList<String>();
		showhideplayersMeta.setDisplayName(MessageManager.c("&d&lShow/Hide Players &7- Right click to show/hide players!"));
		showhideplayersMeta.setLore(showhideplayersLore);
		showhideplayers.setItemMeta(showhideplayersMeta);
		
		ItemStack shop = new ItemStack(Material.GOLD_INGOT, 1);
		ItemMeta shopMeta = navigation.getItemMeta();
		List<String> shopLore = new ArrayList<String>();
		shopMeta.setDisplayName(MessageManager.c("&e&lShop &7- Right click to open the ingame shop!"));
		shopMeta.setLore(shopLore);
		shop.setItemMeta(shopMeta);
		
		ItemStack lobbySelector = new ItemStack(Material.NETHER_STAR, 1);
		ItemMeta lobbySelectorMeta = navigation.getItemMeta();
		List<String> lobbySelectorLore = new ArrayList<String>();
		lobbySelectorMeta.setDisplayName(MessageManager.c("&a&lLobby Selector &7- Right click to switch lobbies!"));
		lobbySelectorMeta.setLore(lobbySelectorLore);
		lobbySelector.setItemMeta(lobbySelectorMeta);


		/*ItemStack modmode = new ItemStack(Material.BREWING_STAND_ITEM, 1);
		ItemMeta modmodeMeta = modmode.getItemMeta();
		List<String> modmodeLore = new ArrayList<String>();
		modmodeMeta.setDisplayName(MessageManager.c("&bModerator Mode"));
		modmodeMeta.setLore(modmodeLore);
		modmode.setItemMeta(modmodeMeta);*/


		player.getInventory().setItem(0, navigation);
		player.getInventory().setItem(1, showhideplayers);
		//player.getInventory().setItem(4, modmode);
		player.getInventory().setItem(7, shop);
		player.getInventory().setItem(8, lobbySelector);
	}
	
	@EventHandler
	public void serverJoin(PlayerJoinEvent event) {
		event.getPlayer().getPlayer().getInventory().clear();
		Player player = event.getPlayer();
		FileConfiguration config = ConfigManager.uuidsCFG;
		event.setJoinMessage(null);
		
		player.getWorld().setThunderDuration(0);
		player.getWorld().setThundering(false);
		
		if (!(config.contains(player.getName()) 
				&& config.getString(player.getName()).equals(player.getUniqueId().toString()))) {
			
			if (config.contains(player.getUniqueId().toString())) {
				for (String uuid : config.getKeys(false)) {
					if (config.getString(uuid).equals(player.getUniqueId().toString())) {
		    			config.set(uuid, null);
					}
	    		}
			}
			config.set(player.getName(), player.getUniqueId().toString());
			config.set(player.getName().toUpperCase(), player.getUniqueId().toString());
			config.set(player.getName().toLowerCase(), player.getUniqueId().toString());
			ConfigManager.saveUUIDs();
			
		}

		File chatlogs = new File("plugins/SGIN-Core/Logs", 
				event.getPlayer().getName() + ".yml");
		
		if (!chatlogs.exists()){
            try {
            	chatlogs.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
		
		RankManager.setDisplayName(player, true);
		sendToLobby(player); giveLobbyItems(player);
		ConfigManager.playersCFG.set(player.getUniqueId().toString() + ".Displayname", player.getDisplayName());
		ConfigManager.savePlayers();
		for (Player pl :Main.main.getServer().getOnlinePlayers()){
			if (pl.getScoreboard().getTeam("hubplayers") != null){
				pl.getScoreboard().getTeam("hubplayers").setSuffix(MessageManager.c(" &e" + Main.main.getServer().getOnlinePlayers().size()));
			}
		}

	}
	
	@EventHandler
	public void hungerLoss(FoodLevelChangeEvent event) {
		
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			
			if (player.getWorld().equals(Bukkit.getWorld("world"))) {
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void worldChange(PlayerChangedWorldEvent event) {
		Player player = event.getPlayer();
		Float f = (float) 0.2;
		player.setWalkSpeed(f);
	}
	
	@EventHandler
	public void onLogin(PlayerLoginEvent event) {
		if (event.getResult() == Result.KICK_FULL) {
			event.allow();
		}
	}

}