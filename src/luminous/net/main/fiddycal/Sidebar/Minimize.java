package luminous.net.main.fiddycal.Sidebar;

import java.util.ArrayList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import luminous.net.main.fiddycal.Managers.MessageManager;

public class Minimize implements CommandExecutor {

	private static ArrayList<String> minimized = new ArrayList<String>();
	
	public static void minimizeSidebar(Player player) {
		if (minimized.contains(player.getName())) {
			minimized.remove(player.getName());
		}
		minimized.add(player.getName());
	}
	
	public static void unMinimizeSidebar(Player player) {
		if (minimized.contains(player.getName())) {
			minimized.remove(player.getName());
		}
	}
	
	public static boolean isMinimized(Player player) {
		if (minimized.contains(player.getName())) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    	
    	if (!(sender instanceof Player)) {
			sender.sendMessage(MessageManager.c(MessageManager.prefix + " &cOnly players are able to do this command&8."));
			return true;
		}
		
		Player player = (Player) sender;

	    if (args.length == 0) {
	    	if (Minimize.isMinimized(player)) {
	    		Minimize.unMinimizeSidebar(player);
	    		player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " The sidebar has been maximized."));
	    	} else {
	    		Minimize.minimizeSidebar(player);
	    		player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " The sidebar has been minimized."));
	    	}
    	} else {
    		player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &cUsage: /minimize"));
    	}
		return false;
    }
	
}