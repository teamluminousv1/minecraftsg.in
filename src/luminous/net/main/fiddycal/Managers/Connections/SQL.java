package luminous.net.main.fiddycal.Managers.Connections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class SQL implements Listener {

	private static Connection connection;
	
	public static Connection getSQLConnection() {
		return connection;
	}
	
	public synchronized static void openConnection() {
		try {
			connection = DriverManager.getConnection(
			"jdbc:mysql://104.219.7.4:3306/mc1512", "mc1512", "2afdbfbdfd");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public synchronized static void closeConnection() {
		try {
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public synchronized static boolean playerDataContainsPlayer(Player player, String table) {
		try {
			PreparedStatement sql = connection.prepareStatement(
			"SELECT * FROM `" + table + "` WHERE player=?;");
			sql.setString(1, player.getName());
			ResultSet resultSet = sql.executeQuery();
			boolean containsPlayer = resultSet.next();
			sql.close();
			resultSet.close();
			return containsPlayer;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@EventHandler
	public void login(PlayerLoginEvent event) {
		openConnection();
		try {
			int previousLogins = 0;
			if (playerDataContainsPlayer(event.getPlayer(), "overall_leaderboards")) {
				PreparedStatement sql = connection.prepareStatement(
				"SELECT logins FROM 'overall_leaderboards' WHERE player=?;");
				sql.setString(1, event.getPlayer().getName());
				ResultSet result = sql.executeQuery();
				result.next();
				previousLogins = result.getInt("logins");
				PreparedStatement loginsUpdate = connection.prepareStatement(
				"UPDATE 'overall_leaderboards' SET logins=? WHERE player=?;");
				loginsUpdate.setInt(1, previousLogins + 1);
				loginsUpdate.setString(2, event.getPlayer().getName());
				loginsUpdate.executeUpdate();
				loginsUpdate.close();
				sql.close();
				result.close();
			} else {
				PreparedStatement newPlayer = connection.prepareStatement(
				"INSERT INTO 'overall_leaderboards' values(?,0,0,1);");
				newPlayer.setString(1, event.getPlayer().getName());
				newPlayer.execute();
				newPlayer.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection();
		}
	}
	
}
