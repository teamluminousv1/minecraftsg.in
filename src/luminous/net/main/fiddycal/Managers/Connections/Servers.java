package luminous.net.main.fiddycal.Managers.Connections;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class Servers implements Listener {

	private static ArrayList<String> connecting = new ArrayList<String>();
	
	public static void startConnection(Player player) {
		if (connecting.contains(player.getName())) {
			connecting.remove(player.getName());
		}
		connecting.add(player.getName());
	}
	
	public static void stopConnection(Player player) {
		if (connecting.contains(player.getName())) {
			connecting.remove(player.getName());
		}
	}
	
	public static boolean isConnecting(Player player) {
		if (connecting.contains(player.getName())) {
			return true;
		} else {
			return false;
		}
	}
	
	@EventHandler
	public void invOpen(InventoryOpenEvent event) {
		Player player = (Player) event.getPlayer();
		if (isConnecting(player)) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void commandProcess(AsyncPlayerChatEvent event) {
		Player player = (Player) event.getPlayer();
		if (isConnecting(player)) {
			if (event.getMessage().startsWith("/")) {
				event.setCancelled(true);
			}
		}
	}
	
}