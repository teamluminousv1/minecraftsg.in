package luminous.net.main.fiddycal.Managers;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.util.Vector;

import luminous.net.main.fiddycal.Main;

public class KnockBack implements Listener {
    
    
    public static void applyKnockBack(final Entity en, final Location loc, final int knock, final boolean sprint) {
        
    	final double velocity = 0.45;
    	final double height = 0.25;
    	
    	Bukkit.getScheduler().scheduleSyncDelayedTask(Main.main, new Runnable(){

            @Override
            public void run() {
                Vector localVector = en.getLocation().toVector().subtract(loc.toVector()).normalize();
                double d = velocity + (double)knock * 0.05;
                if (sprint) {
                    d *= 2.0;
                }
                en.setVelocity(localVector.multiply(d).setY(height));
            }
        });
    	
    }
    
    @EventHandler(priority=EventPriority.LOW)
    public void onDamage(EntityDamageByEntityEvent event) {
        
    	Player en;
        if (event.isCancelled()) {
            return;
        }
        
        if (event.getEntity() instanceof Player 
        		&& event.getDamager() instanceof Player 
        		&& (en = (Player)event.getEntity()).getNoDamageTicks() == 0) {
        	
            Player damager = (Player) event.getDamager();
            int a = damager.getItemInHand().getEnchantmentLevel(Enchantment.KNOCKBACK);
            applyKnockBack((Entity)en, damager.getLocation(), a, damager.isSprinting());
            
        }
        
    }
}