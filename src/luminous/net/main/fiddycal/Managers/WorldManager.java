package luminous.net.main.fiddycal.Managers;

import luminous.net.main.fiddycal.Main;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;

import java.io.File;
import java.io.IOException;

public class WorldManager {
    
    public static void copyWorld(String oldDirectory, String newDirectory, String name) {
        try {
        	File dest = new File("./" + newDirectory + name);
        	File source = new File("./" + oldDirectory + "/");
            FileUtils.copyDirectory(source, dest);
            Bukkit.getServer().createWorld(new WorldCreator(name));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void deleteWorld(String directory, String world){
        Main.main.getServer().unloadWorld(world, true);
        File dir = new File("./" + directory + world);
        try {
            FileUtils.deleteDirectory(dir);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
