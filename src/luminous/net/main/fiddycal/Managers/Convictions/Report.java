package luminous.net.main.fiddycal.Managers.Convictions;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGame;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGameState;
 
public class Report implements Listener, CommandExecutor {
	
	/* TODO 
	 * 
	 * Use over watch replay system:
	 * When an SG game starts, if a players report count is 5 or more;
	 * Usage: (ConfigManager.reportCFG.getInt("Report-Count." + PLAYER_UUID));
	 * Record the match until the player dies. When/If this happens, remove a report.
	 * If the player dies within 1 minute of the match, do not remove a report.
	 * So the player stays in over watch, as any less then 1 minute wouldn't
	 * be enough time for a sufficient verdict. - 5Ocal
	 * 
	 * A votes weight on whether a player is cheating or not is fully
	 * dependent on the players rank and over all play time.
	 * 
	 * */
	
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
	    
    	Player player = (Player) sender; 
	    FileConfiguration config = ConfigManager.playersCFG;
	    FileConfiguration reportConfig = ConfigManager.reportCFG;
	    
	    if (config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("moderator")
				|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("uhchost")
				|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("seniormoderator")
				|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("juniordeveloper")
				|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("developer")
				|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("admin")
				|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("leaddeveloper")
				|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("owner")) {
	    	
	    	if (args.length >= 1) {
	    		if (!(args[0].equalsIgnoreCase("list") || args[0].equalsIgnoreCase("clear"))) {
		    		if (Main.main.getServer().getPlayer(args[0]) == null) {
		    			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " " + MessageManager.playerNotFound));
		    			return true;
		    		}
		    		String reportMessage = "";
		        	for (String arg : args) {
		        		reportMessage = reportMessage + arg + " ";
		            }
		        	if (!ConfigManager.reportCFG.getConfigurationSection("Reports").getKeys(false).contains(player.getName())) {
		        		ConfigManager.reportCFG.set("Reports." + player.getName(), reportMessage);
		        		String uuid = ConfigManager.uuidsCFG.getString(args[0].toLowerCase());
		        		if (ConfigManager.reportCFG.getConfigurationSection("Report-Count").getKeys(false).contains(uuid)) {
		        			ConfigManager.reportCFG.set("Report-Count." + uuid,
		        		    ConfigManager.reportCFG.getInt("Report-Count." + uuid) + 1);
		        		} else {
		        			ConfigManager.reportCFG.set("Report-Count." + uuid, 1);
		        		}
						ConfigManager.saveReports();
						if (ConfigManager.reportCFG.getString("Report-Count" + uuid) != null) {
							if (ConfigManager.reportCFG.getInt("Report-Count" + uuid) > 4) {
								if (Main.main.getSGGameManager().getGameByPlayer(Main.main.getConfig().getString(uuid + ".Name")) != null) {
									SGGame game = Main.main.getSGGameManager().getGameByPlayer(Main.main.getConfig().getString(uuid + ".Name"));
									if (game.getGamestate() == SGGameState.INGAME
										|| game.getGamestate() == SGGameState.GRACEPERIOD
									    || game.getGamestate() == SGGameState.PREDEATHMATCH
									    || game.getGamestate() == SGGameState.PREGAME) {
										// TODO // Record Match (Return to top of page for more info on TODO)
									}
								}
							}
						}
						for (Player p : Bukkit.getOnlinePlayers()) {
							if (config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("moderator")
									|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("uhchost")
									|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("seniormoderator")
									|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("juniordeveloper")
									|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("developer")
									|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("admin")
									|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("leaddeveloper")
									|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("owner")) {
								p.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &4" + " &bjust submitted a report&8."));
							}
						}
					} else {
						player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cYou recently submitted a report&8:"));
						player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &2Conviction Status&8: &ePending"));
						player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &2Submitted Verdicts&8: &8[&6" + 0 + "&8]"));
						player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cPlease wait until this report judged by a moderator&8."));
					}
					player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThank you for your report&8, &bthe player has been sent to our conviction system&8."));
					return true;
		    	} else if (args[0].equalsIgnoreCase("list") || args[0].equalsIgnoreCase("clear")) {
		    		if (args.length == 1) {
		    			if (config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("moderator")
								|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("uhchost")
								|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("seniormoderator")
								|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("juniordeveloper")
								|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("developer")
								|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("admin")
								|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("leaddeveloper")
								|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("owner")) {
		    				if (ConfigManager.reportCFG.getConfigurationSection("Reports").getKeys(false).isEmpty()) {
		    	    			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThere are currently no reports in our DataBase&8."));
		    	    			return true;
		    	    		}
		    	    		
		    	    		player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bList of current reports&8:"));
		    	    		for (String plr : reportConfig.getConfigurationSection("Reports").getKeys(false)) {
		    	    			String[] spl = reportConfig.getString(plr).split(" ");
		    	    			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &r" + plr + "&8: &e" + reportConfig.getString(plr).replace(spl[0], MessageManager.c("&4" + spl[0] + " &8| &e"))));
		    	    		}
		    			} else player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " " + MessageManager.invalidPermission));
		    		} else if (args.length == 2) {
		    			if (config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("moderator")
								|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("uhchost")
								|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("seniormoderator")
								|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("juniordeveloper")
								|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("developer")
								|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("admin")
								|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("leaddeveloper")
								|| config.getString(player.getUniqueId().toString().toLowerCase() + ".Group").equals("owner")) {
		    				if (ConfigManager.reportCFG.getConfigurationSection("Reports").getKeys(false).contains(player.getName())) {
		    					ConfigManager.reportCFG.set("Reports." + args[1], null);
		    					ConfigManager.saveReports();
		    					player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &e" + args[1] + "&8'&bs report was cleared&8."));
		    					return true;
		    				} else player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cThat report could not be found in our DataBase&8."));
		    			} else player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " " + MessageManager.invalidPermission));
		    		} else {
		    			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cUsage: /report <player> [reason]"));
		    		}
		    		return true;
		    	} else {
		    		player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cUsage: /report <player> [reason]"));
		        	return true;
		    	}
	    	} else {
	    		player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cUsage: /report <player> [reason]"));
	        	return true;
	    	}
		        	
		} else {
		    player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " " + MessageManager.invalidPermission));
		    return true;
		}
	}
  
    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
	    Player player =(Player) event.getPlayer(); 
	    FileConfiguration config = ConfigManager.playersCFG;
	    FileConfiguration reportConfig = ConfigManager.reportCFG;
			  
	    if ((config.getString(player.getUniqueId().toString() + ".Group").equals("moderator"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("seniormoderator"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("juniordeveloper"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("developer"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("admin"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("leaddeveloper"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("owner"))) {
		  
		    if (reportConfig.getKeys(false).size() == 0) {
			    player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bWelcome back &r" + player.getDisplayName() + "&8!"));
			    player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThere are currently no reports to list&8!"));
			    player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bGo ahead and slack off a bit, you deserve it&8."));
			    return;
		    } else if (reportConfig.getKeys(false).size() == 1) {
			    player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bWelcome back &r" + player.getDisplayName() + "&8!"));
			    player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThere is currently &8[&c" + reportConfig.getKeys(false).size() + "&8] &buncleared report&8."));
			    player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bUse &e/report list &bto list it&8."));
			    return;
		    } else if (reportConfig.getKeys(false).size() == 2 || reportConfig.getKeys(false).size() >= 2) {
			    player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bWelcome back &r" + player.getDisplayName() + "&8!"));
			    player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThere are currently &8[&c" + reportConfig.getKeys(false).size() + "&8] &buncleared reports&8."));
			    player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bUse &e/report list &bto list them&8."));
			    return;
		    }
	    }
    }
  
}