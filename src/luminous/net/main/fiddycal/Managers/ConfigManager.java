package luminous.net.main.fiddycal.Managers;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class ConfigManager {
	
	public static String pluginFolder = "plugins/SGIN-Core/";
	
	public static File chatLogFolder;
	public static File sgMapDataFolder;
	public static File sgDataFolder;

	public static File bgMapDataFolder;
	public static File bgDataFolder;
	
	public static File uuidsFile;
    public static FileConfiguration uuidsCFG;

	public static File playersFile;
    public static FileConfiguration playersCFG;

	public static File reportFile;
    public static FileConfiguration reportCFG;
    
	public static File sgDataFile;
    public static FileConfiguration sgDataCFG;

	//For Battlegrounds maps
	public static File bgDataFile;
	public static FileConfiguration bgDataCFG;
    
	public static File sgMapDataFile;
    public static FileConfiguration sgMapDataCFG;
    
    public static File bgMapDataFile;
	public static FileConfiguration bgMapDataCFG;
	
	public static File sgStatsFile;
    public static FileConfiguration sgStatsCFG;
    
	public static File bgStatsFile;
    public static FileConfiguration bgStatsCFG;

	//For Battlegrounds kits
	public static File bgKitsFile;
	public static FileConfiguration bgKitsCFG;

	public static void createBGKits() {
		bgKitsFile = new File(pluginFolder + "BattleGrounds/kits.yml");
		bgKitsCFG = YamlConfiguration.loadConfiguration(bgKitsFile);
		if (!bgKitsFile.exists()){
			try {
				bgKitsFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void saveBGKits() {
		try {
			bgKitsCFG.save(bgKitsFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
    
    public static void createUUIDs() {
    	uuidsFile = new File(pluginFolder, "uuids.yml");
    	uuidsCFG = YamlConfiguration.loadConfiguration(ConfigManager.uuidsFile);
        if (!uuidsFile.exists()){
            try {
            	uuidsFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
	}
    
	public static void saveUUIDs() {
		try {
			uuidsCFG.save(uuidsFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void createPlayers() {
		playersFile = new File(pluginFolder, "players.yml");
		playersCFG = YamlConfiguration.loadConfiguration(playersFile);
	    if (!playersFile.exists()){
	        try {
	        	playersFile.createNewFile();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	}
    
	public static void savePlayers() {
		try {
			playersCFG.save(playersFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void createReports() {
		reportFile = new File(pluginFolder, "reports.yml");
		reportCFG = YamlConfiguration.loadConfiguration(ConfigManager.reportFile);
        if (!reportFile.exists()){
            try {
            	reportFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
	}
    
	public static void saveReports() {
		try {
			reportCFG.save(reportFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void createChatLogFolder() {
        try {
            chatLogFolder = new File(pluginFolder + File.separator + "Logs");
            if(!chatLogFolder.exists()){
            	chatLogFolder.mkdirs();
            }
        } catch(SecurityException e) {
        	e.printStackTrace();
        }
	}
	
	public static void createSGData() {
		sgDataFile = new File(pluginFolder + "SurvivalGames/info.yml");
		sgDataCFG = YamlConfiguration.loadConfiguration(ConfigManager.sgDataFile);
        if (!sgDataFile.exists()){
            try {
            	sgDataFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
	}

	public static void createBGData(){
		bgDataFile = new File(pluginFolder + "BattleGrounds/info.yml");
		bgDataCFG = YamlConfiguration.loadConfiguration(ConfigManager.bgDataFile);
		if (!bgDataFile.exists()){
			try {
				bgDataFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void saveBGData() {
		try {
			bgDataCFG.save(bgDataFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
    
	public static void saveSGData() {
		try {
			sgDataCFG.save(sgDataFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void createSGDataFolder() {
        try {
            sgDataFolder = new File(pluginFolder + File.separator + "SurvivalGames");
            if(!sgDataFolder.exists()){
            	sgDataFolder.mkdirs();
            }
        } catch(SecurityException e) {
        	e.printStackTrace();
        }
	}

	public static void createBGDataFolder() {
		try {
			bgDataFolder = new File(pluginFolder + File.separator + "BattleGrounds");
			if(!bgDataFolder.exists()){
				bgDataFolder.mkdirs();
			}
		} catch(SecurityException e) {
			e.printStackTrace();
		}
	}
	
	public static void createSGStats() {
		sgStatsFile = new File(pluginFolder + "SurvivalGames/stats.yml");
		sgStatsCFG = YamlConfiguration.loadConfiguration(sgStatsFile);
	    if (!sgStatsFile.exists()){
	        try {
	        	sgStatsFile.createNewFile();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	}
    
	public static void saveSGStats() {
		try {
			sgStatsCFG.save(sgStatsFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void createSGMapData(String map) {
		try {
            sgMapDataFolder = new File(pluginFolder + File.separator + "SurvivalGames" + File.separator + "Maps" + File.separator + map);
            if(!sgMapDataFolder.exists()){
            	sgMapDataFolder.mkdirs();
            }
        } catch(SecurityException e) {
        	e.printStackTrace();
        }
		
		sgMapDataFile = new File(sgMapDataFolder, "data.yml");
		sgMapDataCFG = YamlConfiguration.loadConfiguration(sgMapDataFile);
	    if (!sgMapDataFile.exists()){
	        try {
	        	sgMapDataFile.createNewFile();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	}
	
	public static void removeSGMapData(String map) {
		try {
            sgMapDataFolder = new File(pluginFolder + File.separator + "SurvivalGames" + File.separator + "Maps" + File.separator + map);
            if(!sgMapDataFolder.exists()){
            	sgMapDataFolder.mkdirs();
            }
        } catch(SecurityException e) {
        	e.printStackTrace();
        }
		
		sgMapDataFile = new File(sgMapDataFolder, "data.yml");
		sgMapDataCFG = YamlConfiguration.loadConfiguration(sgMapDataFile);
	    if (!sgMapDataFile.exists()){
	        try {
	        	sgMapDataFile.createNewFile();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	}
	
	public static FileConfiguration getSGMapData(String map) {
		sgMapDataFile = new File(pluginFolder + "SurvivalGames/Maps/" + map, "data.yml");
		sgMapDataCFG = YamlConfiguration.loadConfiguration(sgMapDataFile);
	    return sgMapDataCFG;
	}
    
	public static void saveSGMapData() {
		try {
			sgMapDataCFG.save(sgMapDataFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void createBGMapData(String map) {
		try {
			bgMapDataFolder = new File(pluginFolder + File.separator + "BattleGrounds" + File.separator + "Maps" + File.separator + map);
			if(!bgMapDataFolder.exists()){
				bgMapDataFolder.mkdirs();
			}
		} catch(SecurityException e) {
			e.printStackTrace();
		}

		bgMapDataFile = new File(bgMapDataFolder, "data.yml");
		bgMapDataCFG = YamlConfiguration.loadConfiguration(bgMapDataFile);
		if (!bgMapDataFile.exists()){
			try {
				bgMapDataFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void removeBGMapData(String map) {
		try {
			bgMapDataFolder = new File(pluginFolder + File.separator + "BattleGrounds" + File.separator + "Maps" + File.separator + map);
			if(!bgMapDataFolder.exists()){
				bgMapDataFolder.mkdirs();
			}
		} catch(SecurityException e) {
			e.printStackTrace();
		}

		bgMapDataFile = new File(bgMapDataFolder, "data.yml");
		bgMapDataCFG = YamlConfiguration.loadConfiguration(bgMapDataFile);
		if (!bgMapDataFile.exists()){
			try {
				bgMapDataFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static boolean doesBGMapExist(String map){
		bgMapDataFile = new File(pluginFolder + "BattleGrounds/Maps/" + map, "data.yml");
		if (bgMapDataFile.exists()) {
			return true;
		}else{
			return false;
		}
	}

	public static FileConfiguration getBGMapData(String map) {
		bgMapDataFile = new File(pluginFolder + "BattleGrounds/Maps/" + map, "data.yml");
		bgMapDataCFG = YamlConfiguration.loadConfiguration(bgMapDataFile);
		return bgMapDataCFG;
	}

	public static void saveBGMapData() {
		try {
			bgMapDataCFG.save(bgMapDataFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void createBGStats() {
		bgStatsFile = new File(pluginFolder + "BattleGrounds/stats.yml");
		bgStatsCFG = YamlConfiguration.loadConfiguration(bgStatsFile);
	    if (!bgStatsFile.exists()){
	        try {
	        	sgStatsFile.createNewFile();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	}
    
	public static void saveBGStats() {
		try {
			bgStatsCFG.save(bgStatsFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
