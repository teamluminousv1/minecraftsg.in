package luminous.net.main.fiddycal.Managers;

import java.util.ArrayList;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import luminous.net.main.fiddycal.Main;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGame;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGameState;
import luminous.net.minecraftsg.net.Staff.ModModeCore;

public class ChatManager implements Listener {

	@SuppressWarnings("unused")
	private ModModeCore mmc;

	@EventHandler
	public void serverChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		FileConfiguration config = ConfigManager.playersCFG;
		String world = player.getWorld().getName();
		Set<Player> allPlayers = event.getRecipients();
	    ArrayList<Player> recipients = new ArrayList<Player>();
	    recipients.addAll(allPlayers);
	    String preChat;
	    
	    if (Main.main.getDisguiseManager().isDisguised(player)) {
	    	player.sendMessage(MessageManager.c("&4&l&oTEMP: &cYou can't talk whilst disguised due to chat bugs."));
	    	event.setCancelled(true);
	    	return;
	    }
	    
	    if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
	    	SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
	    	if (game.getGamestate() == SGGameState.WAITING) {
    			preChat = MessageManager.c("&8[&e" + config.getString(event.getPlayer().getUniqueId().toString() + ".Points") + "&8]");
    		} else {
    			preChat = MessageManager.c("&8[&a" + game.getBm().getBounty(player) + "&8]&c" + game.getDistrictOfPlayer(event.getPlayer().getName()) + "&8|");
    		}
	    	if (game.getSpectators().contains(player.getName())) {
	    		preChat = MessageManager.c("&8[&e" + config.getString(event.getPlayer().getUniqueId().toString() + ".Points") + "&8]&4SPEC&8|");
	    	}
    	} else {
    		preChat = "";
    	}
	    
	    //if (mmc.getPlayer().contains(player.getName())) {
	    	//preChat = MessageManager.c("&4GM&8|");
	    //}
	    
		if (config.getString(player.getUniqueId().toString() + ".Group").equals("member")) {
	    	
	    	event.setMessage("�f" + event.getMessage());
	    	
	    } else if (config.getString(player.getUniqueId().toString() + ".Group").equals("vip") 
	    		|| config.getString(player.getUniqueId().toString() + ".Group").equals("mapmaker")
	    		|| config.getString(player.getUniqueId().toString() + ".Group").equals("retired")
    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("trialbuilder")
    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("builder")
    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("champion")
    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("legend")
    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("guardian")
    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("elite")
    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("royal")) {
	    	
	    	event.setMessage(MessageManager.c("&f" + event.getMessage()));
	    	
	    } else if (config.getString(player.getUniqueId().toString() + ".Group").equals("uhchost") 
    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("moderator")
    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("seniormoderator")
    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("juniordeveloper")
    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("admin")
    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("developer")
    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("leaddeveloper")
    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("owner")) {
	    	
	    	event.setMessage("�b" + event.getMessage());
	    	
	    } else {
	    	
	    	event.setMessage("�f" + event.getMessage());
	    	
	    }
		
		event.setFormat(preChat + player.getDisplayName() + "�8: �r" + event.getMessage());
	    
	    for (Player p : recipients) {
	    	
	    	if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
		    	SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
		    	if (game.getSpectators().contains(player.getName())) {
			    	if (game.getSpectators().contains(p.getName())) continue;
			    	event.getRecipients().remove(p);
		    	}
			}
	    	
	    	if ((config.getString(p.getUniqueId().toString() + ".Group").equals("uhchost")
        			|| config.getString(p.getUniqueId().toString() + ".Group").equals("moderator")
        			|| config.getString(p.getUniqueId().toString() + ".Group").equals("seniormoderator")
        			|| config.getString(p.getUniqueId().toString() + ".Group").equals("juniordeveloper")
        			|| config.getString(p.getUniqueId().toString() + ".Group").equals("admin")
        			|| config.getString(p.getUniqueId().toString() + ".Group").equals("developer")
        			|| config.getString(p.getUniqueId().toString() + ".Group").equals("leaddeveloper")
        			|| config.getString(p.getUniqueId().toString() + ".Group").equals("owner"))) {
	    		
	    		event.getRecipients().remove(p);
    			
	    		if (p.getWorld().equals(player.getWorld())) {
	    			
	    			String format = StringUtils.replaceEach(event.getFormat(),
	    		    new String[] {
	    		        "\"", "\\"
	    		    },
	    		        new String[] {
	    		        "\\\"", "\\\\"
	    		    });
	    			
	    			if (config.getString(player.getUniqueId().toString() + ".Group").equals("member")) {
						
						Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "tellraw "
		                        + p.getName() + " [\"\","
		                        + "{\"text\":\"�8[\"},"
		                        + "{\"text\":\"�6Z\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/teleport " + player.getName() + "\"}},"
		                        + "{\"text\":\"�3K\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/history " + player.getName() + "\"}},"
				                + "{\"text\":\"�2M\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/history " + player.getName() + "\"}},"
						        + "{\"text\":\"�4B\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/history " + player.getName() + "\"}},"
		                        + "{\"text\":\"�8] \"},{\"text\":\"" + format + "\",\"color\":\"gray\"}]");
				    	
				    } else if (config.getString(player.getUniqueId().toString() + ".Group").equals("vip") 
				    		|| config.getString(player.getUniqueId().toString() + ".Group").equals("mapmaker")
				    		|| config.getString(player.getUniqueId().toString() + ".Group").equals("retired")
			    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("trialbuilder")
			    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("builder")
			    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("champion")
			    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("legend")
			    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("guardian")
			    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("elite")
			    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("royal")) {
				    	
				    	Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "tellraw "
		                        + p.getName() + " [\"\","
		                        + "{\"text\":\"�8[\"},"
		                        + "{\"text\":\"�6Z\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/teleport " + player.getName() + "\"}},"
		                        + "{\"text\":\"�3K\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/history " + player.getName() + "\"}},"
				                + "{\"text\":\"�2M\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/history " + player.getName() + "\"}},"
						        + "{\"text\":\"�4B\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/history " + player.getName() + "\"}},"
		                        + "{\"text\":\"�8] \"},{\"text\":\"" + format + "\",\"color\":\"white\"}]");
				    	
				    } else if (config.getString(player.getUniqueId().toString() + ".Group").equals("uhchost") 
			    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("moderator")
			    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("seniormoderator")
			    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("juniordeveloper")
			    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("admin")
			    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("developer")
			    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("leaddeveloper")
			    			|| config.getString(player.getUniqueId().toString() + ".Group").equals("owner")) {
				    	
				    	Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "tellraw "
		                        + p.getName() + " [\"\","
				                + "{\"text\":\"�8[\"},"
				                + "{\"text\":\"�6Z\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/teleport " + player.getName() + "\"}},"
				                + "{\"text\":\"�3K\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/history " + player.getName() + "\"}},"
						        + "{\"text\":\"�2M\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/history " + player.getName() + "\"}},"
								+ "{\"text\":\"�4B\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/history " + player.getName() + "\"}},"
		                        + "{\"text\":\"�8] \"},{\"text\":\"" + format + "\",\"color\":\"aqua\"}]");
				    	
				    } else {
				    	
				    	Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "tellraw "
		                        + p.getName() + " [\"\","
						        + "{\"text\":\"�8[\"},"
				                + "{\"text\":\"�6Z\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/teleport " + player.getName() + "\"}},"
				                + "{\"text\":\"�3K\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/history " + player.getName() + "\"}},"
						        + "{\"text\":\"�2M\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/history " + player.getName() + "\"}},"
								+ "{\"text\":\"�4B\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/history " + player.getName() + "\"}},"
		                        + "{\"text\":\"�8] \"},{\"text\":\"" + format + "\",\"color\":\"white\"}]");
				    	
				    }
	    		}
        	}
	    	
	    	if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
		    	SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
		    	if (game.getSpectators().contains(player.getName())) {
			    	if (game.getSpectators().contains(p.getName())) continue;
			    	event.getRecipients().remove(p);
		    	}
			}
	    	
	    	if (world.equals(p.getWorld().getName())) continue;
		    event.getRecipients().remove(p);
		
	    }
	    
	}
	
}
