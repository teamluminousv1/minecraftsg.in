package luminous.net.main.fiddycal.Managers.DisguiseManager;

import java.util.HashSet;

import org.bukkit.entity.Player;

public class DisguiseManager {

	private HashSet<Disguise> disguised;
	
	public DisguiseManager() {
		this.disguised = new HashSet<>();
	}
	
	public void addDisguise(Disguise disguise, boolean asData) {
		if (asData) {
			this.disguised.add(disguise);
			// TODO: Add skin to database if enabled.
		} else this.disguised.add(disguise);
	}
	
	public void removeDisguise(Disguise disguise) {
		if (this.disguised.contains(disguise)) {
			this.disguised.remove(disguise);
		}
	}
	
	public Disguise getDisguise(Player player) {
		for (Disguise disguise : this.disguised) {
			if (disguise.getUniqueId().equals(player.getUniqueId().toString())) {
				return disguise;
			}
		}
		return null;
	}
	
	public boolean isDisguised(Player player) {
		if (getDisguise(player) != null) {
			return true;
		} else return false;
	}
	
	public HashSet<Disguise> getDisguisedPlayers() {
		return this.disguised;
	}
	
}
