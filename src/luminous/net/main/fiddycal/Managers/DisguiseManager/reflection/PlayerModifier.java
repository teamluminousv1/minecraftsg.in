package luminous.net.main.fiddycal.Managers.DisguiseManager.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.bukkit.entity.Player;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.DisguiseManager.mojang.MojangAPI;
import luminous.net.main.fiddycal.Managers.DisguiseManager.mojang.MojangAPI.SkinRequestException;
import luminous.net.main.fiddycal.Managers.DisguiseManager.skins.SkinCache;

/* import com.github.games647.changeskin.bukkit.ChangeSkinBukkit;
import com.github.games647.changeskin.bukkit.tasks.SkinDownloader;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property; */

import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutNamedEntitySpawn;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import com.mojang.authlib.GameProfile;

public class PlayerModifier {
	
	public PlayerModifier() {}
	
	public void v1_8_R3_changeName(Player player, String name) {
        try {
            Method getHandle = player.getClass().getMethod("getHandle", (Class<?>[]) null);
            Object entityPlayer = getHandle.invoke(player);
            Class<?> entityHuman = entityPlayer.getClass().getSuperclass();
            Field bH = entityHuman.getDeclaredField("bH");
            bH.setAccessible(true);
            bH.set(entityPlayer, new GameProfile(player.getUniqueId(), name));
            for (Player pl : Main.main.getServer().getOnlinePlayers()) {
                pl.hidePlayer(player);
                pl.showPlayer(player);
            }
        } catch (NoSuchMethodException | SecurityException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }
    
	public void v1_7_R4_changeName(Player player, String name) {
		if (Main.main.getServer().getOnlinePlayers().size() == 1) {
			/* For Possible 1.8.x Compatibility
            ((CraftPlayer)pl).getHandle().playerConnection.sendPacket(
            new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, ((CraftPlayer)player).getHandle()));
            */
            GameProfile gp = ((CraftPlayer)player).getProfile();
            try {
                Field nameField = GameProfile.class.getDeclaredField("name");
                nameField.setAccessible(true);
                Field modifiersField = Field.class.getDeclaredField("modifiers");
                modifiersField.setAccessible(true);
                modifiersField.setInt(nameField, nameField.getModifiers() & ~Modifier.FINAL);
                nameField.set(gp, name);
            } catch (IllegalAccessException | NoSuchFieldException ex) {
                throw new IllegalStateException(ex);
            }
            /* For Possible 1.8.x Compatibility
            ((CraftPlayer)pl).getHandle()
            .playerConnection.sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER,
            ((CraftPlayer)player).getHandle()));
            */
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityDestroy(player.getEntityId()));
		}
		for (Player pl : Main.main.getServer().getOnlinePlayers()) {
            if (pl.getName() == player.getName()) continue;
            /* For Possible 1.8.x Compatibility
            ((CraftPlayer)pl).getHandle().playerConnection.sendPacket(
            new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, ((CraftPlayer)player).getHandle()));
            */
            GameProfile gp = ((CraftPlayer)player).getProfile();
            try {
                Field nameField = GameProfile.class.getDeclaredField("name");
                nameField.setAccessible(true);
                Field modifiersField = Field.class.getDeclaredField("modifiers");
                modifiersField.setAccessible(true);
                modifiersField.setInt(nameField, nameField.getModifiers() & ~Modifier.FINAL);
                nameField.set(gp, name);
            } catch (IllegalAccessException | NoSuchFieldException ex) {
                throw new IllegalStateException(ex);
            }
            /* For Possible 1.8.x Compatibility
            ((CraftPlayer)pl).getHandle()
            .playerConnection.sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER,
            ((CraftPlayer)player).getHandle()));
            */
            ((CraftPlayer)pl).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityDestroy(player.getEntityId()));
            ((CraftPlayer)pl).getHandle().playerConnection.sendPacket(new PacketPlayOutNamedEntitySpawn(((CraftPlayer)player).getHandle()));
        }
    }
	
	public void v1_7_R4_changeSkin(Player player, String skinName) {
		try {
			MojangAPI.getUUID(skinName);
			SkinCache.setPlayerSkin(player.getName(), skinName);
			Main.main.getSkinCreator().apply(player, SkinCache.createSkin(player.getName()));
		} catch (SkinRequestException e) {
			e.printStackTrace();
		}
	}
	
}
