package luminous.net.main.fiddycal.Managers.DisguiseManager;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.bukkit.entity.Player;

import luminous.net.main.fiddycal.Main;

public class Disguise {

	private String playerUUID;
	private String disguiseUUID;
	private String playerName;
	private String disguiseName;
	private String disguiseSkin;
	private String date;
	private String time;
	
	public Disguise(Player player, String newName, String newSkin, String displayName) {
		
		System.out.println("Disguise (new): " + player.getName() + " | Disguised as " + newName + " with the skin of " + newSkin + ".");
		
		this.playerUUID = player.getUniqueId().toString();
		this.playerName = player.getName();
		this.disguiseSkin = newSkin;
		this.disguiseName = newName;
		
		Date now = new Date();
		SimpleDateFormat date = new SimpleDateFormat("dd.MM.yy");
		SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
		
		this.date = date.format(now);
		this.time = time.format(now);

		Main.main.getPlayerModifier().v1_8_R3_changeName(player, newName);
		Main.main.getPlayerModifier().v1_7_R4_changeSkin(player, newSkin);
		Main.main.getDisguiseManager().addDisguise(this, false);
		
    	player.setDisplayName(displayName);
		player.setPlayerListName(newName);
		
	}
	
	public String getUniqueId() {
		return this.playerUUID.toString();
	}
	
	public String getName(boolean disguiseName) {
		if (disguiseName) {
			return this.disguiseName;
		} else return this.playerName;
	}
	
	public String getSkin() {
		return this.disguiseSkin;
	}
	
	public String getSkinUniqueId() {
		return this.disguiseUUID;
	}
	
	public String getTime() {
		return this.time;
	}
	
	public String getDate() {
		return this.date;
	}
	
	public void unDisguise() {
		DisguiseManager manager = Main.main.getDisguiseManager();
		Player player = Main.main.getServer().getPlayer(this.playerName);
		Disguise disguise = manager.getDisguise(player);
		Main.main.getPlayerModifier().v1_7_R4_changeName(player, disguise.getName(false));
		Main.main.getPlayerModifier().v1_7_R4_changeSkin(player, disguise.getName(false));
		player.setDisplayName("" + disguise.getName(false));
		player.setPlayerListName(disguise.getName(false));
		manager.removeDisguise(disguise);
	}
	
}
