package luminous.net.main.fiddycal.Managers.DisguiseManager.skins;

import org.bukkit.entity.Player;

import luminous.net.main.fiddycal.Managers.DisguiseManager.reflection.SelfmadeReflection;

public abstract class SkinCreator {
	
	public void apply(Player player, Object props) {
		try {
			if (props == null)
				return;

			Object handle = SelfmadeReflection.invokeMethod(player.getClass(), player, "getHandle");
			Object profile = SelfmadeReflection.invokeMethod(handle.getClass(), handle, "getProfile");
			Object propmap = SelfmadeReflection.invokeMethod(profile.getClass(), profile, "getProperties");
			SelfmadeReflection.invokeMethod(propmap, "clear");
			SelfmadeReflection.invokeMethod(propmap.getClass(), propmap, "put",
			new Class[] { Object.class, Object.class }, new Object[] { "textures", props });
			updateSkin(player);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public abstract void updateSkin(Player p);

}
