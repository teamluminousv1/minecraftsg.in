package luminous.net.main.fiddycal.Managers.DisguiseManager.skins;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.bukkit.entity.Player;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.DisguiseManager.Disguise;
import luminous.net.main.fiddycal.Managers.DisguiseManager.mojang.MojangAPI;
import luminous.net.main.fiddycal.Managers.DisguiseManager.mojang.MojangAPI.SkinRequestException;
import luminous.net.main.fiddycal.Managers.DisguiseManager.reflection.SelfmadeReflection;

public class SkinCache {

	private static Class<?> property;
	private static File folder = Main.main.getDataFolder();
	private static ExecutorService executor;

	static {
		try {
			executor = Executors.newCachedThreadPool();
			property = Class.forName("com.mojang.authlib.properties.Property");
		} catch (Exception e) {
			try {
				property = Class.forName("net.minecraft.util.com.mojang.authlib.properties.Property");
			} catch (Exception exc) {
				System.out.println("Could not find a valid Property class! Please report this issue to 5Ocal!");
			}
		}
	}
	
	public static Object createProperty(String name, String value, String signature) {
		try {
			return SelfmadeReflection.invokeConstructor(property, new Class<?>[] {
			String.class, String.class, String.class }, name, value, signature);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static ExecutorService getExecutor() {
		return executor;
	}
	
	public static Object createSkin(final String name) throws SkinRequestException {
		String skin = name.toLowerCase();
		// if (getPlayerSkin(name) != null)
		//	skin = getPlayerSkin(name);
        Object textures = null;
		textures = null;
		/* getSkinData(skin);
		if (textures != null){
			return textures;
		}
		*/
		final String sname = skin;
		final Object oldprops = textures;
		try {
			Object properties = null;
			properties = MojangAPI.getSkinProperty(MojangAPI.getUUID(sname));
			if (properties == null) return properties;
			boolean shouldUpdate = false;
			String value = Base64Coder.decodeString((String) SelfmadeReflection.invokeMethod(properties, "getValue"));
			String urlbeg = "url\":\"";
			String urlend = "\"}}";
			String newurl = MojangAPI.getStringBetween(value, urlbeg, urlend);
			try {
				value = Base64Coder.decodeString((String) SelfmadeReflection.invokeMethod(oldprops, "getValue"));
				String oldurl = MojangAPI.getStringBetween(value, urlbeg, urlend);
				shouldUpdate = !oldurl.equals(newurl);
			} catch (Exception e) {
				shouldUpdate = true;
			}
			setSkinData(sname, properties);
			if (shouldUpdate) Main.main.getSkinCreator().apply(org.bukkit.Bukkit.getPlayer(name), properties);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return textures;
	}
	
	public static String getPlayerSkin(String name) {
		name = name.toLowerCase();
		File playerFile = new File(folder.getAbsolutePath() + File.separator + File.separator + "Disguises" + File.separator + name + ".disguise");
		try {
			if (!playerFile.exists())
				return null;
			BufferedReader buf = new BufferedReader(new FileReader(playerFile));
			String line, skin = null;
			if ((line = buf.readLine()) != null)
				skin = line;
			buf.close();
			if (skin.equalsIgnoreCase(name))
				playerFile.delete();
			return skin;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return name;
	}
	
	public static Object getSkinData(String name) {
		name = name.toLowerCase();
		File skinFile = new File(folder.getAbsolutePath() + File.separator + "Skins" + File.separator + name + ".skin");
		try {
			if (!skinFile.exists())
				return null;

			BufferedReader buf = new BufferedReader(new FileReader(skinFile));
			String line, value = "", signature = "", timestamp = "";
			for (int i = 0; i < 3; i++)
				if ((line = buf.readLine()) != null)
					if (value.isEmpty()) {
						value = line;
					} else if (signature.isEmpty()) {
						signature = line;
					} else {
						timestamp = line;
					}
			buf.close();
			if (isOld(Long.valueOf(timestamp))) {
				Object skin = MojangAPI.getSkinProperty(MojangAPI.getUUID(name));
				if (skin != null) {
					SkinCache.setSkinData(name, skin);
				}
			}
			return SkinCache.createProperty("textures", value, signature);
		} catch (Exception e) {
			removeSkinData(name);
			System.out.println("Unsupported player format.. removing ("+name+").");
		}
		return null;
	}
    
	public static boolean isOld(long timestamp) {
		if (timestamp + TimeUnit.MINUTES.toMillis(1) <= System.currentTimeMillis()) {
			return true;
		}
		return false;
	}
	public static void initiate(File pluginFolder) {
		folder = pluginFolder;
		File folder = new File(pluginFolder.getAbsolutePath() + File.separator + "Skins" + File.separator);
		folder.mkdirs();
		folder = new File(pluginFolder.getAbsolutePath() + File.separator + "Disguises" + File.separator);
		folder.mkdirs();
	}
	
	public static void removeSkinData(String name) {
		name = name.toLowerCase();
		File skinFile = new File(folder.getAbsolutePath() + File.separator + "Skins" + File.separator + name + ".skin");
		if (skinFile.exists())
			skinFile.delete();
	}
	
	public static void setSkinData(String name, Object textures) {
		name = name.toLowerCase();
		String value = "";
		String signature = "";
		String timestamp = "";
		try {
			value = (String) SelfmadeReflection.invokeMethod(textures, "getValue");
			signature = (String) SelfmadeReflection.invokeMethod(textures, "getSignature");
			timestamp = String.valueOf(System.currentTimeMillis());
		} catch (Exception e) {
			e.printStackTrace();
		}
		File skinFile = new File(folder.getAbsolutePath() + File.separator + "Skins" + File.separator + name + ".skin");
		try {
			if (value.isEmpty() || signature.isEmpty() || timestamp.isEmpty())
				return;
			if (!skinFile.exists())
				skinFile.createNewFile();
			FileWriter writer = new FileWriter(skinFile);
			writer.write(value + "\n" + signature + "\n" + timestamp);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static Map<String, Object> getSkins(int number){
		HashMap<String, Object> thingy = new HashMap<String, Object>();
		Map<String, Object> list = new TreeMap<String, Object>(thingy);
		String path  = Main.main.getDataFolder() + "/Skins/";
        File folder = new File(path);
        String[] fileNames = folder.list();
        int i = 0;
        for (String file : fileNames){
        	if (i >= number){
        	list.put(file.replace(".skin", ""), SkinCache.getSkinDataMenu(file.replace(".skin", "")));
        	}
        	i++;
        }
		return list;
	}
	
	@SuppressWarnings("unused")
	public static Object getSkinDataMenu(String name) {
		name = name.toLowerCase();
		File skinFile = new File(folder.getAbsolutePath() + File.separator + "Skins" + File.separator + name + ".skin");
		try {
			if (!skinFile.exists())
				return null;
			BufferedReader buf = new BufferedReader(new FileReader(skinFile));
			String line, value = "", signature = "", timestamp = "";
			for (int i = 0; i < 3; i++)
				if ((line = buf.readLine()) != null)
					if (value.isEmpty()) {
						value = line;
					} else if (signature.isEmpty()) {
						signature = line;
					} else {
						timestamp = line;
					}
			buf.close();
			return SkinCache.createProperty("textures", value, signature);
		} catch (Exception e) {
			System.out.println("Unsupported player format.. removing (" + name + ").");
		}
		return null;
	}
	
	public static void setPlayerSkin(String name, String skin) {
		name = name.toLowerCase();
		if (Main.main.getServer().getPlayer(name) != null) {
			Player player = Main.main.getServer().getPlayer(name);
			Disguise disguise = Main.main.getDisguiseManager().getDisguise(player);
			if (disguise != null) {
				name = disguise.getName(false);
			}
		}
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>> " + folder.getAbsolutePath() + File.separator + "Disguises" + File.separator + name + ".disguise");
		File playerFile = new File(folder.getAbsolutePath() + File.separator + "Disguises" + File.separator + name + ".disguise");
		try {
			if (skin.equalsIgnoreCase(name) && playerFile.exists()) {
				playerFile.delete();
				return;
			}
			if (!playerFile.exists())
				playerFile.createNewFile();
			FileWriter writer = new FileWriter(playerFile);
			writer.write(skin);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
