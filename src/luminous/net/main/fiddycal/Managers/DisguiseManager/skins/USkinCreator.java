package luminous.net.main.fiddycal.Managers.DisguiseManager.skins;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.DisguiseManager.reflection.SelfmadeReflection;

public class USkinCreator extends SkinCreator {

	private Class<?> PlayOutRespawn;
	private Class<?> EntityHuman;
	private Class<?> PlayOutNamedEntitySpawn;
	private Class<?> PlayOutEntityDestroy;
	private Class<?> PlayOutPlayerInfo;
	private Class<?> PlayOutPosition;
	private Class<?> PlayOutEntityEquipment;
	private Class<?> ItemStack;
	private Class<?> Packet;
	private Class<?> CraftItemStack;
	private Class<?> PlayOutHeldItemSlot;
	private Class<?> EnumPlayerInfoAction;

	private Enum<?> PEACEFUL;
	private Enum<?> REMOVE_PLAYER;
	private Enum<?> ADD_PLAYER;
	private Enum<?> MAINHAND;
	private Enum<?> OFFHAND;
	private Enum<?> HEAD;
	private Enum<?> FEET;
	private Enum<?> LEGS;
	private Enum<?> CHEST;

	// Since literraly no one is able to optimize it, I will
	public USkinCreator() {
		try {
			Packet = SelfmadeReflection.getNMSClass("Packet");
			PlayOutHeldItemSlot = SelfmadeReflection.getNMSClass("PacketPlayOutHeldItemSlot");
			CraftItemStack = SelfmadeReflection.getBukkitClass("inventory.CraftItemStack");
			ItemStack = SelfmadeReflection.getNMSClass("ItemStack");
			PlayOutEntityEquipment = SelfmadeReflection.getNMSClass("PacketPlayOutEntityEquipment");
			PlayOutPosition = SelfmadeReflection.getNMSClass("PacketPlayOutPosition");
			EntityHuman = SelfmadeReflection.getNMSClass("EntityHuman");
			PlayOutNamedEntitySpawn = SelfmadeReflection.getNMSClass("PacketPlayOutNamedEntitySpawn");
			PlayOutEntityDestroy = SelfmadeReflection.getNMSClass("PacketPlayOutEntityDestroy");
			PlayOutPlayerInfo = SelfmadeReflection.getNMSClass("PacketPlayOutPlayerInfo");
			PlayOutRespawn = SelfmadeReflection.getNMSClass("PacketPlayOutRespawn");
			try {
			EnumPlayerInfoAction = SelfmadeReflection.getNMSClass("EnumPlayerInfoAction");
			}catch (Exception e){
			}
			PEACEFUL = SelfmadeReflection.getEnum(SelfmadeReflection.getNMSClass("EnumDifficulty"), "PEACEFUL");
			try {
			REMOVE_PLAYER = SelfmadeReflection.getEnum(PlayOutPlayerInfo, "EnumPlayerInfoAction", "REMOVE_PLAYER");
			ADD_PLAYER = SelfmadeReflection.getEnum(PlayOutPlayerInfo, "EnumPlayerInfoAction", "ADD_PLAYER");
			}catch (Exception e){
				//1.8 or below
				REMOVE_PLAYER = SelfmadeReflection.getEnum(EnumPlayerInfoAction, "REMOVE_PLAYER");
				ADD_PLAYER = SelfmadeReflection.getEnum(EnumPlayerInfoAction, "ADD_PLAYER");
			}
			MAINHAND = SelfmadeReflection.getEnum(SelfmadeReflection.getNMSClass("EnumItemSlot"), "MAINHAND");
			OFFHAND = SelfmadeReflection.getEnum(SelfmadeReflection.getNMSClass("EnumItemSlot"), "OFFHAND");
			HEAD = SelfmadeReflection.getEnum(SelfmadeReflection.getNMSClass("EnumItemSlot"), "HEAD");
			CHEST = SelfmadeReflection.getEnum(SelfmadeReflection.getNMSClass("EnumItemSlot"), "CHEST");
			FEET = SelfmadeReflection.getEnum(SelfmadeReflection.getNMSClass("EnumItemSlot"), "FEET");
			LEGS = SelfmadeReflection.getEnum(SelfmadeReflection.getNMSClass("EnumItemSlot"), "LEGS");
		} catch (Exception e) {

		}
	}

	private void sendPacket(Object playerConnection, Object packet) throws Exception {
		SelfmadeReflection.invokeMethod(playerConnection.getClass(), playerConnection, "sendPacket",
				new Class<?>[] { Packet }, new Object[] { packet });
	}

	@Override
	public void updateSkin(Player player) {
		if (!player.isOnline())
			return;

		try {
			Object ep = SelfmadeReflection.invokeMethod(player, "getHandle");
			Location l = player.getLocation();

			List<Object> set = new ArrayList<>();
			set.add(ep);
			Iterable<?> iterable = set;
            Object removeInfo = null;
            Object removeEntity = null;
            Object addNamed = null;
            Object addInfo = null;
   
			removeInfo = SelfmadeReflection.invokeConstructor(PlayOutPlayerInfo,
					new Class<?>[] { REMOVE_PLAYER.getClass(), Iterable.class }, REMOVE_PLAYER, iterable);
			removeEntity = SelfmadeReflection.invokeConstructor(PlayOutEntityDestroy, new Class<?>[] { int[].class },
					new int[] { player.getEntityId() });
			addNamed = SelfmadeReflection.invokeConstructor(PlayOutNamedEntitySpawn, new Class<?>[] { EntityHuman },
					ep);
			addInfo = SelfmadeReflection.invokeConstructor(PlayOutPlayerInfo,
					new Class<?>[] { ADD_PLAYER.getClass(), Iterable.class }, ADD_PLAYER, iterable);
			// Slowly getting from object to object till i get what I need for
			// the respawn packet
			Object world = SelfmadeReflection.invokeMethod(ep, "getWorld");
			Object difficulty = SelfmadeReflection.invokeMethod(world, "getDifficulty");
			Object worlddata = SelfmadeReflection.getObject(world, "worldData");
			Object worldtype = SelfmadeReflection.invokeMethod(worlddata, "getType");
			int dimension = 0;
			dimension = (int) SelfmadeReflection.getObject(world, "dimension");
			Object playerIntManager = SelfmadeReflection.getObject(ep, "playerInteractManager");
			Enum<?> enumGamemode = (Enum<?>) SelfmadeReflection.invokeMethod(playerIntManager, "getGameMode");

			int gmid = (int) SelfmadeReflection.invokeMethod(enumGamemode, "getId");

			Object respawn = SelfmadeReflection.invokeConstructor(PlayOutRespawn,
					new Class<?>[] { int.class, PEACEFUL.getClass(), worldtype.getClass(), enumGamemode.getClass() },
					dimension, difficulty, worldtype, SelfmadeReflection.invokeMethod(enumGamemode.getClass(), null,
							"getById", new Class<?>[] { int.class }, new Object[] { gmid }));

			Object pos = null;

			try {
				// 1.9+
				pos = SelfmadeReflection.invokeConstructor(PlayOutPosition,
						new Class<?>[] { double.class, double.class, double.class, float.class, float.class, Set.class,
								int.class },
						l.getX(), l.getY(), l.getZ(), l.getYaw(), l.getPitch(), new HashSet<Enum<?>>(), 0);
			} catch (Exception e) {
				// 1.8 -
				pos = SelfmadeReflection.invokeConstructor(PlayOutPosition,
						new Class<?>[] { double.class, double.class, double.class, float.class, float.class,
								Set.class },
						l.getX(), l.getY(), l.getZ(), l.getYaw(), l.getPitch(), new HashSet<Enum<?>>());
			}

			Object hand = null;
			Object mainhand = null;
			Object offhand = null;
			Object helmet = null;
			Object boots = null;
			Object chestplate = null;
			Object leggings = null;

			// MAINHAND is only null if we are less than 1.9
			if (MAINHAND == null) {

				hand = SelfmadeReflection.invokeConstructor(PlayOutEntityEquipment,
						new Class<?>[] { int.class, int.class, ItemStack }, player.getEntityId(), 0,
						SelfmadeReflection.invokeMethod(CraftItemStack, null, "asNMSCopy",
								new Class<?>[] { ItemStack.class }, new Object[] { player.getItemInHand() }));

				helmet = SelfmadeReflection.invokeConstructor(PlayOutEntityEquipment,
						new Class<?>[] { int.class, int.class, ItemStack }, player.getEntityId(), 4,
						SelfmadeReflection.invokeMethod(CraftItemStack, null, "asNMSCopy",
								new Class<?>[] { ItemStack.class },
								new Object[] { player.getInventory().getHelmet() }));

				chestplate = SelfmadeReflection.invokeConstructor(PlayOutEntityEquipment,
						new Class<?>[] { int.class, int.class, ItemStack }, player.getEntityId(), 3,
						SelfmadeReflection.invokeMethod(CraftItemStack, null, "asNMSCopy",
								new Class<?>[] { ItemStack.class },
								new Object[] { player.getInventory().getChestplate() }));

				leggings = SelfmadeReflection.invokeConstructor(PlayOutEntityEquipment,
						new Class<?>[] { int.class, int.class, ItemStack }, player.getEntityId(), 2,
						SelfmadeReflection.invokeMethod(CraftItemStack, null, "asNMSCopy",
								new Class<?>[] { ItemStack.class },
								new Object[] { player.getInventory().getLeggings() }));

				boots = SelfmadeReflection.invokeConstructor(PlayOutEntityEquipment,
						new Class<?>[] { int.class, int.class, ItemStack }, player.getEntityId(), 1,
						SelfmadeReflection.invokeMethod(CraftItemStack, null, "asNMSCopy",
								new Class<?>[] { ItemStack.class }, new Object[] { player.getInventory().getBoots() }));
			}
			// If 1.9+
			else {
				mainhand = SelfmadeReflection.invokeConstructor(PlayOutEntityEquipment,
						new Class<?>[] { int.class, MAINHAND.getClass(), ItemStack }, player.getEntityId(), MAINHAND,
						SelfmadeReflection.invokeMethod(CraftItemStack, null, "asNMSCopy",
								new Class<?>[] { ItemStack.class },
								new Object[] { player.getInventory().getItemInHand() }));

				offhand = SelfmadeReflection.invokeConstructor(PlayOutEntityEquipment,
						new Class<?>[] { int.class, OFFHAND.getClass(), ItemStack }, player.getEntityId(), OFFHAND,
						SelfmadeReflection.invokeMethod(CraftItemStack, null, "asNMSCopy",
								new Class<?>[] { ItemStack.class },
								new Object[] { player.getInventory().getItemInHand() }));

				helmet = SelfmadeReflection.invokeConstructor(PlayOutEntityEquipment,
						new Class<?>[] { int.class, HEAD.getClass(), ItemStack }, player.getEntityId(), HEAD,
						SelfmadeReflection.invokeMethod(CraftItemStack, null, "asNMSCopy",
								new Class<?>[] { ItemStack.class },
								new Object[] { player.getInventory().getHelmet() }));

				chestplate = SelfmadeReflection.invokeConstructor(PlayOutEntityEquipment,
						new Class<?>[] { int.class, CHEST.getClass(), ItemStack }, player.getEntityId(), CHEST,
						SelfmadeReflection.invokeMethod(CraftItemStack, null, "asNMSCopy",
								new Class<?>[] { ItemStack.class },
								new Object[] { player.getInventory().getChestplate() }));

				leggings = SelfmadeReflection.invokeConstructor(PlayOutEntityEquipment,
						new Class<?>[] { int.class, LEGS.getClass(), ItemStack }, player.getEntityId(), LEGS,
						SelfmadeReflection.invokeMethod(CraftItemStack, null, "asNMSCopy",
								new Class<?>[] { ItemStack.class },
								new Object[] { player.getInventory().getLeggings() }));

				boots = SelfmadeReflection.invokeConstructor(PlayOutEntityEquipment,
						new Class<?>[] { int.class, FEET.getClass(), ItemStack }, player.getEntityId(), FEET,
						SelfmadeReflection.invokeMethod(CraftItemStack, null, "asNMSCopy",
								new Class<?>[] { ItemStack.class }, new Object[] { player.getInventory().getBoots() }));
			}

			Object slot = SelfmadeReflection.invokeConstructor(PlayOutHeldItemSlot, new Class<?>[] { int.class },
					player.getInventory().getHeldItemSlot());

			for (Player pOnline : Bukkit.getOnlinePlayers()) {
				final Object craftHandle = SelfmadeReflection.invokeMethod(pOnline, "getHandle");
				Object playerCon = SelfmadeReflection.getObject(craftHandle, "playerConnection");

				if (pOnline.equals(player)) {

					sendPacket(playerCon, removeInfo);
					sendPacket(playerCon, addInfo);
					sendPacket(playerCon, respawn);
					Bukkit.getScheduler().runTask(Main.main, new Runnable() {
						@Override
						public void run() {
							try {
								SelfmadeReflection.invokeMethod(craftHandle, "updateAbilities");
							} catch (Exception e) {
							}
						}

					});
					sendPacket(playerCon, pos);
					sendPacket(playerCon, slot);
					SelfmadeReflection.invokeMethod(pOnline, "updateScaledHealth");
					SelfmadeReflection.invokeMethod(pOnline, "updateInventory");
					SelfmadeReflection.invokeMethod(craftHandle, "triggerHealthUpdate");
					if (pOnline.isOp()) {
						//Fix for the command blocks bug.
						pOnline.setOp(false);
						pOnline.setOp(true);
					}
					continue;
				}
				/*Now checks if the player is in the same world and if can see the player
				 * I did that to try to prevent player duplications.
				 */
				if (pOnline.getWorld().equals(player.getWorld())&&pOnline.canSee(player)&&player.isOnline()) {
					sendPacket(playerCon, removeEntity);
					sendPacket(playerCon, removeInfo);
					sendPacket(playerCon, addInfo);
					sendPacket(playerCon, addNamed);

					if (MAINHAND != null) {
						sendPacket(playerCon, mainhand);
						sendPacket(playerCon, offhand);
					} else
						sendPacket(playerCon, hand);

					sendPacket(playerCon, helmet);
					sendPacket(playerCon, chestplate);
					sendPacket(playerCon, leggings);
					sendPacket(playerCon, boots);
				}else{
					//Only sends player update packet
					sendPacket(playerCon, removeInfo);
					sendPacket(playerCon, addInfo);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
}