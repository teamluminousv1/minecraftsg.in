package luminous.net.main.fiddycal.Managers;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import luminous.net.main.fiddycal.Main;

public class RankManager {
	
	public static void giveRank(CommandSender sender, String targetPlayer, String rank, boolean silent) {
		FileConfiguration config = ConfigManager.playersCFG;
		FileConfiguration uuid = ConfigManager.uuidsCFG;

		if (rank.equalsIgnoreCase("Member")
				|| rank.equalsIgnoreCase("Champion")
                || rank.equalsIgnoreCase("Legend")
                || rank.equalsIgnoreCase("Guardian")
                || rank.equalsIgnoreCase("Elite")
                || rank.equalsIgnoreCase("Royal")
                || rank.equalsIgnoreCase("MapMaker")
                || rank.equalsIgnoreCase("VIP")
                || rank.equalsIgnoreCase("TrialBuilder")
                || rank.equalsIgnoreCase("Builder")
                || rank.equalsIgnoreCase("Moderator")
                || rank.equalsIgnoreCase("UHCHost")
			    || rank.equalsIgnoreCase("SeniorModerator")
				|| rank.equalsIgnoreCase("Admin")
				|| rank.equalsIgnoreCase("Retired")
                || rank.equalsIgnoreCase("JuniorDeveloper")
				|| rank.equalsIgnoreCase("Developer")
				|| rank.equalsIgnoreCase("Moderator")
				|| rank.equalsIgnoreCase("LeadDeveloper")
				|| rank.equalsIgnoreCase("Owner")) {
			
			String orRank = rank.toLowerCase();
			String capRank = Character.toUpperCase(orRank.charAt(0)) + orRank.substring(1);
			
			if (sender.getServer().getPlayer(targetPlayer) != null) {
	            Player tp = Bukkit.getServer().getPlayer(targetPlayer);
	            
	            config.set(tp.getUniqueId().toString() + ".Group", rank.toLowerCase());
	            config.set(tp.getUniqueId().toString() + ".Username", tp.getName());
	            ConfigManager.savePlayers();
	            
	            if (Main.main.getSGGameManager().getGameByPlayer(tp.getName()) == null
	            	&& Main.main.getBGGameManager().getGameByPlayer(tp.getName()) == null
	            	&& Main.main.getSGMakerGameManager().getGameByPlayer(tp.getName()) == null) {
		            setDisplayName(tp, true);
	            } else {
	            	setDisplayName(tp, false);
	            }
	            
	            sender.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bYou have set &e" + tp.getName() + "&b's rank to&8: &e" + capRank + "&8."));
	            
	        } else {

	        	String tpuuid = uuid.getString(targetPlayer);
	        	String tpname = config.getString(tpuuid + ".Username");
	        	
	        	if (config.contains(tpuuid)) {
	        		
	        		config.set(tpuuid.toString() + ".Group", rank.toLowerCase());
	                config.set(tpuuid.toString() + ".Username", tpname);
		            ConfigManager.savePlayers();
	                
	                sender.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bYou have set &e" + tpname + "&b's rank to&8: &e" + capRank + "&8."));
	    			
	        	} else sender.sendMessage(MessageManager.c(MessageManager.enginePrefix + " " + MessageManager.playerNotFound));
	        	
	        }
				
		} else {
			sender.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cThat is not a valid rank&8. &cCurrent ranks&8:"));
			sender.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bStaff ranks&8: &eModerator&8, &eJuniorDeveloper&8, &eUHCHost&8."));
			sender.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bSr. Staff ranks&8: &eSeniorModerator&8, &eAdmin&8, &eDeveloper&8, &eOwner&8."));
			sender.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bDonator ranks&8: &eChampion&8, &eLegend&8, &eGuardian&8, &eElite&8, &eRoyal&8."));
			sender.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bSpecial ranks&8: &eTrialBuilder&8, &eBuilder&8, &eLeadDeveloper&8."));
			sender.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bSuperior ranks&8: &eRetired&8, &eMapMaker&8, &eVIP&8."));
			return;
		}
		
	}
	
	public static String getDeveloperName(String player) {
		String playerName = "";
		String[] chars = player.split("");
        String[] colorCodes = { "�4�l", "�c�l", "�6�l", "�e�l", "�a�l", "�2�l", "�b�l", "�3�l", "�9�l", "�1�l", "�5�l", "�d�l", "�4�l", "�c�l", "�6�l", "�e�l" };
        for (int i = 0; i < player.length(); i++) {
            playerName = playerName + colorCodes[i] + chars[i];
        }
        return playerName;
	}
	
	public static void setDisplayName(Player player, boolean colored) {
		
		FileConfiguration config = ConfigManager.playersCFG;
		
		if (colored == true) {
			// MEMBER RANK
		    if (!config.contains(player.getUniqueId().toString())) {
				config.set(player.getUniqueId().toString() + ".Group", "member");
				config.set(player.getUniqueId().toString() + ".Username", player.getName());
				config.set(player.getUniqueId().toString() + ".Points", 500);
				player.setDisplayName("�2" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName("�2" + ChatColor.stripColor(player.getPlayerListName()));
			}

	        if (config.getString(player.getUniqueId().toString() + ".Group").equals("member")) {
				player.setDisplayName("�2" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName("�2" + ChatColor.stripColor(player.getPlayerListName()));
			}
						
			// CHAMPION RANK
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("champion")) {
				player.setDisplayName("�7" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName("�7" + ChatColor.stripColor(player.getPlayerListName()));
			}
									
		    // LEGEND RANK
		    if (config.getString(player.getUniqueId().toString() + ".Group").equals("legend")) {
				player.setDisplayName("�6" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName("�6" + ChatColor.stripColor(player.getPlayerListName()));
			}
									
			// GUARDIAN RANK
		    if (config.getString(player.getUniqueId().toString() + ".Group").equals("guardian")) {
				player.setDisplayName("�3" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName("�3" + ChatColor.stripColor(player.getPlayerListName()));
			}
											
			// ELITE RANK
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("elite")) {
				player.setDisplayName("�b" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName("�b" + ChatColor.stripColor(player.getPlayerListName()));
			}
						
			// RETIRED RANK
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("royal")) {
			    player.setDisplayName("�a" + ChatColor.stripColor(player.getPlayerListName()));
			    player.setPlayerListName("�a" + ChatColor.stripColor(player.getPlayerListName()));
			}
						
			// MAPMAKER RANK
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("mapmaker")) {
				player.setDisplayName("�d" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName("�d" + ChatColor.stripColor(player.getPlayerListName()));
			}
								
		    // VIP RANK
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("vip")) {
				player.setDisplayName("�5" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName("�5" + ChatColor.stripColor(player.getPlayerListName()));
			}
						
			// RETIRED RANK
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("retired")) {
				player.setDisplayName("�5" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName("�5" + ChatColor.stripColor(player.getPlayerListName()));
			}
									
			// TRIAL BUILDER RANK
		    if (config.getString(player.getUniqueId().toString() + ".Group").equals("trialbuilder")) {
				player.setDisplayName("�9" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName("�9" + ChatColor.stripColor(player.getPlayerListName()));
			}
						
			// BUILDER RANK
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("builder")) {
				player.setDisplayName("�9" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName("�9" + ChatColor.stripColor(player.getPlayerListName()));
			}
									
			// UHCHOST RANK
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("uhchost")) {
				player.setDisplayName("�c" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName("�c" + ChatColor.stripColor(player.getPlayerListName()));
			}
								
			// MODERATOR RANK
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("moderator")) {
				player.setDisplayName("�c" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName("�c" + ChatColor.stripColor(player.getPlayerListName()));
			}
									
			// SRMODERATOR
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("seniormoderator")) {
				player.setDisplayName("�4" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName("�4" + ChatColor.stripColor(player.getPlayerListName()));
		    }
									
			// ADMIN
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("admin")) {
				player.setDisplayName("�4�l" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName("�4�l" + ChatColor.stripColor(player.getPlayerListName()));
			}
									
			// JRDEVELOPER
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("juniordeveloper")) {
				player.setDisplayName(getDeveloperName(ChatColor.stripColor(player.getPlayerListName())));
				player.setPlayerListName(getDeveloperName(ChatColor.stripColor(player.getPlayerListName())));
			}
									
			// DEVELOPER
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("developer")) {
				player.setDisplayName(getDeveloperName(ChatColor.stripColor(player.getPlayerListName())));
				player.setPlayerListName(getDeveloperName(ChatColor.stripColor(player.getPlayerListName())));
			}
									
			// LEADDEVELOPER
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("leaddeveloper")) {
				player.setDisplayName(getDeveloperName(ChatColor.stripColor(player.getPlayerListName())));
				player.setPlayerListName(getDeveloperName(ChatColor.stripColor(player.getPlayerListName())));
			}
			
			// LNCS OWNER
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("csowner")) {
				player.setDisplayName("�4�l" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName("�4�l" + ChatColor.stripColor(player.getPlayerListName()));
			}
			
			// OWNER
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("owner")) {
				player.setDisplayName("�4�l�o" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName("�4�l�o" + ChatColor.stripColor(player.getPlayerListName()));
			}
		} else {
			// MEMBER RANK
		    if (!config.contains(player.getUniqueId().toString())) {
				config.set(player.getUniqueId().toString() + ".Group", "member");
				config.set(player.getUniqueId().toString() + ".Username", player.getName());
				config.set(player.getUniqueId().toString() + ".Points", 500);
				player.setDisplayName("�2" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}

	        if (config.getString(player.getUniqueId().toString() + ".Group").equals("member")) {
				player.setDisplayName("�2" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}
						
			// CHAMPION RANK
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("champion")) {
				player.setDisplayName("�7" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}
									
		    // LEGEND RANK
		    if (config.getString(player.getUniqueId().toString() + ".Group").equals("legend")) {
				player.setDisplayName("�6" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}
									
			// GUARDIAN RANK
		    if (config.getString(player.getUniqueId().toString() + ".Group").equals("guardian")) {
				player.setDisplayName("�3" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}
											
			// ELITE RANK
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("elite")) {
				player.setDisplayName("�b" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}
						
			// RETIRED RANK
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("royal")) {
			    player.setDisplayName("�a" + ChatColor.stripColor(player.getPlayerListName()));
			    player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}
						
			// MAPMAKER RANK
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("mapmaker")) {
				player.setDisplayName("�d" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}
								
		    // VIP RANK
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("vip")) {
				player.setDisplayName("�5" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}
						
			// RETIRED RANK
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("retired")) {
				player.setDisplayName("�5" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}
									
			// TRIAL BUILDER RANK
		    if (config.getString(player.getUniqueId().toString() + ".Group").equals("trialbuilder")) {
				player.setDisplayName("�9" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}
						
			// BUILDER RANK
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("builder")) {
				player.setDisplayName("�9" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}
									
			// UHCHOST RANK
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("uhchost")) {
				player.setDisplayName("�c" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}
								
			// MODERATOR RANK
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("moderator")) {
				player.setDisplayName("�c" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}
									
			// SRMODERATOR
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("seniormoderator")) {
				player.setDisplayName("�4" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
		    }
									
			// ADMIN
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("admin")) {
				player.setDisplayName("�4�l" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}
									
			// JRDEVELOPER
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("juniordeveloper")) {
				player.setDisplayName(getDeveloperName(ChatColor.stripColor(player.getPlayerListName())));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}
									
			// DEVELOPER
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("developer")) {
				player.setDisplayName(getDeveloperName(ChatColor.stripColor(player.getPlayerListName())));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}
									
			// LEADDEVELOPER
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("leaddeveloper")) {
				player.setDisplayName(getDeveloperName(ChatColor.stripColor(player.getPlayerListName())));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}
			
			// LNCS OWNER
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("csowner")) {
				player.setDisplayName("�4�l" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}
			
			// OWNER
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("owner")) {
				player.setDisplayName("�4�l�o" + ChatColor.stripColor(player.getPlayerListName()));
				player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
			}
		}
		
		config.set(player.getUniqueId().toString() + ".Username", player.getName());
		ConfigManager.savePlayers();

	}

}