package luminous.net.main.fiddycal.Managers;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class StaffUtil {

    //Checks if staff
    public static boolean isStaff(Player player){
        FileConfiguration config = ConfigManager.playersCFG;
        if (config.getString(player.getUniqueId().toString() + ".Group").equals("moderator")
                || config.getString(player.getUniqueId().toString() + ".Group").equals("uhchost")
                || config.getString(player.getUniqueId().toString() + ".Group").equals("seniormoderator")
                || config.getString(player.getUniqueId().toString() + ".Group").equals("juniordeveloper")
                || config.getString(player.getUniqueId().toString() + ".Group").equals("developer")
                || config.getString(player.getUniqueId().toString() + ".Group").equals("admin")
                || config.getString(player.getUniqueId().toString() + ".Group").equals("leaddeveloper")
                || config.getString(player.getUniqueId().toString() + ".Group").equals("owner")) {
            return true;
        }else{
            return false;
        }
    }

    public static boolean isPremium(Player player){
        FileConfiguration config = ConfigManager.playersCFG;
        if (config.getString(player.getUniqueId().toString() + ".Group").equals("champion")
                || config.getString(player.getUniqueId().toString() + ".Group").equals("legend")
                || config.getString(player.getUniqueId().toString() + ".Group").equals("guardian")
                || config.getString(player.getUniqueId().toString() + ".Group").equals("elite")
                || config.getString(player.getUniqueId().toString() + ".Group").equals("royal")) {
            return true;
        }else{
            return false;
        }
    }

    public static void banPlayer(Player player, String reason, Integer duration, String durationName){

    }

    public static void kickPlayer(Player player, String reason){

    }

    public static void mutePlayer(Player player, String reasion, Integer duration){

    }

    public static void unmutePlayer(Player player){

    }

    public static void unbanPlayer(Player player){

    }

}