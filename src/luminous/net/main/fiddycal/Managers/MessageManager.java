package luminous.net.main.fiddycal.Managers;

import java.util.Random;
import org.bukkit.ChatColor;

import luminous.net.main.fiddycal.Main;

public class MessageManager {

	public static String serverPrefix = c(Main.main.getConfig().getString("Prefix.Network"));
	public static String prefix = c(Main.main.getConfig().getString("Prefix.SurvivalGames"));
	public static String BGprefix = "&8(&6BG&8)";
	public static String enginePrefix = c(Main.main.getConfig().getString("Prefix.Engine"));
	public static String invalidPermission = c("&cYou do not have access to that&8.");
	public static String unknownCommand = c("Unknown command. Type \"/help\" for help.");
	public static String playerNotFound = c("&cThat player could not be found&8!");
	public static String playerNotOnline = c("&cThat player is not online&8!");
	
	public static int getRandomInt(int low, int high) {
		Random r = new Random(); return r.nextInt((high - low) + 1) + low;
	}
	
	public static boolean isInt(String s) {
	    try {
	        Integer.parseInt(s);
	    } catch (NumberFormatException e) {
	        return false;
	    }
	    return true;
	}
	
	public static String c(String txt) {
        return ChatColor.translateAlternateColorCodes('&', txt);
    }

	public static String getDisplayRank(String rank){
		if (rank.equalsIgnoreCase("Member")) {
			return "Member";
		}
		if (rank.equalsIgnoreCase("Moderator")) {
			return "Moderator";
		}
		if (rank.equalsIgnoreCase("SeniorModerator")) {
			return "SnrModerator";
		}
		if (rank.equalsIgnoreCase("Admin")) {
			return "Admin";
		}
		if (rank.equalsIgnoreCase("Developer")) {
			return "Developer";
		}
		if (rank.equalsIgnoreCase("Moderator")) {
			return "Moderator";
		}
		if (rank.equalsIgnoreCase("Owner")) {
			return "Owner";
		}
		return "null";
	}

	
}
