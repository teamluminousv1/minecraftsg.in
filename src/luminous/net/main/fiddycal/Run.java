package luminous.net.main.fiddycal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Run {
	
	public static <K,V extends Comparable<? super V>> List<Entry<K, V>> sortByValues(Map<K,V> map) {
		List<Entry<K,V>> e = new ArrayList<Entry<K,V>>(map.entrySet());

	    Collections.sort(e, 
	        new Comparator<Entry<K,V>>() {
	            @Override
	            public int compare(Entry<K,V> e1, Entry<K,V> e2) {
	                return e2.getValue().compareTo(e1.getValue());
	            }
	        }
	    );

	    return e;
    }
	
	public static void main(String[] args) {
		LinkedHashMap<String, Integer> sort = new LinkedHashMap<String, Integer>();
		LinkedHashMap<String, Integer> wins = new LinkedHashMap<String, Integer>();
		sort.put("Bob", 6);
		sort.put("Jef", 10);
		sort.put("Ted", 5);
		sort.put("Cas", 2);
		sort.put("Zac", 76);
		sort.put("Sid", 76);
		sort.put("Carl", 76);
		for (Map.Entry<String, Integer> entry : sortByValues(sort)) {
			wins.put(entry.getKey(), entry.getValue());
		}
		System.out.println(wins);
		for (int i = 0; i < wins.size(); i++) {
			String key = (new ArrayList<String>(wins.keySet())).get(i);
			if (key.equals("Sid")) {
				int r = i + 1;
				System.out.println("Name: " + key);
				System.out.println("#" + r);
				return;
			}
        }
		return;
	}
	
}
