package luminous.net.main.fiddycal.Commands.Staff;

import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.main.fiddycal.Managers.StaffUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Mute implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("mute")){
            if (!(commandSender instanceof Player)) {
                commandSender.sendMessage(MessageManager.c(MessageManager.prefix + " &cYou can only execute this command as a player!"));
                return false;
            }else{
                Player player = (Player) commandSender;
                if (StaffUtil.isStaff(player)){
                    if (strings.length > 2){

                    }else{
                        player.sendMessage(MessageManager.c(MessageManager.prefix + " &cUsage: /mute <player> <reason>"));
                    }
                }else{
                    commandSender.sendMessage(MessageManager.c(MessageManager.prefix + " &cYou do not have permission to execute this command!"));
                }
            }
        }
        return false;
    }
}
