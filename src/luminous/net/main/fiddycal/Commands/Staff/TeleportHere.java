package luminous.net.main.fiddycal.Commands.Staff;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;

public class TeleportHere implements CommandExecutor {
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		Player player = (Player) sender;
		FileConfiguration config = ConfigManager.playersCFG;
		
		if (!(sender instanceof Player)) {
			sender.sendMessage("Only players are able to do this command.");
			return true;
		}
		
		if ((config.getString(player.getUniqueId().toString() + ".Group").equals("moderator"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("uhchost"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("seniormoderator"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("juniordeveloper"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("admin"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("developer"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("leaddeveloper"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("owner"))) {
			if (args.length == 1) {
				
				if (args[0].equalsIgnoreCase("all")) {
					for (Player p : Bukkit.getServer().getOnlinePlayers()) {
						if (!p.getName().equals(player.getName())) {
							p.teleport(player);
							player.sendMessage(MessageManager.c(MessageManager.enginePrefix + 
						    " &r" + p.getDisplayName() + " &bhas been teleported to you&8."));
						}
					}
					return true;
				}
				
				if (player.getServer().getPlayer(args[0]) != null) {
		            Player targetPlayer = Bukkit.getServer().getPlayer(args[0]);
		            
		            targetPlayer.teleport(player);
		            player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &r" + targetPlayer.getDisplayName() + " &bhas been teleported to you&8."));
			        targetPlayer.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bYou have been teleported to &r" + player.getDisplayName() + "&8."));
			        return true;
			        
				} else player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " " + MessageManager.playerNotOnline));
				return true;
			} else player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cUsage: /tphere <player|all>"));
			
		} else player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " " + MessageManager.invalidPermission));
		return true;
	}
}
