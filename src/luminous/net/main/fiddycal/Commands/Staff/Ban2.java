package luminous.net.main.fiddycal.Commands.Staff;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import luminous.net.main.fiddycal.Main;

public class Ban2 implements Listener, CommandExecutor {
	
	public static ArrayList<String> evidenceChat = new ArrayList<String>();

	public static HashMap<String, String> banReason = new HashMap<String, String>();
	public static HashMap<String, String> targetPlayerName = new HashMap<String, String>();
	
	private void banPlayer(Player player, String targetPlayer, String type, String evidence, String otherReason) {
		
		if (type.toLowerCase().equals("hacking")) {
			
			player.setOp(true);
			if (evidence == null) {
				player.performCommand("tban " + targetPlayerName.get(player.getName()) + " 1 month Hacking: Unfair Advantage\n&8[&4FleX&8] &7Evidence&8: &cNone\n&8[&4FleX&8] &7Offence&8: &cFirst");
			} else {
				player.performCommand("tban " + targetPlayerName.get(player.getName()) + " 1 month Hacking: Unfair Advantage\n&8[&4FleX&8] &7Evidence&8: &c" + evidence + "\n&8[&4FleX&8] &7Offence&8: &cFirst");
			}
			player.setOp(false);
			
		} else if (type.toLowerCase().equals("teaming")) {
			
			player.setOp(true);
			if (evidence == null) {
				player.performCommand("tban " + targetPlayerName.get(player.getName()) + " 30 minutes Teaming in FFA\n&8[&4FleX&8] &7Evidence&8: &cNone\n&8[&4FleX&8] &7Offence&8: &cFirst");
			} else {
				player.performCommand("tban " + targetPlayerName.get(player.getName()) + " 30 minutes Teaming in FFA\n&8[&4FleX&8] &7Evidence&8: &c" + evidence + "\n&8[&4FleX&8] &7Offence&8: &cFirst");
			}
			player.setOp(false);
			
		} else if (type.toLowerCase().equals("mods")) {
			
			player.setOp(true);
			if (evidence == null) {
				player.performCommand("tban " + targetPlayerName.get(player.getName()) + " 1 month Disallowed/Illegal Modifications\n&8[&4FleX&8] &7Evidence&8: &cNone\n&8[&4FleX&8] &7Offence&8: &cFirst");
			} else {
				player.performCommand("tban " + targetPlayerName.get(player.getName()) + " 1 month Disallowed/Illegal Modifications\n&8[&4FleX&8] &7Evidence&8: &c" + evidence + "\n&8[&4FleX&8] &7Offence&8: &cFirst");
			}
			player.setOp(false);
			
		} else if (type.toLowerCase().equals("glitch")) {
			
			player.setOp(true);
			if (evidence == null) {
				player.performCommand("tban " + targetPlayerName.get(player.getName()) + " 1 week Abusing a Bug/Glitch\n&8[&4FleX&8] &7Evidence&8: &cNone\n&8[&4FleX&8] &7Offence&8: &cFirst");
			} else {
				player.performCommand("tban " + targetPlayerName.get(player.getName()) + " 1 week Abusing a Bug/Glitch\n&8[&4FleX&8] &7Evidence&8: &c" + evidence + "\n&8[&4FleX&8] &7Offence&8: &cFirst");
			}
			player.setOp(false);
			
		} else if (type.toLowerCase().equals("threat")) {
			
			player.setOp(true);
			if (evidence == null) {
				player.performCommand("tban " + targetPlayerName.get(player.getName()) + " 1 week Malicious Threats\n&8[&4FleX&8] &7Evidence&8: &cNone\n&8[&4FleX&8] &7Offence&8: &cFirst");
			} else {
				player.performCommand("tban " + targetPlayerName.get(player.getName()) + " 1 week Malicious Threats\n&8[&4FleX&8] &7Evidence&8: &c" + evidence + "\n&8[&4FleX&8] &7Offence&8: &cFirst");
			}
			player.setOp(false);
			
		} else if (type.toLowerCase().equals("links")) {
			
			player.setOp(true);
			if (evidence == null) {
				player.performCommand("tban " + targetPlayerName.get(player.getName()) + " 1 week Harmful/Unwanted Links\n&8[&4FleX&8] &7Evidence&8: &cNone\n&8[&4FleX&8] &7Offence&8: &cFirst");
			} else {
				player.performCommand("tban " + targetPlayerName.get(player.getName()) + " 1 week Harmful/Unwanted Links\n&8[&4FleX&8] &7Evidence&8: &c" + evidence + "\n&8[&4FleX&8] &7Offence&8: &cFirst");
			}
			player.setOp(false);
			
		} else if (type.toLowerCase().equals("target")) {
			
			player.setOp(true);
			if (evidence == null) {
				player.performCommand("tban " + targetPlayerName.get(player.getName()) + " 30 minutes Constent Targeting/Stalking\n&8[&4FleX&8] &7Evidence&8: &cNone\n&8[&4FleX&8] &7Offence&8: &cFirst");
			} else {
				player.performCommand("tban " + targetPlayerName.get(player.getName()) + " 30 minutes Constent Targeting/Stalking\n&8[&4FleX&8] &7Evidence&8: &c" + evidence + "\n&8[&4FleX&8] &7Offence&8: &cFirst");
			}
			player.setOp(false);
			
		} else if (type.toLowerCase().equals("other")) {
			
			player.setOp(true);
			if (evidence == null) {
				player.performCommand("tban " + targetPlayerName.get(player.getName()) + " 1 week " + otherReason.replace("\n", "").replace("&", "/") + "\n&8[&4FleX&8] &7Evidence&8: &cNone\n&8[&4FleX&8] &7Offence&8: &cFirst");
			} else {
				player.performCommand("tban " + targetPlayerName.get(player.getName()) + " 1 week " + otherReason.replace("\n", "").replace("&", "/") + "\n&8[&4FleX&8] &7Evidence&8: &c" + evidence + "\n&8[&4FleX&8] &7Offence&8: &cFirst");
			}
			player.setOp(false);
			
		} else {
			
			player.setOp(false);
			player.sendMessage("�8[�4FleX�8] �cThere was a problem with your request�8.");
			player.sendMessage("�8[�4FleX�8] �cPlease contact a developer if you see this message�8.");
			
		}
		
		if (evidenceChat.contains(player.getName())) {
		    evidenceChat.remove(player.getName());
		}
		if (targetPlayerName.containsKey(player.getName())) {
			targetPlayerName.remove(player.getName());
		}
		if (banReason.containsKey(player.getName())) {
			banReason.remove(player.getName());
		}
		
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player player = (Player) sender;
		FileConfiguration config = Main.main.getConfig();
		
		if ((config.getString(player.getUniqueId().toString() + ".Group").equals("uhchost"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("moderator"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("seniormoderator"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("juniordeveloper"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("developer"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("admin"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("leaddeveloper"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("owner"))) {
			
			if (args.length == 1) {
				
				if (evidenceChat.contains(player.getName())) {
					player.sendMessage("�8[�4FleX�8] �cYou have a pending punishment�8! �cType \"�eCancel�c\" to cancel the punishment�8.");
					return true;
				}
				
				Inventory ban = Bukkit.createInventory(null, 27, "�bBan�8: �4" + args[0]);

				ItemStack hacking = new ItemStack(Material.IRON_SWORD, 1);
				ItemMeta hackingMeta = hacking.getItemMeta();
				List<String> hackingLore = new ArrayList<String>();
				hackingMeta.setDisplayName("�e" + args[0] + " �bis hacking�8.");
				hackingLore.add("�7This player is cheating and has ");
				hackingLore.add("�7an unfair advantage over others�8.");
				hackingLore.add("");
				hackingLore.add("�bEvidence Required�8: �eYoutube Link");
				hackingMeta.setLore(hackingLore);
				hacking.setItemMeta(hackingMeta);
				
				ItemStack teaming = new ItemStack(Material.NAME_TAG, 1);
				ItemMeta teamingMeta = teaming.getItemMeta();
				List<String> teamingLore = new ArrayList<String>();
				teamingMeta.setDisplayName("�e" + args[0] + " �bis teaming�8.");
				teamingLore.add("�7This player is excessively teaming");
				teamingLore.add("�7on others in a Free For All gamemode�8.");
				teamingLore.add("");
				teamingLore.add("�bEvidence Required�8: �eYoutube Link");
				teamingMeta.setLore(teamingLore);
				teaming.setItemMeta(teamingMeta);
				
				ItemStack mods = new ItemStack(Material.REDSTONE, 1);
				ItemMeta modsMeta = mods.getItemMeta();
				List<String> modsLore = new ArrayList<String>();
				modsMeta.setDisplayName("�e" + args[0] + " �bis using illegal modifications�8.");
				modsLore.add("�7This player is using modifications");
				modsLore.add("�7and has an unfair advantage over others�8.");
				modsLore.add("");
				modsLore.add("�bEvidence Required�8: �eYoutube Link�7, �eImgur Link");
				modsMeta.setLore(modsLore);
				mods.setItemMeta(modsMeta);
				
				ItemStack glitched = new ItemStack(Material.TRIPWIRE_HOOK, 1);
				ItemMeta glitchedMeta = glitched.getItemMeta();
				List<String> glitchedLore = new ArrayList<String>();
				glitchedMeta.setDisplayName("�e" + args[0] + " �bis abusing a glitch�8.");
				glitchedLore.add("�7This player is constantly using, or better yet");
				glitchedLore.add("�7abusing a glitch/bug that should not be preset�8.");
				glitchedLore.add("");
				glitchedLore.add("�bEvidence Required�8: �eYoutube Link");
				glitchedMeta.setLore(glitchedLore);
				glitched.setItemMeta(glitchedMeta);
				
				ItemStack ddosThreats = new ItemStack(Material.BEDROCK, 1);
				ItemMeta ddosThreatsMeta = ddosThreats.getItemMeta();
				List<String> ddosThreatsLore = new ArrayList<String>();
				ddosThreatsMeta.setDisplayName("�e" + args[0] + " �bis threatening others�8.");
				ddosThreatsLore.add("�7This player is sending malicious");
				ddosThreatsLore.add("�7threats to other players�8. �8(�7DoX�8, �7DDoS�8, �7etc.�8)");
				ddosThreatsLore.add("");
				ddosThreatsLore.add("�bEvidence Required�8: �eImgur Link");
				ddosThreatsMeta.setLore(ddosThreatsLore);
				ddosThreats.setItemMeta(ddosThreatsMeta);
				
				ItemStack links = new ItemStack(Material.LEASH, 1);
				ItemMeta linksMeta = links.getItemMeta();
				List<String> linksLore = new ArrayList<String>();
				linksMeta.setDisplayName("�e" + args[0] + " �bis sending malicious links�8.");
				linksLore.add("�7This player is sending links that");
				linksLore.add("�7could be of harm to other players�8.");
				linksLore.add("");
				linksLore.add("�bEvidence Required�8: �eImgur Link");
				linksMeta.setLore(linksLore);
				links.setItemMeta(linksMeta);
				
				ItemStack stalking = new ItemStack(Material.WORKBENCH, 1);
				ItemMeta stalkingMeta = stalking.getItemMeta();
				List<String> stalkingLore = new ArrayList<String>();
				stalkingMeta.setDisplayName("�e" + args[0] + " �bis targeting others�8.");
				stalkingLore.add("�7This player is excessively");
				stalkingLore.add("�7targeting/stalking other players�8.");
				stalkingLore.add("");
				stalkingLore.add("�bEvidence Required�8: �e�eYoutube Link");
				stalkingMeta.setLore(stalkingLore);
				stalking.setItemMeta(stalkingMeta);
				
				ItemStack other = new ItemStack(Material.BOAT, 1);
				ItemMeta otherMeta = other.getItemMeta();
				List<String> otherLore = new ArrayList<String>();
				otherMeta.setDisplayName("�e" + args[0] + " �bis breaking another rule�8.");
				otherLore.add("�7If we do not have something here that we");
				otherLore.add("�7need, please notify a Sr. Staff member ASAP�8.");
				otherLore.add("");
				otherLore.add("�bEvidence Required�8: �eYoutube Link�7, �eImgur Link");
				otherMeta.setLore(otherLore);
				other.setItemMeta(otherMeta);
				
				ItemStack history = new ItemStack(Material.BOOK, 1);
				ItemMeta historyMeta = history.getItemMeta();
				historyMeta.setDisplayName("�e" + args[0] + "�b's punishment history�8.");
				history.setItemMeta(historyMeta);
				
				if (targetPlayerName.containsValue(args[0])) {
	            	player.sendMessage("�8[�4FleX�8] �cThat player is currently being punished�8.");
	            	return true;
				}
				
				if (targetPlayerName.containsKey(player.getName())) {
	            	targetPlayerName.remove(player.getName());
	            }
				
	            targetPlayerName.put(player.getName(), args[0]);

	            ban.setItem(0, history);
	            ban.setItem(3, hacking);
	            ban.setItem(4, teaming);
	            ban.setItem(5, mods);
	            ban.setItem(12, glitched);
	            ban.setItem(13, ddosThreats);
	            ban.setItem(14, links);
	            ban.setItem(21, stalking);
	            ban.setItem(22, other);
	            player.openInventory(ban);
				return true;
				
			} else {
				
				player.sendMessage("�8[�4FleX�8] �cUsage: /ban <player>");
				return true;
				
			}
				
		} else player.sendMessage("�8[�4FleX�8] �cYou do not have access to that�8.");
		return true;
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		
		Player player = (Player) event.getWhoClicked();
	    ItemStack clicked = event.getCurrentItem();
	    Inventory inv = event.getInventory();
	    
	    if (clicked == null) {
		    return;
	    }
	    
	    if (inv.getName().contains("�bBan�8: �4")) {
	    	
	    	if (clicked.getType() == Material.IRON_SWORD) {
	    		
	    		banReason.put(player.getName(), "hacking");
	    		if (evidenceChat.contains(player.getName())) {
	    		    evidenceChat.remove(player.getName());
	    		}
	    		evidenceChat.add(player.getName());
	    		event.setCancelled(true);
	    		player.closeInventory();
	    		player.sendMessage("�8[�4FleX�8] �bPlease enter the evidence to your punishment�8.");
				player.sendMessage("�8[�4FleX�8] �bIf you have been given permission by a Senior Staff member, and no evidence for the punishment is needed, simply type \"�eNone�b\" in the chat�8.");
				player.sendMessage("�8[�4FleX�8] �bIf you can't provide any evidence simply type \"�eCancel�b\"�8.");
				return;
				
	    	} else if (clicked.getType() == Material.NAME_TAG) {
	    		
	    		banReason.put(player.getName(), "teaming");
	    		if (evidenceChat.contains(player.getName())) {
	    		    evidenceChat.remove(player.getName());
	    		}
	    		evidenceChat.add(player.getName());
	    		event.setCancelled(true);
	    		player.closeInventory();
	    		player.sendMessage("�8[�4FleX�8] �bPlease enter the evidence to your punishment�8.");
				player.sendMessage("�8[�4FleX�8] �bIf you have been given permission by a Senior Staff member, and no evidence for the punishment is needed, simply type \"�eNone�b\" in the chat�8.");
				player.sendMessage("�8[�4FleX�8] �bIf you can't provide any evidence simply type \"�eCancel�b\"�8.");
				return;
	    	
	    	} else if (clicked.getType() == Material.REDSTONE) {
	    		
	    		banReason.put(player.getName(), "mods");
	    		if (evidenceChat.contains(player.getName())) {
	    		    evidenceChat.remove(player.getName());
	    		}
	    		evidenceChat.add(player.getName());
	    		event.setCancelled(true);
	    		player.closeInventory();
	    		player.sendMessage("�8[�4FleX�8] �bPlease enter the evidence to your punishment�8.");
				player.sendMessage("�8[�4FleX�8] �bIf you have been given permission by a Senior Staff member, and no evidence for the punishment is needed, simply type \"�eNone�b\" in the chat�8.");
				player.sendMessage("�8[�4FleX�8] �bIf you can't provide any evidence simply type \"�eCancel�b\"�8.");
				return;
				
	    	} else if (clicked.getType() == Material.TRIPWIRE_HOOK) {
	    		
	    		banReason.put(player.getName(), "glitch");
	    		if (evidenceChat.contains(player.getName())) {
	    		    evidenceChat.remove(player.getName());
	    		}
	    		evidenceChat.add(player.getName());
	    		event.setCancelled(true);
	    		player.closeInventory();
	    		player.sendMessage("�8[�4FleX�8] �bPlease enter the evidence to your punishment�8.");
				player.sendMessage("�8[�4FleX�8] �bIf you have been given permission by a Senior Staff member, and no evidence for the punishment is needed, simply type \"�eNone�b\" in the chat�8.");
				player.sendMessage("�8[�4FleX�8] �bIf you can't provide any evidence simply type \"�eCancel�b\"�8.");
				return;
				
	    	} else if (clicked.getType() == Material.BEDROCK) {
	    		
	    		banReason.put(player.getName(), "threat");
	    		if (evidenceChat.contains(player.getName())) {
	    		    evidenceChat.remove(player.getName());
	    		}
	    		evidenceChat.add(player.getName());
	    		event.setCancelled(true);
	    		player.closeInventory();
	    		player.sendMessage("�8[�4FleX�8] �bPlease enter the evidence to your punishment�8.");
				player.sendMessage("�8[�4FleX�8] �bIf you have been given permission by a Senior Staff member, and no evidence for the punishment is needed, simply type \"�eNone�b\" in the chat�8.");
				player.sendMessage("�8[�4FleX�8] �bIf you can't provide any evidence simply type \"�eCancel�b\"�8.");
				return;
				
	    	} else if (clicked.getType() == Material.LEASH) {
			    		
			    banReason.put(player.getName(), "links");
			    if (evidenceChat.contains(player.getName())) {
			    	evidenceChat.remove(player.getName());
			    }
			    evidenceChat.add(player.getName());
			    event.setCancelled(true);
			    player.closeInventory();
			    player.sendMessage("�8[�4FleX�8] �bPlease enter the evidence to your punishment�8.");
				player.sendMessage("�8[�4FleX�8] �bIf you have been given permission by a Senior Staff member, and no evidence for the punishment is needed, simply type \"�eNone�b\" in the chat�8.");
				player.sendMessage("�8[�4FleX�8] �bIf you can't provide any evidence simply type \"�eCancel�b\"�8.");
				return;
				
	    	} else if (clicked.getType() == Material.WORKBENCH) {
	    		
	    		banReason.put(player.getName(), "target");
	    	    if (evidenceChat.contains(player.getName())) {
	    	    	evidenceChat.remove(player.getName());
	    	    }
	    	    evidenceChat.add(player.getName());
	    	    event.setCancelled(true);
	    	    player.closeInventory();
	    	    player.sendMessage("�8[�4FleX�8] �bPlease enter the evidence to your punishment�8.");
	    		player.sendMessage("�8[�4FleX�8] �bIf you have been given permission by a Senior Staff member, and no evidence for the punishment is needed, simply type \"�eNone�b\" in the chat�8.");
	    		player.sendMessage("�8[�4FleX�8] �bIf you can't provide any evidence simply type \"�eCancel�b\"�8.");
	    		return;
	    		
	    	} else if (clicked.getType() == Material.BOAT) {
	    		
	    		banReason.put(player.getName(), "other");
	    	    if (evidenceChat.contains(player.getName())) {
	    	    	evidenceChat.remove(player.getName());
	    	    }
	    	    evidenceChat.add(player.getName());
	    	    event.setCancelled(true);
	    	    player.closeInventory();
	    	    
	    	    //TODO // add more options for "other"
	    	    player.sendMessage("�8[�4FleX�8] �bPlease enter the evidence to your punishment�8.");
	    		player.sendMessage("�8[�4FleX�8] �bIf you have been given permission by a Senior Staff member, and no evidence for the punishment is needed, simply type \"�eNone�b\" in the chat�8.");
	    		player.sendMessage("�8[�4FleX�8] �bIf you can't provide any evidence simply type \"�eCancel�b\"�8.");
	    		return;
	    		
	    	} else if (clicked.getType() == Material.BOOK) {
	    		
	    		if (evidenceChat.contains(player.getName())) {
	    		    evidenceChat.remove(player.getName());
	    		}
	    		event.setCancelled(true);
	    		
	    		String name = targetPlayerName.get(player.getName());
	    		player.performCommand("history " + name);

	    		targetPlayerName.remove(player.getName());
	    		player.closeInventory();
	    		return;
	    		
	    	} else {
	    	
	    		event.setCancelled(true);
	    		player.closeInventory();
	    		return;
	    		
	    	}
    		
	    }
	    return;
	    
	}
	
	@EventHandler
	public void onChatBan(AsyncPlayerChatEvent event) {
		Player player = (Player) event.getPlayer();
		
		if (evidenceChat.contains(player.getName())) {
			
			if (event.getMessage().equalsIgnoreCase("none")) {

				if (banReason.get(player.getName()).equals("other")) {
					banPlayer(player, targetPlayerName.get(player.getName()), banReason.get(player.getName()), null, "Other (Reported to Sr. Staff)");
				} else {
					banPlayer(player, targetPlayerName.get(player.getName()), banReason.get(player.getName()), null, null);
				}
				event.setCancelled(true);
				
			} else if (event.getMessage().equalsIgnoreCase("cancel")) {

				player.sendMessage("�8[�4FleX�8] �bYou have cancelled your pending punishment�8.");
				evidenceChat.remove(player.getName());
			    targetPlayerName.remove(player.getName());
				event.setCancelled(true);
			    return;
				
			} else if (event.getMessage().contains("imgur") 
					|| event.getMessage().contains("youtu")) {
				
				if (banReason.get(player.getName()).equals("other")) {
					banPlayer(player, targetPlayerName.get(player.getName()), "hacking", ChatColor.stripColor(event.getMessage()), "Other (Reported to Sr. Staff)");
				} else {
					banPlayer(player, targetPlayerName.get(player.getName()), "hacking", ChatColor.stripColor(event.getMessage()), null);
				}
                event.setCancelled(true);
				
			} else {
				
				player.sendMessage("�8[�4FleX�8] �cThat is not a valid enough link for evidence�8.");
				player.sendMessage("�8[�4FleX�8] �bIf you have been given permission by a Senior Staff member, and no evidence to the punishment is needed, simply type \"�eNone�b\" in the chat�8.");
				player.sendMessage("�8[�4FleX�8] �bIf you can't provide any evidence simply type \"�eCancel�b\"�8.");
				event.setCancelled(true);
				return;
				
			}
		}
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent event) {
		Player player = (Player) event.getPlayer();
		
		if (evidenceChat.contains(player.getName())) {
		    evidenceChat.remove(player.getName());
		}
		if (targetPlayerName.containsKey(player.getName())) {
			targetPlayerName.remove(player.getName());
		}
		if (banReason.containsKey(player.getName())) {
			banReason.remove(player.getName());
		}
	}
	
	@EventHandler
	public void onWorldChange(PlayerChangedWorldEvent event) {
		Player player = (Player) event.getPlayer();
		
		if (evidenceChat.contains(player.getName())) {
		    evidenceChat.remove(player.getName());
		}
		if (targetPlayerName.containsKey(player.getName())) {
			targetPlayerName.remove(player.getName());
		}
		if (banReason.containsKey(player.getName())) {
			banReason.remove(player.getName());
		}
	}
	
}

