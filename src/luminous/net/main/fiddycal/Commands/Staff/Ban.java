package luminous.net.main.fiddycal.Commands.Staff;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.main.fiddycal.Managers.StaffUtil;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class Ban implements CommandExecutor {

    // Usage: /Ban ThatKawaiiSam 1h Hacks


    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("ban")){
            //Checks if the player is not an instance of Player
            if (!(commandSender instanceof Player)) {
                commandSender.sendMessage(MessageManager.c(MessageManager.prefix + " &cYou can only execute this command as a player!"));
                return false;
            //If the player is an instance of Player
            }else{
                Player player = (Player) commandSender;
                //Checks if the player is staff
                if (StaffUtil.isStaff(player)){
                    //Checks if there is enough args
                    if (strings.length > 3){
                        @SuppressWarnings("deprecation")
						OfflinePlayer op = Main.main.getServer().getOfflinePlayer(strings[1]);
                        //Checks if the player has played before
                        if (op.hasPlayedBefore()) {
                            UUID uuidToBeBanned = op.getUniqueId();
                            //Get Player
                            Player playerToBeBanned = (Player) Main.main.getServer().getOfflinePlayer(uuidToBeBanned);
                            String durationVar = null;
                            //Check the time stamp provided
                            if (strings[2].contains("h")){
                                durationVar = "Hour";
                            }
                            if (strings[2].contains("w")){
                                durationVar = "Week";
                            }
                            if (strings[2].contains("m")){
                                durationVar = "Month";
                            }
                            if (strings[2].contains("y")){
                                durationVar = "Year";
                            }else{
                                player.sendMessage(MessageManager.c(MessageManager.prefix + " &c That is an invalid time variable!"));
                                durationVar = null;
                                return false;
                            }
                            //Bans the player
                            StaffUtil.banPlayer(playerToBeBanned, "null", 1, durationVar);
                        }else{
                            player.sendMessage(MessageManager.c(MessageManager.prefix + " &cThis player can not be banned as he has never joined the server before"));
                        }
                    }else{
                        player.sendMessage(MessageManager.c(MessageManager.prefix + " &cUsage: /ban <player> <reason>"));
                    }
                }else{
                    commandSender.sendMessage(MessageManager.c(MessageManager.prefix + " &cYou do not have permission to execute this command!"));
                }
            }
        }
        return false;
    }
}
