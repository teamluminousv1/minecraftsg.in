package luminous.net.main.fiddycal.Commands.Staff;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;

public class StaffChat implements Listener, CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player player = (Player) sender;
		FileConfiguration config = ConfigManager.playersCFG;
		
		if ((config.getString(player.getUniqueId().toString() + ".Group").equals("moderator"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("uhchost"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("seniormoderator"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("juniordeveloper"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("developer"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("admin"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("leaddeveloper"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("owner"))) {
			
			if (args.length == 0) {
				player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cUsage: /staff <message>"));
				return true;
			} else {
        	
        	StringBuilder sb = new StringBuilder();
        	  for(int i = 0; i < args.length; i++){
        	    sb.append(args[i]).append(" ");
        	  }
        	  String messageToBroacast = sb.toString();
        	  for (Player p : Bukkit.getOnlinePlayers()) {
  				if ((config.getString(p.getUniqueId().toString().toLowerCase() + ".Group").equals("moderator"))
  						|| (config.getString(player.getUniqueId().toString() + ".Group").equals("uhchost"))
  						|| (config.getString(p.getUniqueId().toString().toLowerCase() + ".Group").equals("seniormoderator"))
  						|| (config.getString(p.getUniqueId().toString().toLowerCase() + ".Group").equals("juniordeveloper"))
  						|| (config.getString(p.getUniqueId().toString().toLowerCase() + ".Group").equals("developer"))
  						|| (config.getString(p.getUniqueId().toString().toLowerCase() + ".Group").equals("admin"))
  						|| (config.getString(p.getUniqueId().toString().toLowerCase() + ".Group").equals("leaddeveloper"))
  						|| (config.getString(p.getUniqueId().toString().toLowerCase() + ".Group").equals("owner"))) {
  					p.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &8[&5STAFF&8] &r" + player.getDisplayName() + "&8: &b" + messageToBroacast));
  					
  				}
  			}
			}
		} else player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " " + MessageManager.invalidPermission));
		return true;
	}
}
