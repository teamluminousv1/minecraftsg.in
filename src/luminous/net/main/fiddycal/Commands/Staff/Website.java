package luminous.net.main.fiddycal.Commands.Staff;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import luminous.net.main.fiddycal.Managers.MessageManager;

public class Website implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player player = (Player) sender;
		
		if (args.length == 0) {
			
			player.sendMessage(MessageManager.c(MessageManager.prefix + " &bOur Website&8: &cComing Soon"));
			player.sendMessage(MessageManager.c(MessageManager.prefix + " &bWebsite Status&8: &cOFFLINE&8."));
			
			return true;
		} else player.sendMessage(MessageManager.c(MessageManager.prefix + " &cUsage: /website"));
		return true;
	}

}
