package luminous.net.main.fiddycal.Commands.Staff;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;

public class Broadcast implements CommandExecutor {
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player player = (Player) sender;
		FileConfiguration config = ConfigManager.playersCFG;
		
		if ((config.getString(player.getUniqueId().toString() + ".Group").equals("uhchost"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("moderator"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("seniormoderator"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("juniordeveloper"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("developer"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("admin"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("leaddeveloper"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("owner"))) {
			
			if (args.length == 0) {
				player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cUsage: /broadcast <message>"));
				return true;
			} else if (args.length >= 0) {
        	
			  StringBuilder sb = new StringBuilder();
				
        	  for(int i = 0; i < args.length; i++){
        	    sb.append(args[i]).append(" ");
        	  }
        	  String messageToBroacast = sb.toString();
        	  
        	  player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bBroadcasting to all servers&8..."));
        	  
        	  for (Player p : Bukkit.getOnlinePlayers()) {
        		  p.sendMessage(MessageManager.c("&7<&3&lGLOBAL&7> &8[&4" + player.getPlayerListName() + "&8] &b&l" + messageToBroacast));
        	  }
				
			}
		} else player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " " + MessageManager.invalidPermission));
		return true;
	}
}
