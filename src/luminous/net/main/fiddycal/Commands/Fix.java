package luminous.net.main.fiddycal.Commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import luminous.net.main.fiddycal.Managers.MessageManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class Fix implements CommandExecutor {
	
	public static FileConfiguration config = new YamlConfiguration();
	
	private Map<String, Long> LastUsage = new HashMap<String, Long>();
	private final int cdtime = 10;
	
	public static ArrayList<Player> disguised = new ArrayList<Player>();
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player player = (Player) sender;
		
			if (args.length == 0) {
				
				long LastUsed = 0;
				if (LastUsage.containsKey(sender.getName())) {
					LastUsed = LastUsage.get(sender.getName());
				}
				
				int cdmillis = cdtime * 1000;
				if (System.currentTimeMillis()-LastUsed>=cdmillis) {
					
					for (Player plr : Bukkit.getOnlinePlayers()) {
						if (plr.getName().equals(player.getName())) {
							player.teleport(plr);
							player.sendMessage(MessageManager.c(MessageManager.prefix + " &fYou have been de-ghosted&8."));
							LastUsage.put(sender.getName(), System.currentTimeMillis());
							return true;
						}
					}

				} else {
					int timeLeft = (int) (cdtime-((System.currentTimeMillis()-LastUsed)/1000));
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &fYou can't use that for another &8[&e" + timeLeft + "&8] &fseconds&8."));
				}
					
				} else {
					player.sendMessage(MessageManager.c(MessageManager.prefix + "&cUsage: /fix"));
				}
			return true;
		}
}
