package luminous.net.main.fiddycal.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;

public class Fly implements Listener, CommandExecutor {
	
	public static void startFlight(Player player) {
		
		FileConfiguration config = ConfigManager.playersCFG;
		
		if (config.getString(player.getUniqueId().toString() + ".Group").equals("champion")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("legend")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("guardian")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("elite")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("vip")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("mapmaker")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("builder")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("uhchost")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("moderator")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("seniormoderator")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("juniordeveloper")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("developer")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("admin")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("leaddeveloper")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("owner")) {
		
			player.setAllowFlight(true);
			player.setFlying(true);
			
		}
			
	}
	
	public static void stopFlight(Player player) {
		
		if (player.isFlying() || player.getAllowFlight() == true) {
			player.setFlying(false);
			player.setAllowFlight(false);
		}
		
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		Player player = (Player) sender;
		FileConfiguration config = ConfigManager.playersCFG;
		
		if (!(sender instanceof Player)) {
			sender.sendMessage("&b&lL&e&lMC &3&l>> &cOnly players are able to do this command&8.");
			return true;
		}
		
		if (config.getString(player.getUniqueId().toString() + ".Group").equals("champion")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("legend")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("guardian")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("elite")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("vip")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("mapmaker")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("builder")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("uhchost")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("moderator")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("seniormoderator")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("juniordeveloper")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("developer")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("admin")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("leaddeveloper")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("owner")) {
			
			if (args.length <= 0) {
				
				if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null
					|| Main.main.getSGMakerGameManager().getGameByPlayer(player.getName()) != null
					|| Main.main.getBGGameManager().getGameByPlayer(player.getName()) != null) {
					player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &4You can only do that in hubs and lobbies&8."));
					return true;
				}
				
				if (player.getAllowFlight() == (true)) {
					player.setAllowFlight(false);
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &bYou are no longer able to fly&8."));
				} else {
					player.setAllowFlight(true);
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &bYou are now able to fly&8."));
				}
				return true;
			
			} else if (!(args[0].equalsIgnoreCase("enable") || args[0].equalsIgnoreCase("disable"))) {
				player.sendMessage(MessageManager.c(MessageManager.prefix + " &cUsage: /fly [enable|disable]"));
				
			} else if (args[0].equalsIgnoreCase("enable")) {
				if (player.getAllowFlight() == (true)) {
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &bYou are already able to fly&8."));
				} else {
					player.setAllowFlight(true);
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &bYou are now able to fly&8."));
				}
				return true;
				
			} else if (args[0].equalsIgnoreCase("disable")) {
				if (player.getAllowFlight() == (false)) {
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &bYou are already unable to fly&8."));
				} else {
					player.setAllowFlight(false);
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &bYou are no longer able to fly&8."));
				}
			}
			return true;
			
		} else player.sendMessage(MessageManager.c(MessageManager.prefix + " " + MessageManager.invalidPermission));
	
	return true;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = (Player) event.getPlayer();
		Fly.startFlight(player);
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent event) {
		Player player = (Player) event.getPlayer();
		Fly.stopFlight(player);
	}
}
