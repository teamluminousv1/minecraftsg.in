package luminous.net.main.fiddycal.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;
 
public class Weather implements Listener, CommandExecutor {
	
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    	
    	Player player = (Player) sender; 
        FileConfiguration config = ConfigManager.playersCFG;
        
        if ((config.getString(player.getUniqueId().toString() + ".Group").equals("developer"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("admin"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("leaddeveloper"))
				|| (config.getString(player.getUniqueId().toString() + ".Group").equals("owner"))) {
        	
            if (args.length == 0) {
			
        		Main.changingWeather = false;
        		player.getWorld().setStorm(false);
        		player.getWorld().setThundering(false);
        		player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThis world will now stay sunny&8."));
        		Main.changingWeather = true;
    			return true;
            
            } else {
        	    player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cUsage: /sun"));
        	    return true;
            }
            
        } else {
        	player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " " + MessageManager.invalidPermission));
			return true;
        }
    }
  
    @EventHandler
    public void onRain(WeatherChangeEvent event) {
	  
    	if (Main.changingWeather == true) {
    		event.getWorld().setThundering(false);
    	}
  }
  
}