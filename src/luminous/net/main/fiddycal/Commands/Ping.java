package luminous.net.main.fiddycal.Commands;

import luminous.net.main.fiddycal.Managers.MessageManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Ping implements CommandExecutor {

	public int getPing(Player player) {
        return ((CraftPlayer) player).getHandle().ping;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("ping")){
            if (!(commandSender instanceof Player)){
                commandSender.sendMessage("You can only execute this command as a player!");
            }else{
                Integer ping = getPing((Player) commandSender);
                commandSender.sendMessage(MessageManager.c(MessageManager.prefix + " &fYour ping to the server is &8[&6" + ping + "&8] &fms&8."));
            }

        }
        return false;
    }
}
