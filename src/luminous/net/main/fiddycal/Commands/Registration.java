package luminous.net.main.fiddycal.Commands;

import java.util.ArrayList;
import java.util.List;

public class Registration {
	
	private List<String> registered = new ArrayList<>();

	public boolean isRegistered(String command) {
		for (String r : registered) {
			if (command.startsWith(r)) {
				return true;
			}
		}
		return false;
	}
	
	public void registerCommand(String command) {
		getRegisteredCommands().add(command);
	}
	
	public void unRegisterCommand(String command) {
		getRegisteredCommands().remove(command);
	}
	
	public List<String> getRegisteredCommands() {
		return registered;
	}
	
}
