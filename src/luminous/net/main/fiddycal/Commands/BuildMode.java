package luminous.net.main.fiddycal.Commands;

import java.util.ArrayList;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;

import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;

public class BuildMode implements Listener, CommandExecutor {
	
	public static ArrayList<String> building = new ArrayList<String>();
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player player = (Player) sender;
		FileConfiguration config = ConfigManager.playersCFG;
		
		if (args.length == 0) {
			if ((config.getString(player.getUniqueId().toString() + ".Group").equals("builder"))
					|| (config.getString(player.getUniqueId().toString() + ".Group").equals("admin"))
					|| (config.getString(player.getUniqueId().toString() + ".Group").equals("leaddeveloper"))
					|| (config.getString(player.getUniqueId().toString() + ".Group").equals("owner"))) {
				
				if (building.contains(player.getName())) {
					building.remove(player.getName());
					player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bYou have disabled &eBuild &bmode&8."));
					return true;
				}
				building.add(player.getName());
				player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bYou have enabled &eBuild &bmode&8."));
				player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cBlocks in Survival Games will ont save&8."));
				player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cPlease get an admin if you wish to do this&8."));
				player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bUse &e/build &bto leave this mode&8."));
				
			} else player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " " + MessageManager.invalidPermission));
		}
		
		return true;
	}
	
	@EventHandler
	public void onWorldChange(PlayerChangedWorldEvent event) {
		Player player = (Player) event.getPlayer();
		
		if (building.contains(player.getName())) {
			building.remove(player.getName());
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &eBuild &bmode has been disabled&8."));
		}
	}
	
	@EventHandler
    public void onEntityDamage(EntityDamageByEntityEvent event) {
        Entity player = event.getDamager();
        Entity attacker = event.getEntity();
		
		if (building.contains(player.getName())) {
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bYou cannot attack others while in &eBuild &bmode&8."));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bUse &e/build &bto leave this mode&8."));
            event.setCancelled(true);
            return;
        }
		
		if (building.contains(attacker.getName())) {
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThat player is in &eBuild &bmode&8."));
            event.setCancelled(true);
            return;
		}
		
    }
	
	@EventHandler
	public void onHit(EntityDamageEvent event) {
		Entity player = event.getEntity();
		
		if (building.contains(player.getName())) {
			event.setCancelled(true);
		}
    }
	
}
