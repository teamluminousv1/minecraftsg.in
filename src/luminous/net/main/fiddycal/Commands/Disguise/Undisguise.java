package luminous.net.main.fiddycal.Commands.Disguise;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.main.fiddycal.Managers.StaffUtil;
import luminous.net.main.fiddycal.Managers.DisguiseManager.Disguise;
import luminous.net.main.fiddycal.Managers.DisguiseManager.DisguiseManager;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGame;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGameState;

public class Undisguise implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		Player player = (Player) sender;
		FileConfiguration config = ConfigManager.playersCFG;
		

		if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
			player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &4&l&oTEMP: &cYou can only undiguise in hubs&8."));
			return true;
		}
		
		if (Main.main.getBGGameManager().getGameByPlayer(player.getName()) != null) {
			player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &4&l&oTEMP: &cYou can only undiguise in hubs&8."));
			return true;
		}
		
		if (!(config.getString(player.getUniqueId().toString() + ".Group").equals("guardian")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("elite")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("royal")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("mapmaker")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("retired")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("trialbuilder")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("builder")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("vip")
				|| StaffUtil.isStaff(player))) {
			
			player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " " + MessageManager.invalidPermission));
			return true;
			
		}
		
		if (args.length == 0) {
			
			if (Main.main.getDisguiseManager().isDisguised(player) == false) {
				player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &eYou are not disguised&8."));
			    return true;
			}
			
			if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
				SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
				if (game.getGamestate() != SGGameState.WAITING
					&& game.getGamestate() != SGGameState.CLEANUP
					&& game.getGamestate() != SGGameState.ENDGAME
					&& game.getGamestate() != SGGameState.RESTARTING) {
					player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &4You cannot undiguise in game&8."));
					return true;
				}
			}
			
			String displayName = ConfigManager.playersCFG.getString(player.getUniqueId().toString() + ".Displayname");
			Disguise disguise = Main.main.getDisguiseManager().getDisguise(player);
			
			disguise.unDisguise();
			player.setDisplayName(displayName);
			
			player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &eYou have been undisguised&8."));
			
		} else {
			player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &cUsage: /undisguise"));
		    return true;
		}
		return true;
		
	}
	
}
