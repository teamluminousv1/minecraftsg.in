package luminous.net.main.fiddycal.Commands.Disguise;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.main.fiddycal.Managers.StaffUtil;

public class Disguise implements CommandExecutor {

	private Map<String, Long> LastUsage = new HashMap<String, Long>();
	private final int cdtime = 10;
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		Player player = (Player) sender;
		FileConfiguration config = ConfigManager.playersCFG;
		
		if (!(config.getString(player.getUniqueId().toString() + ".Group").equals("guardian")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("elite")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("royal")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("mapmaker")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("retired")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("trialbuilder")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("builder")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("vip")
				|| StaffUtil.isStaff(player))) {
			
			player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " " + MessageManager.invalidPermission));
			return true;
			
		}
		
		if (args.length == 0) {
			
			if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
				player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &4&l&oTEMP: &cYou can only diguise in hubs&8."));
				return true;
			}
			
			if (Main.main.getBGGameManager().getGameByPlayer(player.getName()) != null) {
				player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &4&l&oTEMP: &cYou can only diguise in hubs&8."));
				return true;
			}
			
			long LastUsed = 0;
			if (LastUsage.containsKey(sender.getName())) {
				LastUsed = LastUsage.get(sender.getName());
			}
			
			int cdmillis = cdtime * 1000;
			if (System.currentTimeMillis()-LastUsed>=cdmillis) {
					
				String disguiselog = "";
				for (String arg : args) {
					disguiselog = disguiselog + arg + " ";
				}
				player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &c&lWarning! &r&cThis command is logged&8."));
				player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &cStaff can still see your real username&8!"));
				player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &eGenerating random username&8..."));
				
				Random r1 = new Random();
				String[] name1 = { "The", "Its", "Itz", "x", "_", "", "i", "am", "dank", "Tha", "mlg", "xX", "", "", "" };
				int s1 = r1.nextInt(name1.length);
					
				Random r2 = new Random();
				String[] name2 = { "Pro", "gaMingg", "STraffe", "AsTRo", "Sick" , "Banana", "banana",
					"pr0", "_xPOWer", "sTRafe", "God", "god", "siCkAz", "peannut", "buTTer", "MnM",
					"Nuteellaaa", "nutALLeh", "Skilled", "skills", "20kk", "memezz", "MEEMEE", "zZz",
					"PHOne", "pHOne", "xyl", "sUn", "SooW", "tilES", "Btr", "cnYY", "carrrrl", "exCEpt",
					"pzFAM", "inDE", "eglot", "dooow", "pote", "qqppr", "yasaw", "jiot", "acChe" };
				int s2 = r2.nextInt(name2.length);
					
				String[] skin = { "Huahwi", "BajanCanadian", "Solr", "Pyr", "Archybot" , "purple",
					"banana", "ProTip", "MEME", "God", "pvp", "GrapeAppleSauce", "sick", "ayyriley", "Woen",
					"nikes", "Riley", "Glen", "Skilled" };
				Random r3 = new Random();
				int s3 = r3.nextInt(skin.length);
				
				new luminous.net.main.fiddycal.Managers.DisguiseManager.Disguise(player, name1[s1] + name2[s2],
						skin[s3], MessageManager.c("&2" + name1[s1] + name2[s2]));
					
				player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &eYou now appear as &r" + player.getDisplayName() + "&8."));
				player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &eTo undisguise, use &8[&6/undisguise&8]."));

				LastUsage.put(sender.getName(), System.currentTimeMillis());
					
			} else {
				int timeLeft = (int) (cdtime-((System.currentTimeMillis()-LastUsed)/1000));
				player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &4You can't use that for another &8[&e" + timeLeft + "&8] &4seconds&8."));
			}
				
		} else {
				
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("developer")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("admin")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("leaddeveloper")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("owner")) {

				player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &cUsage: /disguise"));
				return true;
				
			} else {
				player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &cUsage: /disguise"));
			    return true;
			}
		}
		return true;
		
	}
	
}
