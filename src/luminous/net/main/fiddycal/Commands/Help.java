package luminous.net.main.fiddycal.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;

public class Help implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		FileConfiguration config = ConfigManager.playersCFG;
		
		if (args.length == 0) {
			
			sender.sendMessage(MessageManager.c(MessageManager.prefix + " &6&m            &7 MCSG Help &6&m            &7"));
			sender.sendMessage(MessageManager.c(MessageManager.prefix + " &6/website&8: &7Shows the servers website&8."));
			sender.sendMessage(MessageManager.c(MessageManager.prefix + " &6/teamspeak&8: &7Shows the servers teamspeak&8."));
			sender.sendMessage(MessageManager.c(MessageManager.prefix + " &6/stats [player]&8: &7Shows said players stats&8."));
			sender.sendMessage(MessageManager.c(MessageManager.prefix + " &6/report <player> <reason>&8: &7Reports a player&8."));
			sender.sendMessage(MessageManager.c(MessageManager.prefix + " &6/ping&8: &7Shows the your ping to the server&8."));
			sender.sendMessage(MessageManager.c(MessageManager.prefix + " &6/sponsor&8: &7Sponsor a player in game items&8."));
			sender.sendMessage(MessageManager.c(MessageManager.prefix + " &6/bounty&8: &7Bounty a player in game points&8."));
			sender.sendMessage(MessageManager.c(MessageManager.prefix + " &6/hub&8: &7Sends you to the hub server&8."));
			if (sender instanceof Player) {
				Player player = (Player) sender;
				if (config.getString(player.getUniqueId().toString() + ".Group").equals("champion")
						|| config.getString(player.getUniqueId().toString() + ".Group").equals("legend")
						|| config.getString(player.getUniqueId().toString() + ".Group").equals("guardian")
						|| config.getString(player.getUniqueId().toString() + ".Group").equals("elite")
						|| config.getString(player.getUniqueId().toString() + ".Group").equals("vip")
						|| config.getString(player.getUniqueId().toString() + ".Group").equals("mapmaker")
						|| config.getString(player.getUniqueId().toString() + ".Group").equals("builder")
						|| config.getString(player.getUniqueId().toString() + ".Group").equals("uhchost")
						|| config.getString(player.getUniqueId().toString() + ".Group").equals("moderator")
						|| config.getString(player.getUniqueId().toString() + ".Group").equals("seniormoderator")
						|| config.getString(player.getUniqueId().toString() + ".Group").equals("juniordeveloper")
						|| config.getString(player.getUniqueId().toString() + ".Group").equals("developer")
						|| config.getString(player.getUniqueId().toString() + ".Group").equals("admin")
						|| config.getString(player.getUniqueId().toString() + ".Group").equals("leaddeveloper")
						|| config.getString(player.getUniqueId().toString() + ".Group").equals("owner")) {
					sender.sendMessage(MessageManager.c(MessageManager.prefix + " &6/disguise&8: &7Disguise as another player&8."));
					sender.sendMessage(MessageManager.c(MessageManager.prefix + " &6/fly&8: &7Enables flight&8."));
				}
			}
			
			
		} else sender.sendMessage(MessageManager.c(MessageManager.prefix + " &cUsage: /help"));
		return true;
	}

}
