package luminous.net.main.fiddycal.Commands;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.MessageManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import luminous.net.main.fiddycal.Managers.ConfigManager;

public class Teleport implements Listener, CommandExecutor {
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		Player player = (Player) sender;
		FileConfiguration config = ConfigManager.playersCFG;
		
		if (!(sender instanceof Player)) {
			sender.sendMessage(MessageManager.c(MessageManager.prefix + " &cOnly players are able to do this command&8."));
			return false;
		}
			
		if (config.getString(player.getUniqueId().toString() + ".Group").equals("moderator")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("uhchost")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("seniormoderator")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("juniordeveloper")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("admin")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("developer")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("leaddeveloper")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("owner")) {
			
			if (config.getString(player.getUniqueId().toString() + ".Group").equals("juniordeveloper")
					|| config.getString(player.getUniqueId().toString() + ".Group").equals("admin")
					|| config.getString(player.getUniqueId().toString() + ".Group").equals("developer")
					|| config.getString(player.getUniqueId().toString() + ".Group").equals("leaddeveloper")
					|| config.getString(player.getUniqueId().toString() + ".Group").equals("owner")) {
				
				if (args.length == 1) {
					
					if (player.getServer().getPlayer(args[0]) != null) {
			            Player targetPlayer = Bukkit.getServer().getPlayer(args[0]);
			            	
				        player.teleport(targetPlayer);
				        player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bYou have been teleported to &r" + targetPlayer.getDisplayName() + "&8."));
				        return true;
				        
					} else player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cThat player is not online&8!"));
					return true;
				
			    } else player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cUsage: /teleport <player>"));
				
			} else {
				if (Main.main.getModMode().getPlayer().contains(player.getName())) {
					
					if (args.length == 1) {
						
						if (player.getServer().getPlayer(args[0]) != null) {
				            Player targetPlayer = Bukkit.getServer().getPlayer(args[0]);
				            	
					        player.teleport(targetPlayer);
					        player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bYou have been teleported to &4" + targetPlayer.getDisplayName() + "&8."));
					        return true;
					        
						} else player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " " + MessageManager.playerNotOnline));
						return true;
					
				    } else player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cUsage: /teleport <player>"));
					
				} else player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &4You can only use this in hub servers and &eHI mode&8."));
			}
			
		} else player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " " + MessageManager.invalidPermission));
		return true;
	}
}
