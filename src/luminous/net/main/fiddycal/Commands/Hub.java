package luminous.net.main.fiddycal.Commands;

import luminous.net.minecraftsg.net.Gamemodes.BG.BGGame;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Events.Connect;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGame;

public class Hub implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (!(sender instanceof Player)) {
			sender.sendMessage(MessageManager.c(MessageManager.prefix + " &cOnly players are able to do this command&8."));
			return true;
		}
		
		final Player player = (Player) sender;
		
		if (player.getWorld().equals(Bukkit.getWorld(Main.main.getConfig().getString("Spawn.world")))) {
			player.sendMessage(MessageManager.unknownCommand);
			return true;
		}
		
		if (args.length == 0) {
			player.sendMessage(MessageManager.c(MessageManager.prefix + " &cConnecting to the hub server&8."));
			new BukkitRunnable() {
	            @Override
	            public void run() {
	            	if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
	            		SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
						if (game.getTributes().contains(player.getName())) {
							Location location = player.getLocation();
							location.getWorld().strikeLightningEffect(location);
						}
	            		game.leaveGame(player);
	            	}
					if (Main.main.getBGGameManager().getGameByPlayer(player.getName()) != null) {
						BGGame game = Main.main.getBGGameManager().getGameByPlayer(player.getName());
						game.leaveGame(player);
					}
	            	Connect.sendToLobby(player);
	            	Connect.giveLobbyItems(player);
	            }
	        }.runTaskLater(Main.main, 20L);
		} else {
			player.sendMessage(MessageManager.c(MessageManager.prefix + " &cUsage: /hub"));
		}
		return true;
		
	}
}
