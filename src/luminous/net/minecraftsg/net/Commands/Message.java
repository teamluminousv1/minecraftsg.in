package luminous.net.minecraftsg.net.Commands;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGame;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGameState;
import luminous.net.minecraftsg.net.Gamemodes.SGMaker.SGMakerGame;
import luminous.net.minecraftsg.net.Gamemodes.SGMaker.SGMakerGameState;

public class Message implements CommandExecutor {

	public static HashMap<String, String> messaged = new HashMap<String, String>();
	
	public static void sendMessage(Player player, Player sender, String message) {
		player.sendMessage(MessageManager.c("" + ChatColor.DARK_GRAY + "[" + ChatColor.GRAY + "From " + ChatColor.RESET 
		+ sender.getDisplayName() + ChatColor.DARK_GRAY + "] " + ChatColor.DARK_AQUA + message + ChatColor.DARK_GRAY + "."));
		sender.sendMessage(MessageManager.c("" + ChatColor.DARK_GRAY + "[" + ChatColor.GRAY + "To " + ChatColor.RESET 
		+ player.getDisplayName() + ChatColor.DARK_GRAY + "] " + ChatColor.DARK_AQUA + message + ChatColor.DARK_GRAY + "."));
		messaged.put(player.getName(), sender.getName());
	}
	
	@Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (!(sender instanceof Player)) {
			sender.sendMessage(MessageManager.c(MessageManager.prefix + " &cOnly players are able to do this command&8."));
			return true;
		}
		
		Player player = (Player) sender;
		
		if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) == null
			&& Main.main.getSGMakerGameManager().getGameByPlayer(player.getName()) == null) {
			player.sendMessage(MessageManager.unknownCommand);
			return true;
		}
		
		if (args.length > 1) {
			if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
				SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
				if (game.getGamestate() != SGGameState.WAITING) {
					StringBuilder sb = new StringBuilder();
		        	for(int i = 1; i < args.length; i++) {
		        	    sb.append(args[i]).append(" ");
		        	}
		        	if (Main.main.getServer().getPlayer(args[0]) != null) {
		        		if (game.getPlayers().contains(Main.main.getServer().getPlayer(args[0]).getName())) {
		        			if (game.getTributes().contains(Main.main.getServer().getPlayer(args[0]).getName())) {
		        				if (Main.main.getServer().getPlayer(args[0]).getLocation().distance(player.getLocation()) < 11) {
		        					sendMessage(Main.main.getServer().getPlayer(args[0]), player, sb.toString());
		        					return true;
		        				} else {
		        					player.sendMessage(MessageManager.c(MessageManager.prefix + " &7You must be within &8[&610&8] &7blocks of the player you would like to message&8."));
		        				}
			        		} else {
			        			player.sendMessage(MessageManager.c(MessageManager.prefix + " &cThat player must is not ingame&8."));
			        		}
		        		} else {
		        			player.sendMessage(MessageManager.c(MessageManager.prefix + " " + MessageManager.playerNotOnline));
		        		}
		        		return true;
		        	} else {
		        		player.sendMessage(MessageManager.c(MessageManager.prefix + " " + MessageManager.playerNotOnline));
		        	}
				} else {
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &cYou can not do that in waiting lobbies&8."));
				}
			} else if (Main.main.getSGMakerGameManager().getGameByPlayer(player.getName()) != null) {
				SGMakerGame game = Main.main.getSGMakerGameManager().getGameByPlayer(player.getName());
				if (game.getGamestate() != SGMakerGameState.WAITING) {
					StringBuilder sb = new StringBuilder();
		        	for(int i = 1; i < args.length; i++) {
		        	    sb.append(args[i]).append(" ");
		        	}
		        	if (Main.main.getServer().getPlayer(args[0]) != null) {
		        		if (game.getPlayers().contains(Main.main.getServer().getPlayer(args[0]).getName())) {
		        			if (game.getTributes().contains(Main.main.getServer().getPlayer(args[0]).getName())) {
		        				if (Main.main.getServer().getPlayer(args[0]).getLocation().distance(player.getLocation()) < 11) {
		        					sendMessage(Main.main.getServer().getPlayer(args[0]), player, sb.toString());
		        					return true;
		        				} else {
		        					player.sendMessage(MessageManager.c(MessageManager.prefix + " &7You must be within &8[&610&8] &7blocks of the player you would like to message&8."));
		        				}
			        		} else {
			        			player.sendMessage(MessageManager.c(MessageManager.prefix + " &cThat player must is not ingame&8."));
			        		}
		        		} else {
		        			player.sendMessage(MessageManager.c(MessageManager.prefix + " " + MessageManager.playerNotOnline));
		        		}
		        		return true;
		        	} else {
		        		player.sendMessage(MessageManager.c(MessageManager.prefix + " " + MessageManager.playerNotOnline));
		        	}
				} else {
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &cYou can not do that in waiting lobbies&8."));
				}
			}
		} else {
			player.sendMessage(MessageManager.c(MessageManager.prefix + " &cUsage: /message <player> <message>"));
		}
		return false;
	}
	
}
