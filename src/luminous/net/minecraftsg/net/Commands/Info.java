package luminous.net.minecraftsg.net.Commands;

import org.bukkit.ChatColor;
import luminous.net.minecraftsg.net.Gamemodes.BG.BGGame;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.main.fiddycal.Managers.RankManager;
import luminous.net.minecraftsg.net.Gamemodes.BG.BGGameState;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGame;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGameState;
import luminous.net.minecraftsg.net.Gamemodes.SGMaker.SGMakerGame;
import luminous.net.minecraftsg.net.Gamemodes.SGMaker.SGMakerGameState;

public class Info implements CommandExecutor {

	@Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    	
    	if (!(sender instanceof Player)) {
			sender.sendMessage("          Zime Management          ");
			sender.sendMessage("Players: " + Main.main.getServer().getOnlinePlayers().size());
			sender.sendMessage("TPS: null");
			sender.sendMessage("Region: null");
			sender.sendMessage("Plugin Version: null");
			sender.sendMessage("");
			sender.sendMessage("Zime Gamemodes:");
			for (int i = 0; i < Main.main.getZimeGames().getGames().size(); i++) {
				String[] spl = Main.main.getZimeGames().getGames().get(i).split("-");
				sender.sendMessage("ZimeGameID: " + Main.main.getZimeGames().getZimeID(Main.main.getZimeGames().getGames().get(i)) + "   GameMode: " + spl[0] + "  GamemodeID:" +  spl[1]);
			}
			return true;
		}
		
		Player player = (Player) sender;
		if (Main.main.getBGGameManager().getGameByPlayer(player.getName()) != null){
			BGGame game = Main.main.getBGGameManager().getGameByPlayer(player.getName());
			int timeLeft = 0;
			String map = game.getMap();
			String mapAuthor = null;
			String mapLink = null;
			player.sendMessage(MessageManager.c("&fTimeleft&8: [&e" +
					game.getTm().getMins(timeLeft) + "&8] &eminutes&8, &8[&e" +
					game.getTm().getSeconds(timeLeft) + "&8] &eseconds"));
			player.sendMessage(MessageManager.c("&fCurrent Map&8: &e" + map));
			player.sendMessage(MessageManager.c("&fMap Creator&8: &e" + mapAuthor));
			player.sendMessage(MessageManager.c("&fMap Link&8: &e" + mapLink));
			player.sendMessage(MessageManager.c("&fDevelopers&8: &r"
					+ RankManager.getDeveloperName("Rustywolf") + "&8, &r"
					+ RankManager.getDeveloperName("subv3rsion") + "&8, &r"
					+ RankManager.getDeveloperName("Forairan") + "&8, &r"
					+ RankManager.getDeveloperName("kpwn243") + "&8, &r"
					+ RankManager.getDeveloperName("dv90") + "&8, &r"
					+ RankManager.getDeveloperName("Favorlock") + "&8, &r"
					+ RankManager.getDeveloperName("DV_Raiton") + "&8, &r"
					+ RankManager.getDeveloperName("5Ocal") + "&8, &r"
					+ RankManager.getDeveloperName("ThatKawaiiSam")));
			player.sendMessage(MessageManager.c("&fHosted by&8: &ehttps://mcprohosting.com/ | LLC [US]"));
			player.sendMessage(MessageManager.c("&fRunning the &4FleX&8.&cv1 &fengine&8."));
			return true;
		}
		if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
    	    if (args.length == 0) {
    			SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
    			int timeLeft = 0;
    			String map = null;
    			String mapAuthor = null;
    			String mapLink = null;
    			if (game.getMap().equals("NOT SET")) {
    		    map = "None"; } else { map = game.getMap().replace("_", " "); }
    			if (ConfigManager.sgDataCFG.getString("Maps." + game.getMap() + ".Author") != null) {
    				mapAuthor = ConfigManager.sgDataCFG.getString("Maps." + game.getMap() + ".Author");
    			} else { mapAuthor = "Unknown"; }
    			if (ConfigManager.sgDataCFG.getString("Maps." + game.getMap() + ".Link") != null) {
    				mapLink = ConfigManager.sgDataCFG.getString("Maps." + game.getMap() + ".Link");
    			} else { mapLink = "Coming Soon"; }
    			if (game.getGamestate() == SGGameState.WAITING) {
    			timeLeft = game.getTm().waitingTime; } else 
    			if (game.getGamestate() == SGGameState.PREGAME) {
        		timeLeft = game.getTm().preGame; } else 
                if (game.getGamestate() == SGGameState.GRACEPERIOD) {
                timeLeft = game.getTm().graceperiod; } else 
            	if (game.getGamestate() == SGGameState.INGAME) {
                timeLeft = game.getTm().inGame; } else 
                if (game.getGamestate() == SGGameState.PREDEATHMATCH) {
                timeLeft = game.getTm().preDeathmatch; } else 
                if (game.getGamestate() == SGGameState.DEATHMATCH) {
                timeLeft = game.getTm().deathmatch; } else 
                if (game.getGamestate() == SGGameState.ENDGAME) {
                timeLeft = game.getTm().endGame; } else 
                if (game.getGamestate() == SGGameState.CLEANUP) {
                timeLeft = game.getTm().cleanUp; } else 
                if (game.getGamestate() == SGGameState.RESTARTING) {
                timeLeft = game.getTm().restarting; }
    			player.sendMessage(MessageManager.c("&fStatus&8: &e" + game.getDisplayGamestate(game.getGamestate())));
    			player.sendMessage(MessageManager.c("&fTimeleft&8: [&e" + 
    			game.getTm().getMins(timeLeft) + "&8] &eminutes&8, &8[&e" + 
    			game.getTm().getSeconds(timeLeft) + "&8] &eseconds"));
    			player.sendMessage(MessageManager.c("&fCurrent Map&8: &e" + map));
    			player.sendMessage(MessageManager.c("&fMap Creator&8: &e" + mapAuthor));
    			player.sendMessage(MessageManager.c("&fMap Link&8: &e" + mapLink));
    			player.sendMessage(MessageManager.c("&fDevelopers&8: &r" 
    			+ RankManager.getDeveloperName("Rustywolf") + "&8, &r" 
    			+ RankManager.getDeveloperName("subv3rsion") + "&8, &r" 
    			+ RankManager.getDeveloperName("Forairan") + "&8, &r" 
    			+ RankManager.getDeveloperName("kpwn243") + "&8, &r" 
    			+ RankManager.getDeveloperName("dv90") + "&8, &r" 
    			+ RankManager.getDeveloperName("Favorlock") + "&8, &r" 
    			+ RankManager.getDeveloperName("DV_Raiton") + "&8, &r"
    	    	+ RankManager.getDeveloperName("5Ocal") + "&8, &r"
    	    	+ RankManager.getDeveloperName("ThatKawaiiSam")));
    			player.sendMessage(MessageManager.c("&fHosted by&8: &ehttps://syd.cubedhosting.com/"));
    			player.sendMessage(MessageManager.c("&fRunning the &2" + ChatColor.stripColor(Main.main.getConfig()
    			.getString("Prefix.Engine")).replace("(", "").replace(")", "").replace("Z", "X") + "&8.&av3 " + 
    			Main.main.getConfig().getString("Prefix.Engine") + " &fengine&8."));
    		    return true;
		    } else {
			    player.sendMessage(MessageManager.c(MessageManager.prefix + " &cUsage: /infomation"));
		    }
		} else if (Main.main.getSGMakerGameManager().getGameByPlayer(player.getName()) != null) {
    	    if (args.length == 0) {
    			SGMakerGame game = Main.main.getSGMakerGameManager().getGameByPlayer(player.getName());
    			int timeLeft = 0;
    			String map = null;
    			String mapAuthor = null;
    			String mapLink = null;
    			if (game.getMap().equals("NOT SET")) {
    		    map = "None"; } else { map = game.getMap().replace("_", " "); }
    			if (ConfigManager.sgDataCFG.getString("Maps." + game.getMap() + ".Author") != null) {
    				mapAuthor = ConfigManager.sgDataCFG.getString("Maps." + game.getMap() + ".Author");
    			} else { mapAuthor = "Unknown"; }
    			if (ConfigManager.sgDataCFG.getString("Maps." + game.getMap() + ".Link") != null) {
    				mapLink = ConfigManager.sgDataCFG.getString("Maps." + game.getMap() + ".Link");
    			} else { mapLink = "Coming Soon"; }
    			if (game.getGamestate() == SGMakerGameState.WAITING) {
    			timeLeft = game.getTm().waitingTime; } else 
    			if (game.getGamestate() == SGMakerGameState.PREGAME) {
        		timeLeft = game.getTm().preGame; } else 
                if (game.getGamestate() == SGMakerGameState.GRACEPERIOD) {
                timeLeft = game.getTm().graceperiod; } else 
            	if (game.getGamestate() == SGMakerGameState.INGAME) {
                timeLeft = game.getTm().inGame; } else 
                if (game.getGamestate() == SGMakerGameState.PREDEATHMATCH) {
                timeLeft = game.getTm().preDeathmatch; } else 
                if (game.getGamestate() == SGMakerGameState.DEATHMATCH) {
                timeLeft = game.getTm().deathmatch; } else 
                if (game.getGamestate() == SGMakerGameState.ENDGAME) {
                timeLeft = game.getTm().endGame; } else 
                if (game.getGamestate() == SGMakerGameState.CLEANUP) {
                timeLeft = game.getTm().cleanUp; } else 
                if (game.getGamestate() == SGMakerGameState.RESTARTING) {
                timeLeft = game.getTm().restarting; }
    			player.sendMessage(MessageManager.c("&fStatus&8: &e" + game.getDisplayGamestate(game.getGamestate())));
    			player.sendMessage(MessageManager.c("&fTimeleft&8: [&e" + 
    			game.getTm().getMins(timeLeft) + "&8] &eminutes&8, &8[&e" + 
    			game.getTm().getSeconds(timeLeft) + "&8] &eseconds"));
    			player.sendMessage(MessageManager.c("&fCurrent Map&8: &e" + map));
    			player.sendMessage(MessageManager.c("&fMap Creator&8: &e" + mapAuthor));
    			player.sendMessage(MessageManager.c("&fMap Link&8: &e" + mapLink));
    			player.sendMessage(MessageManager.c("&fDevelopers&8: &r" 
    			+ RankManager.getDeveloperName("Rustywolf") + "&8, &r" 
    			+ RankManager.getDeveloperName("subv3rsion") + "&8, &r" 
    			+ RankManager.getDeveloperName("Forairan") + "&8, &r" 
    			+ RankManager.getDeveloperName("kpwn243") + "&8, &r" 
    			+ RankManager.getDeveloperName("dv90") + "&8, &r" 
    			+ RankManager.getDeveloperName("Favorlock") + "&8, &r" 
    			+ RankManager.getDeveloperName("DV_Raiton") + "&8, &r"
    	    	+ RankManager.getDeveloperName("5Ocal") + "&8, &r"
    	    	+ RankManager.getDeveloperName("ThatKawaiiSam")));
    			player.sendMessage(MessageManager.c("&fHosted by&8: &ehttps://syd.cubedhosting.com/"));
    			player.sendMessage(MessageManager.c("&fRunning the &2" + ChatColor.stripColor(Main.main.getConfig()
    			.getString("Prefix.Engine")).replace("(", "").replace(")", "").replace("Z", "X") + "&8.&av3 " + 
    			Main.main.getConfig().getString("Prefix.Engine") + " &fengine&8."));
    		    return true;
		    }  else if (Main.main.getBGGameManager().getGameByPlayer(player.getName()) != null) {
	    	    if (args.length == 0) {
	    	    	BGGame game = Main.main.getBGGameManager().getGameByPlayer(player.getName());
	    			int timeLeft = 0;
	    			String map = null;
	    			String mapAuthor = null;
	    			String mapLink = null;
	    			if (game.getMap().equals("NOT SET")) {
	    		    map = "None"; } else { map = game.getMap().replace("_", " "); }
	    			if (ConfigManager.sgDataCFG.getString("Maps." + game.getMap() + ".Author") != null) {
	    				mapAuthor = ConfigManager.sgDataCFG.getString("Maps." + game.getMap() + ".Author");
	    			} else { mapAuthor = "Unknown"; }
	    			if (ConfigManager.sgDataCFG.getString("Maps." + game.getMap() + ".Link") != null) {
	    				mapLink = ConfigManager.sgDataCFG.getString("Maps." + game.getMap() + ".Link");
	    			} else { mapLink = "Coming Soon"; }
	            	if (game.getGamestate() == BGGameState.INGAME) {
	                timeLeft = game.getTm().inGame; } else 
	                if (game.getGamestate() == BGGameState.CLEANUP) {
	                timeLeft = game.getTm().cleanUp; } else 
	                if (game.getGamestate() == BGGameState.RESTARTING) {
	                timeLeft = game.getTm().restarting; }
	    			player.sendMessage(MessageManager.c("&fStatus&8: &e" + game.getDisplayGamestate(game.getGamestate())));
	    			player.sendMessage(MessageManager.c("&fTimeleft&8: [&e" + 
	    			game.getTm().getMins(timeLeft) + "&8] &eminutes&8, &8[&e" + 
	    			game.getTm().getSeconds(timeLeft) + "&8] &eseconds"));
	    			player.sendMessage(MessageManager.c("&fCurrent Map&8: &e" + map));
	    			player.sendMessage(MessageManager.c("&fMap Creator&8: &e" + mapAuthor));
	    			player.sendMessage(MessageManager.c("&fMap Link&8: &e" + mapLink));
	    			player.sendMessage(MessageManager.c("&fDevelopers&8: &r" 
	    			+ RankManager.getDeveloperName("Rustywolf") + "&8, &r" 
	    			+ RankManager.getDeveloperName("subv3rsion") + "&8, &r" 
	    			+ RankManager.getDeveloperName("Forairan") + "&8, &r" 
	    			+ RankManager.getDeveloperName("kpwn243") + "&8, &r" 
	    			+ RankManager.getDeveloperName("dv90") + "&8, &r" 
	    			+ RankManager.getDeveloperName("Favorlock") + "&8, &r" 
	    			+ RankManager.getDeveloperName("DV_Raiton") + "&8, &r"
	    	    	+ RankManager.getDeveloperName("5Ocal") + "&8, &r"
	    	    	+ RankManager.getDeveloperName("ThatKawaiiSam")));
	    			player.sendMessage(MessageManager.c("&fHosted by&8: &ehttps://syd.cubedhosting.com/"));
	    			player.sendMessage(MessageManager.c("&fRunning the &2" + ChatColor.stripColor(Main.main.getConfig()
	    			.getString("Prefix.Engine")).replace("(", "").replace(")", "").replace("Z", "X") + "&8.&av3 " + 
	    			Main.main.getConfig().getString("Prefix.Engine") + " &fengine&8."));
	    		    return true;
			    }
		    } else {
			    player.sendMessage(MessageManager.c(MessageManager.prefix + " &cUsage: /infomation"));
		    }
		} else {
			player.sendMessage(MessageManager.unknownCommand);
		}
        return false;
    }
	
}