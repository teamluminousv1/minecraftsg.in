package luminous.net.minecraftsg.net.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GlobalList implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("globallist")){
            if (!(commandSender instanceof Player)){
                commandSender.sendMessage("You can only execute this command as a player!");
            }
        }
        return false;
    }
}
