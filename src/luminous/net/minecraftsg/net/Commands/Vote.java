package luminous.net.minecraftsg.net.Commands;



import luminous.net.main.fiddycal.Main;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGame;

public class Vote implements CommandExecutor {
	
	public static List<String> disabled = new ArrayList<>();
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		Player player = (Player) sender;
		
		if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) == null) {
			player.sendMessage(MessageManager.unknownCommand);
			return true;
		}
		
		if (args.length == 0) {
			SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
			String m1 = game.getPreviousMap(0);
			String m2 = game.getPreviousMap(1);
			String m3 = game.getPreviousMap(2);
			String vm1 = game.getVm().getMaps().get(0);
			String vm2 = game.getVm().getMaps().get(1);
			String vm3 = game.getVm().getMaps().get(2);
			String vm4 = game.getVm().getMaps().get(3);
			String vm5 = game.getVm().getMaps().get(4);
			if (disabled.contains(vm1)) {
				vm1 = MessageManager.c("&c" + game.getVm().getMaps().get(0));
			}
			if (disabled.contains(vm2)) {
				vm2 = MessageManager.c("&c" + game.getVm().getMaps().get(1));
			}
			if (disabled.contains(vm3)) {
				vm3 = MessageManager.c("&c" + game.getVm().getMaps().get(2));
			}
			if (disabled.contains(vm4)) {
				vm4 = MessageManager.c("&c" + game.getVm().getMaps().get(3));
			}
			if (disabled.contains(vm5)) {
				vm5 = MessageManager.c("&c" + game.getVm().getMaps().get(4));
			}
			player.sendMessage(MessageManager.c(MessageManager.prefix + " &2Vote using &8[&a/vote #&8]."));
			player.sendMessage(MessageManager.c(MessageManager.prefix + " &2Previous maps played&8: &7" + m1 + "&8, &7" + m2 + "&8, &7" + m3 + "&8."));
			player.sendMessage(MessageManager.c(MessageManager.prefix + " &a1 &8> |&e "+ game.getVm().getVotes().get(game.getVm().getMaps().get(0)) +" &7Votes &8| &2" + vm1.replace("_", " ")));
			player.sendMessage(MessageManager.c(MessageManager.prefix + " &a2 &8> |&e "+ game.getVm().getVotes().get(game.getVm().getMaps().get(1)) +" &7Votes &8| &2" + vm2.replace("_", " ")));
			player.sendMessage(MessageManager.c(MessageManager.prefix + " &a3 &8> |&e "+ game.getVm().getVotes().get(game.getVm().getMaps().get(2)) +" &7Votes &8| &2" + vm3.replace("_", " ")));
			player.sendMessage(MessageManager.c(MessageManager.prefix + " &a4 &8> |&e "+ game.getVm().getVotes().get(game.getVm().getMaps().get(2)) +" &7Votes &8| &2" + vm4.replace("_", " ")));
			player.sendMessage(MessageManager.c(MessageManager.prefix + " &a5 &8> |&e "+ game.getVm().getVotes().get(game.getVm().getMaps().get(2)) +" &7Votes &8| &2" + vm5.replace("_", " ")));
		} else if (args.length == 1) {
			
			if (MessageManager.isInt(args[0])) {
				if (Integer.parseInt(args[0]) == 1
					|| Integer.parseInt(args[0]) == 2
				    || Integer.parseInt(args[0]) == 3
				    || Integer.parseInt(args[0]) == 4
				    || Integer.parseInt(args[0]) == 5) {
					if (!Main.main.getSGGameManager().getGameByPlayer(player.getName()).getVm().getVoted().contains(player.getName())) {
						Integer RealNumber = Integer.valueOf(args[0]);
						Integer Number = RealNumber - 1;
						String map = Main.main.getSGGameManager().getGameByPlayer(player.getName()).getVm().getMaps().get(Number);
						if (!disabled.contains(map)) {
							Main.main.getSGGameManager().getGameByPlayer(player.getName()).getVm().addVote(player, map);
							player.sendMessage(MessageManager.c(MessageManager.prefix + " &fYou voted for &2" + map.replace("_", " ") + " &fwith &8[&6" + Main.main.getSGGameManager().getGameByPlayer(player.getName()).getVm().getVotes().get(Main.main.getSGGameManager().getGameByPlayer(player.getName()).getVm().getMaps().get(Number)) + "&8] &fvotes&8."));
							return true;
						}else{
							player.sendMessage(MessageManager.c(MessageManager.prefix + " &4The map &e" + map.replace("_", " ") + " &4has been temporarily disabled&8."));
							return true;
						}
					}else{
						player.sendMessage(MessageManager.c(MessageManager.prefix + " &cYou have already voted&8."));
						return true;
					}
				} else {
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &cThere is no map with the number &e" + args[0] + "&8."));
					return true;
				}
			} else {
				player.sendMessage(MessageManager.c(MessageManager.prefix + " &cUsage: <number> must be a &enumeric character"));
				player.sendMessage(MessageManager.c(MessageManager.prefix + " &cUsage: /vote <number>"));
			}
			
		} else {
			player.sendMessage(MessageManager.c(MessageManager.prefix + " &cUsage: /vote <number>"));
		}
		return true;
	}
	
	
}
