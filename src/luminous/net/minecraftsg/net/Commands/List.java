package luminous.net.minecraftsg.net.Commands;

import luminous.net.minecraftsg.net.Gamemodes.BG.BGGame;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGame;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGameState;

public class List implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    	
    	if (!(sender instanceof Player)) {
			sender.sendMessage(MessageManager.c(MessageManager.prefix + " &cOnly players are able to do this command&8."));
			return true;
		}
		
		Player player = (Player) sender;

		if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
	    	if (args.length == 0) {
    			SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
    			int players = game.getPlayers().size();
    			player.sendMessage(MessageManager.c(MessageManager.prefix + " &fThere are &8[&6" + players + "&8/&624&8] &fplayers online&8."));
    			StringBuilder ingame = new StringBuilder();
    			StringBuilder watching = new StringBuilder();
    	        for (Player p : Bukkit.getOnlinePlayers()) {
    	        		java.util.List<String> list = null;
    	        		if (game.getGamestate() == SGGameState.WAITING) {
    	        			list = game.getPlayers();
    	        		} else {
    	        			list = game.getTributes();
    	        		}
    	        		if (list.contains(p.getName())) {
    	        			if (ingame.length() > 0){
        	        			ingame.append(", ");
        	                }
    	        			ingame.append(MessageManager.c("&r" + p.getDisplayName() + "&f"));
    	        		} else if (game.getSpectators().contains(p.getName())) {
    	        			if (watching.length() > 0){
    	        				watching.append(", ");
        	                }
        	        		watching.append(MessageManager.c("&r" + p.getDisplayName() + "&f"));
    	        		}
    	        }
    			player.sendMessage(MessageManager.c("&8- &f&lIngame&8: " + ingame.toString()));
    			player.sendMessage(MessageManager.c("&8- &f&lWatching&8: " + watching.toString()));
    		} else {
    			player.sendMessage(MessageManager.c(MessageManager.prefix + " &cUsage: /list"));
    		}
			return true;
    	} if (Main.main.getBGGameManager().getGameByPlayer(player.getName()) != null){
			if (args.length == 0){
				BGGame game = Main.main.getBGGameManager().getGameByPlayer(player.getName());
				int players = game.getPlayers().size();
				player.sendMessage(MessageManager.c(MessageManager.prefix + " &fThere are &8[&6" + players + "&8/&624&8] &fplayers online&8."));
				StringBuilder ingame = new StringBuilder();
				for (Player p : Bukkit.getOnlinePlayers()) {
					java.util.List<String> list = null;
					list = game.getPlayers();
					if (list.contains(p.getName())) {
						if (ingame.length() > 0) {
							ingame.append(", ");
						}
						ingame.append(MessageManager.c("&r" + p.getDisplayName() + "&f"));
					}
				}
				player.sendMessage(MessageManager.c("&8- &f&lPlayers&8: " + ingame.toString()));
			}else{
				player.sendMessage(MessageManager.c(MessageManager.prefix + " &cUsage: /list"));
			}
			return true;
		} else {
			player.sendMessage(MessageManager.unknownCommand);
		}
        return false;
    }
}
