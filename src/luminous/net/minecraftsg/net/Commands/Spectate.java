package luminous.net.minecraftsg.net.Commands;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGame;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Spectate implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("spectate")){
            if (!(sender instanceof Player)) {
                sender.sendMessage(MessageManager.c(MessageManager.prefix + " &cOnly players are able to do this command&8."));
                return true;
            }

            Player player = (Player) sender;
            if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
            	SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
                if (strings.length == 0) {
                	ArrayList<Player> players = new ArrayList<Player>();
					for (Player e : Bukkit.getOnlinePlayers()) {
						if (game.getTributes().contains(e.getName())) {
							players.add(e);
						}
					}
					Player randomPlayer = players.get(new Random().nextInt(players.size()));
					player.teleport(randomPlayer.getLocation());
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &fTeleporting to &r" + randomPlayer.getDisplayName() + "&8..."));
                } else if (strings.length == 1) {
                	if (game.getTributes().contains(strings[0])){
                        Player playertotpto = Main.main.getServer().getPlayer(strings[0]);
                        player.teleport(playertotpto.getLocation());
    					player.sendMessage(MessageManager.c(MessageManager.prefix + " &fTeleporting to &r" + playertotpto.getDisplayName() + "&8..."));
                    }else{
                        player.sendMessage(MessageManager.c(MessageManager.prefix + " &cThat player isn't in the game&8."));
                        return true;
                    }
                } else {
                    player.sendMessage(MessageManager.c(MessageManager.prefix + " &cUsage: /spectate <player>"));
                }
            }else{
                player.sendMessage(MessageManager.unknownCommand);
            }
        }
        return false;
    }
}
