package luminous.net.minecraftsg.net.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.minecraftsg.net.Gamemodes.BG.BGStats;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGStats;

public class Stats implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    	
    	if (!(sender instanceof Player)) {
			sender.sendMessage(MessageManager.c(MessageManager.prefix + " &cOnly players are able to do this command&8."));
			return true;
		}
		
		Player player = (Player) sender;
		
		if (args.length == 0) {
			if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null
				|| Main.main.getSGMakerGameManager().getGameByPlayer(player.getName()) != null) {
				SGStats.sendPlayerStats(player, player.getName());
			} else if (Main.main.getBGGameManager().getGameByPlayer(player.getName()) != null) {
				BGStats.sendPlayerStats(player, player.getName());
			} else {
				player.sendMessage(MessageManager.unknownCommand);
			}
		} else if (args.length == 1) {
			if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null
				|| Main.main.getSGMakerGameManager().getGameByPlayer(player.getName()) != null) {
				SGStats.sendPlayerStats(player, args[0].toLowerCase());
			} else if (Main.main.getBGGameManager().getGameByPlayer(player.getName()) != null) {
				player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &4Stats for this gamemode"
				+ " may not be implemented. Our Sr staff and development team are still making up our minds. "
				+ "Make sure to tell us if you think we should have stats or make this a statless gamemode."));
				BGStats.sendPlayerStats(player, args[0].toLowerCase());
			} else {
				player.sendMessage(MessageManager.unknownCommand);
			}
	    } else player.sendMessage(MessageManager.c(MessageManager.prefix + " &cUsage: /stats [player]"));
        return false;
    }
}
