package luminous.net.minecraftsg.net.Commands;

import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.main.fiddycal.Managers.StaffUtil;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SilentJoin implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("silentjoin")){
            if (!(commandSender instanceof Player)){
                return false;
            }
            Player player = (Player) commandSender;
            if (StaffUtil.isPremium(player)){
                if (isSilent(player)){
                    player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &fSilent join has been disabled&8."));
                    setSilent(false, player);
                }else{
                    player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &fSilent join has been enabled&8."));
                    setSilent(true, player);
                }
            }else{
                player.sendMessage(MessageManager.c(MessageManager.prefix + " " + MessageManager.invalidPermission));
            }
        }
        return false;
    }

    public static boolean isSilent(Player player){
        if (ConfigManager.playersCFG.get(player.getUniqueId() + ".SilentJoin") != null){
            if (ConfigManager.playersCFG.getBoolean(player.getUniqueId() + ".SilentJoin") == true){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public static void setSilent(Boolean bool, Player player){
        if (bool){
            ConfigManager.playersCFG.set(player.getUniqueId() + ".SilentJoin", true);
            ConfigManager.savePlayers();
        }else{
            ConfigManager.playersCFG.set(player.getUniqueId() + ".SilentJoin", false);
            ConfigManager.savePlayers();
        }
    }
}
