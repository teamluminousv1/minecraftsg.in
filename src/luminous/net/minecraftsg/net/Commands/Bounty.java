package luminous.net.minecraftsg.net.Commands;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGame;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGameState;
import luminous.net.minecraftsg.net.Managers.PointsManager;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;

public class Bounty implements CommandExecutor{

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("bounty")){
            if (commandSender instanceof Player){
                Player player = (Player) commandSender;
                if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null){
                    SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
                    if (game.getSpectators().contains(player.getName())) {
                        if (game.getGamestate() != SGGameState.INGAME ||
                                game.getGamestate() != SGGameState.PREDEATHMATCH ||
                                game.getGamestate() != SGGameState.DEATHMATCH) {

                            if (strings.length == 2) {
                                if (game.getTributes().contains(strings[0])) {
                                    if (MessageManager.isInt(strings[1])) {
                                        if (Integer.valueOf(strings[1]) > 9) {
                                            if (PointsManager.usePoints(player, Integer.valueOf(strings[1])) == true) {
                                                Integer points = Integer.valueOf(strings[1]);
                                                Player giveTo = Main.main.getServer().getPlayer(strings[0]);
                                                game.getBm().addBounty(giveTo, points);
                                                game.messageEveryone(MessageManager.c(MessageManager.prefix + " &3Bounty has been set on " + giveTo.getDisplayName() + " &3by " + player.getDisplayName() + " &3for &8[&e" + game.getBm().getBounty(giveTo) + "&8] &3points."));
                                                giveTo.getScoreboard().getObjective(DisplaySlot.BELOW_NAME).setDisplaySlot(null);
                                                Objective obj = giveTo.getScoreboard().registerNewObjective("undername", "dummy");
                                                obj.setDisplaySlot(DisplaySlot.BELOW_NAME);
                                                obj.setDisplayName(MessageManager.c("&3Bounty"));
                                                obj.getScore(giveTo.getName()).setScore(Main.main.getSGGameManager().getGameByPlayer(player.getName()).getBm().getBounty(player));
                                            } else {
                                                player.sendMessage(MessageManager.c(MessageManager.prefix + " &4You do not have enough sufficient points&8."));
                                            }
                                        } else {
                                            player.sendMessage(MessageManager.c(MessageManager.prefix + " &cBounties must be higher than &8[&e10&8] &cpoints&8."));
                                        }
                                    } else {
                                        player.sendMessage(MessageManager.c(MessageManager.prefix + " &cPlease enter in a valid number&8."));
                                    }
                                } else {
                                    player.sendMessage(MessageManager.c(MessageManager.prefix + " &cThis player is not in the game&8."));
                                }
                            } else {
                                player.sendMessage(MessageManager.c(MessageManager.prefix + " &cUsage: /bounty <player> <amount>"));
                            }
                        }else{
                            player.sendMessage(MessageManager.c(MessageManager.prefix + " &c You can not currently bounty people!"));
                        }
                    }else{
                        player.sendMessage(MessageManager.c(MessageManager.prefix + " &4You must be a spectator to bounty players&8."));
                    }
                }else{
                    player.sendMessage(MessageManager.unknownCommand);
                }
            }else{
                commandSender.sendMessage(MessageManager.c(MessageManager.prefix + " &cOnly players are able to do this command&8."));
            }
        }
        return false;
    }
}
