package luminous.net.minecraftsg.net.Commands;

import java.util.List;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.main.fiddycal.Managers.RankManager;
import luminous.net.main.fiddycal.Managers.WorldManager;
import luminous.net.minecraftsg.net.Gamemodes.BG.BGGame;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGame;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGameState;
import luminous.net.minecraftsg.net.Gamemodes.SGMaker.SGMakerGame;

public class Admin implements CommandExecutor {
	
	private void sendUsage(Player player, String usage) {
		if (usage == null) {
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cUsage: <required> [optional] (tip)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin giverank <player> <rank>"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin sethubspawn"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c&l --- World Commands --- "));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin world create <world> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin world delete <world> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin world teleport <world> [x] [y] [z] (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c&l --- Survival Games Commands --- "));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin sg create <map> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin sg delete <map> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin sg setlobby <id> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin sg setspawn <number> <map> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin sg setdmspawn <number> <map> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin sg setspecspawn <map> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin sg setdmmid <radius> <map> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin sg setauthor <person/s> <map> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin sg setlink <httpLink> <map> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin sg changechests <map> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin sg save <world> <map> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin sg forcestart"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c&l --- BattleGrounds Commands --- "));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin bg create <map> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin bg delete <map> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin bg setlobby <id> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin bg setwarp <direction> <map> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin bg setauthor <person/s> <map> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin bg setlink <httpLink> <map> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin bg save <world> <map> (\"_\" = space)"));
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &c/admin dev arrays"));
			return;
		} else {
			player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cUsage: /admin " + usage));
			return;
		}
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length > 1) {
			if (!args[0].equalsIgnoreCase("giverank")) {
				if (!(sender instanceof Player)) {
					sender.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cOnly players are able to do this command&8."));
					return true;
				}
			}
		}
		
		Player player = (Player) sender;
		FileConfiguration config = ConfigManager.playersCFG;
		
		if (player.getName().equals("5Ocal")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("developer")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("admin")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("leaddeveloper")
				|| config.getString(player.getUniqueId().toString() + ".Group").equals("owner")) {
			
			if (args.length > 1) {
				
				if (args[0].equalsIgnoreCase("giverank")) {
					
					if (args.length == 3) {
						RankManager.giveRank(sender, args[1], args[2], false);
						return true;
					} else if (args.length == 4) {
						if (args[3].equalsIgnoreCase("-s")) {
							RankManager.giveRank(sender, args[1], args[2], true);
							return true;
						} else {
							sendUsage(player, "giverank <player> <rank> [-s]");
						}
					} else {
						sendUsage(player, "giverank <player> <rank> [-s]");
					}
					
				}  else if (args[1].equalsIgnoreCase("sethubspawn")) {
					if (args.length == 2) {
						String spawn = MessageManager.c(
						"&e" + player.getLocation().getBlockX() + .5 + "&7, " +
						"&e" + player.getLocation().getBlockY() + "&7, " +
					    "&e" + player.getLocation().getBlockZ() + .5);
						Main.main.getConfig().set("Spawn.world", player.getWorld().getName());
						Main.main.getConfig().set("Spawn.x", player.getLocation().getBlockX());
						Main.main.getConfig().set("Spawn.y", player.getLocation().getBlockY());
						Main.main.getConfig().set("Spawn.z", player.getLocation().getBlockZ());
						Main.main.saveConfig();
						player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bYou have set the hub spawn&8: &7(&e" + player.getWorld().getName() + "&7) &e" + spawn + "&8."));
						return true;
					} else {
						sendUsage(player, "sethubspawn");
					}
				} else if (args[0].equalsIgnoreCase("sg")) {

					if (args[1].equalsIgnoreCase("create")) {
						if (args.length == 3) {
							if (player.getWorld().equals(Bukkit.getWorld(Main.main.getConfig().getString("Spawn.world")))) {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cYou cannot do this in the hub&8!"));
								return true;
							}
							if (ConfigManager.getSGMapData(args[2]) != null) {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThe map &e" + args[2].replace("_", " ") + " &balready exists&8."));
								return true;
							}
							ConfigManager.createSGMapData(args[2]);
							ConfigManager.saveSGMapData();
							player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bYou have created the map&8: &e" + args[2].replace("_", " ") + "&8."));
							return true;
						} else {
							sendUsage(player, "sg create <map>");
						}
					} else if (args[1].equalsIgnoreCase("remove")) {
						if (args.length == 3) {
							if (ConfigManager.getSGMapData(args[2]) == null) {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThe map &e" + args[2].replace("_", " ") + " &bdoesn't exist&8."));
								return true;
							}
							ConfigManager.sgDataCFG.set("Lobbies." + args[2], null);
							ConfigManager.removeSGMapData(args[2]);
							ConfigManager.saveSGData();
							player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bYou have removed the map&8: &e" + args[2].replace("_", " ") + "&8."));
							return true;
						} else {
							sendUsage(player, "sg remove <map>");
						}
					} else if (args[1].equalsIgnoreCase("setlobbyspawn")) {
						if (args.length == 3){
							if (ConfigManager.getSGMapData(args[2]) == null) {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThe map &e" + args[2].replace("_", " ") + " &bdoesn't exist&8."));
								return true;
							}
							String spawn = MessageManager.c(
							"&e" + player.getLocation().getBlockX() + "&7, " +
							"&e" + player.getLocation().getBlockY() + "&7, " +
						    "&e" + player.getLocation().getBlockZ());
							ConfigManager.sgDataCFG.set("Lobbies." + args[2] + ".Spawn.world", player.getWorld().getName());
							ConfigManager.sgDataCFG.set("Lobbies." + args[2] + ".Spawn.x", player.getLocation().getBlockX());
							ConfigManager.sgDataCFG.set("Lobbies." + args[2] + ".Spawn.y", player.getLocation().getBlockY());
							ConfigManager.sgDataCFG.set("Lobbies." + args[2] + ".Spawn.z", player.getLocation().getBlockZ());
							ConfigManager.saveSGData();
							player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bSet the lobby for id &e" + args[2].replace("_", " ") + "&8: &7(&e" + player.getWorld().getName() + "&7) &e" + spawn + "&8."));
							return true;
						}
					} else if (args[1].equalsIgnoreCase("setspawn")) {
						if (args.length == 4) {
							
							if (args[2].equalsIgnoreCase("1")
								|| args[2].equalsIgnoreCase("2")
								|| args[2].equalsIgnoreCase("3")
								|| args[2].equalsIgnoreCase("4")
								|| args[2].equalsIgnoreCase("5")
								|| args[2].equalsIgnoreCase("6")
								|| args[2].equalsIgnoreCase("7")
								|| args[2].equalsIgnoreCase("8")
								|| args[2].equalsIgnoreCase("9")
								|| args[2].equalsIgnoreCase("10")
								|| args[2].equalsIgnoreCase("11")
								|| args[2].equalsIgnoreCase("12")
								|| args[2].equalsIgnoreCase("13")
								|| args[2].equalsIgnoreCase("14")
								|| args[2].equalsIgnoreCase("15")
								|| args[2].equalsIgnoreCase("16")
								|| args[2].equalsIgnoreCase("17")
								|| args[2].equalsIgnoreCase("18")
								|| args[2].equalsIgnoreCase("19")
								|| args[2].equalsIgnoreCase("20")
								|| args[2].equalsIgnoreCase("21")
								|| args[2].equalsIgnoreCase("22")
								|| args[2].equalsIgnoreCase("23")
								|| args[2].equalsIgnoreCase("24")) {

								if (ConfigManager.getSGMapData(args[3]) == null) {
									player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThe map &e" + args[3].replace("_", " ") + " &bdoesn't exist&8."));
									return true;
								}
							    ConfigManager.sgDataCFG.set("Maps." + args[3] + ".sp." + args[2] + ".x", player.getLocation().getBlockX());
								ConfigManager.sgDataCFG.set("Maps." + args[3] + ".sp." + args[2] + ".y", player.getLocation().getBlockY());
								ConfigManager.sgDataCFG.set("Maps." + args[3] + ".sp." + args[2] + ".z", player.getLocation().getBlockZ());
								ConfigManager.sgDataCFG.set("Maps." + args[3] + ".sp." + args[2] + ".yaw", player.getEyeLocation().getYaw());
								ConfigManager.sgDataCFG.set("Maps." + args[3] + ".sp." + args[2] + ".pitch", player.getEyeLocation().getPitch());
								ConfigManager.saveSGData();
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bSet spawn &8[&c" + args[2] + "&8] &bfor the map&8: &e" + args[3].replace("_", " ") + "&8."));
								return true;
							} else {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cUsage: <number> must be a &enumber &cbelow < 25"));
								sendUsage(player, "sg setspawn <number> <map>");
							}
							
						} else {
							sendUsage(player, "sg setspawn <number> <map>");
						}
					} else if (args[1].equalsIgnoreCase("setdmspawn")) {
						if (args.length == 4) {
							
							if (args[2].equalsIgnoreCase("1")
								|| args[2].equalsIgnoreCase("2")
								|| args[2].equalsIgnoreCase("3")
								|| args[2].equalsIgnoreCase("4")
								|| args[2].equalsIgnoreCase("5")
								|| args[2].equalsIgnoreCase("6")
								|| args[2].equalsIgnoreCase("7")
								|| args[2].equalsIgnoreCase("8")
								|| args[2].equalsIgnoreCase("9")
								|| args[2].equalsIgnoreCase("10")
								|| args[2].equalsIgnoreCase("11")
								|| args[2].equalsIgnoreCase("12")
								|| args[2].equalsIgnoreCase("13")
								|| args[2].equalsIgnoreCase("14")
								|| args[2].equalsIgnoreCase("15")
								|| args[2].equalsIgnoreCase("16")
								|| args[2].equalsIgnoreCase("17")
								|| args[2].equalsIgnoreCase("18")
								|| args[2].equalsIgnoreCase("19")
								|| args[2].equalsIgnoreCase("20")
								|| args[2].equalsIgnoreCase("21")
								|| args[2].equalsIgnoreCase("22")
								|| args[2].equalsIgnoreCase("23")
								|| args[2].equalsIgnoreCase("24")) {
								
								if (ConfigManager.getSGMapData(args[3]) == null) {
									player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThe map &e" + args[3].replace("_", " ") + " &bdoesn't exist&8."));
									return true;
								}
								ConfigManager.sgDataCFG.set("Maps." + args[3] + ".dm." + args[2] + ".x", player.getLocation().getBlockX());
								ConfigManager.sgDataCFG.set("Maps." + args[3] + ".dm." + args[2] + ".y", player.getLocation().getBlockY());
								ConfigManager.sgDataCFG.set("Maps." + args[3] + ".dm." + args[2] + ".z", player.getLocation().getBlockZ());
								ConfigManager.sgDataCFG.set("Maps." + args[3] + ".dm." + args[2] + ".yaw", player.getEyeLocation().getYaw());
								ConfigManager.sgDataCFG.set("Maps." + args[3] + ".dm." + args[2] + ".pitch", player.getEyeLocation().getPitch());
								ConfigManager.saveSGData();
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bSet deathmatch spawn &8[&c" + args[2] + "&8] &bfor the map&8: &e" + args[3].replace("_", " ") + "&8."));
								return true;
							} else {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cUsage: <number> must be a &enumeric character &c< 25"));
								sendUsage(player, "sg setdmspawn <number> <map>");
							}
							
						} else {
							player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cUsage: <number> must be a &enumeric character &c< 25"));
							sendUsage(player, "sg setdmspawn <number> <map>");
						}
					} else if (args[1].equalsIgnoreCase("setspecspawn")) {
						if (args.length == 3) {
							if (ConfigManager.getSGMapData(args[2]) == null) {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThe map &e" + args[3].replace("_", " ") + " &bdoesn't exist&8."));
								return true;
							}
							String spawn = MessageManager.c(
						    "&e" + player.getLocation().getBlockX() + .5 + "&7, " +
							"&e" + player.getLocation().getBlockY() + "&7, " +
						    "&e" + player.getLocation().getBlockZ() + .5);
							ConfigManager.sgDataCFG.set("Maps." + args[2] + ".spec.x", player.getLocation().getBlockX());
							ConfigManager.sgDataCFG.set("Maps." + args[2] + ".spec.y", player.getLocation().getBlockY());
							ConfigManager.sgDataCFG.set("Maps." + args[2] + ".spec.z", player.getLocation().getBlockZ());
							ConfigManager.saveSGData();
							player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bSet spectator spawn for map &e" + args[2].replace("_", " ") + "&8: &e" + spawn + "&8."));
							return true;
						} else {
							sendUsage(player, "sg setspecspawn <map>");
						}
					} else if (args[1].equalsIgnoreCase("setdmmid")) {
						if (args.length == 4) {
							if (MessageManager.isInt(args[2])) {
								if (ConfigManager.getSGMapData(args[3]) == null) {
									player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThe map &e" + args[3].replace("_", " ") + " &bdoesn't exist&8."));
									return true;
								}
								String spawn = MessageManager.c(
							    "&e" + player.getLocation().getBlockX() + .5 + "&7, " +
								"&e" + player.getLocation().getBlockY() + "&7, " +
							    "&e" + player.getLocation().getBlockZ() + .5);
								ConfigManager.sgDataCFG.set("Maps." + args[3] + ".dm.center.x", player.getLocation().getBlockX());
								ConfigManager.sgDataCFG.set("Maps." + args[3] + ".dm.center.z", player.getLocation().getBlockZ());
								ConfigManager.sgDataCFG.set("Maps." + args[3] + ".dm.center.radius", Integer.parseInt(args[2]));
								ConfigManager.saveSGData();
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bSet deathmatch center for map &e" + args[3].replace("_", " ") + "&8: &7(&e" + args[2] + "x&7) &e" + spawn + "&8."));
								return true;
							} else {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cUsage: <radius> must be a &enumeric character"));
								sendUsage(player, "sg setdmmid <radius> <map>");
							}
						} else {
							sendUsage(player, "sg setdmmid <radius> <map>");
						}
					} if (args[1].equalsIgnoreCase("setauthor")){
						if (args.length == 4){
							if (ConfigManager.getSGMapData(args[3]) == null) {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThe map &e" + args[3].replace("_", " ") + " &bdoesn't exist&8."));
								return true;
							}
							ConfigManager.sgDataCFG.set("Maps." + args[3] + ".Author", args[2].replace("_", " "));
							ConfigManager.saveSGData();
							player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bAuthor(s) of the map &e" + args[3] + "&8, &bset to&8: &e" + args[2].replace("_", " ") + "&8."));
							return true;
						}else{
							sendUsage(player, "sg setauthor <person/s> <map>");
						}
					} else if (args[1].equalsIgnoreCase("setlink")){
						if (args.length == 4){
							if (ConfigManager.getSGMapData(args[3]) == null) {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThe map &e" + args[3].replace("_", " ") + " &bdoesn't exist&8."));
								return true;
							}
							ConfigManager.sgDataCFG.set("Maps." + args[3] + ".Link", args[2].replace("_", " "));
							ConfigManager.saveSGData();
							player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bMap link of the map &e" + args[3] + "&8, &bset to&8: &e" + args[2].replace("_", " ") + "&8."));
							return true;
						}else{
							sendUsage(player, "sg setlink <httpLink> <map>");
						}
					} else if (args[1].equalsIgnoreCase("save")) {
						if (args.length == 4){
							if (ConfigManager.getSGMapData(args[3]) == null) {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThe map &e" + args[3].replace("_", " ") + " &bdoesn't exist&8."));
							    return true;
						    }
							if (Bukkit.getWorld(args[2]) == null) {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThe world &e" + args[3].replace("_", " ") + " &bdoesn't exist&8."));
							    return true;
							}
							player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bSending files of world &e" + args[2].replace("_", " ") + " &bto the map&8: &e" + args[3].replace("_", " ") + "&8."));
							WorldManager.copyWorld("plugins/SGIN-Core/SurvivalGames/Maps/", "plugins/SGIN-Core/SurvivalGames/Maps-old/", args[3] + "_before-save");
							WorldManager.copyWorld(args[2], "plugins/SGIN-Core/SurvivalGames/Maps/", args[3]);
							player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bFinished backup of the map&8: &e" + args[3].replace("_", " ") + "&8."));
							return true;
						} else {
							sendUsage(player, "sg save <world> <map>");
						}
					} else if (args[1].equalsIgnoreCase("forcestart")){
						if (args.length == 2){
							if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
								SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
								if (game.getGamestate() != SGGameState.WAITING) {
									player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &4The game you are in has already started&8."));
									return true;
								}
								game.setForceStart(true);
								game.messageEveryone(MessageManager.c(MessageManager.prefix + " &cThe Game has been set to forcestart&8."));
								if (game.getTm().waitingTime > 20){
									game.getTm().waitingTime = 10;
								}
							} else {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &4You are not currently in a game&8."));
							}
							return true;
						}else {
							sendUsage(player, null);
					    }
					}else {
						sendUsage(player, null);
					}
				/*

				Battlegrounds Stuff

				*/
				} else if (args[0].equalsIgnoreCase("bg")) {

					if (args[1].equalsIgnoreCase("create")) {
						if (args.length == 3) {
							if (player.getWorld().equals(Bukkit.getWorld(Main.main.getConfig().getString("Spawn.world")))) {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cYou cannot do this in the hub&8!"));
								return true;
							}
							if (ConfigManager.doesBGMapExist(args[2])) {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThe map &e" + args[2].replace("_", " ") + " &balready exists&8."));
								return true;
							}
							ConfigManager.createBGMapData(args[2]);
							ConfigManager.saveBGMapData();
							player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bYou have created the map&8: &e" + args[2].replace("_", " ") + "&8."));
							return true;
						} else {
							sendUsage(player, "bg create <map>");
						}
					} else if (args[1].equalsIgnoreCase("remove")) {
						if (args.length == 3) {
							if (ConfigManager.getBGMapData(args[2]) == null) {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThe map &e" + args[2].replace("_", " ") + " &bdoesn't exist&8."));
								return true;
							}
							ConfigManager.bgDataCFG.set("Maps." + args[2], null);
							ConfigManager.removeBGMapData(args[2]);
							ConfigManager.saveBGData();
							player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bYou have removed the map&8: &e" + args[2].replace("_", " ") + "&8."));
							return true;
						} else {
							sendUsage(player, "bg remove <map>");
						}
					} else if (args[1].equalsIgnoreCase("setlobby")) {
						if (args.length == 3){
							if (!(ConfigManager.doesBGMapExist(args[2]))) {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThe map &e" + args[2].replace("_", " ") + " &bdoesn't exist&8."));
								return true;
							}
							ConfigManager.bgDataCFG.set("Maps." + args[2] + ".waiting.x", player.getLocation().getBlockX());
							ConfigManager.bgDataCFG.set("Maps." + args[2] + ".waiting.y", player.getLocation().getBlockY());
							ConfigManager.bgDataCFG.set("Maps." + args[2] + ".waiting.z", player.getLocation().getBlockZ());
							ConfigManager.bgDataCFG.set("Maps." + args[2] + ".waiting.yaw", player.getEyeLocation().getYaw());
							ConfigManager.bgDataCFG.set("Maps." + args[2] + ".waiting.pitch", player.getEyeLocation().getPitch());
							ConfigManager.saveSGData();
							player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bSet the lobby for Map &e" + args[2] + "&8."));
							return true;
						}
					} else if (args[1].equalsIgnoreCase("setwarp")) {
						if (args.length == 4) {

							if (args[2].equalsIgnoreCase("North")
									|| args[2].equalsIgnoreCase("South")
									|| args[2].equalsIgnoreCase("East")
									|| args[2].equalsIgnoreCase("West")
									|| args[2].equalsIgnoreCase("Center")) {

								if (ConfigManager.getBGMapData(args[3]) == null) {
									player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThe map &e" + args[3].replace("_", " ") + " &bdoesn't exist&8."));
									return true;
								}
								ConfigManager.bgDataCFG.set("Maps." + args[3] + "."+ args[2] + ".x", player.getLocation().getBlockX());
								ConfigManager.bgDataCFG.set("Maps." + args[3] +"."+ args[2] + ".y", player.getLocation().getBlockY());
								ConfigManager.bgDataCFG.set("Maps." + args[3] +"."+ args[2] + ".z", player.getLocation().getBlockZ());
								ConfigManager.bgDataCFG.set("Maps." + args[3] +"."+ args[2] + ".yaw", player.getEyeLocation().getYaw());
								ConfigManager.bgDataCFG.set("Maps." + args[3] +"."+ args[2] + ".pitch", player.getEyeLocation().getPitch());
								ConfigManager.saveBGData();
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bSet spawn &8[&c" + args[2] + "&8] &bfor the map&8: &e" + args[3].replace("_", " ") + "&8."));
								return true;
							} else {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cUsage: <direction> must be &eNorth, South, East, West, Center&c!"));
								sendUsage(player, "bg setspawn <number> <map>");
							}
						} else {
							sendUsage(player, "sg setspawn <number> <map>");
						}
					} else if (args[1].equalsIgnoreCase("setlink")){
						if (args.length == 4){
							if (ConfigManager.getSGMapData(args[3]) == null) {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThe map &e" + args[3].replace("_", " ") + " &bdoesn't exist&8."));
								return true;
							}
							ConfigManager.sgDataCFG.set("Maps." + args[3] + ".Link", args[2].replace("_", " "));
							ConfigManager.saveSGData();
							player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bMap link of the map &e" + args[3] + "&8, &bset to&8: &e" + args[2].replace("_", " ") + "&8."));
							return true;
						}else{
							sendUsage(player, "sg setlink <httpLink> <map>");
						}
					} else if (args[1].equalsIgnoreCase("save")) {
						if (args.length == 4){
							if (ConfigManager.getBGMapData(args[3]) == null) {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThe map &e" + args[3].replace("_", " ") + " &bdoesn't exist&8."));
								return true;
							}
							if (Bukkit.getWorld(args[2]) == null) {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bThe world &e" + args[3].replace("_", " ") + " &bdoesn't exist&8."));
								return true;
							}
							player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bSending files of world &e" + args[2].replace("_", " ") + " &bto the map&8: &e" + args[3].replace("_", " ") + "&8."));
							WorldManager.copyWorld("plugins/SGIN-Core/BattleGrounds/Maps/", "plugins/SGIN-Core/SurvivalGames/Maps-old/", args[3] + "_before-save");
							WorldManager.copyWorld(args[2], "plugins/SGIN-Core/BattleGrounds/Maps/", args[3]);
							player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bFinished backup of the map&8: &e" + args[3].replace("_", " ") + "&8."));
							return true;
						} else {
							sendUsage(player, "bg save <world> <map>");
						}
					}
				/*

				World Commands

				*/
				}else if (args[0].equalsIgnoreCase("world")) {
					if (args[1].equalsIgnoreCase("create")) {
						if (args.length == 3) {
							if (Bukkit.getWorld(args[2]) == null) {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bCreating world&8: &e" + args[2].replace("_", " ") + "&8."));
								Bukkit.getServer().createWorld(new WorldCreator(args[2]));
								if (Main.main.getConfig().getStringList("Worlds") == null) {
									Main.main.getConfig().set("Worlds", null);
								}
								List<String> worlds = Main.main.getConfig().getStringList("Worlds");
								worlds.add(args[2]);
								if (!worlds.contains(args[2])) {
									Main.main.getConfig().set("Worlds", worlds);
									Main.main.saveConfig();
								}
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bYou have created the world&8: &e" + args[2].replace("_", " ") + "&8."));
								return true;
							} else {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cThe world &e" + args[2].replace("_", " ") + " &calready exists&8."));
							}
						} else {
							sendUsage(player, "world create <world>");
						}
					} else if (args[1].equalsIgnoreCase("delete")) {
						if (args.length == 3) {
							if (Bukkit.getWorld(args[2]) != null) {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bDeleting world&8: &e" + args[2].replace("_", " ") + "&8."));
							    WorldManager.deleteWorld("", args[2]);
						        List<String> worlds = Main.main.getConfig().getStringList("Worlds");
								worlds.remove(args[2]);
								Main.main.getConfig().set("Worlds", worlds);
								Main.main.saveConfig();
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bYou have deleted the world&8: &e" + args[2].replace("_", " ") + "&8."));
								return true;
							} else {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cThe world &e" + args[2].replace("_", " ") + " &cdoesn't exist&8."));
							}
						} else {
							sendUsage(player, "world delete <world>");
						}
					} else if (args[1].equalsIgnoreCase("teleport")) {
						if (args.length == 3) {
							if (Bukkit.getWorld(args[2]) != null) {
								double x = 0.5;
								double y = 100;
								double z = 0.5;
								Location loc = new Location(Bukkit.getWorld(args[2]), x, y, z);
								player.teleport(loc);
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bYou have been teleported to&8: &e" + args[2].replace("_", " ") + "&8. (&e" + x + "&7, &e" + y + "&7, &e" + z + "&8)"));
								return true;
							} else {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cThe world &e" + args[2].replace("_", " ") + " &cdoesn't exist&8."));
							}
						} else if (args.length == 6) {
							if (MessageManager.isInt(args[3])
									&& MessageManager.isInt(args[4])
									&& MessageManager.isInt(args[5])) {
								double x = Integer.parseInt(args[3]);
								double y = Integer.parseInt(args[4]);
								double z = Integer.parseInt(args[5]);
								Location loc = new Location(Bukkit.getWorld(args[2]), x, y, z);
								player.teleport(loc);
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &bYou have been teleported to&8: &e" + args[2].replace("_", " ") + "&8. (&e" + x + "&7, &e" + y + "&7, &e" + z + "&8)"));
								return true;
							} else {
								player.sendMessage(MessageManager.c(MessageManager.enginePrefix + " &cUsage: [x], [y] and [z] must be &enumeric characters"));
						    	sendUsage(player, "world teleport <world> [x] [y] [z]");
							}
						} else {
							sendUsage(player, "world teleport <world> [x] [y] [z]");
						}
					} else {
						sendUsage(player, null);
					}
				} else if (args[0].equalsIgnoreCase("dev")) {
					if (args[1].equalsIgnoreCase("arrays")) {
						if (args.length == 2) {
							if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null){
								SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
								player.sendMessage("Type: SG");
								player.sendMessage("ID: " + game.getID());
								player.sendMessage("ZimeID: " + game.getZimeID());
								player.sendMessage("Map: " + game.getMap());
								player.sendMessage("MapPool: " + game.getVm().getMaps());
								player.sendMessage("State: " + game.getGamestate());
								player.sendMessage("Players: " + game.getPlayers());
								player.sendMessage("Tributes: " + game.getTributes());
								player.sendMessage("Spectators: " + game.getSpectators());
								return true;
							}else if (Main.main.getSGMakerGameManager().getGameByPlayer(player.getName()) != null){
								SGMakerGame game = Main.main.getSGMakerGameManager().getGameByPlayer(player.getName());
								player.sendMessage("Type: SGM");
								player.sendMessage("ID: " + game.getID());
								player.sendMessage("ZimeID: " + game.getZimeID());
								player.sendMessage("Map: " + game.getMap());
								player.sendMessage("MapPool: " + game.getVm().getMaps());
								player.sendMessage("State: " + game.getGamestate());
								player.sendMessage("Players: " + game.getPlayers());
								player.sendMessage("Tributes: " + game.getTributes());
								player.sendMessage("Spectators: " + game.getSpectators());
								return true;
							}else if (Main.main.getBGGameManager().getGameByPlayer(player.getName()) != null){
								BGGame game = Main.main.getBGGameManager().getGameByPlayer(player.getName());
								player.sendMessage("Type: BG");
								player.sendMessage("ID: " + game.getID());
								player.sendMessage("ZimeID: " + game.getZimeID());
								player.sendMessage("Map: " + game.getMap());
								player.sendMessage("State: " + game.getGamestate());
								player.sendMessage("Players: " + game.getPlayers());
								player.sendMessage("Fighting: " + game.getFighting());
								player.sendMessage("Waiting: " + game.getWaiting());
								player.sendMessage("Kills-Hashmap: " + game.getKills());
								return true;
							}else{
								sender.sendMessage("Zime Gamemodes:");
								for (int i = 0; i < Main.main.getZimeGames().getGames().size(); i++) {
									String[] spl = Main.main.getZimeGames().getGames().get(i).split("-");
									sender.sendMessage("ZimeGameID: " + Main.main.getZimeGames().getZimeID(Main.main.getZimeGames().getGames().get(i)) + "   GameMode: " + spl[0] + "  GamemodeID:" +  spl[1]);
								}
							}
						} else {
							sendUsage(player, "dev arrays");
						}
					} else {
						sendUsage(player, null);
					}
				} else {
					sendUsage(player, null);
				}
			
		    } else {
			    sendUsage(player, null);
		    }
	    } else {
		    player.sendMessage(MessageManager.enginePrefix + " " + MessageManager.invalidPermission);
	    }
		return true;
		
	}
	
}
