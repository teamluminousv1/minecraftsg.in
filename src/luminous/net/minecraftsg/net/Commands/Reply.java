package luminous.net.minecraftsg.net.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGame;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGameState;
import luminous.net.minecraftsg.net.Gamemodes.SGMaker.SGMakerGame;
import luminous.net.minecraftsg.net.Gamemodes.SGMaker.SGMakerGameState;

public class Reply implements CommandExecutor {

	@Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (!(sender instanceof Player)) {
			sender.sendMessage(MessageManager.c(MessageManager.prefix + " &cOnly players are able to do this command&8."));
			return true;
		}
		
		Player player = (Player) sender;
		
		if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) == null
			&& Main.main.getSGMakerGameManager().getGameByPlayer(player.getName()) == null) {
			player.sendMessage(MessageManager.unknownCommand);
			return true;
		}
		
		if (args.length > 0) {
			if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
				SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
				if (game.getGamestate() != SGGameState.WAITING) {
					StringBuilder sb = new StringBuilder();
		        	for (int i = 0; i < args.length; i++) {
		        	    sb.append(args[i]).append(" ");
		        	}
		        	if (Main.main.getServer().getPlayer(Message.messaged.get(player.getName())) != null) {
		        		if (game.getPlayers().contains(Main.main.getServer().getPlayer(Message.messaged.get(player.getName())).getName())) {
		        			if (game.getTributes().contains(Main.main.getServer().getPlayer(Message.messaged.get(player.getName())).getName())) {
		        				if (Main.main.getServer().getPlayer(Message.messaged.get(player.getName())).getLocation().distance(player.getLocation()) < 11) {
		        					Message.sendMessage(Main.main.getServer().getPlayer(Message.messaged.get(player.getName())), player, sb.toString());
		        					return true;
		        				} else {
		        					player.sendMessage(MessageManager.c(MessageManager.prefix + " &7You must be within &8[&610&8] &7blocks of the player you would like to message&8."));
		        				}
			        		} else {
			        			player.sendMessage(MessageManager.c(MessageManager.prefix + " &cThat player must is not ingame&8."));
			        		}
		        		} else {
		        			player.sendMessage(MessageManager.c(MessageManager.prefix + " " + MessageManager.playerNotOnline));
		        		}
		        		return true;
		        	} else {
		        		player.sendMessage(MessageManager.c(MessageManager.prefix + " " + MessageManager.playerNotOnline));
		        	}
				} else {
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &cYou can not do that in waiting lobbies&8."));
				}
			} else if (Main.main.getSGMakerGameManager().getGameByPlayer(player.getName()) != null) {
				SGMakerGame game = Main.main.getSGMakerGameManager().getGameByPlayer(player.getName());
				if (game.getGamestate() != SGMakerGameState.WAITING) {
					StringBuilder sb = new StringBuilder();
		        	for (int i = 0; i < args.length; i++) {
		        	    sb.append(args[i]).append(" ");
		        	}
		        	if (Main.main.getServer().getPlayer(Message.messaged.get(player.getName())) != null) {
		        		if (game.getPlayers().contains(Main.main.getServer().getPlayer(Message.messaged.get(player.getName())).getName())) {
		        			if (game.getTributes().contains(Main.main.getServer().getPlayer(Message.messaged.get(player.getName())).getName())) {
		        				if (Main.main.getServer().getPlayer(Message.messaged.get(player.getName())).getLocation().distance(player.getLocation()) < 11) {
		        					Message.sendMessage(Main.main.getServer().getPlayer(Message.messaged.get(player.getName())), player, sb.toString());
		        					return true;
		        				} else {
		        					player.sendMessage(MessageManager.c(MessageManager.prefix + " &7You must be within &8[&610&8] &7blocks of the player you would like to message&8."));
		        				}
			        		} else {
			        			player.sendMessage(MessageManager.c(MessageManager.prefix + " &cThat player must is not ingame&8."));
			        		}
		        		} else {
		        			player.sendMessage(MessageManager.c(MessageManager.prefix + " " + MessageManager.playerNotOnline));
		        		}
		        		return true;
		        	} else {
		        		player.sendMessage(MessageManager.c(MessageManager.prefix + " " + MessageManager.playerNotOnline));
		        	}
				} else {
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &cYou can not do that in waiting lobbies&8."));
				}
			}
		} else {
			player.sendMessage(MessageManager.c(MessageManager.prefix + " &cUsage: /reply <message>"));
		}
		return true;
		
	}
	
}
