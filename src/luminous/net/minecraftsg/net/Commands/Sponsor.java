package luminous.net.minecraftsg.net.Commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import luminous.net.main.fiddycal.Main;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGSponsorManager;
import luminous.net.minecraftsg.net.Managers.PointsManager;

public class Sponsor implements CommandExecutor, Listener {
	
	public static ArrayList<String> world = new ArrayList<String>();
	public static HashMap<String, Boolean> item = new HashMap<String, Boolean>();
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		Player player = (Player) sender;

		if (args.length == 1) {

			if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
				if (Main.main.getSGGameManager().getGameByPlayer(player.getName()).getSpectators().contains(player.getName())) {
					if (Main.main.getSGGameManager().getGameByPlayer(player.getName()).getTributes().contains(args[0])) {
						SGSponsorManager sp = Main.main.getSGGameManager().getGameByPlayer(player.getName()).getSM();
						Inventory sponsorInv = Bukkit.createInventory(null, 9, MessageManager.c("Sponsor: " + args[0]));
						//If Enderpearl is still avaliable
						if (sp.getItemAvaliab("Enderpearl")) {
							Integer price = sp.getItemPrice("Enderpearl");
							ItemStack endepearl = new ItemStack(Material.ENDER_PEARL, 1);
							ItemMeta endepearlMeta = endepearl.getItemMeta();
							List<String> endepearlLore = new ArrayList<String>();
							endepearlLore.add(MessageManager.c("&aPrice: &e" + price));
							endepearlMeta.setLore(endepearlLore);
							endepearl.setItemMeta(endepearlMeta);
							sponsorInv.setItem(0, endepearl);
						}
						//If FNS is still avaliable
						if (sp.getItemAvaliab("FNS")) {
							Integer price = sp.getItemPrice("FNS");
							ItemStack fns = new ItemStack(Material.FLINT_AND_STEEL, 1);
							ItemMeta fnsMeta = fns.getItemMeta();
							List<String> fnsLore = new ArrayList<String>();
							fnsLore.add(MessageManager.c("&aPrice: &e" + price));
							fnsMeta.setLore(fnsLore);
							fns.setItemMeta(fnsMeta);
							sponsorInv.setItem(1, fns);
						}
						//If Soup is still avaliable
						if (sp.getItemAvaliab("Soup")) {
							Integer price = sp.getItemPrice("Soup");
							ItemStack soup = new ItemStack(Material.MUSHROOM_SOUP, 1);
							ItemMeta soupMeta = soup.getItemMeta();
							List<String> soupLore = new ArrayList<String>();
							soupLore.add(MessageManager.c("&aPrice: &e" + price));
							soupMeta.setLore(soupLore);
							soup.setItemMeta(soupMeta);
							sponsorInv.setItem(2, soup);
						}
						//If Arrows is still avaliable
						if (sp.getItemAvaliab("Arrow")) {
							Integer price = sp.getItemPrice("Arrow");
							ItemStack arrow = new ItemStack(Material.ARROW, 4);
							ItemMeta arrowMeta = arrow.getItemMeta();
							List<String> arrowLore = new ArrayList<String>();
							arrowLore.add(MessageManager.c("&aPrice: &e" + price));
							arrowMeta.setLore(arrowLore);
							arrow.setItemMeta(arrowMeta);
							sponsorInv.setItem(3, arrow);
						}
						//If RawPorkchops are still avaliable
						if (sp.getItemAvaliab("RawPorkchop")) {
							Integer price = sp.getItemPrice("RawPorkchop");
							ItemStack pork = new ItemStack(Material.PORK, 1);
							ItemMeta porkMeta = pork.getItemMeta();
							List<String> porkLore = new ArrayList<String>();
							porkLore.add(MessageManager.c("&aPrice: &e" + price));
							porkMeta.setLore(porkLore);
							pork.setItemMeta(porkMeta);
							sponsorInv.setItem(4, pork);
						}
						//If EnchantBottles are still avaliable
						if (sp.getItemAvaliab("EnchantBottles")) {
							Integer price = sp.getItemPrice("EnchantBottles");
							ItemStack enchant = new ItemStack(Material.EXP_BOTTLE, 2);
							ItemMeta enchantMeta = enchant.getItemMeta();
							List<String> enchantLore = new ArrayList<String>();
							enchantLore.add(MessageManager.c("&aPrice: &e" + price));
							enchantMeta.setLore(enchantLore);
							enchant.setItemMeta(enchantMeta);
							sponsorInv.setItem(5, enchant);
						}
						//If Cake is still avaliable
						if (sp.getItemAvaliab("Cake")) {
							Integer price = sp.getItemPrice("Cake");
							ItemStack cake = new ItemStack(Material.CAKE, 1);
							ItemMeta cakeMeta = cake.getItemMeta();
							List<String> cakeLore = new ArrayList<String>();
							cakeLore.add(MessageManager.c("&aPrice: &e" + price));
							cakeMeta.setLore(cakeLore);
							cake.setItemMeta(cakeMeta);
							sponsorInv.setItem(6, cake);
						}

						player.openInventory(sponsorInv);
					} else {
						player.sendMessage(MessageManager.c(MessageManager.prefix + " &cThat player is not online&8."));
					}
				} else {
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &cYou can only sponsor people as a spectator&8."));
				}
			} else {
				player.sendMessage(MessageManager.unknownCommand);
			}
		}else{
			player.sendMessage(MessageManager.c(MessageManager.prefix + " &cUsage: /sponsor <player>"));
		}
		return false;
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {

		Player player = (Player) event.getWhoClicked();
		ItemStack clicked = event.getCurrentItem();
		Inventory inv = event.getInventory();
		String title = ChatColor.stripColor(inv.getTitle());

		if (clicked == null) {
			return;
		}
		//If inventory is named sponsor
		if (title.contains("Sponsor")) {
			//Get the player name from the title
			String[] spl = ChatColor.stripColor(inv.getTitle()).split(" ");
			String playername = spl[1];
			Player playerToBeSponsored = Main.main.getServer().getPlayer(playername);
			//Get the sponsor manager!
			SGSponsorManager sp = Main.main.getSGGameManager().getGameByPlayer(playerToBeSponsored.getName()).getSM();
			//Enderpearl
			if (clicked.getType() == Material.ENDER_PEARL) {
				event.setCancelled(true);
				Integer points = sp.getItemPrice("Enderpearl");
				//If player has enough points
				if (PointsManager.usePoints(player, points) == true) {
					player.closeInventory();
					playerToBeSponsored.getInventory().addItem(new ItemStack(Material.ENDER_PEARL));
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &fItem sent to " + playerToBeSponsored.getDisplayName() + "&8."));
					//Message when recieving sponsor
					recieveSponsorMessage(playerToBeSponsored, player);
					Main.main.getSGGameManager().getGameByPlayer(playerToBeSponsored.getName()).getSM().setItemAvaliab("Enderpearl", false);
				} else {
					player.closeInventory();
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &cYou do not have enough points to complete this action!"));
				}

			}
			//FNS
			if (clicked.getType() == Material.FLINT_AND_STEEL) {
				event.setCancelled(true);
				Integer points = sp.getItemPrice("FNS");
				//If player has enough points
				if (PointsManager.usePoints(player, points) == true) {
					player.closeInventory();
					playerToBeSponsored.getInventory().addItem(new ItemStack(Material.FLINT_AND_STEEL));
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &fItem sent to " + playerToBeSponsored.getDisplayName() + "&8."));
					//Message when recieving sponsor
					recieveSponsorMessage(playerToBeSponsored, player);
					Main.main.getSGGameManager().getGameByPlayer(playerToBeSponsored.getName()).getSM().setItemAvaliab("FNS", false);
				} else {
					player.closeInventory();
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &6You do not have enough points"));
				}
			}
			//Soup
			if (clicked.getType() == Material.MUSHROOM_SOUP) {
				event.setCancelled(true);
				Integer points = sp.getItemPrice("Soup");
				//If player has enough points
				if (PointsManager.usePoints(player, points) == true) {
					player.closeInventory();
					playerToBeSponsored.getInventory().addItem(new ItemStack(Material.MUSHROOM_SOUP));
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &fItem sent to " + playerToBeSponsored.getDisplayName() + "&8."));
					//Message when recieving sponsor
					recieveSponsorMessage(playerToBeSponsored, player);
					Main.main.getSGGameManager().getGameByPlayer(playerToBeSponsored.getName()).getSM().setItemAvaliab("Soup", false);
				} else {
					player.closeInventory();
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &6You do not have enough points"));
				}
			}
			//Arrow
			if (clicked.getType() == Material.ARROW) {
				event.setCancelled(true);
				Integer points = sp.getItemPrice("Arrow");
				//If player has enough points
				if (PointsManager.usePoints(player, points) == true) {
					player.closeInventory();
					playerToBeSponsored.getInventory().addItem(new ItemStack(Material.ARROW, 4));
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &fItem sent to " + playerToBeSponsored.getDisplayName() + "&8."));
					//Message when recieving sponsor
					recieveSponsorMessage(playerToBeSponsored, player);
					Main.main.getSGGameManager().getGameByPlayer(playerToBeSponsored.getName()).getSM().setItemAvaliab("Arrow", false);
				} else {
					player.closeInventory();
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &6You do not have enough points"));
				}
			}
			//RawPorkchop
			if (clicked.getType() == Material.PORK) {
				event.setCancelled(true);
				Integer points = sp.getItemPrice("RawPorkchop");
				//If player has enough points
				if (PointsManager.usePoints(player, points) == true) {
					player.closeInventory();
					playerToBeSponsored.getInventory().addItem(new ItemStack(Material.PORK, 1));
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &fItem sent to " + playerToBeSponsored.getDisplayName() + "&8."));
					//Message when recieving sponsor
					recieveSponsorMessage(playerToBeSponsored, player);
					Main.main.getSGGameManager().getGameByPlayer(playerToBeSponsored.getName()).getSM().setItemAvaliab("RawPorkchop", false);
				} else {
					player.closeInventory();
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &6You do not have enough points"));
				}
			}
			//Enchant Bottles
			if (clicked.getType() == Material.EXP_BOTTLE) {
				event.setCancelled(true);
				Integer points = sp.getItemPrice("EnchantBottles");
				//If player has enough points
				if (PointsManager.usePoints(player, points) == true) {
					player.closeInventory();
					playerToBeSponsored.getInventory().addItem(new ItemStack(Material.EXP_BOTTLE, 2));
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &fItem sent to " + playerToBeSponsored.getDisplayName() + "&8."));
					//Message when recieving sponsor
					recieveSponsorMessage(playerToBeSponsored, player);
					Main.main.getSGGameManager().getGameByPlayer(playerToBeSponsored.getName()).getSM().setItemAvaliab("EnchantBottles", false);
				} else {
					player.closeInventory();
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &6You do not have enough points"));
				}
			}
			//Cake
			if (clicked.getType() == Material.CAKE) {
				event.setCancelled(true);
				Integer points = sp.getItemPrice("Cake");
				//If player has enough points
				if (PointsManager.usePoints(player, points) == true) {
					player.closeInventory();
					playerToBeSponsored.getInventory().addItem(new ItemStack(Material.CAKE, 1));
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &fItem sent to " + playerToBeSponsored.getDisplayName() + "&8."));
					//Message when recieving sponsor
					recieveSponsorMessage(playerToBeSponsored, player);
					Main.main.getSGGameManager().getGameByPlayer(playerToBeSponsored.getName()).getSM().setItemAvaliab("Cake", false);
				} else {
					player.closeInventory();
					player.sendMessage(MessageManager.c(MessageManager.prefix + " &6You do not have enough points"));
				}
			}

		}

	}

	public void recieveSponsorMessage(Player recievingPlayer, Player sendingPlayer){
		recievingPlayer.sendMessage(MessageManager.c(MessageManager.prefix + " &fSuprise - you were sponsored by " + sendingPlayer.getDisplayName() + "&f!"));
		recievingPlayer.sendMessage(MessageManager.c(MessageManager.prefix + " &fRemember to thank them!"));
	}

}
