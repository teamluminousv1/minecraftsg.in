package luminous.net.minecraftsg.net.Sidebar.BG;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.minecraftsg.net.Gamemodes.BG.BGGame;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Ingame {

    public static void setIngameScoreboard(Player player) {
        Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective obj = board.registerNewObjective("bg", "dummy");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR); 
        obj.setDisplayName(MessageManager.c(player.getDisplayName() + " &8-" + " &cnull"));

        Team timeheader = board.registerNewTeam("timeheader");
        timeheader.addEntry(ChatColor.LIGHT_PURPLE.toString());
        timeheader.setPrefix(MessageManager.c("&f" + Main.main.getConfig().getString("Unicode.arrow") + " Time"));
        obj.getScore(ChatColor.LIGHT_PURPLE.toString()).setScore(15);

        Date now = new Date();
        SimpleDateFormat theDate = new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat theTime = new SimpleDateFormat("hh:mm a z");
        SimpleDateFormat theDateAndTime = new SimpleDateFormat("dd.MM HH:mm");

        Team date = board.registerNewTeam("date");
        date.addEntry(ChatColor.DARK_PURPLE.toString());
        date.setPrefix(MessageManager.c("&e" + theDate.format(now)));
        obj.getScore(ChatColor.DARK_PURPLE.toString()).setScore(14);

        Team time = board.registerNewTeam("time");
        time.addEntry(ChatColor.DARK_RED.toString());
        time.setPrefix(MessageManager.c("&e" + theTime.format(now)));
        obj.getScore(ChatColor.DARK_RED.toString()).setScore(13);

        Team datetime = board.registerNewTeam("datetime");
        datetime.addEntry(ChatColor.WHITE.toString());
        datetime.setPrefix(MessageManager.c("&7" + theDateAndTime.format(now) + "Z"));
        obj.getScore(ChatColor.WHITE.toString()).setScore(12);

        Team blank2 = board.registerNewTeam("blank2");
        blank2.addEntry(ChatColor.BLUE.toString());
        blank2.setPrefix(MessageManager.c("&7 "));
        obj.getScore(ChatColor.BLUE.toString()).setScore(11);

        Team server = board.registerNewTeam("server");
        server.addEntry(ChatColor.YELLOW.toString());
        server.setPrefix(MessageManager.c("&f" + Main.main.getConfig().getString("Unicode.arrow") + " Server"));
        obj.getScore(ChatColor.YELLOW.toString()).setScore(10);

        Team servername = board.registerNewTeam("servername");
        servername.addEntry(ChatColor.BLACK.toString());
        servername.setPrefix(MessageManager.c("&6AU&7: &e" + Main.main.getBGGameManager().getGameByPlayer(player.getName()).getZimeID()));
        obj.getScore(ChatColor.BLACK.toString()).setScore(9);

        Team blank3 = board.registerNewTeam("blank3");
        blank3.addEntry(ChatColor.RED.toString());
        blank3.setPrefix(MessageManager.c("&6 "));
        obj.getScore(ChatColor.RED.toString()).setScore(8);

        Team gameheader = board.registerNewTeam("gameheader");
        gameheader.addEntry(ChatColor.GOLD.toString());
        gameheader.setPrefix(MessageManager.c("&f" + Main.main.getConfig().getString("Unicode.arrow") + " Top Kills"));
        obj.getScore(ChatColor.GOLD.toString()).setScore(7);

        Team first = board.registerNewTeam("first");
        first.addEntry(ChatColor.AQUA.toString());
        first.setPrefix(MessageManager.c("&eUnknown&8: &f0"));
        obj.getScore(ChatColor.AQUA.toString()).setScore(6);
        
        Team second = board.registerNewTeam("second");
        second.addEntry(ChatColor.DARK_AQUA.toString());
        second.setPrefix(MessageManager.c("&eUnknown&8: &f0"));
        obj.getScore(ChatColor.DARK_AQUA.toString()).setScore(5);
        
        Team third = board.registerNewTeam("third");
        third.addEntry(ChatColor.DARK_GRAY.toString());
        third.setPrefix(MessageManager.c("&eUnknown&8: &f0"));
        obj.getScore(ChatColor.DARK_GRAY.toString()).setScore(4);
        
        Team fourth = board.registerNewTeam("fourth");
        fourth.addEntry(ChatColor.GRAY.toString());
        fourth.setPrefix(MessageManager.c("&eUnknown&8: &f0"));
        obj.getScore(ChatColor.GRAY.toString()).setScore(3);
        
        Team fifth = board.registerNewTeam("fifth");
        fifth.addEntry(ChatColor.DARK_BLUE.toString());
        fifth.setPrefix(MessageManager.c("&eUnknown&8: &f0"));
        obj.getScore(ChatColor.DARK_BLUE.toString()).setScore(2);
        
        Team kills = board.registerNewTeam("kills");
        kills.addEntry(ChatColor.GREEN.toString());
        kills.setPrefix(MessageManager.c("&3You&8: "));
        kills.setSuffix(MessageManager.c("&f" + Main.main.getBGGameManager().getGameByPlayer(player.getName()).getKills().get(player.getName())));
        obj.getScore(ChatColor.GREEN.toString()).setScore(1);
        

        player.setScoreboard(board);
    }

    public static void updatePlayerKills(Player player){
        Scoreboard sb = player.getScoreboard();
        BGGame game = Main.main.getBGGameManager().getGameByPlayer(player.getName());
        Object[] arrayList = game.getKills().keySet().toArray();
        //First Place
        if (game.getKills().size() > 0) {
            String name = (String) arrayList[0];
            Integer points = game.getKills().get(name);
            sb.getTeam("first").setPrefix(MessageManager.c("&e" + name +"&8 "));
            sb.getTeam("first").setSuffix(MessageManager.c(" &f" + points));
        }else{
            sb.getTeam("first").setPrefix(MessageManager.c("&eUnknown"));
            sb.getTeam("first").setSuffix(MessageManager.c("&8: &f0"));
        }
        //Second Place
        if (game.getKills().size() > 1) {
            String name = (String) arrayList[1];
            Integer points = game.getKills().get(name);
            sb.getTeam("second").setPrefix(MessageManager.c("&e" + name +"&8 "));
            sb.getTeam("second").setSuffix(MessageManager.c(" &f" + points));
        }else{
            sb.getTeam("second").setPrefix(MessageManager.c("&eUnknown"));
            sb.getTeam("second").setSuffix(MessageManager.c("&8: &f0"));
        }
        //Third Place
        if (game.getKills().size() > 2) {
            String name = (String) arrayList[2];
            Integer points = game.getKills().get(name);
            sb.getTeam("third").setPrefix(MessageManager.c("&e" + name +"&8 "));
            sb.getTeam("third").setSuffix(MessageManager.c(" &f" + points));
        }else{
            sb.getTeam("third").setPrefix(MessageManager.c("&eUnknown"));
            sb.getTeam("third").setSuffix(MessageManager.c("&8: &f0"));
        }
        //Forth Place
        if (game.getKills().size() > 3) {
            String name = (String) arrayList[3];
            Integer points = game.getKills().get(name);
            sb.getTeam("fourth").setPrefix(MessageManager.c("&e" + name +"&8 "));
            sb.getTeam("fourth").setSuffix(MessageManager.c(" &f" + points));
        }else{
            sb.getTeam("fourth").setPrefix(MessageManager.c("&eUnknown"));
            sb.getTeam("fourth").setSuffix(MessageManager.c("&8: &f0"));
        }
        //Fifth Place
        if (game.getKills().size() > 4) {
            String name = (String) arrayList[4];
            Integer points = game.getKills().get(name);
            sb.getTeam("fifth").setPrefix(MessageManager.c("&e" + name +"&8 "));
            sb.getTeam("fifth").setSuffix(MessageManager.c(" &f" + points));
        }else{
            sb.getTeam("fifth").setPrefix(MessageManager.c("&eUnknown"));
            sb.getTeam("fifth").setSuffix(MessageManager.c("&8: &f0"));
        }
    }
}
