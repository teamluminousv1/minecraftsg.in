package luminous.net.minecraftsg.net.Sidebar.SG;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.MessageManager;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Lobby {
	
	public static void setLobbyScoreboard(Player player) {
		Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective obj = board.registerNewObjective("sg", "dummy");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		obj.setDisplayName(MessageManager.c("&a" +
		Main.main.getSGGameManager().getGameByPlayer(player.getName()).getDisplayGamestate(Main.main.getSGGameManager().getGameByPlayer(player.getName()).getGamestate()) + " &cnull"));

		Team youheader = board.registerNewTeam("youheader");
		youheader.addEntry(ChatColor.GRAY.toString());
		youheader.setPrefix(MessageManager.c("&f" + Main.main.getConfig().getString("Unicode.arrow") + " You"));
		obj.getScore(ChatColor.GRAY.toString()).setScore(13);
		
		Team you = board.registerNewTeam("you");
		you.addEntry(ChatColor.DARK_AQUA.toString());
		you.setPrefix(MessageManager.c(player.getPlayerListName()));
		obj.getScore(ChatColor.DARK_AQUA.toString()).setScore(12);

		Team blank1 = board.registerNewTeam("blank1");
		blank1.addEntry(ChatColor.AQUA.toString());
		blank1.setPrefix(MessageManager.c("&c "));
		obj.getScore(ChatColor.AQUA.toString()).setScore(11);
		
		Team timeheader = board.registerNewTeam("timeheader");
		timeheader.addEntry(ChatColor.LIGHT_PURPLE.toString());
		timeheader.setPrefix(MessageManager.c("&f" + Main.main.getConfig().getString("Unicode.arrow") + " Time"));
		obj.getScore(ChatColor.LIGHT_PURPLE.toString()).setScore(10);

		Date now = new Date();
		SimpleDateFormat theDate = new SimpleDateFormat("dd MMM yyyy");
		SimpleDateFormat theTime = new SimpleDateFormat("hh:mm a z");
		SimpleDateFormat theDateAndTime = new SimpleDateFormat("dd.MM HH:mm");

		Team date = board.registerNewTeam("date");
		date.addEntry(ChatColor.DARK_PURPLE.toString());
		date.setPrefix(MessageManager.c("&e" + theDate.format(now)));
		obj.getScore(ChatColor.DARK_PURPLE.toString()).setScore(9);

		Team time = board.registerNewTeam("time");
		time.addEntry(ChatColor.DARK_RED.toString());
		time.setPrefix(MessageManager.c("&e" + theTime.format(now)));
		obj.getScore(ChatColor.DARK_RED.toString()).setScore(8);
		
		Team datetime = board.registerNewTeam("datetime");
		datetime.addEntry(ChatColor.WHITE.toString());
		datetime.setPrefix(MessageManager.c("&7" + theDateAndTime.format(now) + "Z"));
		obj.getScore(ChatColor.WHITE.toString()).setScore(7);

		Team blank2 = board.registerNewTeam("blank2");
		blank2.addEntry(ChatColor.BLUE.toString());
		blank2.setPrefix(MessageManager.c("&7 "));
		obj.getScore(ChatColor.BLUE.toString()).setScore(6);

		Team server = board.registerNewTeam("server");
		server.addEntry(ChatColor.YELLOW.toString());
		server.setPrefix(MessageManager.c("&f" + Main.main.getConfig().getString("Unicode.arrow") + " Server"));
		obj.getScore(ChatColor.YELLOW.toString()).setScore(5);

		Team servername = board.registerNewTeam("servername");
		servername.addEntry(ChatColor.BLACK.toString());
		servername.setPrefix(MessageManager.c("&6AU&7: &e" + Main.main.getSGGameManager().getGameByPlayer(player.getName()).getZimeID()));
		obj.getScore(ChatColor.BLACK.toString()).setScore(4);

		Team blank3 = board.registerNewTeam("blank3");
		blank3.addEntry(ChatColor.RED.toString());
		blank3.setPrefix(MessageManager.c("&6 "));
		obj.getScore(ChatColor.RED.toString()).setScore(3);

		Team playersheader = board.registerNewTeam("playersheader");
		playersheader.addEntry(ChatColor.GOLD.toString());
		playersheader.setPrefix(MessageManager.c("&f" + Main.main.getConfig().getString("Unicode.arrow") + " Players"));
		obj.getScore(ChatColor.GOLD.toString()).setScore(2);

		Team players = board.registerNewTeam("players");
		players.addEntry(ChatColor.GREEN.toString());
		players.setPrefix(MessageManager.c("&fPlaying:"));
		players.setSuffix(MessageManager.c(" &f") + Main.main.getSGGameManager().getGameByPlayer(player.getName()).getPlayers().size());
		obj.getScore(ChatColor.GREEN.toString()).setScore(1);



		player.setScoreboard(board);
    }
	
}
