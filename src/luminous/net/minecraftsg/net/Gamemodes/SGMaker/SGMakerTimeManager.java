package luminous.net.minecraftsg.net.Gamemodes.SGMaker;

import luminous.net.main.fiddycal.Events.Connect;
import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Commands.Fly;
import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.main.fiddycal.Managers.RankManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import luminous.net.main.fiddycal.Managers.WorldManager;
import luminous.net.minecraftsg.net.Managers.Fireworks;
import luminous.net.minecraftsg.net.Sidebar.SG.Ingame;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.*;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;

public class SGMakerTimeManager extends BukkitRunnable{

    //Game Class
    private SGMakerGame game;

    //Time integers
    public int waitingTime;
    public int preGame;
    public int graceperiod;
    public int inGame;
    public int restarting;
    public int preDeathmatch;
    public int deathmatch;
    public int endGame;
    public int cleanUp;

    //Usage: new TimeManager(Game);
    public SGMakerTimeManager(SGMakerGame game){
        //Sets game
        this.game = game;
        //Sets Game Times from confguration file
        this.preGame = 30;
        this.graceperiod = 20;
        this.inGame = 1800;
        this.restarting = 4;
        this.waitingTime = 120;
        this.preDeathmatch = 20;
        this.deathmatch = 180;
        this.preDeathmatch = 10;
        this.endGame = 10;
        this.cleanUp = 4;
        //-----------------------------------
        //DO NOT ADD ANYTHING BELOW THIS LINE
        //-----------------------------------
        //Run This Class
        this.runTaskTimer(Main.main, 0, 20);
    }

    public Integer getMins(Integer ints){
        return ints/60;
    }

    public Integer getSeconds(Integer ints){
        return ints - getMins(ints) * 60;
    }

    public String scoreboardTime(Integer ints){
        String mins = "";
        String secs = "";
        if (getMins(ints) < 10){
            mins = "0" + getMins(ints);
        }else{
            mins = String.valueOf(getMins(ints));
        }
        if (getSeconds(ints) < 10){
            secs = "0" + getSeconds(ints);
        }else{
            secs = String.valueOf(getSeconds(ints));
        }

        return mins + ":" + secs;
    }


    @Override
    public void run() {
        SGMakerGameState currentState = this.game.getGamestate();
        if (currentState == SGMakerGameState.WAITING){
            //If timer is equal to 0
            if (this.waitingTime == 0){
                //If there is enough players
                if (this.game.getPlayers().size() > 5) {
                    //Sets the map
                    this.game.setMap(this.game.getVm().getWinningMap());
            		//Copies the map
                    WorldManager.copyWorld("plugins/SGIN-Core/SurvivalGames/Maps/"
                    	+ this.game.getMap(), "", "SGMakerGame-" + this.game.getID());
                    //Adds everyone as a tribute
                    for (String string : this.game.getPlayers()){
                        this.game.getTributes().add(string);
                    }
                    for (String str: this.game.getPlayers()){
                        Player player = Main.main.getServer().getPlayer(str);
                        player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setDisplaySlot(null);
                    }
                    this.game.setupSpawns();
                    //Shuffles all the tributes
                    Collections.shuffle(this.game.getTributes());
                    //Sets the gamestate to pregame
                    for (int i = 0; i < this.game.getTributes().size(); i++) {
                        Player player = Main.main.getServer().getPlayer(this.game.getTributes().get(i));
                        Location loc = this.game.getGameSpawns().get(i);
                        Fly.stopFlight(player);
                        player.teleport(loc);
                    }
                    this.game.setupPlayerDists();
                    this.game.setGamestate(SGMakerGameState.PREGAME);
                    Bukkit.getWorld("SGMakerGame-" + this.game.getID()).setGameRuleValue("keepInventory", "false");
                    Bukkit.getWorld("SGMakerGame-" + this.game.getID()).setGameRuleValue("doDaylightCycle", "true");
                    Bukkit.getWorld("SGMakerGame-" + this.game.getID()).setGameRuleValue("doMobSpawning", "false");
                    Bukkit.getWorld("SGMakerGame-" + this.game.getID()).setGameRuleValue("mobGriefing", "false");
                    Main.main.getServer().getWorld("SGMakerGame-" + this.game.getID()).setTime(6000);
                    this.game.changeChests();
                    this.game.setupChests(false);
                    this.game.getBm().setupBounties();
                    //Sets the sidebar to now display tributes
                    for (String pl: this.game.getPlayers()){
                        Player player = Main.main.getServer().getPlayer(pl);
                        Ingame.setIngameScoreboard(player);
                    }
                    for (String string : this.game.getTributes()){
                        Player pl = Main.main.getServer().getPlayer(string);
                        String m1 = this.game.getPreviousMap(0);
        				String m2 = this.game.getPreviousMap(1);
                        this.game.getPreviousMapsPlayed().clear();
                        this.game.getPreviousMapsPlayed().add(this.game.getMap());
                        this.game.getPreviousMapsPlayed().add(m1);
                        this.game.getPreviousMapsPlayed().add(m2);
                        pl.getScoreboard().getTeam("players").setSuffix(MessageManager.c(" &f") + this.game.getTributes().size());
                        pl.sendMessage(MessageManager.c(MessageManager.prefix + " &eMap name&8: &2" + this.game.getMap().replace("_", " ")));
                        pl.sendMessage(MessageManager.c(MessageManager.prefix + " &eMap author&8: &2" + ConfigManager.sgDataCFG.getString("Maps." + this.game.getMap() + ".Author")));
                        //pl.sendMessage(MessageManager.c(MessageManager.prefix + " &eMap link&8: &2" + ConfigManager.sgDataCFG.getString("Maps." + this.game.getMap() + ".Link")));
                        pl.sendMessage(MessageManager.c(MessageManager.prefix + " &cPlease wait &8[&e30&8] &cseconds before the games begin."));
                    }
                    for(Entity e : Main.main.getServer().getWorld("SGMakerGame-" + this.game.getID()).getEntities()){
                        if (e.getType() == EntityType.COW
                        	|| e.getType() == EntityType.SLIME
                        	|| e.getType() == EntityType.SKELETON
                        	|| e.getType() == EntityType.ZOMBIE
                        	|| e.getType() == EntityType.OCELOT
                        	|| e.getType() == EntityType.CREEPER
                        	|| e.getType() == EntityType.ENDERMAN
                        	|| e.getType() == EntityType.SPIDER
                        	|| e.getType() == EntityType.PIG_ZOMBIE
                        	|| e.getType() == EntityType.CAVE_SPIDER
                        	|| e.getType() == EntityType.SILVERFISH
                        	|| e.getType() == EntityType.BLAZE
                        	|| e.getType() == EntityType.BAT
                        	|| e.getType() == EntityType.MAGMA_CUBE
                        	|| e.getType() == EntityType.WITCH
                        	|| e.getType() == EntityType.PIG
                        	|| e.getType() == EntityType.SHEEP){
                            e.remove();
                        }
                    }
                    //If there is not enough players
                }else{
                    if (this.game.getForceStart() == true) {
                        //Sets the map
                        this.game.setMap(this.game.getVm().getWinningMap());
                        //Copies the map
                        WorldManager.copyWorld("plugins/SGIN-Core/SurvivalGames/Maps/"
                                + this.game.getMap(), "", "SGMakerGame-" + this.game.getID());
                        //Adds everyone as a tribute
                        for (String string : this.game.getPlayers()){
                            this.game.getTributes().add(string);
                        }
                        for (String str: this.game.getPlayers()){
                            Player player = Main.main.getServer().getPlayer(str);
                            player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setDisplaySlot(null);
                        }
                        this.game.setupSpawns();
                        //Shuffles all the tributes
                        Collections.shuffle(this.game.getTributes());
                        //Sets the gamestate to pregame
                        for (int i = 0; i < this.game.getTributes().size(); i++) {
                            Player player = Main.main.getServer().getPlayer(this.game.getTributes().get(i));
                            Location loc = this.game.getGameSpawns().get(i);
                            Fly.stopFlight(player);
                            player.teleport(loc);
                        }
                        this.game.setupPlayerDists();
                        this.game.setGamestate(SGMakerGameState.PREGAME);
                        Bukkit.getWorld("SGMakerGame-" + this.game.getID()).setGameRuleValue("keepInventory", "false");
                        Bukkit.getWorld("SGMakerGame-" + this.game.getID()).setGameRuleValue("doDaylightCycle", "true");
                        Bukkit.getWorld("SGMakerGame-" + this.game.getID()).setGameRuleValue("doMobSpawning", "false");
                        Bukkit.getWorld("SGMakerGame-" + this.game.getID()).setGameRuleValue("mobGriefing", "false");
                        Main.main.getServer().getWorld("SGMakerGame-" + this.game.getID()).setTime(6000);
                        this.game.changeChests();
                        this.game.setupChests(false);
                        this.game.getBm().setupBounties();
                        //Sets the sidebar to now display tributes
                        for (String pl: this.game.getPlayers()){
                            Player player = Main.main.getServer().getPlayer(pl);
                            Ingame.setIngameScoreboard(player);
                        }
                        for (String string : this.game.getTributes()){
                            Player pl = Main.main.getServer().getPlayer(string);
                            String m1 = this.game.getPreviousMap(0);
                            String m2 = this.game.getPreviousMap(1);
                            this.game.getPreviousMapsPlayed().clear();
                            this.game.getPreviousMapsPlayed().add(this.game.getMap());
                            this.game.getPreviousMapsPlayed().add(m1);
                            this.game.getPreviousMapsPlayed().add(m2);
                            pl.getScoreboard().getTeam("players").setSuffix(MessageManager.c(" &f") + this.game.getTributes().size());
                            pl.sendMessage(MessageManager.c(MessageManager.prefix + " &eMap name&8: &2" + this.game.getMap().replace("_", " ")));
                            pl.sendMessage(MessageManager.c(MessageManager.prefix + " &eMap author&8: &2" + ConfigManager.sgDataCFG.getString("Maps." + this.game.getMap() + ".Author")));
                            //pl.sendMessage(MessageManager.c(MessageManager.prefix + " &eMap link&8: &2" + ConfigManager.sgDataCFG.getString("Maps." + this.game.getMap() + ".Link")));
                            pl.sendMessage(MessageManager.c(MessageManager.prefix + " &cPlease wait &8[&e30&8] &cseconds before the games begin."));
                        }
                        for(Entity e : Main.main.getServer().getWorld("SGMakerGame-" + this.game.getID()).getEntities()){
                            if (e.getType() == EntityType.COW
                            	|| e.getType() == EntityType.SLIME
                            	|| e.getType() == EntityType.SKELETON
                            	|| e.getType() == EntityType.ZOMBIE
                            	|| e.getType() == EntityType.OCELOT
                            	|| e.getType() == EntityType.CREEPER
                            	|| e.getType() == EntityType.ENDERMAN
                            	|| e.getType() == EntityType.SPIDER
                            	|| e.getType() == EntityType.PIG_ZOMBIE
                            	|| e.getType() == EntityType.CAVE_SPIDER
                            	|| e.getType() == EntityType.SILVERFISH
                            	|| e.getType() == EntityType.BLAZE
                            	|| e.getType() == EntityType.BAT
                            	|| e.getType() == EntityType.MAGMA_CUBE
                            	|| e.getType() == EntityType.WITCH
                            	|| e.getType() == EntityType.PIG
                            	|| e.getType() == EntityType.SHEEP){
                                e.remove();
                            }
                        }
                    }else {
                        this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &4Not enough players, &4timer reset."));
                        this.waitingTime = 120;
                    }
                }
            }else{
                Integer mins = getMins(this.waitingTime);
                Integer secs = getSeconds(this.waitingTime);
                if (this.waitingTime > 40) {
                    if (this.game.getPlayers().size() > 5) {
                        this.waitingTime = 30;
                        this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &cThe Lobby time has been set to &8[&e30&8] &cseconds."));
                    }
                }
                if (mins == 3 && secs == 0 || mins == 2 && secs == 0){
                    this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &8[&e" + mins + "&8] &cminutes until lobby ends!"));
                }
                if (mins == 2 && secs == 30 || mins == 1 && secs == 30){
                    this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &8[&e" + mins + ".5&8] &cminutes until lobby ends!"));
                }
                if (mins == 1 && secs == 0 || mins == 0 && secs == 30 || mins == 0 && secs == 15 || mins == 0 && secs == 10 || mins == 0 && secs == 5 || mins == 0 && secs == 4 || mins == 0 && secs == 3 || mins == 0 && secs == 2){
                    this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &8[&e" + this.waitingTime + "&8] &cseconds until lobby ends!"));
                }
                if (this.waitingTime == 15 || this.waitingTime == 30 || this.waitingTime == 45 || this.waitingTime == 60 || this.waitingTime == 75 || this.waitingTime == 90 || this.waitingTime == 105){
                	this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &2Players waiting&8: [&6" + game.getPlayers().size() + "&8/&624&8]. &2Game requires &8[&66&8] &2to play&8."));
                    this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &2Vote using &8[&a/vote #&8]."));
                	for (String string : this.game.getPlayers()){
                        Player pl = Main.main.getServer().getPlayer(string);
                        String m1 = this.game.getPreviousMap(0);
        				String m2 = this.game.getPreviousMap(1);
        				String m3 = this.game.getPreviousMap(2);
        				pl.sendMessage(MessageManager.c(MessageManager.prefix + " &2Previous maps played&8: &7" + m1 + "&8, &7" + m2 + "&8, &7" + m3 + "&8."));
                    }
                    this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &a1 &8> |&e "+ game.getVm().getVotes().get(game.getVm().getMaps().get(0)) +" &7Votes &8| &2" + game.getVm().getMaps().get(0).replace("_", " ")));
                    this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &a2 &8> |&e "+ game.getVm().getVotes().get(game.getVm().getMaps().get(1)) +" &7Votes &8| &2" + game.getVm().getMaps().get(1).replace("_", " ")));
                    this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &a3 &8> |&e "+ game.getVm().getVotes().get(game.getVm().getMaps().get(2)) +" &7Votes &8| &2" + game.getVm().getMaps().get(2).replace("_", " ")));
                    this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &a4 &8> |&e "+ game.getVm().getVotes().get(game.getVm().getMaps().get(3)) +" &7Votes &8| &2" + game.getVm().getMaps().get(3).replace("_", " ")));
                    this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &a5 &8> |&e "+ game.getVm().getVotes().get(game.getVm().getMaps().get(4)) +" &7Votes &8| &2" + game.getVm().getMaps().get(4).replace("_", " ")));
                }
                if (mins == 0 && secs == 1){
                    this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &8[&e" + this.waitingTime + "&8] &csecond until lobby ends!"));
                }
                for (String pl: this.game.getPlayers()){
                    Player player = Main.main.getServer().getPlayer(pl);
                    player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setDisplayName(MessageManager.c("&a" + this.game.getDisplayGamestate(SGMakerGameState.WAITING) + " &c" + scoreboardTime(this.waitingTime) ));
                }
                this.waitingTime--;
            }
        }
        if (currentState == SGMakerGameState.PREGAME){
            if (this.preGame != 0){
                if (this.preGame == 30 || this.preGame == 20 || this.preGame == 10 || this.preGame == 5 || this.preGame == 4 || this.preGame == 3 || this.preGame == 2) {
                    this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &8[&e" + this.preGame + "&8] &cseconds until the games begin!"));
                }
                if (this.preGame == 1){
                    this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &8[&e" + this.preGame + "&8] &csecond until the games begin!"));
                }
                for (String pl: this.game.getPlayers()){
                    Player player = Main.main.getServer().getPlayer(pl);
                    player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setDisplayName(MessageManager.c("&a"+ this.game.getDisplayGamestate(SGMakerGameState.PREGAME) + " &c" + scoreboardTime(this.preGame) ));
                }
                this.preGame--;
            }else{
            	for (String player : this.game.getPlayers()) {
            		if (Main.main.getServer().getPlayer(player) != null) {
            			Main.main.getServer().getPlayer(player).setFoodLevel(40);
            		}
            	}
                this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &3The games have begun!"));
                this.game.setGamestate(SGMakerGameState.INGAME);
                for (String str : this.game.getTributes()){
                    Player player = Main.main.getServer().getPlayer(str);
                    Objective obj = player.getScoreboard().registerNewObjective("undername", "dummy");
                    obj.setDisplaySlot(DisplaySlot.BELOW_NAME);
                    obj.setDisplayName(MessageManager.c("&3Bounty"));
                    obj.getScore(player.getName()).setScore(Main.main.getSGGameManager().getGameByPlayer(player.getName()).getBm().getBounty(player));
                }
            }
        }
        if (currentState == SGMakerGameState.INGAME){
            if (this.inGame != 0){
                Integer mins = getMins(this.inGame);
                Integer secs = getSeconds(this.inGame);
                if (this.game.getTributes().size() < 4){
                    if (this.inGame > 60){
                        this.inGame = 60;
                        this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &8[&e" + this.inGame + "&8] &cseconds until deathmatch!"));
                    }
                }
                if (mins == 25 && secs == 0 || mins == 20 && secs == 0 || mins == 15 && secs == 0 
                	|| mins == 10 && secs == 0 || mins == 5 && secs == 0 || mins == 4 && secs == 0 
                    || mins == 4 && secs == 0 ||mins == 3 && secs == 0 || mins == 2 && secs == 0) {
                    this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &8[&e" + getMins(this.inGame) + "&8] &cminutes until deathmatch!"));
                }
                if (mins == 1 && secs ==  0 || mins == 0 && secs ==  30 || mins == 0 && secs == 15 || mins == 0 && secs == 10 || mins == 0 && secs == 5 || mins == 0 && secs == 4 || mins == 0 && secs == 3 || mins == 0 && secs == 2){
                    this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &8[&e" + this.inGame + "&8] &cseconds until deathmatch!"));

                }
                if (mins == 0 && secs == 1) {
                	this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &8[&e" + this.inGame + "&8] &csecond until deathmatch!"));
                }
                if (mins == 17 && secs == 0) {
                	this.game.getOpenedChestLocations().clear();
                	this.game.getOpenedChestInventories().clear();
                	this.game.setupChests(false);
                	StringBuilder passedTributes = new StringBuilder();
                	for (String passed : game.getPastTributes()) {
	        			if (passedTributes.length() > 0) {
	        				passedTributes.append(", ");
    	                }
	        			passedTributes.append(MessageManager.c("&r" + passed + "&f"));
                	}
                	this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &3Sponsors have refilled the chests!"));
                	this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &fThese tributes have passed: &r" + passedTributes.toString()));
                }
                for (String pl: this.game.getPlayers()){
                    Player player = Main.main.getServer().getPlayer(pl);
                    player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setDisplayName(MessageManager.c("&a" + this.game.getDisplayGamestate(SGMakerGameState.INGAME) +" &c" + scoreboardTime(this.inGame)));
                }
                this.inGame--;
            }else{
            	if (ConfigManager.sgDataCFG.getString("Maps." + this.game.getMap() + ".dm.1") == null) {
            		for (int i = 0; i < this.game.getTributes().size(); i++) {
                        Player player = Main.main.getServer().getPlayer(this.game.getTributes().get(i));
                        Location loc = this.game.getGameSpawns().get(i);
                        player.teleport(loc);
                    }
            	} else {
            		List<Location> used = new ArrayList<Location>();
            		for (int i = 0; i < this.game.getTributes().size(); i++) {
                        Player player = Main.main.getServer().getPlayer(this.game.getTributes().get(i));
                        if (this.game.getDeathmatchSpawns().get(i) != null) {
                        	Location loc = this.game.getDeathmatchSpawns().get(i);
                            player.teleport(loc); used.add(loc);
                        } else {
                        	if (!used.contains(this.game.getGameSpawns().get(i))) {
                            	player.teleport(this.game.getGameSpawns().get(i));
                            	used.add(this.game.getGameSpawns().get(i));
                        	} else {
                        		player.teleport(this.game.getGameSpawns().get(i + 1));
                        		used.add(this.game.getGameSpawns().get(i + 1));
                        		if (i == 24) { i = 0; }
                        	}
                        }
                    }
            	}
                this.game.setGamestate(SGMakerGameState.PREDEATHMATCH);
                this.game.setupRadius();
                for (String str: this.game.getSpectators()){
                    Player player = Main.main.getServer().getPlayer(str);
                    player.teleport(this.game.getSpecSpawn());
                }
                for (String str : this.game.getTributes()){
                    Player player = Main.main.getServer().getPlayer(str);
                    Objective obj = player.getScoreboard().registerNewObjective("undername", "dummy");
                    obj.setDisplaySlot(DisplaySlot.BELOW_NAME);
                    obj.setDisplayName(MessageManager.c("&3Bounty"));
                    obj.getScore(player.getName()).setScore(Main.main.getSGGameManager().getGameByPlayer(player.getName()).getBm().getBounty(player));
                }
            }
        }
        if (currentState == SGMakerGameState.PREDEATHMATCH){
            if (this.preDeathmatch != 0){
                if (this.preDeathmatch == 10){
                    this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &4Please allow &8[&e10&8] &4seconds for all players to load the map."));
                }
                if (this.preDeathmatch == 5 || this.preDeathmatch == 4 || this.preDeathmatch == 3|| this.preDeathmatch == 2){
                    this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &8[&e" + this.preDeathmatch + "&8] &cseconds until deathmatch!"));
                }
                if (this.preDeathmatch == 1){
                    this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &8[&e" + this.preDeathmatch + "&8] &csecond until deathmatch!"));
                }
                for (String pl: this.game.getPlayers()){
                    Player player = Main.main.getServer().getPlayer(pl);
                    player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setDisplayName(MessageManager.c("&a" + this.game.getDisplayGamestate(SGMakerGameState.PREDEATHMATCH) +" &c" + scoreboardTime(this.preDeathmatch)));
                }
                this.preDeathmatch--;
            }else{
                this.game.setGamestate(SGMakerGameState.DEATHMATCH);
            	this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &cFight to the death!"));
                for (String str : this.game.getTributes()){
                    Player player = Main.main.getServer().getPlayer(str);
                    Objective obj = player.getScoreboard().registerNewObjective("undername", "dummy");
                    obj.setDisplaySlot(DisplaySlot.BELOW_NAME);
                    obj.setDisplayName(MessageManager.c("&3Bounty"));
                    obj.getScore(player.getName()).setScore(Main.main.getSGGameManager().getGameByPlayer(player.getName()).getBm().getBounty(player));
                }
            }
        }
        if (currentState == SGMakerGameState.DEATHMATCH){
            if (this.deathmatch != 0){
                Integer mins = getMins(this.deathmatch);
                Integer secs = getSeconds(this.deathmatch);
                //Remove 1 from the radius every 10 seconds!
                if( mins == 1 && secs == 50 ||
                    mins == 1 && secs == 40 ||
                    mins == 1 && secs == 30 ||
                    mins == 1 && secs == 20 ||
                    mins == 1 && secs == 10 ||
                    mins == 1 && secs == 0 ||
                    mins == 0 && secs == 50 ||
                    mins == 0 && secs == 40 ||
                    mins == 0 && secs == 30 ||
                    mins == 0 && secs == 20 ||
                    mins == 0 && secs == 10){
                    this.game.radius--;
                }
                if(secs % 5 == 0){
                	for (String str : this.game.getTributes()){
                        Player player = Main.main.getServer().getPlayer(str);
                        Location locAtPlayer = player.getLocation();
                        Double x = locAtPlayer.getX();
                        Double z = locAtPlayer.getZ();
                        if (x > this.game.dmx + this.game.radius || x < this.game.dmx - this.game.radius){
                        	player.getWorld().playEffect(locAtPlayer, Effect.MOBSPAWNER_FLAMES, 1);
                        	player.getWorld().strikeLightningEffect(locAtPlayer);
                        	player.damage(4);
                            player.sendMessage(MessageManager.c(MessageManager.prefix + " &4Please return to spawn!"));
                        }
                        if (z > this.game.dmz + this.game.radius || z < this.game.dmz - this.game.radius){
                        	player.getWorld().playEffect(locAtPlayer, Effect.MOBSPAWNER_FLAMES, 1);
                        	player.getWorld().strikeLightningEffect(locAtPlayer);
                        	player.damage(0.5);
                            player.sendMessage(MessageManager.c(MessageManager.prefix + " &4Please return to spawn!"));
                        }
                    }
                }
                if (mins == 3 && secs == 0 || mins == 2 && secs == 0){
                    this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &8[&e" + mins + "&8] &cminutes until the deathmatch ends!"));
                }
                if (mins == 1 && secs == 0) {
                	this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &8[&e" + mins + "&8] &cminute until the deathmatch ends!"));
                }
                if (mins == 0 && secs == 30 || mins == 0 && secs == 15 || mins == 0 && secs == 10
                || mins == 0 && secs == 5 || mins == 0 && secs == 4 || mins == 0 && secs == 3 || mins == 0 && secs == 2) {
                	this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &8[&e" + secs + "&8] &cseconds until the deathmatch ends!"));
                }
                if (mins == 0 && secs == 1) {
                    this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &8[&e" + secs + "&8] &csecond until the deathmatch ends!"));
                }
                for (String pl: this.game.getPlayers()){
                    Player player = Main.main.getServer().getPlayer(pl);
                    player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setDisplayName(MessageManager.c("&a" + this.game.getDisplayGamestate(SGMakerGameState.DEATHMATCH) +" &c" + scoreboardTime(this.deathmatch)));
                }
                this.deathmatch--;
            }else{
                //This will only occur if the players in deathmatch are still alive when the timer runs out!
                this.game.setGamestate(SGMakerGameState.ENDGAME);
                String winner  = this.game.getHighestKills();
                this.game.setWinner(winner);
                this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &aThe games have ended!"));
                this.game.messageEveryone(MessageManager.c(MessageManager.prefix + " &r" + Bukkit.getPlayer(this.game.getWinner()).getDisplayName() + " &ahas won the Survival Games!"));
            }
        }
        if (currentState == SGMakerGameState.ENDGAME){
            if (this.endGame != 0){
                if (this.endGame == 10) {
                    for (Location loc : this.game.getGameSpawns()) {
                    	Fireworks.sendFirework(loc);
                    }
                }
                for (String pl: this.game.getPlayers()){
                    Player player = Main.main.getServer().getPlayer(pl);
                    player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setDisplayName(MessageManager.c("&a" + this.game.getDisplayGamestate(SGMakerGameState.ENDGAME) +" &c" + scoreboardTime(this.endGame)));
                }
                this.endGame--;
            }else{
                this.game.setGamestate(SGMakerGameState.CLEANUP);
            }
        }
        if (currentState == SGMakerGameState.CLEANUP){
            if (this.cleanUp != 0){
                for (String pl: this.game.getPlayers()){
                    Player player = Main.main.getServer().getPlayer(pl);
                    player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setDisplayName(MessageManager.c("&a" + this.game.getDisplayGamestate(SGMakerGameState.CLEANUP) +" &c" + scoreboardTime(this.cleanUp)));
                }
                this.cleanUp--;
            }else{
            	if (!this.game.getPlayers().isEmpty()) {
            		for (String str : this.game.getPlayers()){
                        Player player = Main.main.getServer().getPlayer(str);
                        Connect.sendToLobby(player);
                        Connect.giveLobbyItems(player);
                        RankManager.setDisplayName(player, true);
                    }
                    this.game.getPlayers().clear();
                    this.game.getTributes().clear();
                    this.game.getSpectators().clear();
            	}
                WorldManager.deleteWorld("", "SGMakerGame-" + this.game.getID());
                this.game.setGamestate(SGMakerGameState.RESTARTING);
            }
        }
        if (currentState == SGMakerGameState.RESTARTING){
            if (this.restarting != 0){
                this.restarting--;
            }else{
                this.game.newGame();
                this.preGame = 30;
                this.graceperiod = 20;
                this.inGame = 1800;
                this.restarting = 4;
                this.waitingTime = 120;
                this.preDeathmatch = 20;
                this.deathmatch = 180;
                this.preDeathmatch = 10;
                this.endGame = 10;
                this.cleanUp = 4;
            }
        }
    }
}