package luminous.net.minecraftsg.net.Gamemodes.SGMaker;

import luminous.net.main.fiddycal.Managers.ConfigManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("unused")
public class SGMakerMapPool{

    private List<String> maps;
    private List<String> possibleMaps;
    private SGMakerGame game;
    private SGMakerVoteManager vm;


    public SGMakerMapPool(SGMakerVoteManager vm){
        this.maps = new ArrayList<>();
        this.vm = vm;
        this.possibleMaps = new ArrayList<>();
        for (String id : ConfigManager.sgDataCFG.getConfigurationSection("Maps").getKeys(false)) {
            possibleMaps.add(id);
        }
        Collections.shuffle(this.possibleMaps);
        //Sets the maps that can be voted for!
        this.vm.getMaps().add(this.possibleMaps.get(0));
        this.vm.getMaps().add(this.possibleMaps.get(1));
        this.vm.getMaps().add(this.possibleMaps.get(2));
        this.vm.getMaps().add(this.possibleMaps.get(3));
        this.vm.getMaps().add(this.possibleMaps.get(4));

        //Sets the map votes!
        this.vm.getVotes().put(this.possibleMaps.get(0), 0);
        this.vm.getVotes().put(this.possibleMaps.get(1), 0);
        this.vm.getVotes().put(this.possibleMaps.get(2), 0);
        this.vm.getVotes().put(this.possibleMaps.get(3), 0);
        this.vm.getVotes().put(this.possibleMaps.get(4), 0);

    }

}
