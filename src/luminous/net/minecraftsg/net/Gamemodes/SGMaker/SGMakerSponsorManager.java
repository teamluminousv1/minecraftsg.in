package luminous.net.minecraftsg.net.Gamemodes.SGMaker;

public class SGMakerSponsorManager {

	@SuppressWarnings("unused")
	private SGMakerGame game;

	//items
	private boolean FNS;
	private boolean Enderpearl;
	private boolean Soup;
	private boolean Arrow;
	private boolean RawPorkchop;
	private boolean EnchantBottles;
	private boolean Cake;


	//Prices for the items
	private int FNSPrice;
	private int EnderpearlPrice;
	private int SoupPrice;
	private int ArrowPrice;
	private int RawPorkchopPrice;
	private int EnchantBottlesPrice;
	private int CakePrice;


	//Usage: new SponsorManager(game);
    public SGMakerSponsorManager(SGMakerGame game) {
		this.game = game;
		//Makes it so that the items are usable
		this.FNS = true;
		this.Enderpearl = true;
		this.Soup = true;
		this.Arrow = true;
		this.RawPorkchop = true;
		this.EnchantBottles = true;
		this.Cake = true;

		//Sets the prices of the items
		this.EnderpearlPrice = 150;
		this.FNSPrice = 50;
		this.SoupPrice = 20;
		this.ArrowPrice = 50;
		this.RawPorkchopPrice = 10;
		this.EnchantBottlesPrice = 100;
		this.CakePrice = 30;
    }

	//Sets the Item Avaliability
	public void setItemAvaliab(String itemID, boolean torf){
		if (itemID.equalsIgnoreCase("FNS")) {
			FNS = torf;
		}
		if (itemID.equalsIgnoreCase("Enderpearl")) {
			Enderpearl = torf;
		}
		if (itemID.equalsIgnoreCase("Soup")) {
			Soup = torf;
		}
		if (itemID.equalsIgnoreCase("Arrow")) {
			Arrow = torf;
		}
		if (itemID.equalsIgnoreCase("RawPorkchop")) {
			RawPorkchop = torf;
		}
		if (itemID.equalsIgnoreCase("EnchantBottles")) {
			EnchantBottles = torf;
		}
		if (itemID.equalsIgnoreCase("Cake")){
			Cake = torf;
		}else{
			return;
		}
	}

	//Gets the Item Avaliability
	public boolean getItemAvaliab(String itemID){
		if (itemID.equalsIgnoreCase("FNS")) {
			return FNS;
		}
		if (itemID.equalsIgnoreCase("Enderpearl")) {
			return Enderpearl;
		}
		if (itemID.equalsIgnoreCase("Soup")) {
			return Soup;
		}
		if (itemID.equalsIgnoreCase("Arrow")) {
			return Arrow;
		}
		if (itemID.equalsIgnoreCase("RawPorkchop")) {
			return RawPorkchop;
		}
		if (itemID.equalsIgnoreCase("EnchantBottles")) {
			return EnchantBottles;
		}
		if (itemID.equalsIgnoreCase("Cake")) {
			return Cake;
		}
		return false;
	}

	//Gets the Item Price
	public Integer getItemPrice(String itemID){
		if (itemID.equalsIgnoreCase("FNS")) {
			return FNSPrice;
		}
		if (itemID.equalsIgnoreCase("Enderpearl")) {
			return EnderpearlPrice;
		}
		if (itemID.equalsIgnoreCase("Soup")) {
			return SoupPrice;
		}
		if (itemID.equalsIgnoreCase("Arrow")) {
			return ArrowPrice;
		}
		if (itemID.equalsIgnoreCase("RawPorkchop")) {
			return RawPorkchopPrice;
		}
		if (itemID.equalsIgnoreCase("EnchantBottles")) {
			return EnchantBottlesPrice;
		}
		if (itemID.equalsIgnoreCase("Cake")) {
			return CakePrice;
		}
		return null;
	}


}
