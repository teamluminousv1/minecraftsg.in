package luminous.net.minecraftsg.net.Gamemodes.SGMaker;

public enum SGMakerGameState {
    WAITING, PREGAME, GRACEPERIOD,
    INGAME, DEATHMATCH, PREDEATHMATCH,
    ENDGAME, CLEANUP, RESTARTING
}
