package luminous.net.minecraftsg.net.Gamemodes.SGMaker;

import org.bukkit.entity.Player;

import java.util.HashMap;

public class SGMakerBountyManager {

    private SGMakerGame game;

    public HashMap<String, Integer> playerBounties;

    public SGMakerBountyManager(SGMakerGame game){
        this.game = game;
        this.playerBounties = new HashMap<>();
    }

    public void setupBounties(){
        for (String str : this.game.getTributes()){
            playerBounties.put(str, 0);
        }
    }

    public void addBounty(Player player, Integer amount){
        String playerName = player.getName();
        Integer previousInt = playerBounties.get(playerName);
        playerBounties.put(playerName, previousInt + amount);
    }

    public void removeBounty(Player player){
        String playerName = player.getName();
        playerBounties.remove(playerName);
    }

    public Integer getBounty(Player player){
        return playerBounties.get(player.getName());
    }
}
