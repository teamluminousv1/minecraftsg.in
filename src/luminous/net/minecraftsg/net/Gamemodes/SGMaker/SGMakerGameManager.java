package luminous.net.minecraftsg.net.Gamemodes.SGMaker;

import java.util.HashSet;
import java.util.Set;

public class SGMakerGameManager {
    //Set of all the games
    private Set<SGMakerGame> games;

    //Usage: new GameManager();
    public SGMakerGameManager() {
        //Creates fresh list for the games
        games = new HashSet<>();
    }
    //Adds a Game to the set
    public void addGame(SGMakerGame game) {
        games.add(game);
    }

    //Removes a Game from the set
    public void tempRemoveGame(SGMakerGame game){
        games.remove(game);
    }

    public Set<SGMakerGame> getGames(){
        return games;
    }
    //Gets the Game by ID
    public SGMakerGame getGameByID(int id) {
        for (SGMakerGame game : games) {
            if (game.getID() == id) {
                return game;
            }
        }

        return null;
    }
    //Get the Game at the Player
    public SGMakerGame getGameByPlayer(String player) {
        for (SGMakerGame game : games) {
            if (game.getPlayers().contains(player)) {
                return game;
            }
        }

        return null;
    }
}
