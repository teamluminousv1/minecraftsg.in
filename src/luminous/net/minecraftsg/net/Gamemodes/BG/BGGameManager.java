package luminous.net.minecraftsg.net.Gamemodes.BG;

import java.util.HashSet;
import java.util.Set;

public class BGGameManager {
    //Set of all the games
    private Set<BGGame> games;

    //Usage: new GameManager();
    public BGGameManager() {
        //Creates fresh list for the games
        games = new HashSet<>();
    }
    //Adds a Game to the set
    public void addGame(BGGame game) {
        games.add(game);
    }

    //Removes a Game from the set
    public void tempRemoveGame(BGGame game){
        games.remove(game);
    }

    public Set<BGGame> getGames(){
        return games;
    }
    //Gets the Game by ID
    public BGGame getGameByID(int id) {
        for (BGGame game : games) {
            if (game.getID() == id) {
                return game;
            }
        }

        return null;
    }
    //Get the Game at the Player
    public BGGame getGameByPlayer(String player) {
        for (BGGame game : games) {
            if (game.getPlayers().contains(player)) {
                return game;
            }
        }

        return null;
    }
}
