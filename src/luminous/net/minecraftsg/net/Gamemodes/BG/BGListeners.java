package luminous.net.minecraftsg.net.Gamemodes.BG;

import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.minecraftsg.net.Managers.Fireworks;
import luminous.net.minecraftsg.net.Sidebar.BG.Ingame;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import luminous.net.main.fiddycal.Main;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class BGListeners implements Listener {
	
	@EventHandler
	public void damage(EntityDamageEvent event) {
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
		    if (Main.main.getBGGameManager().getGameByPlayer(player.getName()) != null) {
		    	BGGame game = Main.main.getBGGameManager().getGameByPlayer(player.getName());
		    	if (game.getGamestate() == BGGameState.CLEANUP
		    		|| game.getGamestate() == BGGameState.RESTARTING
		    		|| game.getWaiting().contains(player.getName())) {
			    	event.setCancelled(true);
		    	}
		    }
		} else if (event.getEntity() instanceof ItemFrame) {
			event.setCancelled(true);
        }
	}
	
	@EventHandler
	public void itemFrameBreaking(HangingBreakByEntityEvent event) {
		if (event.getRemover() instanceof Player) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void damage(EntityDamageByEntityEvent event) {
		if (event.getDamager() instanceof Player) {
			Player player = (Player) event.getDamager();
		    if (Main.main.getBGGameManager().getGameByPlayer(player.getName()) != null) {
		    	BGGame game = Main.main.getBGGameManager().getGameByPlayer(player.getName());
		    	if (game.getWaiting().contains(player.getName())) {
			    	event.setCancelled(true);
		    	}
		    }
		}
	}
	
	@EventHandler
	public void blockBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
	    if (Main.main.getBGGameManager().getGameByPlayer(player.getName()) != null) {
	    	BGGame game = Main.main.getBGGameManager().getGameByPlayer(player.getName());
	    	if(game.getGamestate() == BGGameState.CLEANUP
	    		|| game.getGamestate() == BGGameState.RESTARTING
			    || game.getWaiting().contains(player.getName())) {
	    		event.setCancelled(true);
	    	} else {
	    		if (event.getBlock().getType() == Material.FIRE
			    	|| event.getBlock().getType() == Material.FLINT_AND_STEEL) {
			    	return;
			    } else {
					event.setCancelled(true);
			    }
	    	}
	    }
	}
	
	/*@EventHandler
	public void blockPlace(BlockPlaceEvent event) {
		Player player = event.getPlayer();
	    if (Main.main.getBGGameManager().getGameByPlayer(player.getName()) != null) {
	    	BGGame game = Main.main.getBGGameManager().getGameByPlayer(player.getName());
	    	if (game.getGamestate() == BGGameState.CLEANUP
	    		|| game.getGamestate() == BGGameState.RESTARTING
			    || game.getWaiting().contains(player.getName())) {
	    		event.setCancelled(true);
	    	} else {
	    		if (event.getBlock().getType() == Material.FIRE
		    		|| event.getBlock().getType() == Material.FLINT_AND_STEEL
		    		|| event.getBlock().getType() == Material.CAKE
		    		|| event.getBlock().getType() == Material.WEB) {
		    		return;
		    	} else {
				    event.setCancelled(true);
		    	}
	    	}
	    }
	}*/

	@EventHandler
	public void itemDrop(PlayerDropItemEvent event) {
		Player player = event.getPlayer();
	    if (Main.main.getBGGameManager().getGameByPlayer(player.getName()) != null) {
	    	BGGame game = Main.main.getBGGameManager().getGameByPlayer(player.getName());
	    	if (game.getGamestate() == BGGameState.RESTARTING
			    || game.getWaiting().contains(player.getName())) {
		    	event.setCancelled(true);
	    	}
	    }
	}
	
	@EventHandler
	public void itemPickup(PlayerPickupItemEvent event) {
		Player player = event.getPlayer();
	    if (Main.main.getBGGameManager().getGameByPlayer(player.getName()) != null) {
	    	BGGame game = Main.main.getBGGameManager().getGameByPlayer(player.getName());
	    	if (game.getGamestate() == BGGameState.CLEANUP
	    		|| game.getGamestate() == BGGameState.RESTARTING
			    || game.getWaiting().contains(player.getName())) {
		    	event.setCancelled(true);
	    	}
	    }
	}
	
	@EventHandler
	public void food(FoodLevelChangeEvent event) {
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
		    if (Main.main.getBGGameManager().getGameByPlayer(player.getName()) != null) {
		    	BGGame game = Main.main.getBGGameManager().getGameByPlayer(player.getName());
		    	if (game.getGamestate() == BGGameState.RESTARTING
				    || game.getWaiting().contains(player.getName())) {
			    	event.setCancelled(true);
		    	}
		    }
		}
	}
	
	@EventHandler
	public void fns(PlayerInteractEvent event) {
		if (Main.main.getBGGameManager().getGameByPlayer(event.getPlayer().getName()) != null) {
			if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				if (event.getMaterial() == Material.FLINT_AND_STEEL) {
					if (event.getClickedBlock().getType() == Material.IRON_FENCE
						|| event.getClickedBlock().getType() == Material.DEAD_BUSH
						|| event.getClickedBlock().getType() == Material.LONG_GRASS
						|| event.getBlockFace() != BlockFace.UP) {
						return;
					}
					ItemStack fns = event.getItem();
	                fns.setDurability((short)(fns.getDurability() + 17));
				}
			}
		}
	}


	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		if (Main.main.getBGGameManager().getGameByPlayer(e.getEntity().getName()) != null){
			BGGame game = Main.main.getBGGameManager().getGameByPlayer(e.getEntity().getName());
			e.getEntity().setHealth(20);
			e.getDrops().clear();
			e.setDeathMessage(null);
			Fireworks.sendFirework(e.getEntity().getLocation());
			if (game.getGamestate() == BGGameState.INGAME){
				if (e.getEntity().getKiller() != null) {
					if (game.getKills().get(e.getEntity().getKiller().getName()) != null) {
						Integer kills = game.getKills().get(e.getEntity().getKiller().getName());
						game.getKills().put(e.getEntity().getKiller().getName(), kills + 1);
					} else {
						game.getKills().put(e.getEntity().getKiller().getName(), 1);
					}
					Player killer = e.getEntity().getKiller();
					PotionEffect re = new PotionEffect(PotionEffectType.REGENERATION, 80, 1);
					PotionEffect ab = new PotionEffect(PotionEffectType.ABSORPTION, 2400, 1);
					for (PotionEffect effect : killer.getActivePotionEffects()) {
						killer.removePotionEffect(effect.getType());
					}
					killer.addPotionEffect(ab);
					killer.addPotionEffect(re);
					killer.getScoreboard().getTeam("kills").setSuffix(MessageManager.c("&f" + game.getKills().get(killer.getName())));
				}
				game.leaveFighting(e.getEntity());
				game.joinWaiting(e.getEntity());
			}
			for (String str : game.getPlayers()){
				Player player = Main.main.getServer().getPlayer(str);
				Ingame.updatePlayerKills(player);
			}

		}
	}
}
