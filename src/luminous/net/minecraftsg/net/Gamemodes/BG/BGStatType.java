package luminous.net.minecraftsg.net.Gamemodes.BG;

public enum BGStatType {

	GAMES_WON, GAMES_PLAYED, GAMES_LOST,
	CHESTS_OPENED, KILLS, DEATHMATCHES
	
}
