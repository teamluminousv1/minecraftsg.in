package luminous.net.minecraftsg.net.Gamemodes.BG;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;

public class BGStats implements Listener {

	public static void addStats(String playerName, BGStatType statType, int amount) {
		FileConfiguration config = ConfigManager.bgStatsCFG;
		FileConfiguration uuids = ConfigManager.uuidsCFG;
		if (config.getString(uuids.getString(playerName.toLowerCase())) != null) {
			String uuid = uuids.getString(playerName.toLowerCase());
			if (statType == BGStatType.CHESTS_OPENED) {
				config.set(uuid + ".Chests-Opened", config.getInt(uuid + ".Chests-Opened") + amount);
			}
			if (statType == BGStatType.GAMES_PLAYED) {
				config.set(uuid + ".Games-Played", config.getInt(uuid + ".Games-Played") + amount);
			}
			if (statType == BGStatType.KILLS) {
				config.set(uuid + ".Player-Kills", config.getInt(uuid + ".Player-Kills") + amount);
			}
			if (statType == BGStatType.GAMES_WON) {
				config.set(uuid + ".Games-Won", config.getInt(uuid + ".Games-Won") + amount);
			}
			if (statType == BGStatType.DEATHMATCHES) {
				config.set(uuid + ".Deathmatches", config.getInt(uuid + ".Deathmatches") + amount);
			}
			ConfigManager.saveSGStats();
		}
	}
	
	public static void removeStats(String playerName, BGStatType statType, int amount) {
		FileConfiguration config = ConfigManager.bgStatsCFG;
		FileConfiguration uuids = ConfigManager.uuidsCFG;
		if (config.getString(uuids.getString(playerName.toLowerCase())) != null) {
			String uuid = uuids.getString(playerName.toLowerCase());
			if (statType == BGStatType.CHESTS_OPENED) {
				config.set(uuid + ".Chests-Opened", config.getInt(uuid + ".Chests-Opened") - amount);
			}
			if (statType == BGStatType.GAMES_PLAYED) {
				config.set(uuid + ".Games-Played", config.getInt(uuid + ".Games-Played") - amount);
			}
			if (statType == BGStatType.KILLS) {
				config.set(uuid + ".Player-Kills", config.getInt(uuid + ".Player-Kills") - amount);
			}
			if (statType == BGStatType.GAMES_WON) {
				config.set(uuid + ".Games-Won", config.getInt(uuid + ".Games-Won") - amount);
			}
			if (statType == BGStatType.DEATHMATCHES) {
				config.set(uuid + ".Deathmatches", config.getInt(uuid + ".Deathmatches") - amount);
			}
			ConfigManager.saveSGStats();
		}
	}
	
	public boolean resetStats(String playerName) {
		FileConfiguration config = ConfigManager.bgStatsCFG;
		FileConfiguration uuids = ConfigManager.uuidsCFG;
		if (config.getString(uuids.getString(playerName.toLowerCase())) != null) {
			String uuid = uuids.getString(playerName.toLowerCase());
		    config.set(uuid + ".Chests-Opened", 0);
		    config.set(uuid + ".Games-Played", 0);
		    config.set(uuid + ".Player-Kills", 0);
		    config.set(uuid + ".Games-Won", 0);
		    config.set(uuid + ".Deathmatches", 0);
		    ConfigManager.saveSGStats();
			// { true == Player found. } else { Player not found. }
		    return true;
		} else {
			// { false == Player not found. } else { Player found. }
			return false;
		}
	}
	
	public void setupStats(Player player) {
		FileConfiguration config = ConfigManager.bgStatsCFG;
		config.set(player.getUniqueId().toString() + ".Chests-Opened", 0);
		config.set(player.getUniqueId().toString() + ".Games-Played", 0);
		config.set(player.getUniqueId().toString() + ".Player-Kills", 0);
		config.set(player.getUniqueId().toString() + ".Total-Lifespan.s", 0);
		config.set(player.getUniqueId().toString() + ".Total-Lifespan.m", 0);
		config.set(player.getUniqueId().toString() + ".Total-Lifespan.h", 0);
		config.set(player.getUniqueId().toString() + ".Total-Lifespan.d", 0);
		config.set(player.getUniqueId().toString() + ".Total-Lifespan.w", 0);
		config.set(player.getUniqueId().toString() + ".Games-Won", 0);
		config.set(player.getUniqueId().toString() + ".Deathmatches", 0);
		ConfigManager.saveSGStats();
	}
	
	public static void sendPlayerStats(Player player, String playerNameForStats) {
		FileConfiguration config = ConfigManager.bgStatsCFG;
		FileConfiguration uuids = ConfigManager.uuidsCFG;
		if (config.getString(uuids.getString(playerNameForStats.toLowerCase())) != null) {
			String uuid = uuids.getString(playerNameForStats.toLowerCase());
			int points = Integer.parseInt(ConfigManager.playersCFG.getString(uuid + ".Points"));
			int deathmatches = Integer.parseInt(config.getString(uuid + ".Deathmatches"));
			int chestOpened = Integer.parseInt(config.getString(uuid + ".Chests-Opened"));
			int gamesPlayed = Integer.parseInt(config.getString(uuid + ".Games-Played"));
			int playerKills = Integer.parseInt(config.getString(uuid + ".Player-Kills"));
			int gamesWon = Integer.parseInt(config.getString(uuid + ".Games-Won"));
			int s = config.getInt(player.getUniqueId().toString() + ".Total-Lifespan.s");
			int m = config.getInt(player.getUniqueId().toString() + ".Total-Lifespan.m");
			int h = config.getInt(player.getUniqueId().toString() + ".Total-Lifespan.h");
			int d = config.getInt(player.getUniqueId().toString() + ".Total-Lifespan.d");
			int w = config.getInt(player.getUniqueId().toString() + ".Total-Lifespan.w");
			if (Bukkit.getPlayer(playerNameForStats) != null) {
				player.sendMessage(MessageManager.c("&8&m     &r " + Bukkit.getPlayer(playerNameForStats).getDisplayName() + "&8'&fs stats &8&m     &r"));
			} else player.sendMessage(MessageManager.c("&8&m     &r " + ConfigManager.playersCFG.getString(uuid + ".Displayname") + "&8'&fs stats &8&m     &r"));
			player.sendMessage(MessageManager.c("&fGame Rank&8: &e#" + getGameRank(gamesWon)));
			player.sendMessage(MessageManager.c("&fPlayer Points&8: &e" + points));
			player.sendMessage(MessageManager.c("&fChests Opened&8: &e" + chestOpened));
			player.sendMessage(MessageManager.c("&fGames Played&8: &e" + gamesPlayed));
			player.sendMessage(MessageManager.c("&fPlayer Kills&8: &e" + playerKills));
			player.sendMessage(MessageManager.c("&fGames Won&8: &e" + gamesWon));
			player.sendMessage(MessageManager.c("&fDeathmatches&8: &e" + deathmatches));
			if ((gamesPlayed - gamesWon) == 0) {
				player.sendMessage(MessageManager.c("&fKill / Death Ratio&8: &eNo deaths"));
			} else player.sendMessage(MessageManager.c("&fKill / Death Ratio&8: &e" + playerKills / (gamesPlayed - gamesWon)));
			player.sendMessage(MessageManager.c("&fTotal Lifespan&8: &e" + w + "w" + d + "d" + h + "h" + m + "m" + s + "s"));
		} else {
			player.sendMessage(MessageManager.c(MessageManager.prefix + " " + MessageManager.playerNotFound));
		}
	}
	
	public static Integer getGameRank(Integer wins) {
		ArrayList<Integer> ranks = new ArrayList<Integer>();
		for (String uuid : ConfigManager.bgStatsCFG.getKeys(false)) {
			ranks.add(Integer.parseInt(ConfigManager.bgStatsCFG.getString(uuid + ".Games-Won")));
		}
		Integer[] ri = ranks.toArray(new Integer[ranks.size()]);
		Arrays.sort(ri, Collections.reverseOrder());
		for (int i = 0; i < ranks.size(); i++) {
			if (ri[i] == wins) {
				return i + 1;
			}
        }
		return 0;
	}
	
	@EventHandler
	public void serverJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		if (!ConfigManager.bgStatsCFG.contains(player.getUniqueId().toString())) {
			setupStats(player);
		}

	}
	
}
