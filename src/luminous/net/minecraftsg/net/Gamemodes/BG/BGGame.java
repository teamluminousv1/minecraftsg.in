package luminous.net.minecraftsg.net.Gamemodes.BG;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Commands.Fly;
import luminous.net.main.fiddycal.Managers.*;
import luminous.net.minecraftsg.net.Commands.SilentJoin;
import luminous.net.minecraftsg.net.Sidebar.BG.Ingame;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.DisplaySlot;

import java.util.*;

public class BGGame {

    //Game ID
    private int ID;
    //Gamestate
    private BGGameState gamestate;
    //Player Lists
    private List<String> players;
    private List<String> fighting;
    private List<String> waiting;
    private List<String> maps;

    //Map
    private String map;

    //Game Kills
    private Map <String, Integer> kills;

    //Player
    private String winner;

    private Integer zimeID;

    //Game Time
    private BGTimeManager tm;

    //Spawn for the Lobby of the game
    private Location waitingSpawn;
    //List of Locations for the game spawns
    private HashMap<String, Location> gameWarps;


    //Usage new Game(ID);
    public BGGame(int ID) {
        //Sets the Game ID
        this.ID = ID;
        this.map = null;
        //Generate fresh Lists
        this.waiting = new ArrayList<>();
        this.fighting = new ArrayList<>();
        this.players = new ArrayList<>();
        this.maps = new ArrayList<>();
        this.gameWarps = new HashMap<>();
        this.kills = new HashMap<>();
        //Sets the winner to null;
        this.winner = null;
        //Adds all ready maps to a map pool
        this.addMaps();
        //Sets Sets the Map to a random map
        this.randomMap();
        //Sets the TimeManager
        this.tm = new BGTimeManager(this);
        //Copies the map over
        WorldManager.copyWorld("plugins/SGIN-Core/BattleGrounds/Maps/"
                + this.getMap(), "", "BGGame-" + this.getID());
        //Sets up the spawns
        this.setupSpawns();
        //-----------------------------------
        //DO NOT ADD ANYTHING BELOW THIS LINE
        //-----------------------------------
        //Add the game to the Game Manager
        Main.main.getBGGameManager().addGame(this);
        Main.main.getZimeGames().newGame("BG-" + this.getID());
        this.zimeID = Main.main.getZimeGames().getZimeID("BG-" + this.getID());
        this.setGamestate(BGGameState.INGAME);
    }

    public Integer getZimeID(){
        return zimeID;
    }

    //Sets the gamestate for this game
    public void setGamestate(BGGameState gamestate){
        this.gamestate = gamestate;
    }
    //Gets the gamestate
    public BGGameState getGamestate(){
        return this.gamestate;
    }
    //Sets the Map
    public void setMap(String map){
        this.map = map;
    }
    //Gets the map
    public String getMap(){
        return this.map;
    }
    //Gets the Map of Kills
    public Map<String,Integer> getKills(){
        return this.kills;
    }
    //Sets the spawns Map
    public void setupSpawns() {
    	String name = "BGGame-" + this.getID();
        //Sets the Waiting Spawn
        int wx = Integer.parseInt(ConfigManager.bgDataCFG.getString("Maps." + this.getMap() + ".waiting.x"));
        int wy = Integer.parseInt(ConfigManager.bgDataCFG.getString("Maps." + this.getMap() + ".waiting.y"));
        int wz = Integer.parseInt(ConfigManager.bgDataCFG.getString("Maps." + this.getMap() + ".waiting.z"));
        Location ws = new Location(Bukkit.getWorld(name), wx + .5, wy, wz + .5);
        this.waitingSpawn = ws;
        
        //Sets the West Spawn
        int ax = Integer.parseInt(ConfigManager.bgDataCFG.getString("Maps." + this.getMap() + ".west.x"));
        int ay = Integer.parseInt(ConfigManager.bgDataCFG.getString("Maps." + this.getMap() + ".west.y"));
        int az = Integer.parseInt(ConfigManager.bgDataCFG.getString("Maps." + this.getMap() + ".west.z"));
        Location as = new Location(Bukkit.getWorld(name), ax + .5, ay, az + .5);
        this.gameWarps.put("West", as);

        //Sets the East Spawn
        int bx = Integer.parseInt(ConfigManager.bgDataCFG.getString("Maps." + this.getMap() + ".east.x"));
        int by = Integer.parseInt(ConfigManager.bgDataCFG.getString("Maps." + this.getMap() + ".east.y"));
        int bz = Integer.parseInt(ConfigManager.bgDataCFG.getString("Maps." + this.getMap() + ".east.z"));
        Location bs = new Location(Bukkit.getWorld(name), bx + .5, by, bz + .5);
        this.gameWarps.put("East", bs);

        //Sets the South Spawn
        int cx = Integer.parseInt(ConfigManager.bgDataCFG.getString("Maps." + this.getMap() + ".south.x"));
        int cy = Integer.parseInt(ConfigManager.bgDataCFG.getString("Maps." + this.getMap() + ".south.y"));
        int cz = Integer.parseInt(ConfigManager.bgDataCFG.getString("Maps." + this.getMap() + ".south.z"));
        Location cs = new Location(Bukkit.getWorld(name), cx + .5, cy, cz + .5);
        this.gameWarps.put("South", cs);

        //Sets the South Spawn
        int dx = Integer.parseInt(ConfigManager.bgDataCFG.getString("Maps." + this.getMap() + ".north.x"));
        int dy = Integer.parseInt(ConfigManager.bgDataCFG.getString("Maps." + this.getMap() + ".north.y"));
        int dz = Integer.parseInt(ConfigManager.bgDataCFG.getString("Maps." + this.getMap() + ".north.z"));
        Location ds = new Location(Bukkit.getWorld(name), dx + .5, dy, dz + .5);
        this.gameWarps.put("North", ds);

        //Sets the Center Spawn
        int ex = Integer.parseInt(ConfigManager.bgDataCFG.getString("Maps." + this.getMap() + ".center.x"));
        int ey = Integer.parseInt(ConfigManager.bgDataCFG.getString("Maps." + this.getMap() + ".center.y"));
        int ez = Integer.parseInt(ConfigManager.bgDataCFG.getString("Maps." + this.getMap() + ".center.z"));
        Location es = new Location(Bukkit.getWorld(name), ex + .5, ey, ez + .5);
        this.gameWarps.put("Center", es);
        //TODO add pitches to all the locations
    }
    //Gets the Highest Kills
    public String getHighestKills() {
    	if (!this.getPlayers().isEmpty()) {
    		Random r = new Random();
        	int i = r.nextInt(this.getPlayers().size());
        	String winningKills = this.getPlayers().get(i);
        	if (this.kills == null || this.kills.isEmpty()) {
        		return winningKills;
        	}
            for (Map.Entry<String, Integer> entry : this.kills.entrySet()) {
                if (entry.getValue() > this.kills.get(winningKills)) {
                    winningKills = entry.getKey();
                }
            }
            return winningKills;
    	} else {
    		return "None";
    	}
    }
    //Gets all the spawn locations Map
    public HashMap<String, Location> getGameWarps() {
        return gameWarps;
    }
    //Get Display Gamestate
    public String getDisplayGamestate(BGGameState gamestate){
        if (gamestate == BGGameState.INGAME){
            return "LiveGame";
        }
        if (gamestate == BGGameState.CLEANUP){
            return "Cleanup";
        }
        if (gamestate == BGGameState.RESTARTING){
            return "Restarting";
        }
        return null;
    }

    //Messages Everyone in the game
    public void messageEveryone(String message){
        for (String pl: players) {
            if (Bukkit.getPlayer(pl) != null){
                Player player = Bukkit.getPlayer(pl);
                player.sendMessage(message);
            }

        }
    }

    //Get ID
    public int getID() {
        return ID;
    }
    //Get List of Waiting in this game
    public List<String> getWaiting() {
        return waiting;
    }
    //Get List of Fighting in this game
    public List<String> getFighting() {
        return fighting;
    }
    //Get Total of Players in this game
    public List<String> getPlayers() {
        return players;
    }
    //Get TimeManager of this game
    public BGTimeManager getTm() {
        return tm;
    }

    //Sets the Winner of the Game
    public void setWinner(String player){
		//BGStats.addStats(player, BGStatType.GAMES_WON, 1);
        this.winner = player;
    }

    //Adds player to the fighting arraylist
    public void joinFighting(Player player){
        String playerName = player.getName();
        this.getFighting().add(playerName);
        /*

        Note to Caleb, add in the edit GUI so that it gets the kit from there

         */
        //Adds the items to the inventory
        player.getInventory().setItem(0, new ItemStack(Material.IRON_SWORD));
        player.getInventory().setItem(1, new ItemStack(Material.FISHING_ROD));
        player.getInventory().setItem(3, new ItemStack(Material.BOW));
        player.getInventory().setItem(4, new ItemStack(Material.ARROW, 11));
        player.getInventory().setItem(8, new ItemStack(Material.COOKED_BEEF, 32));
        player.getInventory().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
        player.getInventory().setHelmet(new ItemStack(Material.IRON_HELMET));
        player.getInventory().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
        player.getInventory().setBoots(new ItemStack(Material.IRON_BOOTS));
        player.setGameMode(GameMode.SURVIVAL);
    }
    //Removes player from the fighting arraylist
    public void leaveFighting(Player player){
        String playerName = player.getName();
        this.getFighting().remove(playerName);
        //Clears the inventory of the player
        player.getInventory().clear();
    }
    //Adds player to the waiting arraylist
    public void joinWaiting(Player player){
        player.teleport(this.waitingSpawn);
        String playerName = player.getName();
        this.getWaiting().add(playerName);
        //Random Util stuff
        player.setHealth(20.0);
        player.setFoodLevel(20);
        Fly.stopFlight(player);
        player.getInventory().clear();
        player.setGameMode(GameMode.SURVIVAL);
        //Teleport the player to the right location
        //Sets the items of the player when they are ready
        //TODO custom items to edit the GUI and warp to the locations
    }
    //Removes player from the waiting arraylist
    public void leaveWaiting(Player player){
        String playerName = player.getName();
        this.getWaiting().remove(playerName);
        player.getInventory().clear();
    }

    //Gets the player to join the game!
    public void joinGame(Player player){
        if (this.getPlayers().size() > 40) {
            player.sendMessage(MessageManager.c(MessageManager.BGprefix + " &4This game seems to be full&8."));
            player.sendMessage(MessageManager.c(MessageManager.BGprefix + " &4Purchase a rank to get priority access&8."));
            return;
        } else {
            if (this.getGamestate() == BGGameState.INGAME) {
                //If the player stats do not exist, then put them in the Hashmap as 0.
                if (!this.getKills().containsKey(player.getName())){
                    this.getKills().put(player.getName(), 0);
                }
                //Add to the player arraylist
                this.getPlayers().add(player.getName());
                this.joinWaiting(player);
                RankManager.setDisplayName(player, false);
                if (!(SilentJoin.isSilent(player))){
                    this.messageEveryone(MessageManager.c("&r" + player.getDisplayName() + " &6has joined&8."));
                }
                player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setDisplaySlot(null);
                Ingame.setIngameScoreboard(player);
                Ingame.updatePlayerKills(player);
            }else{
                //Cant Join
                player.sendMessage(MessageManager.c(MessageManager.BGprefix + "&cYou cannot currently join the game!"));
                return;
            }
        }
    }

    //Gets the Location of the Waiting Spawn
    public Location getWaitingSpawn(){
        return this.waitingSpawn;
    }

    //Gets the player to leave the game!
    public void leaveGame(Player player){
        if (this.getPlayers().contains(player.getName())) {
            this.getPlayers().remove(player.getName());
        }
        if (this.getWaiting().contains(player.getName())) {
            this.getWaiting().remove(player.getName());
        }
        if (this.getFighting().contains(player.getName())) {
            this.getFighting().remove(player.getName());
        }
        if (!(SilentJoin.isSilent(player))){
            this.messageEveryone(MessageManager.c("&r" + player.getDisplayName() + " &6has left&8."));
        }
        for (String pl: this.getPlayers()) {
            Player player1 = Main.main.getServer().getPlayer(pl);
            if (player1.getScoreboard().getTeam("players") != null) {
                player1.getScoreboard().getTeam("players").setSuffix(MessageManager.c(" &f" + this.getFighting().size()));
            }
            if (player1.getScoreboard().getTeam("watching") != null) {
                player1.getScoreboard().getTeam("watching").setSuffix(MessageManager.c(" &f" + this.getWaiting().size()));
            }
        }
        RankManager.setDisplayName(player, false);
    }

    //Sets a random map
    public void randomMap(){
        Collections.shuffle(this.getMaps());
        this.map = this.getMaps().get(0);
    }
    
    //Adds all finished maps to the map pool
    public void addMaps(){
    	if (!this.getMaps().isEmpty()) {
    		this.getMaps().clear();
    	}
        for (String id : ConfigManager.bgDataCFG.getConfigurationSection("Maps").getKeys(false)) {
            this.maps.add(id);
        }
    }
    
    //gets all bg maps for this game
    public List<String> getMaps(){
        return this.maps;
    }

    //Gets the Winner
    public String getWinner(){
        return winner;
    }

    //Method for teleporting the player based on the location given!
    public void teleportPlayer(Player player, String location){
        if (location.equalsIgnoreCase("North")){
            Location north = this.gameWarps.get("North");
            player.teleport(north);
            player.sendMessage(MessageManager.c(MessageManager.BGprefix + " &fWarped &2North&f!"));
        }
        if (location.equalsIgnoreCase("West")){
            Location west = this.gameWarps.get("West");
            player.teleport(west);
            player.sendMessage(MessageManager.c(MessageManager.BGprefix + " &fWarped &2West&f!"));
        }
        if (location.equalsIgnoreCase("East")){
            Location east = this.gameWarps.get("East");
            player.teleport(east);
            player.sendMessage(MessageManager.c(MessageManager.BGprefix + " &fWarped &2East&f!"));
        }
        if (location.equalsIgnoreCase("South")){
            Location south = this.gameWarps.get("South");
            player.teleport(south);
            player.sendMessage(MessageManager.c(MessageManager.BGprefix + " &fWarped &2South&f!"));
        }
        if (location.equalsIgnoreCase("Center")){
            Location center = this.gameWarps.get("Center");
            player.teleport(center);
            player.sendMessage(MessageManager.c(MessageManager.BGprefix + " &fWarped &2Center&f!"));
        }
    }

    public void newMap(){
        this.players = new ArrayList<>();
        this.waiting = new ArrayList<>();
        this.fighting = new ArrayList<>();
        this.kills = new HashMap<>();
        this.winner = null;
        this.getMaps().remove(this.getMaps().get(0));
        this.randomMap();
        WorldManager.copyWorld("plugins/SGIN-Core/BattleGrounds/Maps/"
                + this.getMap(), "", "BGGame-" + this.getID());
        this.setupSpawns();
        this.setGamestate(BGGameState.INGAME);
    }
    
    public void newGame(){
        this.players = new ArrayList<>();
        this.waiting = new ArrayList<>();
        this.fighting = new ArrayList<>();
        this.maps = new ArrayList<>();
        this.kills = new HashMap<>();
        this.winner = null;
        this.addMaps();
        this.randomMap();
        WorldManager.copyWorld("plugins/SGIN-Core/BattleGrounds/Maps/"
                + this.getMap(), "", "BGGame-" + this.getID());
        this.setupSpawns();
        this.setGamestate(BGGameState.INGAME);
    }

}
