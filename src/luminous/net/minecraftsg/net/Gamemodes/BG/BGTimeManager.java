package luminous.net.minecraftsg.net.Gamemodes.BG;

import luminous.net.main.fiddycal.Events.Connect;
import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.main.fiddycal.Managers.RankManager;

import luminous.net.main.fiddycal.Managers.WorldManager;

import org.bukkit.entity.*;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;

public class BGTimeManager extends BukkitRunnable{

    //Game Class
    private BGGame game;

    //Time integers
    public int inGame;
    public int restarting;
    public int cleanUp;

    //Usage: new TimeManager(Game);
    public BGTimeManager(BGGame game){
        //Sets game
        this.game = game;
        //Sets Game Times from confguration file
        this.inGame = 360;
        this.cleanUp = 5;
        this.restarting = 10;
        //-----------------------------------
        //DO NOT ADD ANYTHING BELOW THIS LINE
        //-----------------------------------
        //Run This Class
        this.runTaskTimer(Main.main, 0, 20);
    }

    public Integer getMins(Integer ints){
        return ints/60;
    }

    public Integer getSeconds(Integer ints){
        return ints - getMins(ints) * 60;
    }

    public String scoreboardTime(Integer ints){
        String mins = "";
        String secs = "";
        if (getMins(ints) < 10){
            mins = "0" + getMins(ints);
        }else{
            mins = String.valueOf(getMins(ints));
        }
        if (getSeconds(ints) < 10){
            secs = "0" + getSeconds(ints);
        }else{
            secs = String.valueOf(getSeconds(ints));
        }

        return mins + ":" + secs;
    }


    @Override
    public void run() {
        BGGameState currentState = this.game.getGamestate();
        if (currentState == BGGameState.INGAME){
            if (this.inGame != 0){
                Integer mins = getMins(this.inGame);
                Integer secs = getSeconds(this.inGame);
                if (mins == 5 && secs == 0 ||
                        mins == 4 && secs == 0 ||
                        mins == 3 && secs == 0 ||
                        mins == 2 && secs == 0){
                    //If the time is 4 mins 3 mins 2 mins 1 mins
                    this.game.messageEveryone(MessageManager.c(MessageManager.BGprefix + " &8[&6" + mins + "&8] &eminutes until the next game&8!"));
                }
                /* if (mins == 1 && secs == 0 ||
                		mins == 5 && secs == 30 ||
                        mins == 4 && secs == 30 ||
                        mins == 3 && secs == 30 ||
                        mins == 2 && secs == 30 ||
                        mins == 1 && secs == 30){
                    //If the time is 4.5 mins 3.5 mins 2.5 mins 1.5 mins
                    this.game.messageEveryone(MessageManager.c(MessageManager.BGprefix + " &cThe game ends in &8[&e" + mins + ".5&8] &cminutes."));
                } */
                if (mins == 1 && secs == 0 ||
                		mins == 0 && secs == 30 ||
                        mins == 0 && secs == 15 ||
                        mins == 0 && secs == 10 ||
                        mins == 0 && secs == 5 ||
                        mins == 0 && secs == 4 ||
                        mins == 0 && secs == 3 ||
                        mins == 0 && secs == 2){
                    //If the time is below 1 eg 30, 15, 10, etc
                    this.game.messageEveryone(MessageManager.c(MessageManager.BGprefix + " &8[&6" + this.inGame + "&8] &eseconds until the next game&8!"));
                }
                if (mins == 0 && secs == 1){
                    // If the time is 1
                    this.game.messageEveryone(MessageManager.c(MessageManager.BGprefix + " &8[&6" + secs + "&8] &esecond until the next game&8!"));
                }
                if (!this.game.getPlayers().isEmpty()) {
                	for (String pl: this.game.getPlayers()){
                        Player player = Main.main.getServer().getPlayer(pl);
                        player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setDisplayName(MessageManager.c(player.getDisplayName() + " &8-" + " &c" + scoreboardTime(this.inGame)));
                        //TODO set the timer in the EXP bar for each player!
                    }
                }
                this.inGame--;
            }else{
            	this.game.setWinner(this.game.getHighestKills());
            	String winnerUUID = ConfigManager.uuidsCFG.getString(this.game.getWinner());
            	String winner = ConfigManager.playersCFG.getString(winnerUUID + ".Displayname");
            	this.game.messageEveryone(MessageManager.c("&6&lThe winner this round was... " + winner + "&6!"));
            	this.game.messageEveryone(MessageManager.c("&6The map will rotate in 5 seconds."));
                this.game.setGamestate(BGGameState.CLEANUP);
                this.game.setWinner(this.game.getHighestKills());
            }
        }
        if (currentState == BGGameState.CLEANUP){
            if (this.cleanUp != 0){
            	if (!this.game.getPlayers().isEmpty()) {
            		for (String pl: this.game.getPlayers()){
                        Player player = Main.main.getServer().getPlayer(pl);
                        player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setDisplayName(MessageManager.c(player.getDisplayName() + " &8-" + " &c" + scoreboardTime(this.cleanUp)));
                    }
            	}
                this.cleanUp--;
            }else{
            	if (this.game.getMaps().size() == 1) {
            		if (!this.game.getPlayers().isEmpty()) {
                		for (String str : this.game.getPlayers()){
                            Player player = Main.main.getServer().getPlayer(str);
                            Connect.sendToLobby(player);
                            Connect.giveLobbyItems(player);
                            RankManager.setDisplayName(player, false);
                        }
                        this.game.getPlayers().clear();
                        this.game.getWaiting().clear();
                        this.game.getFighting().clear();
                	}
                    WorldManager.deleteWorld("", "BGGame-" + this.game.getID());
                    this.game.setGamestate(BGGameState.RESTARTING);
            	} else {
            		if (!this.game.getPlayers().isEmpty()) {
            			for (String str : this.game.getPlayers()){
                            Player player = Main.main.getServer().getPlayer(str);
                            Connect.sendToLobby(player);
                        }
            		}
            		this.game.newMap();
            		if (!this.game.getPlayers().isEmpty()) {
            			for (String str : this.game.getPlayers()){
                            Player player = Main.main.getServer().getPlayer(str);
                            player.teleport(this.game.getWaitingSpawn());
                        }
            		}
            	}
            }
        }
        if (currentState == BGGameState.RESTARTING){
            if (this.restarting != 0){
                this.restarting--;
            }else{
            	this.game.newGame();
                this.inGame = 360;
                this.cleanUp = 5;
                this.restarting = 10;
            }
        }
    }
}
