package luminous.net.minecraftsg.net.Gamemodes.a_GUI;

import java.util.ArrayList;

import luminous.net.minecraftsg.net.Gamemodes.BG.BGGame;
import luminous.net.minecraftsg.net.Gamemodes.BG.BGGameState;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.MessageManager;
import org.bukkit.inventory.meta.SkullMeta;

public class BGGUI {

	public static ItemStack sortID(BGGame game) {
		int size; if (game.getPlayers().size() == 0) { 
		size = 1; } else { size = game.getPlayers().size(); }
		ItemStack item = new ItemStack(Material.STAINED_CLAY, size);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(MessageManager.c("&aAUBG " + game.getZimeID()));
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(MessageManager.c("&cPlayers: &f" + game.getPlayers().size() + "/40"));
		lore.add(MessageManager.c(""));
		lore.add(MessageManager.c("&a" + game.getDisplayGamestate(game.getGamestate()).toUpperCase()));
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}
	
	public static void openBGGUI(Player player) {
		Inventory bgInv = Bukkit.createInventory(null, 54, MessageManager.c("&bBattlefield"));

		ItemStack title = new ItemStack(Material.SKULL_ITEM, 1);
		SkullMeta meta = (SkullMeta) title.getItemMeta();
		meta.setDisplayName(MessageManager.c("&bGame Menu"));
		meta.setOwner(player.getName());
		title.setItemMeta(meta);

		ItemStack title2 = new ItemStack(Material.IRON_SWORD, 1);
		ItemMeta meta2 = title2.getItemMeta();
		meta2.setDisplayName(MessageManager.c("&aBattlefield"));
		title2.setItemMeta(meta2);

		bgInv.setItem(0, new ItemStack(Material.STONE));
		bgInv.setItem(1, new ItemStack(Material.STONE));
		bgInv.setItem(2, new ItemStack(Material.STONE));
		bgInv.setItem(3, title2);
		bgInv.setItem(4, title);
		bgInv.setItem(5, title2);
		bgInv.setItem(6, new ItemStack(Material.STONE));
		bgInv.setItem(7, new ItemStack(Material.STONE));
		bgInv.setItem(8, new ItemStack(Material.STONE));

		for (BGGame game : Main.main.getBGGameManager().getGames()) {
			if (game.getGamestate() == BGGameState.INGAME) {
				ItemStack item = sortID(game);
				item.setDurability((short)4);
				bgInv.addItem(item);
			}
		}

		for (BGGame game : Main.main.getBGGameManager().getGames()) {
			if (game.getGamestate() == BGGameState.CLEANUP) {
				ItemStack item = sortID(game);
				item.setDurability((short)14);
				bgInv.addItem(item);
			}
		}

		for (BGGame game : Main.main.getBGGameManager().getGames()) {
			if (game.getGamestate() == BGGameState.RESTARTING) {
				ItemStack item = sortID(game);
				item.setDurability((short)14);
				bgInv.addItem(item);
			}
		}


		bgInv.setItem(0, null);
		bgInv.setItem(1, null);
		bgInv.setItem(2, null);
		bgInv.setItem(3, title2);
		bgInv.setItem(4, title);
		bgInv.setItem(5, title2);
		bgInv.setItem(6, null);
		bgInv.setItem(7, null);
		bgInv.setItem(8, null);
		player.openInventory(bgInv);
	}
	
}
