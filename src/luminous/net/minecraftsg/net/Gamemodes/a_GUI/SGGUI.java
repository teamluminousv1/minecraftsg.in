package luminous.net.minecraftsg.net.Gamemodes.a_GUI;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGame;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGameState;

public class SGGUI {

	public static ItemStack sortID(SGGame game) {
		int size; if (game.getPlayers().size() == 0) { 
		size = 1; } else { size = game.getPlayers().size(); }
		ItemStack item = new ItemStack(Material.STAINED_CLAY, size);
		ItemMeta meta = item.getItemMeta();
		Integer gameID = game.getID();
		meta.setDisplayName(MessageManager.c("&aAUSG " + Main.main.getZimeGames().getZimeID("SG-" + gameID)));
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(MessageManager.c("&cPlayers: &f" + game.getPlayers().size() + "/24"));
		lore.add(MessageManager.c(""));
		lore.add(MessageManager.c("&a" + game.getDisplayGamestate(game.getGamestate()).toUpperCase()));
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}
	
	public static void openSGGUI(Player player) {

		Inventory sgInv = Bukkit.createInventory(null, 54, MessageManager.c("&bSurvival Games"));
		
		ItemStack title = new ItemStack(Material.SKULL_ITEM, 1);
		SkullMeta meta = (SkullMeta) title.getItemMeta();
		meta.setDisplayName(MessageManager.c("&bGame Menu"));
        meta.setOwner(player.getName());
		title.setItemMeta(meta);
		
		ItemStack title2 = new ItemStack(Material.BOW, 1);
		ItemMeta meta2 = title2.getItemMeta();
		meta2.setDisplayName(MessageManager.c("&aSurvival Games"));
		title2.setItemMeta(meta2);
		
		sgInv.setItem(0, new ItemStack(Material.STONE));
		sgInv.setItem(1, new ItemStack(Material.STONE));
		sgInv.setItem(2, new ItemStack(Material.STONE));
		sgInv.setItem(3, title2);
		sgInv.setItem(4, title);
		sgInv.setItem(5, title2);
		sgInv.setItem(6, new ItemStack(Material.STONE));
		sgInv.setItem(7, new ItemStack(Material.STONE));
		sgInv.setItem(8, new ItemStack(Material.STONE));
		
		for (SGGame game : Main.main.getSGGameManager().getGames()) {
			if (game.getGamestate() == SGGameState.WAITING) {
				ItemStack item = sortID(game);
				item.setDurability((short)5);
				sgInv.addItem(item);
			}
		}
		
		for (SGGame game : Main.main.getSGGameManager().getGames()) {
			if (game.getGamestate() == SGGameState.PREGAME) {
				ItemStack item = sortID(game);
				item.setDurability((short)4);
				sgInv.addItem(item);
			}
		}
		
		for (SGGame game : Main.main.getSGGameManager().getGames()) {
			if (game.getGamestate() == SGGameState.GRACEPERIOD) {
				ItemStack item = sortID(game);
				item.setDurability((short)4);
				sgInv.addItem(item);
			}
		}
		
		for (SGGame game : Main.main.getSGGameManager().getGames()) {
			if (game.getGamestate() == SGGameState.INGAME) {
				ItemStack item = sortID(game);
				item.setDurability((short)4);
				sgInv.addItem(item);
			}
		}
		
		for (SGGame game : Main.main.getSGGameManager().getGames()) {
			if (game.getGamestate() == SGGameState.PREDEATHMATCH) {
				ItemStack item = sortID(game);
				item.setDurability((short)4);
				sgInv.addItem(item);
			}
		}
		
		for (SGGame game : Main.main.getSGGameManager().getGames()) {
			if (game.getGamestate() == SGGameState.DEATHMATCH) {
				ItemStack item = sortID(game);
				item.setDurability((short)4);
				sgInv.addItem(item);
			}
		}
		
		for (SGGame game : Main.main.getSGGameManager().getGames()) {
			if (game.getGamestate() == SGGameState.ENDGAME) {
				ItemStack item = sortID(game);
				item.setDurability((short)14);
				sgInv.addItem(item);
			}
		}
		
		for (SGGame game : Main.main.getSGGameManager().getGames()) {
			if (game.getGamestate() == SGGameState.CLEANUP) {
				ItemStack item = sortID(game);
				item.setDurability((short)14);
				sgInv.addItem(item);
			}
		}
		
		for (SGGame game : Main.main.getSGGameManager().getGames()) {
			if (game.getGamestate() == SGGameState.RESTARTING) {
				ItemStack item = sortID(game);
				item.setDurability((short)14);
				sgInv.addItem(item);
			}
		}
		
		sgInv.setItem(0, null);
		sgInv.setItem(1, null);
		sgInv.setItem(2, null);
		sgInv.setItem(3, title2);
		sgInv.setItem(4, title);
		sgInv.setItem(5, title2);
		sgInv.setItem(6, null);
		sgInv.setItem(7, null);
		sgInv.setItem(8, null);
		player.openInventory(sgInv);

		
	}
	
}
