package luminous.net.minecraftsg.net.Gamemodes.a_GUI;

import java.util.ArrayList;
import java.util.List;

import luminous.net.main.fiddycal.Events.Connect;
import luminous.net.main.fiddycal.Main;
import luminous.net.minecraftsg.net.Gamemodes.BG.BGGame;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGame;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.main.fiddycal.Managers.Connections.Servers;

public class GUI implements Listener {

	public static void openLobbyGUI(Player player){

		Inventory inv = Bukkit.createInventory(null, 9, MessageManager.c("Lobby Selector"));
		
		int size = Main.main.getServer().getWorld(Main.main.getConfig().getString("Spawn.world")).getPlayers().size();
		ItemStack eye = new ItemStack(Material.EYE_OF_ENDER, size);
		ItemMeta eyeMeta = eye.getItemMeta();
		eyeMeta.setDisplayName(MessageManager.c("&aHUB 1"));
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(MessageManager.c("&cPlayers: &f" + size + "/48"));
		eyeMeta.setLore(lore);
		eye.setItemMeta(eyeMeta);
		
		inv.addItem(eye);
		
	}
	
	public static void openMainGUI(Player player){

		World hubWorld = Main.main.getServer().getWorld(Main.main.getConfig().getString("Spawn.world"));
		String s = MessageManager.c("&l" + Main.main.getConfig().getString("Unicode.square"));
		
		int sgOnline = 0;
		int bgOnline = 0;
		int hubOnline = hubWorld.getPlayers().size();
		
		for (SGGame game : Main.main.getSGGameManager().getGames()) {
			sgOnline = sgOnline + game.getPlayers().size();
		}

		for (BGGame game : Main.main.getBGGameManager().getGames()) {
			bgOnline = bgOnline + game.getPlayers().size();
		}
		
		//Creates the Inventory
		Inventory mainInv = Bukkit.createInventory(null, 45, MessageManager.c("MinecraftSG Network Navigation"));
		
		//Creates the ItemStacks
		ItemStack hub = new ItemStack(Material.WATCH, 1);
		ItemMeta hubMeta = hub.getItemMeta();
		List<String> hubLore = new ArrayList<String>();
		hubMeta.setDisplayName(MessageManager.c("            &2MinecraftSG Hub"));
		hubLore.add(MessageManager.c("&8" + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s));
		hubLore.add(MessageManager.c(""));
		hubLore.add(MessageManager.c("&b            [ Join " + "&e" + hubOnline + " &bPlayers ]"));
		hubLore.add(MessageManager.c(""));
		hubLore.add(MessageManager.c("&aUse the MinecraftSG hub to travel"));
		hubLore.add(MessageManager.c("&a   between the different games."));
		hubLore.add(MessageManager.c("&a     Take a moment to hang out"));
		hubLore.add(MessageManager.c("&a      and chat with friends, or"));
		hubLore.add(MessageManager.c("&a             join a game and"));
		hubLore.add(MessageManager.c("&a      get right into the action!"));
		//hubLore.add(MessageManager.c(""));
		//hubLore.add(MessageManager.c("&b     Left Click &7to &b&lBrowse Servers"));
		hubLore.add(MessageManager.c(""));
		hubLore.add(MessageManager.c("&8" + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s));
		hubMeta.setLore(hubLore);
		hub.setItemMeta(hubMeta);

		ItemStack sg = new ItemStack(Material.BOW, 1);
		ItemMeta sgMeta = sg.getItemMeta();
		List<String> sgLore = new ArrayList<String>();
		sgMeta.setDisplayName(MessageManager.c("           &6Survival Games"));
		sgLore.add(MessageManager.c("&8" + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s));
		sgLore.add(MessageManager.c(""));
		sgLore.add(MessageManager.c("&b         [ Join " + "&e" + sgOnline + " &bPlayers ]"));
		sgLore.add(MessageManager.c(""));
		sgLore.add(MessageManager.c("&eThe premier MCSG experience!"));
		sgLore.add(MessageManager.c("&e    Join 23 other tributes in"));
		sgLore.add(MessageManager.c("&e        a fight for victory!"));
		sgLore.add(MessageManager.c(""));
		sgLore.add(MessageManager.c("&bLeft Click &7to &b&lBrowse Servers"));
		sgLore.add(MessageManager.c(""));
		sgLore.add(MessageManager.c("&8" + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s));
		sgMeta.setLore(sgLore);
		sg.setItemMeta(sgMeta);

		ItemStack bg = new ItemStack(Material.IRON_SWORD, 1);
		ItemMeta bgMeta = bg.getItemMeta();
		List<String> bgLore = new ArrayList<String>();
		bgMeta.setDisplayName(MessageManager.c("             &6Battlefield"));
		bgLore.add(MessageManager.c("&8" + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s));
		bgLore.add(MessageManager.c(""));
		bgLore.add(MessageManager.c("&b         [ Join " + "&e" + bgOnline + " &bPlayers ]"));
		bgLore.add(MessageManager.c(""));
		bgLore.add(MessageManager.c("&e    A PvP free-for-all, fight"));
		bgLore.add(MessageManager.c("&e          your way to the"));
		bgLore.add(MessageManager.c("&e        top in Battlefield!"));
		bgLore.add(MessageManager.c(""));
		bgLore.add(MessageManager.c("&bLeft Click &7to &b&lBrowse Servers"));
		bgLore.add(MessageManager.c(""));
		bgLore.add(MessageManager.c("&8" + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s));
		bgMeta.setLore(bgLore);
		bg.setItemMeta(bgMeta);

		ItemStack comingSoon = new ItemStack(Material.FIREWORK_CHARGE, 1);
		ItemMeta comingSoonMeta = comingSoon.getItemMeta();
		List<String> comingSoonLore = new ArrayList<String>();
		comingSoonMeta.setDisplayName(MessageManager.c("&8[ &cRemoved &8]"));
		comingSoonLore.add(MessageManager.c("&4&oThis gamemode was removed"));
		comingSoonLore.add(MessageManager.c("&4&odue to the lack of activity&8."));
		comingSoonMeta.setLore(comingSoonLore);
		comingSoon.setItemMeta(comingSoonMeta);

		//Sets the positions of the items in the GUI
		mainInv.setItem(3, sg);
		mainInv.setItem(5, bg);
		mainInv.setItem(22, hub);
		mainInv.setItem(11, comingSoon);
		mainInv.setItem(15, comingSoon);
		mainInv.setItem(29, comingSoon);
		mainInv.setItem(33, comingSoon);
		mainInv.setItem(39, comingSoon);
		mainInv.setItem(41, comingSoon);

		//Opens the inventory
		player.openInventory(mainInv);
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		
		Player player = (Player) event.getWhoClicked();
	    ItemStack clicked = event.getCurrentItem();
	    Inventory inv = event.getInventory();
	    
	    if (clicked == null) {
		    return;
	    }

	    if (clicked.getType() == null) {
	    	return;
	    }
	    
	    if (clicked.getType() == Material.AIR) {
	    	return;
	    }
	    //If the inventory name is Survival Games
	    if (inv.getName().contains(MessageManager.c("Survival Games"))) {
			event.setCancelled(true);
			if (clicked.getType() == Material.BOW || clicked.getType() == Material.SKULL_ITEM){
				return;
			}else {
				String[] spl = ChatColor.stripColor(clicked.getItemMeta().getDisplayName()).split(" ");
				Integer gameID = Integer.valueOf(spl[1]);
				Integer newGameID = Main.main.getZimeGames().getGameByZimeID(gameID);
				player.closeInventory(); Servers.startConnection(player);
				player.sendMessage(MessageManager.c(MessageManager.prefix + " &cConnecting to &e" + spl[0] + " " + spl[1] + "&8."));
				new BukkitRunnable() {
					@Override
					public void run() {
						Main.main.getSGGameManager().getGameByID(newGameID).joinGame(player);
						Servers.stopConnection(player);
					}
				}.runTaskLater(Main.main, MessageManager.getRandomInt(15, 25));
				return;
			}
		}
		if (inv.getName().contains(MessageManager.c("Battlefield"))){
			event.setCancelled(true);
			if (clicked.getType() == Material.IRON_SWORD || clicked.getType() == Material.SKULL_ITEM){
				return;
			}else {
				String[] spl = ChatColor.stripColor(clicked.getItemMeta().getDisplayName()).split(" ");
				Integer gameID = Integer.valueOf(spl[1]);
				Integer newGameID = Main.main.getZimeGames().getGameByZimeID(gameID);
				player.closeInventory(); Servers.startConnection(player);
				player.sendMessage(MessageManager.c(MessageManager.prefix + " &cConnecting to &e" + spl[0] + " " + spl[1] + "&8."));
				new BukkitRunnable() {
					@Override
					public void run() {
						Main.main.getBGGameManager().getGameByID(newGameID).joinGame(player);
						Servers.stopConnection(player);
					}
				}.runTaskLater(Main.main, MessageManager.getRandomInt(15, 25));
				return;
			}
		}
		//If the inventory name is Server Navigation
		if (inv.getName().contains(MessageManager.c("MinecraftSG Network Navigation"))) {
			event.setCancelled(true);
			if (clicked.getType() == Material.WATCH){
				player.closeInventory();
				Connect.teleportToSpawn(player);
			} else if (clicked.getType() == Material.BOW){
				player.closeInventory();
				SGGUI.openSGGUI(player);
			} else if (clicked.getType() == Material.IRON_SWORD){
				player.closeInventory();
				BGGUI.openBGGUI(player);
			} else {
				player.sendMessage(MessageManager.c(MessageManager.BGprefix + " &4This gamemode has been removed&8."));
			}
		}else if (inv.getName().contains(MessageManager.c("Lobby Selector"))) {
			event.setCancelled(true);
			if (clicked.getType() == Material.EYE_OF_ENDER){
				player.closeInventory();
			} else {
				String[] spl = ChatColor.stripColor(clicked.getItemMeta().getDisplayName()).split(" ");
				player.closeInventory(); Servers.startConnection(player);
				player.sendMessage(MessageManager.c(MessageManager.prefix + " &cConnecting to &e" + spl[0] + " " + spl[1] + "&8."));
				new BukkitRunnable() {
					@Override
					public void run() {
						player.sendMessage(MessageManager.c(MessageManager.BGprefix + " &4There was an error "
						+ "fetching data from " + spl[0] + "-" + spl[1] + ", your connection has been "
						+ "cancelled&8."));
						Servers.stopConnection(player);
					}
				}.runTaskLater(Main.main, MessageManager.getRandomInt(15, 25));
				return;
			}
		} else {
			return;
		}
	    
	}
	
	@EventHandler
	public void openInv(InventoryOpenEvent event) {
		Player player = (Player) event.getPlayer();
		if (Servers.isConnecting(player)) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void sendCommand(AsyncPlayerChatEvent event) {
		Player player = (Player) event.getPlayer();
		if (event.getMessage().startsWith("/")) {
			if (Servers.isConnecting(player)) {
				event.setCancelled(true);
			}
		}
	}
	
}
