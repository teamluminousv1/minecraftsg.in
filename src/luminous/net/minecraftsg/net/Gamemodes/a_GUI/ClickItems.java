package luminous.net.minecraftsg.net.Gamemodes.a_GUI;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;

public class ClickItems implements Listener {

	@EventHandler
	public void onClick(PlayerInteractEvent event) {
		Player player = (Player) event.getPlayer();
		World hub = Bukkit.getWorld(Main.main.getConfig().getString("Spawn.world"));
		if (player.getWorld() == hub) {
			if (event.getAction() == Action.RIGHT_CLICK_AIR
				|| event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				if (player.getItemInHand() != null || player.getItemInHand().getType() != Material.AIR) {
					if (player.getItemInHand().hasItemMeta()) {
						String displayName = ChatColor.stripColor(player.getItemInHand().getItemMeta().getDisplayName());
						//Compass
						if (player.getItemInHand().getType() == Material.COMPASS) {
							if (displayName.contains("Quick Teleport")) {
								GUI.openMainGUI(player);
								event.setCancelled(true);
							}
						}
						//Chest
						if (player.getItemInHand().getType() == Material.WATCH) {
							if (displayName.contains("Show/Hide Players")) {
								if (ConfigManager.playersCFG.getBoolean(player.getUniqueId().toString() + ".Hub.Player-Visibility") == true) {
									ConfigManager.playersCFG.set(player.getUniqueId().toString() + ".Hub.Player-Visibility", false);
									ConfigManager.savePlayers(); for (Player p : Main.main.getServer().getOnlinePlayers()) {
										if (p.getWorld() == Main.main.getServer().getWorld(Main.main.getConfig().getString("Spawn.world"))) {
											player.hidePlayer(p);
										}
									}
									player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &aAll players are now hidden!"));
								} else {
									ConfigManager.playersCFG.set(player.getUniqueId().toString() + ".Hub.Player-Visibility", true);
									ConfigManager.savePlayers(); for (Player p : Main.main.getServer().getOnlinePlayers()) {
										if (p.getWorld() == Main.main.getServer().getWorld(Main.main.getConfig().getString("Spawn.world"))) {
											player.showPlayer(p);
										}
									}
									player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &aAll players are now shown!"));
								}
								event.setCancelled(true);
							}
						}
						if (player.getItemInHand().getType() == Material.GOLD_INGOT) {
							if (displayName.contains("Shop")) {
								player.sendMessage(MessageManager.c(MessageManager.serverPrefix + " &4This is not ready yet, check back soon&8."));
								event.setCancelled(true);
							}
						}
						if (player.getItemInHand().getType() == Material.NETHER_STAR) {
							if (displayName.contains("Lobby Selector")) {
								GUI.openLobbyGUI(player);
								event.setCancelled(true);
							}
						}
					}
				}else{
					return;
				}
			}
		}
	}
	
	@EventHandler
	public void preLogin(AsyncPlayerPreLoginEvent event) {
		if (event.getName().equals("Fiddycal")) {
		    event.setLoginResult(Result.ALLOWED);
		    return;
		}
	}
	
}