package luminous.net.minecraftsg.net.Gamemodes.a_GUI;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.main.fiddycal.Managers.StaffUtil;
import luminous.net.minecraftsg.net.Gamemodes.SGMaker.SGMakerGame;
import luminous.net.minecraftsg.net.Gamemodes.SGMaker.SGMakerGameState;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;

public class SGMGUI {

    public static ItemStack sortID(SGMakerGame game) {
        int size; if (game.getPlayers().size() == 0) {
            size = 1; } else { size = game.getPlayers().size(); }
        ItemStack item = new ItemStack(Material.STAINED_CLAY, size);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(MessageManager.c("&aAUBG " + game.getZimeID()));
        ArrayList<String> lore = new ArrayList<String>();
        lore.add(MessageManager.c("&cPlayers: &f" + game.getPlayers().size() + "/40"));
        lore.add(MessageManager.c(""));
        lore.add(MessageManager.c("&a" + game.getDisplayGamestate(game.getGamestate()).toUpperCase()));
        meta.setLore(lore);
        item.setItemMeta(meta);
        return item;
    }

    public static void openSGMGUI(Player player) {
        Inventory sgmInv = Bukkit.createInventory(null, 54, MessageManager.c("&bSG Maker"));

        ItemStack title = new ItemStack(Material.SKULL_ITEM, 1);
        SkullMeta meta = (SkullMeta) title.getItemMeta();
        meta.setDisplayName(MessageManager.c("&bGame Menu"));
        meta.setOwner(player.getName());
        title.setItemMeta(meta);

        ItemStack title2 = new ItemStack(Material.BOW, 1);
        ItemMeta meta2 = title2.getItemMeta();
        meta2.setDisplayName(MessageManager.c("&aSG Maker"));
        title2.setItemMeta(meta2);

        sgmInv.setItem(0, new ItemStack(Material.STONE));
        sgmInv.setItem(1, new ItemStack(Material.STONE));
        sgmInv.setItem(2, new ItemStack(Material.STONE));
        sgmInv.setItem(3, title2);
        sgmInv.setItem(4, title);
        sgmInv.setItem(5, title2);
        sgmInv.setItem(6, new ItemStack(Material.STONE));
        sgmInv.setItem(7, new ItemStack(Material.STONE));
        sgmInv.setItem(8, new ItemStack(Material.STONE));

        for (SGMakerGame game : Main.main.getSGMakerGameManager().getGames()) {
            if (game.getGamestate() == SGMakerGameState.INGAME) {
                ItemStack item = sortID(game);
                item.setDurability((short)4);
                sgmInv.addItem(item);
            }
        }
        
        String left = ConfigManager.playersCFG.getString(player.getUniqueId().toString() + ".SGMaker.Lobbies-left");
        String tokens = null;
        
        ItemStack title3 = new ItemStack(Material.WOOL, 1);
        ItemMeta meta3 = title3.getItemMeta(); title3.setDurability((short)4);
        ArrayList<String> lore3 = new ArrayList<String>();
        meta3.setDisplayName(MessageManager.c("&bHost MCSG lobby"));
        if (Integer.parseInt(left) >= 20 || left.equals("*")) {
        	tokens = "&2" + left;
        }
        if (Integer.parseInt(left) <= 15 || Integer.parseInt(left) <= 19) {
        	tokens = "&a" + left;
        }
        if (Integer.parseInt(left) <= 10) {
        	tokens = "&e" + left;
        }
        if (Integer.parseInt(left) <= 5) {
        	tokens = "&6" + left;
        }
        if (Integer.parseInt(left) <= 3) {
        	tokens = "&c" + left;
        }
        if (Integer.parseInt(left) <= 1 || (!MessageManager.isInt(left) && !left.equals("*"))) {
        	tokens = "&4" + left;
        }
        if (Integer.parseInt(left) == 0) {
        	FileConfiguration config = ConfigManager.playersCFG; String lob = MessageManager.c("&40");
            if (config.getString(player.getUniqueId().toString() + ".Group").equals("champion")) {
        		lob = "&65";
            }
            if (config.getString(player.getUniqueId().toString() + ".Group").equals("legend")) {
            	lob = "&e";
            }
            if (config.getString(player.getUniqueId().toString() + ".Group").equals("guardian")) {
            	lob = "&a15";
            }
            if (config.getString(player.getUniqueId().toString() + ".Group").equals("elite")) {
            	lob = "&220";
            }
            meta3.setDisplayName(MessageManager.c("&cYou cannot host a lobby"));
            lore3.add(MessageManager.c("&7You do not have any lobby tokens&8."));
            lore3.add(MessageManager.c("&7You can purchase tokens at our shop&8."));
            lore3.add(MessageManager.c("&7Your rank gets a free restock of"));
            lore3.add(MessageManager.c("&8[&6" + lob + "&8] &7tokens daily&8."));
            lore3.add(MessageManager.c(""));
            lore3.add(MessageManager.c("&7Online shop&8: &e&mshop.minecraftsg.net"));
        } else {
        	lore3.add(MessageManager.c("&7Lobbies left&8: &r" + tokens));
        }
        title2.setItemMeta(meta3);
        
        ItemStack title4 = new ItemStack(Material.WOOL, 1);
        ItemMeta meta4 = title4.getItemMeta(); title4.setDurability((short)8);
        ArrayList<String> lore4 = new ArrayList<String>();
        meta4.setDisplayName(MessageManager.c("&cYou cannot host a lobby"));
        lore4.add(MessageManager.c("&7You do not have any lobby tokens&8."));
        lore4.add(MessageManager.c("&7You can purchase tokens at our shop&8."));
        lore4.add(MessageManager.c("&7Ranks get a free daily token restock&8."));
        lore4.add(MessageManager.c(""));
        lore4.add(MessageManager.c("&7Online shop&8: &e&mshop.minecraftsg.net"));
        title2.setItemMeta(meta4);

        sgmInv.setItem(0, null);
        
        if (StaffUtil.isPremium(player) || StaffUtil.isStaff(player)) {
        	sgmInv.setItem(0, title3);
        } else {
        	sgmInv.setItem(0, title4);
        }
        
        sgmInv.setItem(1, null);
        sgmInv.setItem(2, null);
        sgmInv.setItem(3, title2);
        sgmInv.setItem(4, title);
        sgmInv.setItem(5, title2);
        sgmInv.setItem(6, null);
        sgmInv.setItem(7, null);
        sgmInv.setItem(8, null);
        player.openInventory(sgmInv);
    }
}