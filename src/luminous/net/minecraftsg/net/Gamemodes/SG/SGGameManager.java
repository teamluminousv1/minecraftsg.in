package luminous.net.minecraftsg.net.Gamemodes.SG;

import java.util.HashSet;
import java.util.Set;

public class SGGameManager {
    //Set of all the games
    private Set<SGGame> games;

    //Usage: new GameManager();
    public SGGameManager() {
        //Creates fresh list for the games
        games = new HashSet<>();
    }
    //Adds a Game to the set
    public void addGame(SGGame game) {
        games.add(game);
    }

    //Removes a Game from the set
    public void tempRemoveGame(SGGame game){
        games.remove(game);
    }

    public Set<SGGame> getGames(){
        return games;
    }
    //Gets the Game by ID
    public SGGame getGameByID(int id) {
        for (SGGame game : games) {
            if (game.getID() == id) {
                return game;
            }
        }

        return null;
    }
    //Get the Game at the Player
    public SGGame getGameByPlayer(String player) {
        for (SGGame game : games) {
            if (game.getPlayers().contains(player)) {
                return game;
            }
        }

        return null;
    }
}
