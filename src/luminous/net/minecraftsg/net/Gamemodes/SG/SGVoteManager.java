package luminous.net.minecraftsg.net.Gamemodes.SG;

import java.io.File;
import java.io.IOException;
import java.util.*;

import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class SGVoteManager {

	private List<String> voted;
	private HashMap<String, Integer> votes;
	private SGGame game;
	private List<String> maps;
	private SGMapPool mapPool;

	//Usage: new VoteManager(game);
    public SGVoteManager(SGGame game){
		this.game = game;
    	this.voted = new ArrayList<>();
    	this.votes = new HashMap<>();
		this.maps = new ArrayList<>();
		this.mapPool = new SGMapPool(this);
    }

	//Add the vote to the player
    public void addVote(Player player, String map){
		this.voted.add(player.getName());
		Integer currentVotes = this.votes.get(map);
		this.votes.put(map, currentVotes + 1);
	}

	public HashMap<String, Integer> getVotes(){
		return votes;
	}

	public List<String> getVoted(){
		return voted;
	}


	public List<String> getMaps() {
		return maps;
	}

    //Gets the Winning Map!
    public String getWinningMap() {
    	Map.Entry<String,Integer> winningMap = null;

		for(Map.Entry<String,Integer> entry : this.votes.entrySet()) {
			if (winningMap == null || entry.getValue() > winningMap.getValue()) {
				winningMap = entry;
			}
		}
        return winningMap.getKey();
    }
    
	public void moveWorld(int id, File folder) {
		File world = new File("SGGame-" + id); File file;
		if (folder.exists()) {
			File files[] = folder.listFiles();
	        for (int i = 0; i < files.length; i++) {
	            if (files[i].isDirectory()) {
	                moveWorld(id, files[i]);
	            } else {
	            	try {
	    	            if (!world.exists()) {
	    	            	world.mkdirs();
	    	            }
	    	        } catch(SecurityException e) {
	    	        	e.printStackTrace();
	    	        }

	            	file = new File(world, files[i].getName());
	        	    if (!file.exists()) {
	        	        try {
	        	        	file.createNewFile();
	        	        } catch (IOException e) {
	        	            e.printStackTrace();
	        	        }
	        	    }
	            }
	        }
	    }
	}

}
