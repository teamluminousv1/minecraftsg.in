package luminous.net.minecraftsg.net.Gamemodes.SG;

public enum SGGameState {
    WAITING, PREGAME, GRACEPERIOD,
    INGAME, DEATHMATCH, PREDEATHMATCH,
    ENDGAME, CLEANUP, RESTARTING
}
