package luminous.net.minecraftsg.net.Gamemodes.SG;

import org.bukkit.entity.Player;

import java.util.HashMap;

public class SGBountyManager {

    private SGGame game;

    public HashMap<String, Integer> playerBounties;

    public SGBountyManager(SGGame game){
        this.game = game;
        this.playerBounties = new HashMap<>();
    }

    public void setupBounties(){
        for (String str : this.game.getTributes()){
            playerBounties.put(str, 0);
        }
    }

    public void addBounty(Player player, Integer amount){
        String playerName = player.getName();
        Integer previousInt = playerBounties.get(playerName);
        playerBounties.put(playerName, previousInt + amount);
    }

    public void removeBounty(Player player){
        String playerName = player.getName();
        playerBounties.remove(playerName);
    }

    public Integer getBounty(Player player){
        return playerBounties.get(player.getName());
    }
}
