package luminous.net.minecraftsg.net.Gamemodes.SG;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.inventory.ItemStack;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;

public class SGListeners implements Listener {
	
	@EventHandler
	public void damage(EntityDamageEvent event) {
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
		    if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
		    	SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
		    	if (game.getGamestate() == SGGameState.WAITING
		    		|| game.getGamestate() == SGGameState.PREGAME
		    		|| game.getGamestate() == SGGameState.PREDEATHMATCH
		    		|| game.getGamestate() == SGGameState.CLEANUP
		    	    || game.getGamestate() == SGGameState.ENDGAME
		    		|| game.getGamestate() == SGGameState.RESTARTING
		    		|| game.getSpectators().contains(player.getName())) {
			    	event.setCancelled(true);
		    	}
		    }
		} else if (event.getEntity() instanceof ItemFrame) {
			event.setCancelled(true);
        }
	}
	
	@EventHandler
	public void itemFrameBreaking(HangingBreakByEntityEvent event) {
		if (event.getRemover() instanceof Player) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void fallIntoVoid(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
		    SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
		    if (game.getGamestate() == SGGameState.WAITING) {
		        if (player.getLocation().getBlockY() < 0) {
		        	String ww = ConfigManager.sgDataCFG.getString("Lobbies." + game.getID() + ".Spawn.world");
			        int wx = Integer.parseInt(ConfigManager.sgDataCFG.getString("Lobbies." + game.getID() + ".Spawn.x"));
			        int wy = Integer.parseInt(ConfigManager.sgDataCFG.getString("Lobbies." + game.getID() + ".Spawn.y"));
			        int wz = Integer.parseInt(ConfigManager.sgDataCFG.getString("Lobbies." + game.getID() + ".Spawn.z"));
			        Location ws = new Location(Bukkit.getWorld(ww), wx + .5, wy, wz + .5);
			    	player.teleport(ws);
		    	}
		    }
		}
	}
	
	@EventHandler
	public void damage(EntityDamageByEntityEvent event) {
		if (event.getDamager() instanceof Player) {
			Player player = (Player) event.getDamager();
		    if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
		    	SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
		    	if (game.getSpectators().contains(player.getName())) {
			    	event.setCancelled(true);
		    	}
		    }
		}
	}
	
	@EventHandler
	public void blockBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
	    if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
	    	SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
	    	if (game.getGamestate() == SGGameState.WAITING
	    		|| game.getGamestate() == SGGameState.PREGAME
	    		|| game.getGamestate() == SGGameState.PREDEATHMATCH
	    		|| game.getGamestate() == SGGameState.CLEANUP
	    	    || game.getGamestate() == SGGameState.ENDGAME
	    		|| game.getGamestate() == SGGameState.RESTARTING
			    || game.getSpectators().contains(player.getName())) {
	    		event.setCancelled(true);
	    	} else {
	    		if (event.getBlock().getType() == Material.FIRE
			    	|| event.getBlock().getType() == Material.FLINT_AND_STEEL
			    	|| event.getBlock().getType() == Material.LEAVES
			    	|| event.getBlock().getType() == Material.LEAVES_2
			    	|| event.getBlock().getType() == Material.WEB
			    	|| event.getBlock().getType() == Material.BROWN_MUSHROOM
			    	|| event.getBlock().getType() == Material.RED_MUSHROOM
					|| event.getBlock().getType() == Material.DOUBLE_PLANT
			    	|| event.getBlock().getType() == Material.RED_ROSE
			    	|| event.getBlock().getType() == Material.DEAD_BUSH
			    	|| event.getBlock().getType() == Material.YELLOW_FLOWER
			    	|| event.getBlock().getType() == Material.LONG_GRASS
			    	|| event.getBlock().getType() == Material.WATER_LILY
			    	|| event.getBlock().getType() == Material.VINE) {
			    	return;
			    } else {
					event.setCancelled(true);
			    }
	    	}
	    }
	}
	
	@EventHandler
	public void blockPlace(BlockPlaceEvent event) {
		Player player = event.getPlayer();
	    if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
	    	SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
	    	if (game.getGamestate() == SGGameState.WAITING
	    		|| game.getGamestate() == SGGameState.PREGAME
	    		|| game.getGamestate() == SGGameState.PREDEATHMATCH
	    		|| game.getGamestate() == SGGameState.CLEANUP
	    	    || game.getGamestate() == SGGameState.ENDGAME
	    		|| game.getGamestate() == SGGameState.RESTARTING
			    || game.getSpectators().contains(player.getName())) {
	    		event.setCancelled(true);
	    	} else {
	    		if (event.getBlock().getType() == Material.FIRE
		    		|| event.getBlock().getType() == Material.FLINT_AND_STEEL
		    		|| event.getBlock().getType() == Material.CAKE
		    		|| event.getBlock().getType() == Material.WEB) {
		    		return;
		    	} else {
				    event.setCancelled(true);
		    	}
	    	}
	    }
	}

	@EventHandler
	public void itemDrop(PlayerDropItemEvent event) {
		Player player = event.getPlayer();
	    if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
	    	SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
	    	if (game.getGamestate() == SGGameState.WAITING
	    		|| game.getGamestate() == SGGameState.PREGAME
	    		|| game.getGamestate() == SGGameState.RESTARTING
			    || game.getSpectators().contains(player.getName())) {
		    	event.setCancelled(true);
	    	}
	    }
	}
	
	@EventHandler
	public void itemPickup(PlayerPickupItemEvent event) {
		Player player = event.getPlayer();
	    if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
	    	SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
	    	if (game.getGamestate() == SGGameState.WAITING
	    		|| game.getGamestate() == SGGameState.PREGAME
	    		|| game.getGamestate() == SGGameState.PREDEATHMATCH
	    		|| game.getGamestate() == SGGameState.CLEANUP
	    	    || game.getGamestate() == SGGameState.ENDGAME
	    		|| game.getGamestate() == SGGameState.RESTARTING
			    || game.getSpectators().contains(player.getName())) {
		    	event.setCancelled(true);
	    	}
	    }
	}
	
	@EventHandler
	public void food(FoodLevelChangeEvent event) {
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
		    if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
		    	SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
		    	if (game.getGamestate() == SGGameState.WAITING
		    		|| game.getGamestate() == SGGameState.PREGAME
		    		|| game.getGamestate() == SGGameState.RESTARTING
				    || game.getSpectators().contains(player.getName())) {
			    	event.setCancelled(true);
		    	}
		    }
		}
	}
	
	@EventHandler
	public void fns(PlayerInteractEvent event) {
		if (Main.main.getSGGameManager().getGameByPlayer(event.getPlayer().getName()) != null) {
			if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				if (event.getMaterial() == Material.FLINT_AND_STEEL) {
					if (event.getClickedBlock().getType() == Material.IRON_FENCE
						|| event.getClickedBlock().getType() == Material.DEAD_BUSH
						|| event.getClickedBlock().getType() == Material.LONG_GRASS
						|| event.getBlockFace() != BlockFace.UP) {
						return;
					}
					ItemStack fns = event.getItem();
	                fns.setDurability((short)(fns.getDurability() + 17));
				}
			}
			if (event.getAction() == Action.PHYSICAL) {
				if (Main.main.getSGGameManager().getGameByPlayer(event.getPlayer().getName()).getSpectators().contains(event.getPlayer().getName())){
					event.setCancelled(true);
				}
			}
		}
	}
	
	@EventHandler
	public void spectate(PlayerInteractEvent event) {
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK
			|| event.getAction() == Action.RIGHT_CLICK_AIR) {
			if (Main.main.getSGGameManager().getGameByPlayer(event.getPlayer().getName()) != null) {
				SGGame game = Main.main.getSGGameManager().getGameByPlayer(event.getPlayer().getName());
				if (game.getSpectators().contains(event.getPlayer().getName())) {
					ArrayList<Player> players = new ArrayList<Player>();
					for (Player e : Bukkit.getOnlinePlayers()) {
						if (game.getTributes().contains(e.getName())) {
							players.add(e);
						}
					}
					Player randomPlayer = players.get(new Random().nextInt(players.size()));
					event.getPlayer().teleport(randomPlayer.getLocation());
					event.getPlayer().sendMessage(MessageManager.c(MessageManager.prefix + " &fTeleporting to &r" + randomPlayer.getDisplayName() + "&8..."));
					event.setCancelled(true);
				}
			}
		}
	}
	
	@EventHandler
	public void playerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
	    	SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
	    	if (game.getGamestate() == SGGameState.PREGAME
	    		|| game.getGamestate() == SGGameState.PREDEATHMATCH) {
				if (Main.main.getSGGameManager().getGameByPlayer(player.getName()).getTributes().contains(player.getName())) {
					if (event.getTo().getBlockX() != event.getFrom().getBlockX()
							|| event.getTo().getBlockZ() != event.getFrom().getBlockZ()) {
						event.getPlayer().teleport(event.getFrom());
						return;
					}
				}
	    	}
	    }
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		if (e.getEntity() instanceof Player){
			e.setDeathMessage(null);
			if (Main.main.getSGGameManager().getGameByPlayer(e.getEntity().getName()) != null){
				SGGame game = Main.main.getSGGameManager().getGameByPlayer(e.getEntity().getName());
				if (game.getGamestate() == SGGameState.INGAME ||  game.getGamestate() == SGGameState.DEATHMATCH){
					if (e.getEntity().getKiller() != null) {
						if (game.getKills().get(e.getEntity().getKiller().getName()) != null) {
							Integer kills = game.getKills().get(e.getEntity().getKiller().getName());
							game.getKills().put(e.getEntity().getKiller().getName(), kills + 1);
						} else {
							game.getKills().put(e.getEntity().getKiller().getName(), 1);
						}
					}
					Location location = e.getEntity().getLocation();
					location.getWorld().strikeLightningEffect(location);
					e.getEntity().setHealth(20.0); e.getEntity().setFoodLevel(20);
					game.removeTribute(e.getEntity(), e.getEntity().getKiller());
				}
			}
		}
	}

	@EventHandler
	public void onBoat(VehicleEnterEvent e){
		if (Main.main.getSGGameManager().getGameByPlayer(e.getEntered().getName()) != null){
			if (Main.main.getSGGameManager().getGameByPlayer(e.getEntered().getName()).getSpectators().contains(e.getEntered().getName())){
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void clickOpenChest(PlayerInteractEvent event) {
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (Main.main.getSGGameManager().getGameByPlayer(event.getPlayer().getName()) != null) {
				SGGame game = Main.main.getSGGameManager().getGameByPlayer(event.getPlayer().getName());
				if (!game.getSpectators().contains(event.getPlayer().getName())) {
					if (game.getGamestate() == SGGameState.GRACEPERIOD
						    || game.getGamestate() == SGGameState.INGAME
						    || game.getGamestate() == SGGameState.DEATHMATCH) {
						if (!game.getOpenedChestLocations().contains(event.getClickedBlock().getLocation())) {
				            game.getOpenedChestLocations().add(event.getClickedBlock().getLocation());
			            }
					}
				}
			}
		}
	}
	
	@EventHandler
	public void openChest(InventoryOpenEvent e){
		if (e.getInventory().getType() == InventoryType.CHEST) {
			if (Main.main.getSGGameManager().getGameByPlayer(e.getPlayer().getName()) != null){
				SGGame game = Main.main.getSGGameManager().getGameByPlayer(e.getPlayer().getName());
				if (game.getGamestate() == SGGameState.GRACEPERIOD
				    || game.getGamestate() == SGGameState.INGAME
				    || game.getGamestate() == SGGameState.DEATHMATCH) {
					if (!game.getOpenedChestInventories().contains(e.getInventory().toString())) {
			            SGStats.addStats(e.getPlayer().getName(), SGStatType.CHESTS_OPENED, 1);
			            game.getOpenedChestInventories().add(e.getInventory().toString());
		            }
				}
			}
		}
	}
	
	@EventHandler
	public void chunkLoad(ChunkLoadEvent event) {
		if (event.getWorld().getName().contains("SGGame-")) {
			String[] spl = event.getWorld().getName().split("-");
			if (Main.main.getSGGameManager().getGameByID(Integer.parseInt(spl[1])) != null) {
				SGGame game = Main.main.getSGGameManager().getGameByID(Integer.parseInt(spl[1]));
				if (game.getGamestate() == SGGameState.WAITING
					|| game.getGamestate() == SGGameState.CLEANUP
					|| game.getGamestate() == SGGameState.ENDGAME) {
					return;
				} else {
					game.changeChests();
					game.setupChests();
				}
			}
		}
	}
	
}
