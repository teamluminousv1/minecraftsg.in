package luminous.net.minecraftsg.net.Gamemodes.SG;

public enum SGStatType {

	GAMES_WON, GAMES_PLAYED, GAMES_LOST,
	CHESTS_OPENED, KILLS, DEATHMATCHES
	
}
