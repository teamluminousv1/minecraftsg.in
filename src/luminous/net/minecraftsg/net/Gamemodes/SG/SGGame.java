package luminous.net.minecraftsg.net.Gamemodes.SG;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Commands.Fly;
import luminous.net.main.fiddycal.Managers.ConfigManager;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.main.fiddycal.Managers.RankManager;
import luminous.net.minecraftsg.net.Commands.SilentJoin;
import luminous.net.minecraftsg.net.Managers.ChestManager;
import luminous.net.minecraftsg.net.Managers.PointsManager;
import luminous.net.minecraftsg.net.Sidebar.SG.Ingame;
import luminous.net.minecraftsg.net.Sidebar.SG.Lobby;

import org.bukkit.*;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class SGGame {

    //Game ID
    private int ID;
    //Vote Manager
    private SGVoteManager vm;
    //Gamestate
    private SGGameState gamestate;
    //Player Lists
    private List<String> tributes;
    private List<String> spectators;
    private List<String> players;
    private List<String> previousMaps;

    //Map
    private String map;

    //Game Kills
    private Map <String, Integer> kills;

    //Player
    private String winner;

    //Game Time
    private SGTimeManager tm;

    //Zime ID
    private Integer zimeID;

    //Spawn for the Waiting Lobby of the game
    private Location waitingSpawn;
    //List of Locations for the game spawns
    private List<Location> gameSpawns;
    //List of Locations for the Deathmatch game spawns
    private List<Location> deathmatchSpawns;
    private List<Location> openedChestLocations;
    private List<String> openedChestInventories;
    private List<String> passedTributes;
    //Location for Spectators to be teleported to
    private Location specSpawn;

    private SGBountyManager bm;

    //Tier 1/2 locations
    private List<Location> t1;
    private List<Location> t2;

    private Map<String, Integer> playerdist;

    //Deathmatch radius locations
    public int radius;
    public int dmx;
    public int dmz;

    private SGSponsorManager sm;

    private Boolean forceStart;


    //Usage new Game(ID);
    public SGGame(int ID) {
        //Sets the Game ID
        this.ID = ID;
        //Sets the Game State to Waiting
        this.setGamestate(SGGameState.WAITING);
        //Generate fresh Lists
        this.tributes = new ArrayList<>();
        this.spectators = new ArrayList<>();
        this.players = new ArrayList<>();
        this.gameSpawns = new ArrayList<>();
        this.deathmatchSpawns = new ArrayList<>();
        this.t1 = new ArrayList<>();
        this.t2 = new ArrayList<>();
        this.previousMaps = new ArrayList<>();
        this.passedTributes = new ArrayList<>();
        this.openedChestInventories = new ArrayList<>();
        this.openedChestLocations = new ArrayList<>();
        this.winner = null;
        this.kills = new HashMap<>();
        this.playerdist = new HashMap<>();
        this.forceStart = false;
        //Sets the current Map to null
        this.map = "NOT SET";
        //Creates the VoteManager for this game
        this.vm = new SGVoteManager(this);
        //Creates the SponsorManager for this game`
        this.sm = new SGSponsorManager(this);
        //Sets the TimeManager
        this.tm = new SGTimeManager(this);
        //Sets the Waiting Lobby Spawn
        String ww = ConfigManager.sgDataCFG.getString("Lobbies." + this.ID + ".Spawn.world");
        int wx = Integer.parseInt(ConfigManager.sgDataCFG.getString("Lobbies." + this.ID + ".Spawn.x"));
        int wy = Integer.parseInt(ConfigManager.sgDataCFG.getString("Lobbies." + this.ID + ".Spawn.y"));
        int wz = Integer.parseInt(ConfigManager.sgDataCFG.getString("Lobbies." + this.ID + ".Spawn.z"));
        Location ws = new Location(Bukkit.getWorld(ww), wx + .5, wy, wz + .5);
        this.waitingSpawn = ws;
        //Creates the previous maps
        this.previousMaps = new ArrayList<>();
        this.dmx = 0;
        this.dmz = 0;
        this.radius = 30;
        this.bm = new SGBountyManager(this);
        this.sm = new SGSponsorManager(this);
        //Sets the Game Spawns
        //Sets the Deathmatch Spawns
        //-----------------------------------
        //DO NOT ADD ANYTHING BELOW THIS LINE
        //-----------------------------------
        //Add the game to the Game Manager
        Main.main.getSGGameManager().addGame(this);
        Main.main.getZimeGames().newGame("SG-" + this.getID());
        this.zimeID = Main.main.getZimeGames().getZimeID("SG-" + this.ID);

    }

    public Integer getZimeID(){
        return this.zimeID;
    }

    public void setForceStart(boolean torf){
        forceStart = torf;
    }

    public Boolean getForceStart(){
        return forceStart;
    }

    public SGBountyManager getBm(){
        return this.bm;
    }

    //Sets the gamestate for this game
    public void setGamestate(SGGameState gamestate){
        this.gamestate = gamestate;
    }
    //Gets the gamestate
    public SGGameState getGamestate(){
        return this.gamestate;
    }
    //Sets the Map
    public void setMap(String map){
        this.map = map;
    }
    //Gets the map
    public String getMap(){
        return this.map;
    }
    //Gets Tier1 chests
    public List<Location> getTeir1Chests(){
        return this.t1;
    }
    //Gets Tier2 chests
    public List<Location> getTeir2Chests(){
        return this.t2;
    }
    //Gets Tier2 chests
    public List<String> getOpenedChestInventories(){
        return this.openedChestInventories;
    }
    //Gets Tier2 chests
    public List<Location> getOpenedChestLocations(){
        return this.openedChestLocations;
    }

    public Map<String,Integer> getKills(){
        return this.kills;
    }

    public SGSponsorManager getSM(){
        return sm;
    }

    public void setupSpawns() {
    	String name = "SGGame-" + this.getID();
    	if (ConfigManager.sgDataCFG.getString("Maps." + this.getMap() + ".sp") != null) {
    		for (String id : ConfigManager.sgDataCFG.getConfigurationSection("Maps." + this.getMap() + ".sp").getKeys(false)){
                int gx = ConfigManager.sgDataCFG.getInt("Maps." + this.getMap() + ".sp." + id + ".x");
                int gy = ConfigManager.sgDataCFG.getInt("Maps." + this.getMap() + ".sp." + id + ".y");
                int gz = ConfigManager.sgDataCFG.getInt("Maps." + this.getMap() + ".sp." + id + ".z");
                Float yaw = Float.parseFloat(ConfigManager.sgDataCFG.getString("Maps." + this.getMap() + ".sp." + id + ".yaw"));
                Float pitch = Float.parseFloat(ConfigManager.sgDataCFG.getString("Maps." + this.getMap() + ".sp." + id + ".pitch"));
                Location gs = new Location(Bukkit.getWorld(name), gx + .5, gy, gz + .5, yaw, pitch);
                this.gameSpawns.add(gs);
            }
    	}
    	if (ConfigManager.sgDataCFG.getString("Maps." + this.getMap() + ".dm.1") != null) {
    		for (String id : ConfigManager.sgDataCFG.getConfigurationSection("Maps." + this.getMap() + ".dm").getKeys(false)){
    			if (MessageManager.isInt(id)) {
    				int dx = ConfigManager.sgDataCFG.getInt("Maps." + this.getMap() + ".dm." + id + ".x");
                    int dy = ConfigManager.sgDataCFG.getInt("Maps." + this.getMap() + ".dm." + id + ".y");
                    int dz = ConfigManager.sgDataCFG.getInt("Maps." + this.getMap() + ".dm." + id + ".z");
                    Float yaw = Float.parseFloat(ConfigManager.sgDataCFG.getString("Maps." + this.getMap() + ".dm." + id + ".yaw"));
                    Float pitch = Float.parseFloat(ConfigManager.sgDataCFG.getString("Maps." + this.getMap() + ".dm." + id + ".pitch"));
                    Location ds = new Location(Bukkit.getWorld(name), dx + .5, dy, dz + .5, yaw, pitch);
                    this.deathmatchSpawns.add(ds);
    			}
            }
    	}
    	if (ConfigManager.sgDataCFG.getString("Maps." + this.getMap() + ".spec") != null) {
    		int sx = ConfigManager.sgDataCFG.getInt("Maps." + this.getMap() + ".spec.x");
            int sy = ConfigManager.sgDataCFG.getInt("Maps." + this.getMap() + ".spec.y");
            int sz = ConfigManager.sgDataCFG.getInt("Maps." + this.getMap() + ".spec.z");
            Location ss = new Location(Bukkit.getWorld(name), sx + .5, sy, sz + .5);
            this.specSpawn = ss;
    	}
    }

    public String getHighestKills() {
    	Random r = new Random();
    	int i = r.nextInt(this.getTributes().size());
    	String winningKills = this.getTributes().get(i);
    	if (this.kills == null || this.kills.isEmpty()) {
    		return winningKills;
    	}
        for (Map.Entry<String, Integer> entry : this.kills.entrySet()) {
            if (entry.getValue() > this.kills.get(winningKills)) {
                winningKills = entry.getKey();
            }
        }
        return winningKills;
    }

    public void setupPlayerDists(){
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        //1
        numbers.add(1);
        numbers.add(1);
        //2
        numbers.add(2);
        numbers.add(2);
        //3
        numbers.add(3);
        numbers.add(3);
        //4
        numbers.add(4);
        numbers.add(4);
        //5
        numbers.add(5);
        numbers.add(5);
        //6
        numbers.add(6);
        numbers.add(6);
        //7
        numbers.add(7);
        numbers.add(7);
        //8
        numbers.add(8);
        numbers.add(8);
        //9
        numbers.add(9);
        numbers.add(9);
        //10
        numbers.add(10);
        numbers.add(10);
        //11
        numbers.add(11);
        numbers.add(11);
        //12
        numbers.add(12);
        numbers.add(12);
        for (String str : this.getTributes()){
            this.playerdist.put(str, (Integer) numbers.get(0));
            numbers.remove(0);
        }
    }

    public Integer getDistrictOfPlayer(String playerName){
        return playerdist.get(playerName);
    }

    public Location getSpecSpawn(){
        return specSpawn;
    }
    
    public void changeChests() {
    	World world = Bukkit.getWorld("SGGame-" + this.getID());
    	for (Chunk chunk : world.getLoadedChunks()) {
    		for (BlockState chest : chunk.getTileEntities()) {
    			if (chest.getType() == Material.ENDER_CHEST) {
    				chest.getBlock().setType(Material.CHEST);
    				//((org.bukkit.material.EnderChest) world.getBlockAt(
    				//chest.getBlock().getLocation())).setFacingDirection(BlockFace.EAST);
    				t2.add(chest.getBlock().getLocation());
        	    }
        	}
    	}
    }
    
	public void setupChests() {
        String name = "SGGame-" + this.getID();
    	for (Chunk chunk : Bukkit.getWorld(name).getLoadedChunks()) {
    		for (BlockState chest : chunk.getTileEntities()) {
    			if (chest instanceof Chest) {
    				Inventory contents = ((Chest) chest).getInventory();
    				if (!this.getOpenedChestLocations().contains(chest.getLocation())) {
						int size = contents.getSize() - 1;
						if (chest.getType() == Material.CHEST) {
							contents.clear(); if (size == 53) {
								if (this.t2.contains(chest.getBlock().getLocation())) {
		    	    				this.getOpenedChestLocations().add(chest.getLocation());
		    	    				int r1 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i1 = ChestManager.getRandomT2Item();
		    	        			if (!contents.contains(i1)) { contents.setItem(r1, i1); }
		    	        			int r2 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i2 = ChestManager.getRandomT2Item();
		    	        			if (!contents.contains(i2)) { contents.setItem(r2, i2); }
		    	        			int r3 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i3 = ChestManager.getRandomT2Item();
		    	        			if (!contents.contains(i3)) { contents.setItem(r3, i3); }
		    	        			int r4 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i4 = ChestManager.getRandomT2Item();
		    	        			if (!contents.contains(i4)) { contents.setItem(r4, i4); }
		    	        			int r5 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i5 = ChestManager.getRandomT2Item();
		    	        			if (!contents.contains(i5)) { contents.setItem(r5, i5); }
		    	        			int r6 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i6 = ChestManager.getRandomT2Item();
		    	        			if (!contents.contains(i6)) { contents.setItem(r6, i6); }
		    	        			int r7 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i7 = ChestManager.getRandomT2Item();
		    	        			if (!contents.contains(i7)) { contents.setItem(r7, i7); }
		    	        			int r8 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i8 = ChestManager.getRandomT2Item();
		    	        			if (!contents.contains(i8)) { contents.setItem(r8, i8); }
		    	        			int r9 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i9 = ChestManager.getRandomT2Item();
		    	        			if (!contents.contains(i9)) { contents.setItem(r9, i9); }
		    	        			int r10 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i10 = ChestManager.getRandomT2Item();
		    	        			if (!contents.contains(i10)) { contents.setItem(r10, i10); }
		    	    			} else {
		    	    				this.t1.add(chest.getBlock().getLocation());
		    	    				this.getOpenedChestLocations().add(chest.getLocation());
		    		    			int r1 = MessageManager.getRandomInt(0, size);
		    		    	        ItemStack i1 = ChestManager.getRandomT1Item();
		    		    	        if (!contents.contains(i1)) { contents.setItem(r1, i1); }
		    		    	        int r2 = MessageManager.getRandomInt(0, size);
		    		    	        ItemStack i2 = ChestManager.getRandomT1Item();
		    		    	        if (!contents.contains(i2)) { contents.setItem(r2, i2); }
		    		    	        int r3 = MessageManager.getRandomInt(0, size);
		    		    	        ItemStack i3 = ChestManager.getRandomT1Item();
		    		    	        if (!contents.contains(i3)) { contents.setItem(r3, i3); }
		    		    	        int r4 = MessageManager.getRandomInt(0, size);
		    		    	        ItemStack i4 = ChestManager.getRandomT1Item();
		    		    	        if (!contents.contains(i4)) { contents.setItem(r4, i4); }
		    		    	        int r5 = MessageManager.getRandomInt(0, size);
		    		    	        ItemStack i5 = ChestManager.getRandomT1Item();
		    	        			if (!contents.contains(i5)) { contents.setItem(r5, i5); }
		    	        			int r6 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i6 = ChestManager.getRandomT1Item();
		    	        			if (!contents.contains(i6)) { contents.setItem(r6, i6); }
		    	        			int r7 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i7 = ChestManager.getRandomT1Item();
		    	        			if (!contents.contains(i7)) { contents.setItem(r7, i7); }
		    	        			int r8 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i8 = ChestManager.getRandomT1Item();
		    	        			if (!contents.contains(i8)) { contents.setItem(r8, i8); }
		    	        			int r9 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i9 = ChestManager.getRandomT1Item();
		    	        			if (!contents.contains(i9)) { contents.setItem(r9, i9); }
		    	        			int r10 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i10 = ChestManager.getRandomT1Item();
		    	        			if (!contents.contains(i10)) { contents.setItem(r10, i10); }
		    	    			}
							} else {
								if (this.t2.contains(chest.getBlock().getLocation())) {
		    	    				this.getOpenedChestLocations().add(chest.getLocation());
		    	    				int r1 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i1 = ChestManager.getRandomT2Item();
		    	        			if (!contents.contains(i1)) { contents.setItem(r1, i1); }
		    	        			int r2 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i2 = ChestManager.getRandomT2Item();
		    	        			if (!contents.contains(i2)) { contents.setItem(r2, i2); }
		    	        			int r3 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i3 = ChestManager.getRandomT2Item();
		    	        			if (!contents.contains(i3)) { contents.setItem(r3, i3); }
		    	        			int r4 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i4 = ChestManager.getRandomT2Item();
		    	        			if (!contents.contains(i4)) { contents.setItem(r4, i4); }
		    	        			int r5 = MessageManager.getRandomInt(0, size);
		    	        			ItemStack i5 = ChestManager.getRandomT2Item();
		    	        			if (!contents.contains(i5)) { contents.setItem(r5, i5); }
		    	    			} else {
		    	    				this.t1.add(chest.getBlock().getLocation());
		    	    				this.getOpenedChestLocations().add(chest.getLocation());
		    		    			int r1 = MessageManager.getRandomInt(0, size);
		    		    	        ItemStack i1 = ChestManager.getRandomT1Item();
		    		    	        if (!contents.contains(i1)) { contents.setItem(r1, i1); }
		    		    	        int r2 = MessageManager.getRandomInt(0, size);
		    		    	        ItemStack i2 = ChestManager.getRandomT1Item();
		    		    	        if (!contents.contains(i2)) { contents.setItem(r2, i2); }
		    		    	        int r3 = MessageManager.getRandomInt(0, size);
		    		    	        ItemStack i3 = ChestManager.getRandomT1Item();
		    		    	        if (!contents.contains(i3)) { contents.setItem(r3, i3); }
		    		    	        int r4 = MessageManager.getRandomInt(0, size);
		    		    	        ItemStack i4 = ChestManager.getRandomT1Item();
		    		    	        if (!contents.contains(i4)) { contents.setItem(r4, i4); }
		    		    	        int r5 = MessageManager.getRandomInt(0, size);
		    		    	        ItemStack i5 = ChestManager.getRandomT1Item();
		    		    	        if (!contents.contains(i5)) { contents.setItem(r5, i5); }
		    	    			}
							}
						}
					}
    			}
    		}
    	}
    }

    public List<Location> getGameSpawns() {
        return gameSpawns;
    }
    
    public List<String> getPastTributes() {
        return passedTributes;
    }
    
    public List<Location> getDeathmatchSpawns() {
        return deathmatchSpawns;
    }

    //Gets the player to join the game!
    public void joinGame(Player player){
        if (this.gamestate == SGGameState.WAITING){
            this.getPlayers().add(player.getName());
            player.getInventory().clear();
            player.getInventory().setHelmet(null);
            player.getInventory().setChestplate(null);
            player.getInventory().setLeggings(null);
            player.getInventory().setBoots(null);
            player.setPlayerListName(ChatColor.stripColor(player.getPlayerListName()));
            player.teleport(this.waitingSpawn);
            player.getInventory().setHeldItemSlot(0);
            player.setHealth(20.0);
            player.setFoodLevel(20);
            Fly.startFlight(player);
            if (!(SilentJoin.isSilent(player))){
                this.messageEveryone(MessageManager.c("&r" + player.getDisplayName() + " &6has joined&8."));
            }
            Lobby.setLobbyScoreboard(player);
            for (String pl: this.getPlayers()){
                Player player1 = Main.main.getServer().getPlayer(pl);
                if (player1.getScoreboard().getTeam("players") != null) {
                    player1.getScoreboard().getTeam("players").setSuffix(MessageManager.c(" &f" + this.getPlayers().size()));
                }
            }

        } else if (this.gamestate == SGGameState.PREGAME){
            player.sendMessage(MessageManager.c(MessageManager.prefix + " &4You cannot join this game right now&8."));
            player.sendMessage(MessageManager.c(MessageManager.prefix + " &4Purchase a rank to get priority access&8."));
            return;
        } else {
            RankManager.setDisplayName(player, false);
            this.addSpectator(player);
            Ingame.setIngameScoreboard(player);
            player.getInventory().clear();
            player.getInventory().setHelmet(null);
            player.getInventory().setChestplate(null);
            player.getInventory().setLeggings(null);
            player.getInventory().setBoots(null);
            player.teleport(this.specSpawn);
            for (Player p : Bukkit.getOnlinePlayers()) {
            	p.hidePlayer(player);
            }
            player.getInventory().setHeldItemSlot(0);
            player.setHealth(20.0);
            player.setFoodLevel(20);
            player.setAllowFlight(true);
            player.setFlying(true);
            if (!(SilentJoin.isSilent(player))){
                this.messageEveryone(MessageManager.c("&r" + player.getDisplayName() + " &6has joined&8."));
            }
        	return;
        }

    }

    //Gets the player to leave the game!
    public void leaveGame(Player player){
    	if (this.getPlayers().contains(player.getName())) {
            this.getPlayers().remove(player.getName());
    	}
    	if (this.getSpectators().contains(player.getName())) {
    		this.removeSpectator(player);
    	}
    	if (this.getTributes().contains(player.getName())) {
    		this.removeTribute(player, null);
    	}
        if (!(SilentJoin.isSilent(player))){
            this.messageEveryone(MessageManager.c("&r" + player.getDisplayName() + " &6has left&8."));
        }
        player.setAllowFlight(false);
        player.setFlying(false);
        Fly.startFlight(player);
        for (String pl: this.getPlayers()) {
            Player player1 = Main.main.getServer().getPlayer(pl);
            if (this.gamestate == SGGameState.WAITING) {
                player1.getScoreboard().getTeam("players").setSuffix(MessageManager.c(" &f" + this.getPlayers().size()));
            } else {
                if (player1.getScoreboard().getTeam("players") != null) {
                    player1.getScoreboard().getTeam("players").setSuffix(MessageManager.c(" &f" + this.getTributes().size()));
                }
                if (player1.getScoreboard().getTeam("watching") != null) {
                    player1.getScoreboard().getTeam("watching").setSuffix(MessageManager.c(" &f" + this.getSpectators().size()));
                }
            }
        }
        RankManager.setDisplayName(player, true);
    }
    
    public String getDisplayGamestate(SGGameState gamestate){
        if (gamestate == SGGameState.WAITING){
            return "Lobby";
        }
        if (gamestate == SGGameState.PREGAME){
            return "PreGame";
        }
        if (gamestate == SGGameState.GRACEPERIOD){
            return "GracePeriod";
        }
        if (gamestate == SGGameState.INGAME){
            return "LiveGame";
        }
        if (gamestate == SGGameState.PREDEATHMATCH){
            return "PreDeathmatch";
        }
        if (gamestate == SGGameState.DEATHMATCH){
            return "Deathmatch";
        }
        if (gamestate == SGGameState.ENDGAME){
            return "EndGame";
        }
        if (gamestate == SGGameState.CLEANUP){
            return "Cleanup";
        }
        if (gamestate == SGGameState.RESTARTING){
            return "Restarting";
        }
        return null;
    }
    
    public String getPreviousMap(int map){
        if (this.previousMaps.size() == 0) {
        	this.previousMaps.add("Unknown");
        	this.previousMaps.add("Unknown");
        	this.previousMaps.add("Unknown");
        }
        return previousMaps.get(map);
    }
    
    public List<String> getPreviousMapsPlayed(){
        if (this.previousMaps.size() == 0) {
        	this.previousMaps.add("Unknown");
        	this.previousMaps.add("Unknown");
        	this.previousMaps.add("Unknown");
        }
        return this.previousMaps;
    }

    //Messages only the Spectators
    public void messageSpectators(String message){
        for (String pl: spectators) {
            if (Bukkit.getPlayer(pl) != null){
                Player player = Bukkit.getPlayer(pl);
                player.sendMessage(message);
            }

        }
    }
    //Messages only the Tributes
    public void messageTributes(String message){
        for (String pl: tributes) {
            if (Bukkit.getPlayer(pl) != null){
                Player player = Bukkit.getPlayer(pl);
                player.sendMessage(message);
            }

        }
    }
    //Messages Everyone in the game
    public void messageEveryone(String message){
        for (String pl: players) {
            if (Bukkit.getPlayer(pl) != null){
                Player player = Bukkit.getPlayer(pl);
                player.sendMessage(message);
            }

        }
    }

    //Get ID
    public int getID() {
        return ID;
    }
    //Get VoteManager of this Game
    public SGVoteManager getVm() {
        return vm;
    }
    //Get List of Tributes in this game
    public List<String> getTributes() {
        return tributes;
    }
    //Get List of Spectators in this game
    public List<String> getSpectators() {
        return spectators;
    }
    //Get Total of Players in this game
    public List<String> getPlayers() {
        return players;
    }
    //Get TimeManager of this game
    public SGTimeManager getTm() {
        return tm;
    }

    public void endGame(){
        Main.main.getSGGameManager().tempRemoveGame(this);
    }

    public void setWinner(String player){
		SGStats.addStats(player, SGStatType.GAMES_WON, 1);
        this.winner = player;
    }
    
    public void addSpectator(Player player){
        this.getSpectators().add(player.getName());
        if (!(this.getPlayers().contains(player.getName()))){
            this.getPlayers().add(player.getName());
        }
        for (String p : this.getPlayers()) {
        	if (Bukkit.getPlayer(p) != null) {
        		Bukkit.getPlayer(p).hidePlayer(player);
        	}
        }
        for (String str: this.getPlayers()){
            Player players = Main.main.getServer().getPlayer(str);
            if (players.getScoreboard().getTeam("watching") != null) {
            	players.getScoreboard().getTeam("watching").setSuffix(MessageManager.c(" &r" + this.getSpectators().size()));
            }
        }
        player.setAllowFlight(true);
        player.setFlying(true);
    }

    public void removeSpectator(Player player){
        this.getSpectators().remove(player.getName());
    }
    
    public void removeTribute(Player player, Entity cause){
    	this.getTributes().remove(player.getName());
    	this.getPastTributes().add(player.getDisplayName());
    	if (this.getPlayers().contains(player.getName())) {
    		this.addSpectator(player);
    	    player.sendMessage(MessageManager.c(MessageManager.prefix + " &aYou have been eliminated from the games."));
    	}
    	this.messageEveryone(MessageManager.c(MessageManager.prefix + " &aOnly &8[&6" + this.getTributes().size() + "&8] &atributes remain!"));
        if (this.getSpectators().size() == 1) {
            this.messageEveryone(MessageManager.c(MessageManager.prefix + " &aThere is &8[&6" + this.getSpectators().size() + "&8] &aspectator watching the game."));
        } else {
        	this.messageEveryone(MessageManager.c(MessageManager.prefix + " &aThere are &8[&6" + this.getSpectators().size() + "&8] &aspectators watching the game."));
        }
    	this.messageEveryone(MessageManager.c("&6A cannon can be heard in the distance&8."));
    	if (this.getPlayers().contains(player.getName())) {
	        player.sendMessage(MessageManager.c(MessageManager.prefix + " &aYou have been eliminated from the games."));
    		player.sendMessage(MessageManager.c(MessageManager.prefix + " &3You've lost &8[&e" + 0 + "&8] &3points for dying&8!"));
    		if (cause instanceof Player) {
        		cause.sendMessage(MessageManager.c(MessageManager.prefix + " &3You've gained &8[&e" + 0 + "&8] &3points for killing " + player.getDisplayName() + "&8!"));
        		SGStats.addStats(cause.getName(), SGStatType.KILLS, 1);
                if (Main.main.getSGGameManager().getGameByPlayer(player.getName()).getBm().getBounty(player) != 0){
                    Player cause1 = (Player) cause;
                    Integer bounty = Main.main.getSGGameManager().getGameByPlayer(player.getName()).getBm().getBounty(player);
                    PointsManager.givePoints(cause1, bounty);
                    cause1.sendMessage(MessageManager.c(MessageManager.prefix + " &3You have recieved &8[&e" + bounty + "&8]&3 extra points from bounties set on " + cause1.getDisplayName()));
                    Main.main.getSGGameManager().getGameByPlayer(player.getName()).messageEveryone(MessageManager.c("&6A bounty of &8[&a" + bounty + "&8] &6points has been claimed upon " + player.getDisplayName() + "&6's death&8."));
                    Main.main.getSGGameManager().getGameByPlayer(player.getName()).getBm().removeBounty(player);
                }
        	}else{
                Main.main.getSGGameManager().getGameByPlayer(player.getName()).getBm().removeBounty(player);
            }
    	}
    	if (this.getGamestate() == SGGameState.GRACEPERIOD
    		|| this.getGamestate() == SGGameState.PREDEATHMATCH
    		|| this.getGamestate() == SGGameState.DEATHMATCH
    		|| this.getGamestate() == SGGameState.INGAME) {
    		SGStats.addStats(player.getName(), SGStatType.GAMES_LOST, 1);
    	}
        for (String str: this.getPlayers()){
            Player players = Main.main.getServer().getPlayer(str);
            if (players.getScoreboard().getTeam("watching") != null) {
                players.getScoreboard().getTeam("watching").setSuffix(MessageManager.c(" &r" + this.getSpectators().size()));
            }
        }
        for (String str: this.getPlayers()){
            Player players = Main.main.getServer().getPlayer(str);
            if (players.getScoreboard().getTeam("players") != null) {
                players.getScoreboard().getTeam("players").setSuffix(MessageManager.c(" &r" + this.getTributes().size()));
            }
        }
        if (this.getTributes().size() < 2){
        	Bukkit.getWorld("SGGame-" + this.getID()).setTime(18000);
            this.setGamestate(SGGameState.ENDGAME);
            this.setWinner(this.getTributes().get(0));
            this.messageEveryone(MessageManager.c(MessageManager.prefix + " &aThe games have ended!"));
            Player winner = Main.main.getServer().getPlayer(this.getWinner());
            if (this.getBm().getBounty(winner) != 0){
                winner.sendMessage(MessageManager.c(MessageManager.prefix + " &aYou've gained &8[&e" + this.getBm().getBounty(winner) + "&8] &aextra points from your bounty!"));
                PointsManager.givePoints(winner, this.getBm().getBounty(winner));
            }
            this.messageEveryone(MessageManager.c(MessageManager.prefix + " &r" + Bukkit.getPlayer(this.winner).getDisplayName() + " &ahas won the Survival Games!"));
        }
    }

    public void newGame() {
        this.tributes = new ArrayList<>();
        this.spectators = new ArrayList<>();
        this.players = new ArrayList<>();
        this.gameSpawns = new ArrayList<>();
        this.deathmatchSpawns = new ArrayList<>();
        this.t1 = new ArrayList<>();
        this.t2 = new ArrayList<>();
        this.passedTributes = new ArrayList<>();
        this.openedChestInventories = new ArrayList<>();
        this.openedChestLocations = new ArrayList<>();
        this.winner = null;
        this.kills = new HashMap<>();
        this.playerdist = new HashMap<>();
        //Sets the current Map to null
        this.map = "NOT SET";
        //Creates the VoteManager for this game
        this.vm = new SGVoteManager(this);
        //Creates the SponsorManager for this game
        this.sm = new SGSponsorManager(this);
        this.bm = new SGBountyManager(this);
        //Sets the TimeManager
        this.gamestate = SGGameState.WAITING;
        this.forceStart = false;
    }

    //Returns the winner
    public String getWinner(){
        return winner;
    }

    //This is to setup the radius for the deathmatch lightning!
    public void setupRadius(){
        if (ConfigManager.sgDataCFG.getString("Maps." + this.getMap() + ".dm.center") != null) {
            dmx = ConfigManager.sgDataCFG.getInt("Maps." + this.getMap() + ".dm.center.x");
            dmz = ConfigManager.sgDataCFG.getInt("Maps." + this.getMap() + ".dm.center.z");
            radius = ConfigManager.sgDataCFG.getInt("Maps." + this.getMap() + ".dm.center.radius");
        }
    }

}
