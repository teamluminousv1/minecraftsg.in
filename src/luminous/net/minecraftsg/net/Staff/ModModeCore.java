package luminous.net.minecraftsg.net.Staff;

import luminous.net.main.fiddycal.Events.Connect;
import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGame;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class ModModeCore {

    private List<String> modModePlayers;

    public ModModeCore(){

        //Create list
        this.modModePlayers = new ArrayList<>();

    }

    public List<String> getPlayer(){
        return modModePlayers;
    }

    //Add Player to ModMode
    public void addPlayer(Player player){
        modModePlayers.add(player.getName());
        giveItems(player);
        player.sendMessage(MessageManager.c(MessageManager.prefix + " &6You have been &cREMOVED&6 from &eModerator Mode&6!"));
    }

    //Remove Player from ModMode
    public void removePlayer(Player player){
        modModePlayers.remove(player.getName());
        Connect.sendToLobby(player);
        Connect.giveLobbyItems(player);
        player.sendMessage(MessageManager.c(MessageManager.prefix + " &6You have been &aADDED&6 to &eModerator Mode&6!"));
    }

    //Give Items for ModMode
    public void giveItems(Player player){
        ItemStack navigation = new ItemStack(Material.COMPASS, 1);
        ItemMeta navigationMeta = navigation.getItemMeta();
        List<String> navigationLore = new ArrayList<String>();
        navigationMeta.setDisplayName(MessageManager.c("&bModerator Navigation"));
        navigationMeta.setLore(navigationLore);
        navigation.setItemMeta(navigationMeta);

        ItemStack freeze = new ItemStack(Material.ICE, 1);
        ItemMeta freezeMeta = navigation.getItemMeta();
        List<String> freezeLore = new ArrayList<String>();
        freezeMeta.setDisplayName(MessageManager.c("&bFreeze Player"));
        freezeMeta.setLore(freezeLore);
        freeze.setItemMeta(freezeMeta);

        ItemStack inspect = new ItemStack(Material.BOOK, 1);
        ItemMeta inspectMeta = navigation.getItemMeta();
        List<String> inspectLore = new ArrayList<String>();
        inspectMeta.setDisplayName(MessageManager.c("&bInspect Inventory"));
        inspectMeta.setLore(inspectLore);
        inspect.setItemMeta(inspectMeta);

        ItemStack vanish = new ItemStack(Material.INK_SACK, 1, (short) 1);
        ItemMeta vanishMeta = navigation.getItemMeta();
        List<String> vanishLore = new ArrayList<String>();
        vanishMeta.setDisplayName(MessageManager.c("&bVanish"));
        vanishMeta.setLore(vanishLore);
        vanish.setItemMeta(vanishMeta);

        ItemStack leave = new ItemStack(Material.WOOD_DOOR, 1);
        ItemMeta leaveMeta = navigation.getItemMeta();
        List<String> leaveLore = new ArrayList<String>();
        leaveMeta.setDisplayName(MessageManager.c("&bLeave Moderator Mode"));
        leaveMeta.setLore(leaveLore);
        leave.setItemMeta(leaveMeta);

        player.getInventory().clear();

        player.getInventory().setItem(0, navigation);
        player.getInventory().setItem(1, freeze);
        player.getInventory().setItem(4, leave);
        player.getInventory().setItem(7, vanish);
        player.getInventory().setItem(8, inspect);


    }

    //Open Moderator Navigation
    public static void openSGGUI(Player player) {

        Inventory sgInv = Bukkit.createInventory(null, 54, MessageManager.c("Moderator Navigation"));

        ItemStack players = new ItemStack(Material.SKULL_ITEM, 1);
        ItemMeta playersMeta = players.getItemMeta();
        List<String> playersLore = new ArrayList<String>();
        playersMeta.setDisplayName(MessageManager.c("&bTeleport to Player"));
        playersMeta.setLore(playersLore);
        players.setItemMeta(playersMeta);

        ItemStack games = new ItemStack(Material.IRON_SWORD, 1);
        ItemMeta gamesMeta = games.getItemMeta();
        List<String> gamesLore = new ArrayList<String>();
        gamesMeta.setDisplayName(MessageManager.c("&bTeleport to Game"));
        gamesMeta.setLore(gamesLore);
        games.setItemMeta(gamesMeta);

        sgInv.setItem(2, players);
        sgInv.setItem(6, games);

        player.openInventory(sgInv);

    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {

        @SuppressWarnings("unused")
		Player player = (Player) event.getWhoClicked();
        ItemStack clicked = event.getCurrentItem();
        Inventory inv = event.getInventory();

        if (clicked == null) {
            return;
        }

        if (clicked.getType() == null) {
            return;
        }

        if (clicked.getType() == Material.AIR) {
            return;
        }

        if (inv.getName().contains(MessageManager.c("Moderator Navigation"))) {
            String displayNameOfItem = ChatColor.stripColor(clicked.getItemMeta().getDisplayName());
            if (displayNameOfItem.equalsIgnoreCase("Teleport to Player")){
               inv.clear();
               int count = 8;
               for (Player players : Main.main.getServer().getOnlinePlayers()){
                   ItemStack games = new ItemStack(Material.SKULL_ITEM, 1);
                   ItemMeta gamesMeta = games.getItemMeta();
                   List<String> gamesLore = new ArrayList<String>();
                   gamesMeta.setDisplayName(MessageManager.c(players.getDisplayName()));
                   gamesMeta.setLore(gamesLore);
                   games.setItemMeta(gamesMeta);
                   count++;
                   inv.setItem(count, games);
               }
            }
            if (displayNameOfItem.equalsIgnoreCase("Teleport to Game")){
                inv.clear();
                for (SGGame game: Main.main.getSGGameManager().getGames()){
                    Integer gameID = game.getID();
                    String mapName = game.getMap();
                    String gameState = game.getDisplayGamestate(game.getGamestate());
                    Integer spectators = game.getSpectators().size();
                    Integer tributes = game.getTributes().size();
                    Integer totalPlayers = game.getPlayers().size();
                    ItemStack games = new ItemStack(Material.SAND, 1);
                    ItemMeta gamesMeta = games.getItemMeta();
                    List<String> gamesLore = new ArrayList<String>();
                    gamesMeta.setDisplayName(MessageManager.c("&bSG-" + gameID));
                    gamesLore.add(MessageManager.c("&6GameID: &e" + gameID));
                    gamesLore.add(MessageManager.c("&6Map: &e" + mapName));
                    gamesLore.add(MessageManager.c("&6Gamestate: &e" + gameState));
                    gamesLore.add(MessageManager.c("&7&m--------------&r"));
                    gamesLore.add(MessageManager.c("&6Tributes: &e" + tributes));
                    gamesLore.add(MessageManager.c("&6Spectators: &e" + spectators));
                    gamesLore.add(MessageManager.c("&6Total: &e" + totalPlayers));
                    gamesMeta.setLore(gamesLore);
                    games.setItemMeta(gamesMeta);
                    inv.setItem(gameID + 9, games);
                }
            }
        }
    }
}
