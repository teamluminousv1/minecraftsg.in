package luminous.net.minecraftsg.net.Managers;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import luminous.net.main.fiddycal.Main;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGame;
import luminous.net.minecraftsg.net.Gamemodes.SG.SGGameState;

public class HidePlayers implements Listener {

	@EventHandler
	public void serverJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		String world = Main.main.getConfig().getString("Spawn.world");
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.hidePlayer(player);
			player.hidePlayer(p);
			if (p.getWorld().equals(Bukkit.getWorld(world))) {
				p.showPlayer(player);
				player.showPlayer(p);
			}
		}
	}
	
	@EventHandler
	public void worldChange(PlayerChangedWorldEvent event) {
		Player player = event.getPlayer();
		String world = Main.main.getConfig().getString("Spawn.world");
		if (player.getWorld().getName().equals(world)) {
			for (Player p : Bukkit.getOnlinePlayers()) {
				p.hidePlayer(player);
				player.hidePlayer(p);
				if (p.getWorld().getName().equals(world)) {
					p.showPlayer(player);
					player.showPlayer(p);
				}
			}
			return;
		}
		if (Main.main.getSGGameManager().getGameByPlayer(player.getName()) != null) {
			SGGame game = Main.main.getSGGameManager().getGameByPlayer(player.getName());
			for (Player p : Bukkit.getOnlinePlayers()) {
				p.hidePlayer(player);
				player.hidePlayer(p);
			}
			if (game.getGamestate() == SGGameState.WAITING) {
				for (String p : game.getPlayers()) {
					if (Main.main.getServer().getPlayer(p) != null) {
						Player pl = Main.main.getServer().getPlayer(p);
						pl.showPlayer(player);
						player.showPlayer(pl);
					}
				}
			} else {
				if (game.getSpectators().contains(player.getName())) {
					for (String p : game.getTributes()) {
						if (Main.main.getServer().getPlayer(p) != null) {
							Player pl = Main.main.getServer().getPlayer(p);
							pl.hidePlayer(player);
							player.showPlayer(pl);
						}
					}
					for (String p : game.getSpectators()) {
						if (Main.main.getServer().getPlayer(p) != null) {
							Player pl = Main.main.getServer().getPlayer(p);
							pl.hidePlayer(player);
							player.hidePlayer(pl);
						}
					}
				}
			}
		} else {
			for (Player p : Bukkit.getOnlinePlayers()) {
				p.hidePlayer(player);
				player.hidePlayer(p);
				if (p.getWorld().equals(player.getWorld())) {
					p.showPlayer(player);
					player.showPlayer(p);
				}
			}
		}
	}
	
}
