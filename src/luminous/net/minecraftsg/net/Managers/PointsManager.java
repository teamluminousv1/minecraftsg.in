package luminous.net.minecraftsg.net.Managers;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import luminous.net.main.fiddycal.Managers.ConfigManager;

public class PointsManager implements Listener {

	public static boolean usePoints(Player player, int cost) {
		FileConfiguration config = ConfigManager.playersCFG;
		
		int currentPoints = Integer.parseInt(
				config.getString(player.getUniqueId().toString() + ".Points"));
		
		if (currentPoints >= cost) {
			config.set(player.getUniqueId().toString() + ".Points", currentPoints - cost);
			ConfigManager.savePlayers();
			return true;
		} else {
			return false;
		}
	}

	public static void removePoints(Player player, int points){
		FileConfiguration config = ConfigManager.playersCFG;

		int currentPoints = Integer.parseInt(
				config.getString(player.getUniqueId().toString() + ".Points"));

		config.set(player.getUniqueId().toString() + ".Points", currentPoints - points);
		ConfigManager.savePlayers();
	}
	
	public static void givePoints(Player player, int points) {
		FileConfiguration config = ConfigManager.playersCFG;
		
		int currentPoints = Integer.parseInt(
				config.getString(player.getUniqueId().toString() + ".Points"));
		
		config.set(player.getUniqueId().toString() + ".Points", currentPoints + points);
		ConfigManager.savePlayers();
	}
	
	public static int getPoints(Player player) {
		FileConfiguration config = ConfigManager.playersCFG;
		int points = Integer.parseInt(
				config.getString(player.getUniqueId().toString() + ".Points"));
		return points;
	}
	
	//TODO: Points in chat when player is in lobby and game is in waiting. Bounty and district when else.
	
}
