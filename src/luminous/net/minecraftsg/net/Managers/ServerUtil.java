package luminous.net.minecraftsg.net.Managers;

import java.util.ArrayList;

import luminous.net.main.fiddycal.Managers.ConfigManager;

public class ServerUtil {

	public static Integer getUnusedGameId() {
		ArrayList<Integer> servers = new ArrayList<Integer>();
		for (String id : ConfigManager.sgDataCFG.getConfigurationSection("Lobbies").getKeys(false)) {
			servers.add(Integer.parseInt(id));
		}
		for (String id : ConfigManager.bgDataCFG.getConfigurationSection("Lobbies").getKeys(false)) {
			servers.add(Integer.parseInt(id));
		}
		servers.add(1);
		for (int i = 0; i < servers.size(); i++) {
			if (!servers.contains(i)) {
				return i;
			}
		}
		return null;
	}
	
}
