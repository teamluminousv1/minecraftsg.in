package luminous.net.minecraftsg.net.Managers;

import java.util.Random;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ChestManager {
	
	public static ItemStack getRandomT1Item() {
		ItemStack woodaxe = new ItemStack(Material.WOOD_AXE, 1);
		ItemStack stoneaxe = new ItemStack(Material.STONE_AXE, 1);
		ItemStack woodsword = new ItemStack(Material.WOOD_SWORD, 1);
		ItemStack stonesword = new ItemStack(Material.STONE_SWORD, 1);
		ItemStack fishingrod = new ItemStack(Material.FISHING_ROD, 1);
		ItemStack leatherhelmet = new ItemStack(Material.LEATHER_HELMET, 1);
		ItemStack leatherchestplate = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
		ItemStack leatherleggings = new ItemStack(Material.LEATHER_LEGGINGS, 1);
		ItemStack leatherboots = new ItemStack(Material.LEATHER_BOOTS, 1);
		ItemStack arrow = new ItemStack(Material.ARROW, 4);
		ItemStack bow = new ItemStack(Material.BOW, 1);
		ItemStack flint = new ItemStack(Material.FLINT, 1);
		ItemStack stick = new ItemStack(Material.STICK, 1);
		ItemStack feather = new ItemStack(Material.FEATHER, 4);
		ItemStack cookie = new ItemStack(Material.COOKIE, 2);
		ItemStack carrot = new ItemStack(Material.CARROT, 2);
		ItemStack fish = new ItemStack(Material.RAW_FISH, 1);
		ItemStack melon = new ItemStack(Material.MELON, 2);
		ItemStack rawpork = new ItemStack(Material.PORK, 1);
		ItemStack rawchicken = new ItemStack(Material.PORK, 1);
		ItemStack rawbeef = new ItemStack(Material.RAW_BEEF, 1);
		ItemStack bread = new ItemStack(Material.BREAD, 1);
		ItemStack apple = new ItemStack(Material.APPLE, 2);
		ItemStack goldingot = new ItemStack(Material.GOLD_INGOT, 1);
		ItemStack ironingot = new ItemStack(Material.IRON_INGOT, 1);
		ItemStack pie = new ItemStack(Material.PUMPKIN_PIE, 1);
		ItemStack[] item = { woodaxe, stoneaxe, woodsword, stonesword, fishingrod, leatherhelmet, 
		leatherchestplate, leatherleggings, leatherboots, arrow, bow, flint, stick, feather, cookie,
		carrot, fish, melon, rawpork, rawbeef, bread, apple, goldingot, ironingot, pie, rawchicken };
		Random random = new Random(); int i = random.nextInt(item.length);
		return item[i];
	}
	
	public static ItemStack getRandomT2Item() {
		ItemStack diamond = new ItemStack(Material.DIAMOND, 1);
		ItemStack ironingot = new ItemStack(Material.IRON_INGOT, 1);
		ItemStack ironhelmet = new ItemStack(Material.IRON_HELMET, 1);
		ItemStack stonesword = new ItemStack(Material.STONE_SWORD, 1);
		ItemStack ironchestplate = new ItemStack(Material.IRON_CHESTPLATE, 1);
		ItemStack ironleggings = new ItemStack(Material.IRON_LEGGINGS, 1);
		ItemStack ironboots = new ItemStack(Material.IRON_BOOTS, 1);
		ItemStack goldenapple = new ItemStack(Material.GOLDEN_APPLE, 1);
		ItemStack chainhelmet = new ItemStack(Material.CHAINMAIL_HELMET, 1);
		ItemStack chainchestplate = new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1);
		ItemStack chainleggings = new ItemStack(Material.CHAINMAIL_LEGGINGS, 1);
		ItemStack chainboots = new ItemStack(Material.CHAINMAIL_BOOTS, 1);
		ItemStack goldhelmet = new ItemStack(Material.GOLD_HELMET, 1);
		ItemStack goldchestplate = new ItemStack(Material.GOLD_CHESTPLATE, 1);
		ItemStack goldleggings = new ItemStack(Material.GOLD_LEGGINGS, 1);
		ItemStack flintandsteel = new ItemStack(Material.FLINT_AND_STEEL, 1);
		ItemStack goldcarrot = new ItemStack(Material.GOLDEN_CARROT, 1);
		ItemStack rawpork = new ItemStack(Material.GRILLED_PORK, 1);
		ItemStack cookedchicken = new ItemStack(Material.BOW, 1);
		ItemStack rawbeef = new ItemStack(Material.COOKED_BEEF, 1);
		ItemStack cookedpotato = new ItemStack(Material.BAKED_POTATO, 1);
		ItemStack goldboots = new ItemStack(Material.GOLD_BOOTS, 1);
		ItemStack pie = new ItemStack(Material.PUMPKIN_PIE, 1);
		ItemStack arrow = new ItemStack(Material.ARROW, 4);
		ItemStack boat = new ItemStack(Material.BOAT, 1);
		ItemStack stick = new ItemStack(Material.STICK, 2);
		ItemStack bow = new ItemStack(Material.BOW, 1);
		ItemStack[] item = { arrow, bow, flintandsteel, stick, rawpork, rawbeef, 
		ironingot, pie, goldboots, cookedpotato, cookedchicken, goldcarrot, goldleggings,
		goldchestplate, goldhelmet, chainboots, chainleggings, chainchestplate, chainhelmet, ironboots,
		ironleggings, ironchestplate, ironhelmet, diamond, goldenapple, boat, stonesword };
		Random random = new Random(); int i = random.nextInt(item.length);
		return item[i];
	}
	
}
