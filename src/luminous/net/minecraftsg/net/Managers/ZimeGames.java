package luminous.net.minecraftsg.net.Managers;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Objects;

public class ZimeGames {

    //Set of all the games
    private ArrayList<String> games;

    //Usage: new GameManager();
    public ZimeGames() {
        //Creates fresh list for the games
        games = new ArrayList<>();
    }
    //Adds a Game to the set
    public void newGame(String game) {
        games.add(game);
    }

    //Remoes a Game from the set
    public void removeGame(String game){
        games.remove(game);
    }

    //Returns the ArrayList of games
    public ArrayList<String> getGames(){
        return games;
    }


    public Integer getZimeID(String game){
        for (int i = 0; i < games.size(); i++) {
            if (Objects.equals(games.get(i), game)){
                return i + 1;
            }
        }
        return null;
    }

    public Integer getGameByZimeID(Integer zimeID){
        if (games.get(zimeID -1) != null){
            String[] spl = games.get(zimeID - 1).split("-");
            Integer newNumber1 = Integer.valueOf(spl[1]);
            return newNumber1;
        }else{
            return null;
        }
    }

    /*

    TODO so that people cant greif the lobby.

     */
    public String getGamemodeByPlayer(Player player){
        //Work In Progress
        return null;
    }
       /*
    //Gets the Game by ID
    public SGMakerGame getGameByID(int id) {
        for (SGMakerGame game : games) {
            if (game.getID() == id) {
                return game;
            }
        }

        return null;
    }
    //Get the Game at the Player
    public SGMakerGame getGameByPlayer(String player) {
        for (SGMakerGame game : games) {
            if (game.getPlayers().contains(player)) {
                return game;
            }
        }

        return null;
    }*/
}
