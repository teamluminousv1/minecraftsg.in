package luminous.net.minecraftsg.net.Managers;

import luminous.net.main.fiddycal.Main;
import luminous.net.main.fiddycal.Managers.MessageManager;
import luminous.net.main.fiddycal.Managers.StaffUtil;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class SignsManager implements Listener {

    @EventHandler
    public void onSignCreate(SignChangeEvent event) {
        //For BattleGrounds Warp
        if (StaffUtil.isStaff(event.getPlayer())) {
            if (event.getLine(0).equalsIgnoreCase("[warp]")) {
                if (event.getLine(1).equalsIgnoreCase("north")) {
                    event.setLine(0, "");
                    event.setLine(1, MessageManager.c("&1Warp"));
                    event.setLine(2, MessageManager.c("&0North"));
                    event.setLine(3, "");
                    event.getPlayer().sendMessage(MessageManager.c(MessageManager.BGprefix + " &fSign has been set&8."));
                }
                if (event.getLine(1).equalsIgnoreCase("south")) {
                    event.setLine(0, "");
                    event.setLine(1, MessageManager.c("&1Warp"));
                    event.setLine(2, MessageManager.c("&0South"));
                    event.setLine(3, "");
                    event.getPlayer().sendMessage(MessageManager.c(MessageManager.BGprefix + " &fSign has been set&8."));
                }
                if (event.getLine(1).equalsIgnoreCase("east")) {
                    event.setLine(0, "");
                    event.setLine(1, MessageManager.c("&1Warp"));
                    event.setLine(2, MessageManager.c("&0East"));
                    event.setLine(3, "");
                    event.getPlayer().sendMessage(MessageManager.c(MessageManager.BGprefix + " &fSign has been set&8."));
                }
                if (event.getLine(1).equalsIgnoreCase("west")) {
                    event.setLine(0, "");
                    event.setLine(1, MessageManager.c("&1Warp"));
                    event.setLine(2, MessageManager.c("&0West"));
                    event.setLine(3, "");
                    event.getPlayer().sendMessage(MessageManager.c(MessageManager.BGprefix + " &fSign has been set&8."));
                }
                if (event.getLine(1).equalsIgnoreCase("center")) {
                    event.setLine(0, "");
                    event.setLine(1, MessageManager.c("&1Warp"));
                    event.setLine(2, MessageManager.c("&0Center"));
                    event.setLine(3, "");
                    event.getPlayer().sendMessage(MessageManager.c(MessageManager.BGprefix + " &fSign has been set&8."));
                }
            }
        }
    }

    @EventHandler
    public void onSignClick(PlayerInteractEvent e) {
        String playerName = e.getPlayer().getName();
        Player player = e.getPlayer();
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (Main.main.getBGGameManager().getGameByPlayer(playerName) != null) {
                if (e.getClickedBlock() != null) {
                    if (e.getClickedBlock().getType() == Material.SIGN || e.getClickedBlock().getType() == Material.SIGN_POST || e.getClickedBlock().getType() == Material.WALL_SIGN) {
                        Sign clickedSign = (Sign) e.getClickedBlock().getState();
                        if (clickedSign.getLine(1) != null) {
                            String line1 = ChatColor.stripColor(clickedSign.getLine(1));
                            if (line1.equalsIgnoreCase("Warp")) {
                                String line2 = ChatColor.stripColor(clickedSign.getLine(2));
                                //Remove the player from the Waiting array
                                Main.main.getBGGameManager().getGameByPlayer(playerName).leaveWaiting(player);
                                //Adds the player to the Fighting array
                                Main.main.getBGGameManager().getGameByPlayer(playerName).joinFighting(player);
                                //Teleports the player to the designated place
                                Main.main.getBGGameManager().getGameByPlayer(playerName).teleportPlayer(player, line2);
                            }
                        }
                    }
                }
            }
        }
    }
}
